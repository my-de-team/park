package com.park.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD) //作用在方法上
@Retention(RetentionPolicy.RUNTIME) //可以同过AOP获取改啊注解数据类型
public @interface IdempotentAnnotation {
}
