package com.park.vos.systemvo;

import lombok.Data;

import java.util.Date;

@Data
public class LoginLogVo {

    /**
     * 用户名
     */
    private String username;

    /**
     * 用户ip
     */
    private String ipAddress;

    /**
     * 用户地址
     */
    private String ipSource;

    /**
     * 浏览器
     */
    private String brower;

    /**
     * 操作系统
     */
    private String os;

    /**
     * 登陆状态, 0登录成功,1登录失败
     */
    private Integer state;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 备注信息
     */
    private String wechat;

    /**
     * 手机号码
     */
    private String mobile;
}
