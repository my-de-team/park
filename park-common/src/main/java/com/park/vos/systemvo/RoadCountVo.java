package com.park.vos.systemvo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class RoadCountVo implements Serializable {
    /**
     * 统计日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date countTime;

    /**
     * 路段总数（个）
     */
    private Integer roadCountNum;

    /**
     * 统计的路段id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long roadId;

    /**
     * 当日该路段订单总数
     */
    private Integer orderCountNum;

    /**
     * 当日该路段营收金额（元）
     */
    private BigDecimal revenueAmount;

    /**
     * 统计当日退款订单总数
     */
    private Integer refundOrder;

    /**
     * 统计当日退款金额总数
     */
    private BigDecimal refundAmount;

    /**
     * 当日无订单的路段数（个）
     */
    private Integer zeroOrderNum;
}
