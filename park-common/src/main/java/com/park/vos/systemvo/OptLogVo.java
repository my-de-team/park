package com.park.vos.systemvo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.Date;

@Data
public class OptLogVo {

    /**
     * 模块标题
     */
    private String title;

    /**
     * 业务类型（1 新增 2 修改 3 删除 ）
     */
    private Integer businessType;

    /**
     * 方法名称
     */
    private String methodName;

    /**
     * 方法路径
     */
    private String methodUrl;

    /**
     * 请求方式
     */
    private String reqType;

    /**
     * 操作者
     */
    private String optName;

    /**
     * 请求路径
     */
    private String optUrl;

    /**
     * 操作者ip
     */
    private String optIp;

    /**
     * 操作者地址
     */
    private String optAddress;

    /**
     * 请求参数
     */
    private String reqParam;

    /**
     * 返回信息
     */
    private String respData;

    /**
     * 操作日期
     */
    private Date optTime;

}
