package com.park.vos.systemvo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class AttendanceCountVo implements Serializable {
    /**
     * 统计月份
     */
    private String countMonth;

    /**
     * 统计日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date countTime;

    /**
     * 巡检员id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long inspectorId;

    /**
     * 巡检员姓名
     */
    private String inspectorName;

    /**
     * 排班天数
     */
    private Integer scheduleDays;

    /**
     * 正常天数
     */
    private Integer normalDays;

    /**
     * 异常天数
     */
    private Integer abnormalDays;
}
