package com.park.vos.systemvo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 各种统计表条件查询的通用vo
 */
@Data
public class QueryCountCommonVo implements Serializable {
    /**
     * 统计日期起始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startTime;
    /**
     * 统计日期结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endTime;
    /**
     * 路段id,路段统计表可用
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long roadId;
    /**
     * 巡检员姓名,考勤表可用
     */
    private String inspectorName;
}
