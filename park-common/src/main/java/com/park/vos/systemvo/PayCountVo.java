package com.park.vos.systemvo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class PayCountVo implements Serializable {
    /**
     * 统计日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date countTime;

    /**
     * 当日主动支付次数统计（次）
     */
    private Integer activePay;

    /**
     * 当日现金支付次数统计（次）
     */
    private Integer cashPay;

    /**
     * 当日二维码收款次数统计（次）
     */
    private Integer codePay;

    /**
     * 当日微信支付金额统计（元）
     */
    private BigDecimal wechatPayAmount;

    /**
     * 当日支付宝支付金额统计（元）
     */
    private BigDecimal alipayPayAmount;

    /**
     * 当日现金支付金额统计（元）
     */
    private BigDecimal cashPayAmount;

    /**
     * 当日银联支付金额统计（元）
     */
    private BigDecimal unionPayAmount;

}
