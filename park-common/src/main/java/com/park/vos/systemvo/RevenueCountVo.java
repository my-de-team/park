package com.park.vos.systemvo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class RevenueCountVo implements Serializable {

    /**
     * 统计日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date countTime;

    /**
     * 统计当日的订单总数（包含异常订单）
     */
    private Integer orderCount;

    /**
     * 当日营收额总数（收入减去退款金额）
     */
    private BigDecimal revenue;

    /**
     * 统计当日退款订单总数
     */
    private Integer refundOrder;

    /**
     * 统计当日退款金额总数
     */
    private BigDecimal refundAmount;

    /**
     * 统计当日新注册用户数
     */
    private Integer newUser;


}
