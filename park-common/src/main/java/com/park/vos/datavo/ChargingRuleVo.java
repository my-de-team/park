package com.park.vos.datavo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class ChargingRuleVo implements Serializable {


    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    //@ApiModelProperty("泊车路段ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long roadSegmentId;

    //@ApiModelProperty("泊位ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parkingPositionId;

    //@ApiModelProperty("是否工作日：1-是，0-否")
    private Integer isWeekday;

    //@ApiModelProperty("是否繁忙时间段：1-是，0-否")
    private Integer isBusyTime;

    //@ApiModelProperty("起始时间")
    private String enterTime;

    //@ApiModelProperty("结束时间")
    private String exitTime;

    //@ApiModelProperty("每小时收费率（收费标准）")
    private BigDecimal ratePerHour;

    //@ApiModelProperty("最低计费时长（分钟）")
    private Integer minDuration;

    //@ApiModelProperty("最高计费时长（分钟）")
    private Integer maxDuration;

    //@ApiModelProperty("免费时长（分钟）")
    private Integer freeDuration;

    //@ApiModelProperty("计费是否包含免费时长状态：1-是，2-否")
    private Integer state;

    //@ApiModelProperty("封顶金额")
    private BigDecimal ceilingPrice;

}
