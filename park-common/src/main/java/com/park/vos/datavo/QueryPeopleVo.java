package com.park.vos.datavo;

import lombok.Data;

import java.io.Serializable;

@Data
public class QueryPeopleVo implements Serializable {
    private String employeeName;
    private String phone;
    private String password;
    private Integer roleState;
}
