package com.park.vos.datavo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class InspectorFaultVo implements Serializable {
    // 主键id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    // 反馈单号
    private String feedbackNo;

    // 反馈时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date feedbackTime;

    // 巡检员id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long inspectorId;

    // 巡检员名字
    private String inspectorName;

    // 巡检员手机号
    private String inspectorPhone;

    // 地磁编号
    private String magnetismNumber;

    // 所属泊位
    private String roadBerth;

    // 反馈内容
    private String feedbackContent;

    // 处理结果
    private String processingResult;

    // 处理时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date processingTime;

    // 图片数量
    private Integer imgCount;

    // 0：故障， 1：其他
    private Integer faultType;

    // 0：未处理， 1：已处理
    private Integer state;

    // 图片集合
    private List<String> imgUrlList;
}
