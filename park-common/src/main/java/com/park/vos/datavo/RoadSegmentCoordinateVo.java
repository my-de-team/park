package com.park.vos.datavo;

import lombok.Data;

@Data
public class RoadSegmentCoordinateVo {

    private Long id;

    private String segmentName;

    /**
     * 经度
     */
    private String longitude;

    /**
     * 纬度
     */
    private String latitude;
}
