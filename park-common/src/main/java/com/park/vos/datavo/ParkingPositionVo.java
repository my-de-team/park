package com.park.vos.datavo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class ParkingPositionVo {

    /**
     * 泊车路段ID
     */
    //@ApiModelProperty("泊车路段ID")
    private Long roadSegmentId;

    /**
     * 泊位名称
     */
    //@ApiModelProperty("泊位名称")
    private String parkingName;

    /**
     * 泊位编号
     */
    //@ApiModelProperty("泊位编号")
    private String berthNumber;

    /**
     * 地磁编号
     */
    //@ApiModelProperty("地磁编号")
    private String magneticNumber;

    /**
     * 泊位状态：1.有车 2.无车 3.未激活
     */
    //@ApiModelProperty("泊位状态：1.有车 2.无车 3.未激活")
    private Integer state;

    /**
     * 激活时间
     */
    //@ApiModelProperty("激活时间")
    //@TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date activationTime;


    //@ApiModelProperty("创建时间")
    //@TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;


    //@ApiModelProperty("更新时间")
    //@TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
}
