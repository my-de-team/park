package com.park.vos.datavo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class FeedbackVo implements Serializable {
    // 主键id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    // 反馈单号
    private String feedbackNo;

    // 反馈时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date feedbackTime;

    // 用户id
    private Long userId;

    private String name;

    private String phone;

    // 处理结果
    private String processingResult;

    // 反馈内容
    private String feedbackContent;

    // 图片数量
    private Integer imgCount;


    // 处理时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date processingTime;

    // 0：待处理， 1：已处理
    private Integer state;

    // 图片集合
    private List<String> imgUrlList;
}
