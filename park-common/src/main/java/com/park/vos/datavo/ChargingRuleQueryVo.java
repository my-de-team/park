package com.park.vos.datavo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.sql.Date;

@Data
public class ChargingRuleQueryVo {

    //@ApiModelProperty("泊车路段ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long roadSegmentId;

    //@ApiModelProperty("泊位ID")
    //@JsonSerialize(using = ToStringSerializer.class)
    //private Long parkingPositionId;

    //@ApiModelProperty("驶入时间")
    private Date startTime;

    //@ApiModelProperty("驶出时间")
    private Date endTime;
}
