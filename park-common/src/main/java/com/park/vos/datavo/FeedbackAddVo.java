package com.park.vos.datavo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class FeedbackAddVo implements Serializable {

     // 反馈时间
    private Date feedbackTime;

     // 用户id
    private Long userId;

     // 反馈内容
    private String feedbackContent;

    //图片
    private List<String> imgUrlList;

     // 0：待处理， 1：已处理
    private Integer state;
}
