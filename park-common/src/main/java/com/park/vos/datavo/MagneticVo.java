package com.park.vos.datavo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class MagneticVo implements Serializable {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 地磁编号
     */
    private String magneticNumber;

    /**
     * 地磁名称
     */
    private String magneticName;

    /**
     * 所属路段
     */
    private String segmentName;

    /**
     * 绑定泊位
     */
    private String berthageBind;

    /**
     * 泊位情况,0:有车1:无车
     */
    private Integer berthageCondition;

    /**
     * 地磁状态,0:未激活1:在线2:离线
     */
    private Integer state;

    /**
     * 状态更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long roadId;


}
