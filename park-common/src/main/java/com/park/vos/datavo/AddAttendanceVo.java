package com.park.vos.datavo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
public class AddAttendanceVo implements Serializable {

    // 巡检员id
    @NotNull(message = "巡检员不允许为空")
    @JsonSerialize(using = ToStringSerializer.class)

    private Long inspectorId;

    // 打卡时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @NotNull(message = "打卡时间不允许为空")
    private Date clockTime;

    // 打卡日期
    @NotNull(message = "打卡日期不允许为空")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date clockDate;

    // 打卡类型  0:上班 1：下班
    private Integer clockType;

}
