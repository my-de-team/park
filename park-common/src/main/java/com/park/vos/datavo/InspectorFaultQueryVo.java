package com.park.vos.datavo;

import lombok.Data;

import javax.validation.Valid;
import java.io.Serializable;

@Data
public class InspectorFaultQueryVo implements Serializable {
    private String InspectorName;
    private String InspectorPhone;
    private String roadBerth;
    private Integer state;
}
