package com.park.vos.datavo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data

public class InspectorFaultAddVo implements Serializable {

    // 巡检员id
    @JsonSerialize(using = ToStringSerializer.class)
    private Long inspectorId;

    // 地磁编号
    private String magneticNumber;

    // 所属泊位
    private String roadBerth;

    // 反馈内容
    private String feedbackContent;

    // 0：故障， 1：其他
    private Integer faultType;

    //图片
    private List<String> imgUrlList;
}
