package com.park.vos.datavo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class RoadSegmentVo {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 泊位路段名称
     */
    private String segmentName;

    /**
     * 路段类型
     */
    private String segmentType;

    /**
     * 所属地区
     */
    private String region;

    /**
     * 泊位数量
     */
    private Integer parkingSpots;

    /**
     * 限制泊位数
     */
    private Integer restrictedSpots;

    /**
     * 巡检员
     */
    private String inspector;

    /**
     * 运维人员ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long maintenanceStaff;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    /**
     * 经度
     */
    private String longitude;

    /**
     * 纬度
     */
    private String latitude;

    /**
     *  员工姓名
     */
    private List<String> employeeName;

    private List<String> maintenanceNameList;
}
