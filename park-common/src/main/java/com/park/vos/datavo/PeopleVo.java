package com.park.vos.datavo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
public class PeopleVo implements Serializable {
    private Long id;

    /**
     * 员工名称
     */
    @NotNull(message = "不允许为空")
    private String employeeName;

    /**
     * 手机号码
     */
    @NotNull(message = "不允许为空")
    private String phone;

    /**
     * 密码
     */
    @NotNull(message = "不允许为空")
    private String password;

    /**
     * 管理路段
     */
    @NotNull(message = "不允许为空")
    private List<String> segmentNameList;

    /**
     * 路段id
     */
    @NotNull(message = "不允许为空")
    private List<Long> roadId;

    /**
     * 执勤时间
     */
    @NotNull(message = "不允许为空")
    private String dutyTime;

    /**
     * 订单完成率
     */
    private String orderRate;

    /**
     * 所属分组
     */
    private String owningGroup;

    /**
     * 角色状态,0:巡检员1:运维人员
     */
    private Integer roleState;

    /**
     * 状态,0:正常
     */
    private Integer state;
}
