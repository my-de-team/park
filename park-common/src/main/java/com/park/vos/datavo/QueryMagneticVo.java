package com.park.vos.datavo;

import lombok.Data;

import java.io.Serializable;

@Data
public class QueryMagneticVo implements Serializable {
    private String magneticNumber;
    private String magneticName;
    private Long roadId;
    private String segmentName;
}
