package com.park.vos.datavo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class PdaLogAddVo implements Serializable {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long peopleId;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long roadId;
    private String login;
    private String deviceNumber;
}
