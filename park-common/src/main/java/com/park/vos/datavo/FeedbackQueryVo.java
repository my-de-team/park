package com.park.vos.datavo;

import lombok.Data;

import java.io.Serializable;

@Data
public class FeedbackQueryVo implements Serializable {
    private String name;
    private String mobile;
    private Integer state;
}
