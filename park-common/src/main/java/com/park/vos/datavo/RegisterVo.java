package com.park.vos.datavo;

import lombok.Data;

import javax.validation.constraints.Pattern;

@Data
public class RegisterVo {
    @Pattern(regexp = "^1(3|4|5|6|7|8|9)\\d{9}$", message = "请输入正确的手机号")
    private String mobile;
    @Pattern(regexp = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,10}$", message = "必须包含大小写字母和数字的组合，不能使用特殊字符，长度在8-10之间")
    private String password;
    @Pattern(regexp = "^[0-9]{4}$", message = "必须输入4位验证码")
    private String mobilecode;
    private Integer roleState;
    private String employeeName;
}
