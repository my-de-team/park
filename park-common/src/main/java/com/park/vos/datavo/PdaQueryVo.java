package com.park.vos.datavo;

import lombok.Data;

import java.io.Serializable;

@Data
public class PdaQueryVo implements Serializable {
    private String deviceNumber;
    private String deviceName;
    private Long roadId;
    private String segmentName;
}
