package com.park.vos.datavo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

@Data
public class RoadSegmentAddVo {
    /**
     * 路段名称
     */
    //@ApiModelProperty("路段名称")
    private String segmentName;

    /**
     * 路段类型
     */
    //@ApiModelProperty("路段类型")
    private String segmentType;

    /**
     * 路段编号 road_segment
     */
    //@ApiModelProperty("路段编号")
    private String roadSegment;


    /**
     * 所属地区
     */
    //@ApiModelProperty("所属地区")
    private String region;

    /**
     * 泊位数量
     */
    //@ApiModelProperty("泊位数量")
    private Integer parkingSpots;

    /**
     * 限制泊位数
     */
    //@ApiModelProperty("限制泊位数")
    private Integer restrictedSpots;

    /**
     * 巡检员
     */
    //@ApiModelProperty("巡检员")
    private String inspector;

    /**
     * 运维人员ID
     */
    //@ApiModelProperty("运维人员ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long maintenanceStaff;

    /**
     * 经度
     */
    //@ApiModelProperty("经度")
    private String longitude;

    /**
     * 纬度
     */
    //@ApiModelProperty("纬度")
    private String latitude;
}
