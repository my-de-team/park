package com.park.vos.datavo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

@Data
public class PeopleLoginVo implements Serializable {
    private String phone;
    private String password;
    @JsonSerialize(using = ToStringSerializer.class)
    private Long roadId;
    private String deviceNumber;
}
