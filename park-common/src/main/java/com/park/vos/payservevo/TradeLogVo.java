package com.park.vos.payservevo;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * ade-交易记录vo,添加支付信息
 */
@Data
public class TradeLogVo implements Serializable {


    /**
     * 交易标题
     */
    private String title;
    /**
     * 订单id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long orderId;
    /**
     * 支付平台,1支付宝,2银联,3微信,4现金
     */
    private Integer payPlatform;

    /**
     * 交易类型,1支付,2退款
     */
    private Integer tradeType;

    /**
     * 交易金额,单位元
     */
    private BigDecimal amount;

    /**
     * 请求流水号,支付模块生成,唯一,调用时返回
     */
    private String reqFlow;
    /**
     * 交易状态,0进行中,1成功,2失败,默认0
     */
    private Integer state;

    private static final long serialVersionUID = 1L;
}