package com.park.vos.userservevo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserLoginInfoVo implements Serializable {

    /**
     * 用户id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /**
     * 用户手机号
     */
    private String mobile;
    /**
     * 用户名
     */
    private String name;
    /**
     * 用户微信号
     */
    private String wechat;
    /**
     * token口令
     */
    private String token;
}
