package com.park.vos.userservevo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class AppealManageVo {
    /**
     * 申诉表ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 订单表ID 为了退款接口
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long orderId;

    /**
     * 处理人
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long processUserId;

    /**
     * 留言
     */
    private String leaveWords;

    /**
     * 退款金额
     */
    private BigDecimal refundAmount;
}
