package com.park.vos.userservevo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户绑定的车辆信息
 */
@Data
public class CarInfoVo implements Serializable {

    /**
     * 车辆主键id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 车牌号
     */
    private String number;

    /**
     * 车牌类型
     */
    private Integer type;

    /**
     * 用户id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 绑定状态
     */
    private Integer state;

    /**
     * 创建时间 绑定时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date bindTime;

    /**
     * 是否删除
     */
    private Integer isDeleted;


}
