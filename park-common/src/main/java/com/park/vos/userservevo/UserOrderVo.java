package com.park.vos.userservevo;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户订单信息
 */
@Data
public class UserOrderVo implements Serializable {

    /**
     * 订单id
     */

    /**
     * 订单编号
     */

    /**
     * 用户编号/手机号
     */

    /**
     * 订单编号
     */

    /**
     * 提交时间
     */

    /**
     * 车牌号
     */

    /**
     * 订单金额
     */

    /**
     * 订单状态
     */

    /**
     * 支付方式
     */

}
