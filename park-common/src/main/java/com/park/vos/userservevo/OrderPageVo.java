package com.park.vos.userservevo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/*传给前台的数据*/
@Data
@ExcelIgnoreUnannotated
/*提交时间记得单独加，否则会乱码*/
@ColumnWidth(15)

public class OrderPageVo implements Serializable {
    /**
     * 订单ID，雪花算法自动生成
     */
    @ExcelProperty("订单ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 订单编号，前缀【ORN】+ 雪花算法自动生成
     */
    @ExcelProperty("订单编号")
    private String orderNumber;

    /**
     * 订单提交时间，创建时间
     */
    @ColumnWidth(20)
    @ExcelProperty("提交时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date orderSubmissionTime;

    /**
     * 车牌号码，格式【粤B】+ 英文与字母组合（五位）
     */
    @ExcelProperty("车牌号码")
    private String plateNumber;

    /**
     * 所属路段
     */
    @ExcelProperty("所属路段")
    private String roadSegment;

    /**
     * 泊位编号
     */
    @ExcelProperty("泊位编号")
    private String berthNumber;

    /**
     * 订单金额，【订单金额】 = 【停车时间】* 【对应时间段单价】
     */
    @ExcelProperty("订单金额")
    private BigDecimal orderAmount;

    /**
     * 实付金额，实际付款金额
     */
    private BigDecimal orderRealityAmount;

    /**
     * 订单状态，0-进行中，1-待支付，2-已支付，3-已完成
     */
    @ExcelProperty("订单状态")
    private Integer orderState;

    /**
     * 支付方式 1-支付宝支付，2-银联，3-微信支付，4-现金支付
     */
    private Integer wayPay;

    /**
     * 绑定用户
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long boundUser;

    /*需要去其他表查询的*/

    /**
     * 巡检员id
     */
    private Long inspectorId;

    /*巡检员姓名*/
    @ExcelProperty("巡检员")
    private String employeeName;

    /**
     * 处理情况，0-待处理，1-已退款，2-不通过
     */
//    private Integer appealProcessState;




}
