package com.park.vos.userservevo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

@Data
public class PictureUrlVos {

    /**
     * 图片表ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 图片路径
     */
    private String pictureUrl;

}
