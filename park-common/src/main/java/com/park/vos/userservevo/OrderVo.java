package com.park.vos.userservevo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class OrderVo implements Serializable {
    /**
     * 订单ID，雪花算法自动生成
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 订单编号，前缀【ORN】+ 雪花算法自动生成
     */
    private String orderNumber;

    /**
     * 订单提交时间，创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date orderSubmissionTime;

    /**
     * 车牌号码，格式【粤B】+ 英文与字母组合（五位）
     */
    private String plateNumber;

    /**
     * 所属路段
     */
    private String roadSegment;

    /**
     * 泊位编号
     */
    private String berthNumber;

    /**
     * 巡检员,用于关联员工表的巡检员ID
     */
    private Long inspectorId;

    /**
     * 订单金额，【订单金额】 = 【停车时间】* 【对应时间段单价】
     */
    private BigDecimal orderAmount;

    /**
     * 订单状态，0-进行中，1-待支付，2-已支付，3-已完成
     */
    private Integer orderState;

    /**
     * 绑定用户
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long boundUser;

    /**
     * 地磁编号
     */
    private String magneticNumber;

    /**
     * 驶入时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date driveTime;

    /**
     * 驶离时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date leaveTime;

    /**
     * 停车时长，【驶离时间】 — 【驶入时间】
     */
    private String parkTime;

    /**
     * 订单是否异常，0-正常订单（默认），1-异常订单（用户申诉后订单状态变为异常）
     */
    private Integer orderAbnormalState;

    /**
     * 实付金额，实际付款金额
     */
    private BigDecimal orderRealityAmount;

    /**
     * 支付方式，1-支付宝支付，2-微信支付，3-现金支付
     */
    private Integer wayPay;

    /**
     * 支付时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date payTime;

    /**
     * 支付流水号
     */
    private String payNumber;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    /**
     * 修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime;

    /**
     * 逻辑删除，0-未删除（默认），1-已删除
     */
    private Integer isDeleted;

    /**
     * 乐观锁，版本号（默认0）
     */
    private Integer version;
}
