package com.park.vos.userservevo;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserLoginVo implements Serializable {

//    private String mobile;
    /**
     * 微信唯一标识
     */
    private String wechat;

    /**
     * 用户名称 微信名称
     */
    private String name;

    /**
     * 头像
     */
    private String img;
}
