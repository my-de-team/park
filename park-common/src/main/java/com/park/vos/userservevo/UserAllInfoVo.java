package com.park.vos.userservevo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户详细信息
 */
@Data
public class UserAllInfoVo implements Serializable {
    /**
     * 用户主键id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 用户名称
     */
    private String name;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 微信id
     */
    private String wechat;
    /**
     * 头像
     */
    private String img;

    /**
     * 绑定车辆数量
     */
    private Integer bindNumber;

    /**
     * 是否绑定微信号 0: 绑定 1: 没绑定
     */
    private Integer isBind;

    /**
     * 用户状态 0: 正常 1: 异常
     */
    private Integer state;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**
     * 最近登录时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastLoginTime;

}
