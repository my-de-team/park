package com.park.vos.userservevo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class OrderAppealVo {

    /**
     * 订单表ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long orderId;

    /**
     * 订单编码
     */
    private String orderNumber;

    /**
     * 申诉内容
     */
    private String appealContent;

    /**
     * 申诉时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date appealTime;

    /**
     * 处理情况，0-待处理，1-已退款，2-不通过
     */
    private Integer appealProcessState;

    /**
     * 修改金额
     */
    private BigDecimal modificationAmount;

    /**
     * 退款金额
     */
    private BigDecimal refundAmount;

    /**
     * 处理时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date processTime;

    /**
     * 处理人
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long processUserId;

    /**
     * 留言
     */
    private String leaveWords;

    /*申诉图片*/
    private String pictureUrl;
}
