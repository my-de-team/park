package com.park.vos.userservevo;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserQueryVo implements Serializable {

    /**
     * 昵称
     */
    private String name;
    /**
     * 微信是否绑定
     */
    private Integer isBind;
    /**
     * 手机号码
     */
    private String mobile;
    /**
     * 状态
     */
    private Integer state;
}
