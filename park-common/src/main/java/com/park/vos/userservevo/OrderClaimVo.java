package com.park.vos.userservevo;

import lombok.Data;

@Data
public class OrderClaimVo {
    /**
     * 用户主键id
     */
    private Long id;

    /**
     * 泊位编号
     */
    private String berthNumber;
}
