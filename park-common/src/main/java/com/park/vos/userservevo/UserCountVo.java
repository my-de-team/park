package com.park.vos.userservevo;

import lombok.Data;

import java.io.Serializable;

/**
 * 账户信息 用户订单与车辆统计
 */
@Data
public class UserCountVo implements Serializable {


    /**
     * 已缴订单数量
     */

    /**
     * 消费金额
     */

    /**
     * 未缴费订单
     */

    /**
     * 未缴费订单数量
     */


}
