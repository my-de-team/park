package com.park.vos.userservevo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/*前台传过来的数据*/
@Data
public class OrderQueryPageVo implements Serializable {

    /**
     * 订单ID，雪花算法自动生成
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 订单编号，前缀【ORN】+ 雪花算法自动生成
     */
    private String orderNumber;

    /**
     * 车牌号码，格式【粤B】+ 英文与字母组合（五位）
     */
    private String plateNumber;

    /**
     * 泊位编号
     */
    private String berthNumber;

    /**
     * 订单状态，0-进行中，1-待支付，2-已支付，3-已完成
     */
    private Integer orderState;

    /**
     * 订单提交时间，范围查询开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date orderSubStartTime;

    /**
     * 订单提交时间，范围查询结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date orderSubEndTime;

    /**
     * 逻辑删除，0-未删除（默认），1-已删除
     */
    private Integer isDeleted;

    /**
     * 订单是否异常，0-正常订单（默认），1-异常订单（用户申诉后订单状态变为异常）
     */
    private Integer orderAbnormalState;

    /**
     * 绑定用户
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long boundUser;

    /**
     * 所属路段
     */
    private String roadSegment;

    /*判断导出订单字段*/
    private Integer exportOr;
}
