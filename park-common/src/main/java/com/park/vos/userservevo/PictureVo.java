package com.park.vos.userservevo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class PictureVo implements Serializable {
    /**
     * 图片表ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 订单表ID，与订单表关联的字段
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long orderId;

    /**
     * 订单编号，与订单表关联的字段
     */
    private String orderNumber;

    /**
     * 图片路径
     */
    private String pictureUrl;

    /**
     * 图片属性，0-驶入图片，1-申诉图片
     */
    private Integer pictureAttribute;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    /**
     * 修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime;

    /**
     * 逻辑删除，0-未删除（默认），1-已删除
     */
    private Integer isDeleted;

    /**
     * 乐观锁，版本号（默认0）
     */
    private Integer version;

}
