package com.park.vos.userservevo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserInfoVo implements Serializable {

    /**
     * 用户主键id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 用户名称
     */
    private String name;

    /**
     * 手机号
     */
    private String mobile;


    /**
     * 头像
     */
    private String img;

    /**
     * 微信id
     */
    private String wechat;

}
