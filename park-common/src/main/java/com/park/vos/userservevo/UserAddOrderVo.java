package com.park.vos.userservevo;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserAddOrderVo implements Serializable {

    /**
     * 手机号
     */
    private String mobile;

}
