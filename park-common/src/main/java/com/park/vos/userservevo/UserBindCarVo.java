package com.park.vos.userservevo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户绑定车辆
 */
@Data
public class UserBindCarVo implements Serializable {

    /**
     * 用户id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;
    /**
     * 用户手机号
     */
    private String mobile;
    /**
     * 车牌号
     */
    private String number;
    /**
     * 车牌类型
     */
    private Integer type;
}
