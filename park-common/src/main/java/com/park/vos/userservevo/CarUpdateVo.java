package com.park.vos.userservevo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * 修改车牌号 修改绑定状态的vo
 */
@Data
public class CarUpdateVo implements Serializable {
    /**
     * 车辆id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 用户id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    private String number;

    /**
     * 车辆绑定状态 0: 绑定 1: 未绑定
     */
    private Integer state;
}
