package com.park.vos.userservevo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class AppealQueryVo implements Serializable {

    /**
     * 订单表ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long orderId;

    /**
     * 处理情况，0-待处理，1-已退款，2-不通过
     */
    private Integer appealProcessState;

    /**
     * 逻辑删除，0-未删除（默认），1-已删除
     */
    private Integer isDeleted;

    /*时间范围-起始时间*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date startTime;
    /*时间范围-终止时间*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date endTime;
}
