package com.park.vos.userservevo;

import com.park.vos.systemvo.LoginLogVo;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class LogVo implements Serializable {

    /**
     * 登录欣喜
     */
    private LoginLogVo loginLogVo;
    /**
     * 请求参数
     */
    private String reqParam;

    /**
     * 返回信息
     */
    private String respData;
}
