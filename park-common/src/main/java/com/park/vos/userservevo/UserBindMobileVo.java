package com.park.vos.userservevo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserBindMobileVo implements Serializable {

//    @JsonSerialize(using = ToStringSerializer.class)
//    private Long id;

    private String wechat;

    private String mobile;
}
