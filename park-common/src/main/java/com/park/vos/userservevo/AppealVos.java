package com.park.vos.userservevo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

@Data
public class AppealVos {
    /**
     * 申诉表ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 订单表ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long orderId;

    /**
     * 订单编号，前缀【ORN】+ 雪花算法自动生成
     */
    private String orderNumber;

    /**
     * 车牌号码，格式【粤B】+ 英文与字母组合（五位）
     */
    private String plateNumber;

    /**
     * 所属路段
     */
    private String roadSegment;

    /**
     * 泊位编号
     */
    private String berthNumber;

    /**
     * 地磁编号
     */
    private String magneticNumber;

    /**
     * 巡检员,用于关联员工表的巡检员ID
     */
    private Long inspectorId;

    /**
     * 驶入时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date driveTime;

    /**
     * 驶离时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date leaveTime;

    /**
     * 停车时长，【驶离时间】 — 【驶入时间】
     */
    private String parkTime;

    /**
     * 实付金额，实际付款金额
     */
    private BigDecimal orderRealityAmount;

    /**
     * 支付方式，1-支付宝支付，2-微信支付，3-现金支付
     */
    private Integer wayPay;

    /**
     * 支付时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date payTime;

    /**
     * 支付流水号
     */
    private String payNumber;

    /**
     * 订单状态，0-进行中，1-待支付，2-已支付，3-已完成
     */
    private Integer orderState;

    /**
     * 绑定用户
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long boundUser;

    /**
     * 申诉内容
     */
    private String appealContent;

    /**
     * 申诉时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date appealTime;

    /**
     * 处理情况，0-待处理，1-已退款，2-不通过
     */
    private Integer appealProcessState;

    /**
     * 修改金额
     */
    private BigDecimal modificationAmount;

    /**
     * 退款金额
     */
    private BigDecimal refundAmount;

    /**
     * 处理时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date processTime;

    /**
     * 处理人
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long processUserId;

    /**
     * 处理人
     */
    private String processUser;

    /**
     * 留言
     */
    private String leaveWords;


    /*照片存储表*/
    /*驶入图片*/
    private ArrayList<String> pictureUrl;

    /*申诉图片*/
    private ArrayList<String> appealPictureUrl;
}
