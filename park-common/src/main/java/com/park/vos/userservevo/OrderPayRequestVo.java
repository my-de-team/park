package com.park.vos.userservevo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

@Data
public class OrderPayRequestVo {
    /*订单id*/
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /*支付方式 1-支付宝支付，2-银联，3-微信支付，4-现金支付*/
    private Integer wayPay;
}
