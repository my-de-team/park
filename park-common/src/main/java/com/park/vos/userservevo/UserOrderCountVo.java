package com.park.vos.userservevo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class UserOrderCountVo implements Serializable {

    /**
     * 总订单数
     */
    private Integer sum;

    /**
     * 总计消费金额
     */
    private BigDecimal moneySum;

    /**
     * 代缴金额
     */
    private BigDecimal noPayMoney;

    /**
     * 待支付订单数
     */
    private Integer noPaySum;
}
