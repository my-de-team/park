package com.park.vos.userservevo;

import lombok.Data;

import java.io.Serializable;

@Data
public class OpenIdVo implements Serializable {
    /**
     * 每次登录小程序生成的随机码
     */
    private String code;
    /**
     * 小程序开发者的appId,在微信公众平台获取
     */
    private String appId;
    /**
     * 程序开发者的密钥,在微信公众平台获取
     */
    private String secret;
}
