package com.park.vos.userservevo;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserRegisterVo implements Serializable {

    /**
     * 用户主键id
     */
//    private Long id;

    /**
     * 用户名称 微信名称
     */
    private String name;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 密码 初次绑定 密码=手机号
     */
//    private String password;

    /**
     * 微信号
     */
    private String wechat;

    /**
     * 头像
     */
    private String img;


}
