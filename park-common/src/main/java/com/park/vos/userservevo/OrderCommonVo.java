package com.park.vos.userservevo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

@Data
public class OrderCommonVo implements Serializable {
    /**
     * 订单ID，雪花算法自动生成
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 订单编号，前缀【ORN】+ 雪花算法自动生成
     */
    private String orderNumber;
}
