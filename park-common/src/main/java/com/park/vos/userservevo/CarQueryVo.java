package com.park.vos.userservevo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

@Data
public class CarQueryVo implements Serializable {


    /**
     * 车牌号
     */
    private String number;

    /**
     * 车牌类型
     */
    private Integer type;

    /**
     * 绑定状态 0 绑定 1 未绑定
     */
    private Integer state;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 用户id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;


}
