package com.park.expection;

public class UserCardAlreadyBoundException extends Exception{
    public UserCardAlreadyBoundException() {
        super("该卡号已经被绑定，请更换");
    }
}