package com.park.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.Map;

public class JwtUtils {
	//token有效时间
    public static final long EXPIRE = 10000 * 60 * 60;
    //只有服务器知道的key
    public static final String APP_SECRET = "xxxxxx0000ooooosecuret";
    public static String getJwtToken(Map<String,Object> claim){//claim载荷
        String JwtToken = Jwts.builder()
            	//头部信息
                .setHeaderParam("typ", "JWT")
                .setHeaderParam("alg", "HS256")
                //当前时间
                .setIssuedAt(new Date())
                //有效时间截止
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRE))
                //设置载荷
                .addClaims(claim)
            	//认证
                .signWith(SignatureAlgorithm.HS256, APP_SECRET)
                .compact();
        return JwtToken;
    }
    /**
     * 判断token是否存在与有效
     * @param jwtToken
     * @return
     */
    public static boolean checkToken(String jwtToken) {
        if(jwtToken == null || "".equals(jwtToken)) return false;
        try {
            Jwts.parser().setSigningKey(APP_SECRET).parseClaimsJws(jwtToken);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    /**
     * 判断token是否存在与有效
     * @param request
     * @return
     */
/*    public static boolean checkToken(HttpServletRequest request) {
        try {
            *//**
             * 必须放在请求头中，并且key叫token
             *//*
            String jwtToken = request.getHeader("token");
            if(StringUtils.isEmpty(jwtToken)) return false;
            Jwts.parser().setSigningKey(APP_SECRET).parseClaimsJws(jwtToken);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }*/
    /**
     * 根据给定的key获取token体里面的数据，
     */
    public static String getClaimByJwtToken(String token,String key) {
        if(token == null || "".equals(token)) return "";
        Jws<Claims> claimsJws = Jwts.parser().setSigningKey(APP_SECRET).parseClaimsJws(token);
        Claims claims = claimsJws.getBody();
        return (String)claims.get(key);
    }

    public static void main(String[] args) {
        /*Map<String, Object> claim = new HashMap<>();
        claim.put("ip", "192.168.2.55");
        claim.put("username", "admin");
        claim.put("userId", "12");
        *//*生成token*//*
        String token = JwtUtils.getJwtToken(claim);
        System.out.println("token: " + token);*/
        String token ="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2ODI0OTk4MjEsImV4cCI6MTY4MjQ5OTg4MSwiaXAiOiIxOTIuMTY4LjIuNTUiLCJ1c2VySWQiOiIxMiIsInVzZXJuYW1lIjoiYWRtaW4ifQ.Ed-lVK2n4711XOPcBLZTExuq5iPe6NYa9h51JU-aocQ";
        boolean flag = JwtUtils.checkToken(token);
        String username = JwtUtils.getClaimByJwtToken(token, "username");
        System.out.println(flag);
        System.out.println(username);
    }
}
