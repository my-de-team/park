package com.park.utils;

import lombok.Data;

import java.util.List;

@Data
public class PageUtil<T> {
    /**
     * 当前页
     */
    private Long pageNum;
    /**
     * 每页条数
     */
    private Long pageSize;
    /**
     * 总条数
     */
    private Long total;
    /**
     * 分页数据
     */
    private List<T> pageData;
}
