package com.park.utils;

public class CustomUtil {

    /**
     * 生成编号
     *
     * @param prefix 编号前缀
     * @return String类型的编号
     */
    public static String getCodeNumber(String prefix) {
        long code = new SnowflakeIdWorker(1, 1, 1).nextId();
        return prefix + code;
    }
}
