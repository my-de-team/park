package com.park.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Result<T> {

    /**
     * 响应码
     */
    private String code;
    /**
     * 响应信息
     */
    private String message;
    /**
     * 响应数据
     */
    private T data;

    /**
     * 响应成功
     *
     * @param data 响应数据
     */
    public static <T> Result<T> success(T data) {
        return send(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMessage(), data);
    }

    /**
     * 响应成功 无需返回数据 只要状态码
     *
     * @param resultCode 响应码
     * @return
     */
    public static Result success(ResultCode resultCode) {
        return send(resultCode.getCode(), resultCode.getMessage(), null);
    }

    /**
     * 响应成功
     *
     * @param resultCode 响应码
     * @param data       响应数据
     * @return
     */
    public static <T> Result<T> success(ResultCode resultCode, T data) {
        return send(resultCode.getCode(), resultCode.getMessage(), data);
    }

    /**
     * 自定义响应信息
     *
     * @param code    自定义响应码
     * @param message 自定义响应信息
     * @param data    响应数据 没有写null
     * @return
     */

    public static <T> Result<T> send(String code, String message, T data) {
        return new Result<T>(code, message, data);
    }

    /**
     * 响应失败
     */
    public static <T> Result<T> err(ResultCode resultCode) {
        return send(resultCode.getCode(), resultCode.getMessage(), null);
    }


}
