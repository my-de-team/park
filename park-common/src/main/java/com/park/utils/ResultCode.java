package com.park.utils;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public enum ResultCode {

    SUCCESS("200", "请求成功"),
    FAIL("500", "请求失败"),
    LOGIN_SUCCESS("200", "登陆成功"),
    REGISTER_SUCCESS("200", "注册成功"),
    UPDATE_SUCCESS("200", "修改成功"),
    DELETE_SUCCESS("200", "删除成功"),
    INSERT_SUCCESS("200", "添加成功"),
    PHONE_SUCCESS("200", "手机号校验成功"),
    SMS_SUCCESS("200", "短信发送成功"),
    BIND_MOBILE_SUCCESS("200", "用户绑订手机号成功"),
    MOBILE_SUCCESS("200", "手机号可用"),
    USERNAME_SUCCESS("200", "用户名可用"),

    LOGIN_PASSWORD_FAIL("602", "密码错误,登陆失败！！！"),
    UPDATE_FAIL("603", "修改失败"),
    DELETE_FAIL("604", "删除失败"),
    INSERT_FAIL("605", "添加失败"),
    POWER_FAIL("606", "没有权限！！！"),
    REGISTER_USERNAME_FAIL("607", "用户名已存在！！！"),
    REGISTER_PHONE_FAIL("608", "手机号已存在！！！"),
    REGISTER_EMAIL_FAIL("609", "电子邮箱已存在！！！"),
    EXCEPTION("610", "服务器出现异常！！！！"),
    TOKEN_FAIL("611", "token过期或非法！！！"),
    PHONE_FAIL("612", "该手机号已注册！！！"),
    KONG("613", "该数据不能为空！！！"),
    MOBILE_FAIL("614", "手机号没绑定！！！"),
    INSERT_USER_FAIL("615", "新增用户异常！！！"),
    UPDATE_USER_FAIL("616", "修改用户异常！！！"),
    DELETE_USER_FAIL("617", "删除用户异常！！！"),
    OTHER_USER_FAIL("618", "删除用户异常！！！"),
    ;

    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
