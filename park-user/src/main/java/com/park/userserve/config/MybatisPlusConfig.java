package com.park.userserve.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import com.baomidou.mybatisplus.core.injector.ISqlInjector;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Date;

@Configuration
public class MybatisPlusConfig implements MetaObjectHandler {
    /**
     * mybatis-plus分页配置
     *
     * @return
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        PaginationInnerInterceptor innerInterceptor = new PaginationInnerInterceptor(DbType.MYSQL);
        innerInterceptor.setOverflow(true);
        interceptor.addInnerInterceptor(innerInterceptor);
        return interceptor;
    }

    /**
     * 逻辑删除 3.2版本要配置
     *
     * @return
     */
    @Bean
    public ISqlInjector sqlInjector() {
        return new DefaultSqlInjector();
    }


    /**
     * mybatis 新增自动注入配置
     *
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        /**
         * 新增数据时自动给数据库create_time
         *                    update_time
         *                    is_deleted 字段赋值
         * 自定义sql无效
         */
        System.out.println("metaObject = " + metaObject);
        this.strictInsertFill(metaObject, "createTime", Date.class, new Date());
        this.strictInsertFill(metaObject, "updateTime", Date.class, new Date());
        this.strictInsertFill(metaObject, "isDeleted", Integer.class, 0);


    }

    /**
     * mybatis 修改自动注入配置
     *
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        /**
         * 修改数据时自动给数据库update_time字段赋值
         * 自定义sql无效
         */
        this.strictInsertFill(metaObject, "updateTime", Date.class, new Date());
    }
}
