package com.park.userserve.esentity;

import com.park.userserve.entity.User;
import lombok.Data;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

@Data
public class UserLoginLogVo implements Serializable {


    private HttpServletRequest request;

    /**
     * 用户名
     */
    private User user;

    /**
     * 登陆状态, 0登录成功,1登录失败
     */
    private Integer state;


}
