package com.park.userserve.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.park.userserve.entity.Car;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.park.vos.userservevo.CarInfoVo;
import com.park.vos.userservevo.CarQueryVo;
import com.park.vos.userservevo.CarUpdateVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author MIN
 * @description 针对表【tp_car(用户绑定车辆表)】的数据库操作Mapper
 * @Entity com.park.userserve.entity.Car
 */
public interface CarMapper extends BaseMapper<Car> {

    int countUserBindCarById(Long userId);

    int updateCarById(CarUpdateVo carUpdateVo);

    List<CarInfoVo> getUserCarByUserId(Long userId);

    /**
     * @param carPage
     * @param queryWrapper
     * @param carQueryVo
     * @return
     */
    Page<Car> getUserCarById(@Param("carPage") Page<Car> carPage,
                                   @Param("ew") QueryWrapper<Car> queryWrapper,
                                   @Param("carQueryVo") CarQueryVo carQueryVo);

    /**
     * @param carPage
     * @param queryWrapper
     * @return
     */
    Page<CarInfoVo> getAllCar(@Param("carPage") Page<CarInfoVo> carPage,
                              @Param("ew") QueryWrapper<CarInfoVo> queryWrapper);


    List<Long> isBind(String carNumber);
}




