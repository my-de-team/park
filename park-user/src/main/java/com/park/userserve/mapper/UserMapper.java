package com.park.userserve.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.park.userserve.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.park.vos.userservevo.UserAllInfoVo;
import com.park.vos.userservevo.UserInfoVo;
import com.park.vos.userservevo.UserQueryVo;
import org.apache.ibatis.annotations.Param;

import javax.management.Query;
import java.util.List;

/**
 * @author MIN
 * @description 针对表【tp_user(用户表)】的数据库操作Mapper
 * @createDate 2023-07-13 10:39:21
 * @Entity com.park.userserve.entity.User
 */
public interface UserMapper extends BaseMapper<User> {

    User login(String wechat);

    /**
     * 根据用户id查询个人信息
     *
     * @param id 用户id
     * @return UserInfoVo
     */
    UserInfoVo getUserInfoById(Long id);

    /**
     * 根据手机号查询个人信息
     *
     * @param mobile 用户手机号
     * @return UserInfoVo
     */
    UserInfoVo getUserInfoByMobile(String mobile);


    /**
     * 根据id修改用户 is_deleted=1
     *
     * @param id 用户id
     * @return
     */
    int disableUserById(Long id);

    Long getUserId(String mobile);

    String getUserMobile(Long id);

    /**
     * 平台端 根据id获取用户详细信息
     *
     * @param id
     * @return
     */
    UserAllInfoVo getUserAllInfoById(Long id);


    /**
     * 平台 分页查询用户数据
     *
     * @param page
     * @param userQueryVo
     * @return
     */
    //List<UserAllInfoVo> getUserInfo(Page<UserAllInfoVo> userAllInfoPage, UserQueryVo userQueryVo);
    Page<UserAllInfoVo> getUserInfo(@Param("page") Page<UserAllInfoVo> page,
                                    @Param("userQueryVo") UserQueryVo userQueryVo,
                                    @Param("query") QueryWrapper<UserAllInfoVo> query);

    /**
     * 修改用户绑定的车辆数据
     *
     * @param id     用户id
     * @param number 修改后的数据
     * @return
     */
    int updateBindCarNumber(@Param("id") Long id, @Param("number") int number);

    /**
     * 修改用户信息
     *
     * @param user
     * @return
     */
    int updateUserInfoById(User user);

    /**
     * 判断手机号是否已使用
     *
     * @param mobile
     * @return
     */
    List<Long> mobileIsRepeat(String mobile);

    /**
     * 判断name是否已使用
     *
     * @param name
     * @return
     */
    List<Long> nameIsRepeat(String name);
}




