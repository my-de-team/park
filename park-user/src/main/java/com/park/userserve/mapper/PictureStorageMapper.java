package com.park.userserve.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.park.userserve.entity.PictureStorage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.park.vos.userservevo.PictureUrlVos;
import com.park.vos.userservevo.PictureVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author 86132
* @description 针对表【tp_picture_storage(存储驶入图片表)】的数据库操作Mapper
* @createDate 2023-07-13 14:40:20
* @Entity com.park.userserve.entity.PictureStorage
*/
public interface PictureStorageMapper extends BaseMapper<PictureStorage> {

    IPage<PictureVo> queryPagePicture(IPage<PictureVo> page, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

    List<PictureUrlVos> queryPictureUrl(Long orderId);

    List<PictureUrlVos> queryAppealPictureUrl(Long orderId);
}




