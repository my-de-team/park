package com.park.userserve.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.park.userserve.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.park.vos.userservevo.AppealVo;
import com.park.vos.userservevo.OrderPageVo;
import com.park.vos.userservevo.OrderVos;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author 86132
* @description 针对表【tp_order_list(订单表)】的数据库操作Mapper
* @createDate 2023-07-13 14:35:07
* @Entity com.park.userserve.entity.Order
*/
public interface OrderMapper extends BaseMapper<Order>{

    IPage<OrderPageVo> queryPageOrder(IPage<OrderPageVo> page, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

    List<OrderPageVo> queryOrderByUserId(Long boundUser);

    OrderVos queryOrderById(Long id);
}




