package com.park.userserve.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.park.userserve.entity.Appeal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.park.vos.userservevo.AppealPageVo;
import com.park.vos.userservevo.AppealQueryVo;
import com.park.vos.userservevo.AppealVo;
import com.park.vos.userservevo.AppealVos;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author 86132
* @description 针对表【tp_appeal_list(售后申诉表)】的数据库操作Mapper
* @createDate 2023-07-13 14:39:02
* @Entity com.park.userserve.entity.Appeal
*/
public interface AppealMapper extends BaseMapper<Appeal> {

    List<AppealQueryVo> queryAplAllState();

    IPage<AppealPageVo> queryPageAppeal(IPage<AppealPageVo> page, @Param(Constants.WRAPPER) QueryWrapper queryWrapper);

    AppealVos queryAppealById(Long id);
}




