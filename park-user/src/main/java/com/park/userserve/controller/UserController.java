package com.park.userserve.controller;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.park.annotation.Log;
import com.park.userserve.service.UserService;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import com.park.vos.userservevo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


@RestController
@RequestMapping("/user")
@Api(tags = "用户模块")
public class UserController {

    @Autowired
    private UserService userService;


    /**
     * 用户登录控制器
     *
     * @param userLoginVo wechatid
     * @return token口令
     */
    @ApiOperation("用户登录")
    @PostMapping("/login")
    public Result<String> login(@RequestBody UserLoginVo userLoginVo, HttpServletRequest request) {

        /**
         * 1,opid 后台发请求
         * 2,验证是否有
         * 3 有登录 进入主页面 没有返回状态
         */
        System.out.println("userLoginVo = " + userLoginVo);
        String token = userService.login(userLoginVo, request);
        if (token.equals("0")) {
            return Result.success(ResultCode.MOBILE_FAIL);
        }
        return Result.success(ResultCode.LOGIN_SUCCESS, token);
    }

    /**
     * 用户端注册
     *
     * @param userRegisterVo 注册数据
     * @return
     */
    @ApiOperation("用户注册")
    @PostMapping("/register")
    @Log
    public Result register(@RequestBody UserRegisterVo userRegisterVo) {
        System.out.println("userRegisterVo = " + userRegisterVo);
        userService.addUser(userRegisterVo);
        return Result.success(ResultCode.REGISTER_SUCCESS);
    }

    @ApiOperation("用户绑定手机号")
    @PostMapping("/bindMobile")
    @Log
    public Result bindMobile(@RequestBody UserBindMobileVo userBindMobileVo, HttpServletRequest request) {
        System.out.println("userBindMobileVo = " + userBindMobileVo);
        userService.bindMobile(userBindMobileVo, request);
        return Result.success(ResultCode.BIND_MOBILE_SUCCESS);
    }


    /**
     * 根据id获取个人信息
     *
     * @param id 用户id
     * @return
     */
    @ApiOperation("用户端 跟据id查看个人信息")
    @GetMapping("/id/{id}")
    @Log
    public Result<UserInfoVo> getUserInfoById(@PathVariable("id") Long id) {
        System.out.println("id = " + id);
        UserInfoVo userInfo = userService.getUserInfoById(id);
        return Result.success(userInfo);
    }

    /**
     * 根据手机号获取个人信息
     *
     * @param mobile 手机号
     * @return
     */
    @ApiOperation("用户端 跟据手机号查看个人信息")
    @GetMapping("/mobile/{mobile}")
    @Log
    public Result<UserInfoVo> getUserInfoByMobile(@PathVariable("mobile") String mobile) {
        System.out.println("mobile = " + mobile);
        UserInfoVo userInfo = userService.getUserInfoByMobile(mobile);
        return Result.success(userInfo);
    }

    @ApiOperation("统计当天注册人员数量")
    @GetMapping("/userCount")
    @Log
    public Result<Integer> getUserByCreateTime() {
        Integer userCount = userService.getUserByCreateTime();
        return Result.success(userCount);
    }

    @ApiOperation("判断手机号是否重复")
    @GetMapping("/isRepeat/{mobile}")
    @Log
    public Result mobileIsRepeat(@PathVariable("mobile") String mobile) {
        System.out.println("mobile = " + mobile);
        userService.mobileIsRepeat(mobile);
        return Result.success(ResultCode.MOBILE_SUCCESS);
    }

    @ApiOperation("判断name是否重复")
    @GetMapping("/name/isRepeat/{name}")
    @Log
    public Result nameIsRepeat(@PathVariable("name") String name) {
        System.out.println("name = " + name);
        userService.nameIsRepeat(name);
        return Result.success(ResultCode.USERNAME_SUCCESS);
    }

    /**
     * 平台端 根据用户id获取用户详细信息
     *
     * @param id
     * @return
     */
    @ApiOperation("平台 根据用户id获取用户详细信息")
    @GetMapping("/{id}")
    @Log
    public Result getUserAllInfo(@PathVariable("id") Long id) {
        System.out.println("id = " + id);
        UserAllInfoVo userAllInfo = userService.getUserAllInfoById(id);
        return Result.success(userAllInfo);
    }

    @ApiOperation("平台 条件查询全部用户")
    @PostMapping("/{pageNum}/{pageSize}")
    @Log
    public Result<PageUtil<UserAllInfoVo>> getAllUser(@PathVariable("pageNum") Integer pageNum,
                                                      @PathVariable("pageSize") Integer pageSize,
                                                      @RequestBody UserQueryVo userQueryVo) {
        System.out.println("pageNum = " + pageNum);
        System.out.println("pageSize = " + pageSize);
        System.out.println("userQueryVo = " + userQueryVo);
        PageUtil<UserAllInfoVo> userInfo = userService.getUserInfo(pageNum, pageSize, userQueryVo);
        return Result.success(userInfo);
    }

    /**
     * 用户修改绑定的手机号
     */
    @ApiOperation("用户修改绑定的手机号")
    @PutMapping("")
    @Log
    public Result updateMobileById(@RequestBody UserMobileVo userMobileVo) {
        System.out.println("userMobileVo = " + userMobileVo);
        boolean b = userService.updateMobileById(userMobileVo);
        return Result.success(ResultCode.UPDATE_SUCCESS);
    }

    /**
     * 用户修改基本信息
     */
    @ApiOperation("用户修改基本信息")
    @PutMapping("/userInfo")
    @Log
    public Result updateUserInfoById(@RequestBody UserUpdateVo userUpdateVo) {
        System.out.println("userUpdateVo = " + userUpdateVo);
        userService.updateUserInfoById(userUpdateVo);
        return Result.success(ResultCode.UPDATE_SUCCESS);
    }

    /**
     * 根据id逻辑删除用户
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    @Log
    public Result deleteUserById(@PathVariable("id") Long id) {
        System.out.println("id = " + id);
        userService.deleteUserById(id);
        return Result.success(ResultCode.DELETE_SUCCESS);
    }
}
