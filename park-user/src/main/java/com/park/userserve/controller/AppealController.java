package com.park.userserve.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.park.annotation.Log;
import com.park.userserve.service.AppealService;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import com.park.vos.userservevo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Api(tags = "异常订单处理模块")
@RestController
@RequestMapping("/appeal")
public class AppealController {

    @Resource
    private AppealService appealService;

    /*根据id查异常订单信息*/
    @Log
    @ApiOperation("根据订单表ID和编码查异常订单")
    @GetMapping("/queryAppealByOrderId/{orderId}")
    public Result<List<AppealVo>> queryAppealByOrderId(@PathVariable("orderId") Long orderId){
        List<AppealVo> appealVo = appealService.queryAppealByOrderId(orderId);
        if (appealVo != null && !"".equals(appealVo)){
            return Result.success(ResultCode.SUCCESS,appealVo);
        }else {
            return Result.err(ResultCode.FAIL);
        }
    }

    /*添加订单异常信息*/
    @Log
    @ApiOperation("添加异常订单信息")
    @PostMapping("/addAppeal")
    public Result addAppeal(@RequestBody AppealVo appealVo){
        int flag = appealService.addAppeal(appealVo);
        if (flag > 0){
            return Result.success(ResultCode.INSERT_SUCCESS);
        }else {
            return Result.err(ResultCode.INSERT_FAIL);
        }
    }

    /*修改订单异常信息*/
    @Log
    @ApiOperation("修改异常订单信息")
    @PutMapping ("/updateAppeal")
    public Result updateAppeal(@RequestBody AppealVo appealVo){
        int flag = appealService.updateAppeal(appealVo);
        if (flag > 0){
            return Result.success(ResultCode.UPDATE_SUCCESS);
        }else {
            return Result.err(ResultCode.UPDATE_FAIL);
        }
    }

    /*删除订单异常信息*/
    @Log
    @ApiOperation("删除异常订单信息")
    @DeleteMapping("/deleteAppeal/{id}")
    public Result deleteAppeal(@PathVariable("id") Long id){
        int flag = appealService.deleteAppeal(id);
        if (flag > 0){
            return Result.success(ResultCode.DELETE_SUCCESS);
        }else {
            return Result.err(ResultCode.DELETE_FAIL);
        }
    }

    /*根据订单表id查售后状态（处理情况）*/
    @Log
    @ApiOperation("根据订单ID查状态")
    @GetMapping("/queryAlStateByOrId/{orderId}")
    public Result<AppealQueryVo> queryAlStateByOrId(@PathVariable("orderId") Long orderId){
        AppealQueryVo appealQueryVo = appealService.queryAlStateByOrId(orderId);
        if (appealQueryVo != null && !"".equals(appealQueryVo)){
            return Result.success(ResultCode.SUCCESS,appealQueryVo);
        }else {
            return Result.err(ResultCode.FAIL);
        }
    }

    /*查询所有售后申诉表的状态*/
    @Log
    @ApiOperation("查所有售后申诉表状态")
    @PostMapping("/queryAplAllState")
    public Result<List<AppealQueryVo>> queryAplAllState(){
        List<AppealQueryVo> appealQueryVo = appealService.queryAplAllState();
        return Result.success(ResultCode.SUCCESS,appealQueryVo);
    }

    /*异常订单列表*/
    /*前台传过来的查询条件暂时没设，后期可根据条件更改*/
    @Log
    @ApiOperation("异常订单列表")
    @PostMapping("/queryPageAppeal/{pageNum}/{pageSize}")
    public Result<PageUtil<AppealPageVo>> queryPageAppeal(@PathVariable("pageNum") Integer pageNum,
                                  @PathVariable("pageSize") Integer pageSize,
                                  @RequestBody AppealQueryVo appealQueryVo){
        PageUtil<AppealPageVo> pageUtil = appealService.queryPageAppeal(pageNum,pageSize,appealQueryVo);
        return Result.success(ResultCode.SUCCESS,pageUtil);
    }

    /*导出异常订单*/
    @Log
    @ApiOperation("导出异常订单")
    @PostMapping("/exportQueryPageAppeal/{pageNum}/{pageSize}")
    public Result<PageUtil<AppealPageVo>> exportQueryPageAppeal(@PathVariable("pageNum") Integer pageNum,
                                                          @PathVariable("pageSize") Integer pageSize,
                                                          @RequestBody AppealQueryVo appealQueryVo){
        PageUtil<AppealPageVo> pageUtil = appealService.queryPageAppeal(pageNum,pageSize,appealQueryVo);

        List<AppealPageVo> appealPageVos = pageUtil.getPageData();

        String directoryPath = "F:/XiangMuAll/FourIderXiangMu/parking/excelStorage/appeal_details.xlsx";
        EasyExcel.write(directoryPath,AppealPageVo.class).sheet("sheet1").doWrite(appealPageVos);
        return Result.success(ResultCode.SUCCESS,pageUtil);
    }

    /*根据ID查异常订单详情*/
    @Log
    @ApiOperation("根据ID查异常订单详情")
    @GetMapping("/queryAppealById/{id}")
    public Result<AppealVos> queryAppealById(@PathVariable("id") Long id){
        AppealVos appealVos = appealService.queryAppealById(id);
        if (appealVos != null && !"".equals(appealVos)){
            return Result.success(ResultCode.SUCCESS,appealVos);
        }else {
            return Result.err(ResultCode.FAIL);
        }
    }

    @ApiOperation("订单申诉不通过")
    @PutMapping("/appealNoPass")
    @Log
    public Result appealNoPass(@RequestBody AppealManageVo appealManageVo){
        int flag = appealService.appealNoPass(appealManageVo);
        if (flag > 0){
            return Result.success(ResultCode.UPDATE_SUCCESS);
        }else {
            return Result.err(ResultCode.UPDATE_FAIL);
        }
    }

    @ApiOperation("订单申诉通过退款")
    @PutMapping("/appealPass")
    @Log
    public Result appealPass(@RequestBody AppealManageVo appealManageVo){
        int flag = appealService.appealPass(appealManageVo);
        if (flag > 0){
            return Result.success(ResultCode.UPDATE_SUCCESS);
        }else {
            return Result.err(ResultCode.UPDATE_FAIL);
        }
    }

}
