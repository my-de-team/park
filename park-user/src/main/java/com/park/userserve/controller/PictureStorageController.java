package com.park.userserve.controller;

import com.park.annotation.Log;
import com.park.userserve.service.PictureStorageService;
import com.park.utils.AssertUtils;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import com.park.vos.userservevo.PictureUrlVos;
import com.park.vos.userservevo.PictureVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Api(tags = "照片存储模块")
@RestController
@RequestMapping("/picture/storage")
public class PictureStorageController {

    @Resource
    private PictureStorageService pictureStorageService;

    /*添加照片*/
    @Log
    @ApiOperation("添加照片")
    @PostMapping("/addPicture")
    public Result addPicture(@RequestBody PictureVo pictureVo){
        int flag = pictureStorageService.addPicture(pictureVo);
        if (flag > 0){
            return Result.success(ResultCode.INSERT_SUCCESS);
        }else {
            return Result.err(ResultCode.INSERT_FAIL);
        }
    }
    /*批量添加照片*/
    @Log
    @ApiOperation("批量添加照片")
    @PostMapping("/addAllPicture")
    public Result addAllPicture(@RequestBody PictureVo pictureVo){
        int flag = pictureStorageService.addAllPicture(pictureVo);
        if (flag > 0){
            return Result.success(ResultCode.INSERT_SUCCESS);
        }else {
            return Result.err(ResultCode.INSERT_FAIL);
        }

    }
    /*删除照片*/
    @Log
    @ApiOperation("删除照片")
    @DeleteMapping("/deletePicture/{id}")
    public Result deletePicture(@PathVariable("id") Long id){
        int flag = pictureStorageService.deletePicture(id);
        if (flag > 0){
            return Result.success(ResultCode.DELETE_SUCCESS);
        }else {
            return Result.err(ResultCode.DELETE_FAIL);
        }
    }
    /*根据ID查询照片*/
    @Log
    @ApiOperation("根据ID查照片")
    @GetMapping("/queryPictureById/{id}")
    public Result queryPictureById(@PathVariable("id") Long id){
        PictureVo pictureVo = pictureStorageService.queryPictureById(id);
        if (!"".equals(pictureVo) && pictureVo != null){
            return Result.success(ResultCode.SUCCESS,pictureVo);
        }else {
            return Result.err(ResultCode.FAIL);
        }
    }
    /*修改照片*/
    @Log
    @ApiOperation("修改照片")
    @PutMapping("/updatePicture")
    public Result updatePicture(@RequestBody PictureVo pictureVo){
        int flag = pictureStorageService.updatePicture(pictureVo);
        if (flag > 0){
            return Result.success(ResultCode.UPDATE_SUCCESS);
        }else {
            return Result.err(ResultCode.UPDATE_FAIL);
        }
    }
    /*分页查询照片*/
    @Log
    @ApiOperation("分页查询照片")
    @PostMapping("/queryPagePicture/{pageNum}/{pageSize}")
    public Result queryPagePicture(@PathVariable("pageNum") Long pageNum,
                                   @PathVariable("pageSize") Long pageSize,
                                   @RequestBody PictureVo pictureVo){
        PageUtil<PictureVo> pageUtil = pictureStorageService.queryPagePicture(pageNum,pageSize,pictureVo);
        return Result.success(ResultCode.SUCCESS,pageUtil);

    }

    /*查询订单驶入图片*/
    @Log
    @ApiOperation("查询订单驶入图片")
    @PostMapping("/queryPictureUrl/{orderId}")
    public Result<List<PictureUrlVos>> queryPictureUrl(@PathVariable("orderId") Long orderId){
        List<PictureUrlVos> pictureUrlVos = pictureStorageService.queryPictureUrl(orderId);
        if (pictureUrlVos != null && !"".equals(pictureUrlVos)){
            return Result.success(ResultCode.SUCCESS,pictureUrlVos);
        }else {
            return Result.err(ResultCode.FAIL);
        }

    }
    /*查询订单申诉图片*/
    @ApiOperation("查询订单申诉图片")
    @PostMapping("/queryAppealPictureUrl/{orderId}")
    @Log
    public Result<List<PictureUrlVos>> queryAppealPictureUrl(@PathVariable("orderId") Long orderId){
        List<PictureUrlVos> pictureUrlVos = pictureStorageService.queryAppealPictureUrl(orderId);
        if (pictureUrlVos != null && !"".equals(pictureUrlVos)){
            return Result.success(ResultCode.SUCCESS,pictureUrlVos);
        }else {
            return Result.err(ResultCode.FAIL);
        }
    }

}
