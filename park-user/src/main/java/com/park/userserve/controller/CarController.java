package com.park.userserve.controller;

import com.park.annotation.Log;
import com.park.userserve.service.CarService;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import com.park.vos.userservevo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/car")
@Api(tags = "车辆管理模块")
public class CarController {

    @Autowired
    private CarService carService;

    @ApiOperation("车辆管理页面分页条件查询")
    @PostMapping("/{pageNum}/{pageSize}")
    @Log
    public Result getAllCar(@PathVariable("pageNum") Integer pageNum,
                            @PathVariable("pageSize") Integer pageSize,
                            @RequestBody CarQueryVo carQueryVo) {
        System.out.println("pageNum = " + pageNum);
        System.out.println("pageSize = " + pageSize);
        System.out.println("carQueryVo = " + carQueryVo);
        PageUtil<CarInfoVo> allCar = carService.getAllCar(pageNum, pageSize, carQueryVo);
        return Result.success(allCar);
    }

    @ApiOperation("根据用户id查询车辆详细信息")
    @PostMapping("/userCar/{pageNum}/{pageSize}")
    @Log
    public Result<PageUtil<CarInfoVo>> getUserCarById(@PathVariable("pageNum") Integer pageNum,
                                                      @PathVariable("pageSize") Integer pageSize,
                                                      @RequestBody CarQueryVo carQueryVo) {
        System.out.println("carQueryVo = " + carQueryVo);
        PageUtil<CarInfoVo> userCars = carService.getUserCarById(pageNum, pageSize, carQueryVo);
        return Result.success(userCars);
    }

    /**
     * 用户绑定车辆
     */
    @ApiOperation("用户绑定车辆")
    @PostMapping("")
    @Log
    public Result userUpdateCar(@RequestBody UserBindCarVo userBindCarVo) {
        System.out.println("userBindCarVo = " + userBindCarVo);
        carService.bindCar(userBindCarVo);
        return Result.success(ResultCode.INSERT_SUCCESS);
    }

    /**
     * 用户解绑/修改车牌号车辆
     */
    @ApiOperation("用户解绑/修改车辆")
    @PutMapping("")
    @Log
    public Result userUpdateCarById(@RequestBody CarUpdateVo carUpdateVo) {
        System.out.println("carUpdateVo = " + carUpdateVo);
        carService.userUpdateCarById(carUpdateVo);
        return Result.success(ResultCode.UPDATE_SUCCESS);
    }

    @ApiOperation("用户统计数据")
    @GetMapping("/count/{id}")
    @Log
    public Result<UserOrderCountVo> countByUserId(@PathVariable("id") Long id) {
        System.out.println("id = " + id);
        UserOrderCountVo userOrderCount = carService.countByUserId(id);
        return Result.success(userOrderCount);
    }


    /**
     * 删除车辆
     */
    @ApiOperation("用户删除车辆")
    @DeleteMapping("/{id}")
    @Log
    public Result deleteCarById(@PathVariable("id") Long id) {
        System.out.println("id = " + id);
        carService.deleteCarById(id);
        return Result.success(ResultCode.DELETE_SUCCESS);
    }


}
