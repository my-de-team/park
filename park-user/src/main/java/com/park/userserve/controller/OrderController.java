package com.park.userserve.controller;


import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.park.annotation.Log;
import com.park.userserve.service.OrderService;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import com.park.vos.userservevo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.sl.usermodel.Sheet;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.UUID;

@Api(tags = "订单模块")
@RestController
@RequestMapping("/order")
public class OrderController {

    @Resource
    private OrderService orderService;

    @Log
    @ApiOperation("导出订单")
    @PostMapping("/exportQueryPageOrder/{pageNum}/{pageSize}")
    public Result<PageUtil<OrderPageVo>> exportQueryPageOrder(@PathVariable("pageNum") Long pageNum,
                                                              @PathVariable("pageSize") Long pageSize,
                                                              @RequestBody OrderQueryPageVo orderQueryPageVo) {
        PageUtil<OrderPageVo> pageUtil = orderService.queryPageOrder(pageNum, pageSize, orderQueryPageVo);

        List<OrderPageVo> orderPageVos = pageUtil.getPageData();
        System.out.println("要导出的数据" + orderPageVos);

        orderQueryPageVo.setExportOr(1);
        if (orderQueryPageVo.getExportOr() == 1) {
            //String fileName = UUID.randomUUID().toString();  //使用UUID生成唯一文件名，不会有覆盖分险
            //String fileName = "order_details.xlsx"; // 导出文件名
            String directoryPath = "F:/XiangMuAll/FourIderXiangMu/parking/excelStorage/order_details.xlsx"; // 设置导出文件的目录路径
            EasyExcel.write(directoryPath, OrderPageVo.class).sheet("Sheet1").doWrite(orderPageVos);
        }

        return Result.success(ResultCode.SUCCESS, pageUtil);
    }

    /*分页查询（包含基本条件）*/
    @Log
    @ApiOperation("分页条件查询")
    @PostMapping("/queryPageOrder/{pageNum}/{pageSize}")
    public Result<PageUtil<OrderPageVo>> queryPageOrder(@PathVariable("pageNum") Long pageNum,
                                                        @PathVariable("pageSize") Long pageSize,
                                                        @RequestBody OrderQueryPageVo orderQueryPageVo) {
        PageUtil<OrderPageVo> pageUtil = orderService.queryPageOrder(pageNum, pageSize, orderQueryPageVo);
        return Result.success(ResultCode.SUCCESS, pageUtil);
    }

    /*根据ID查询订单的详细信息*/
    @Log
    @ApiOperation("根据id查订单详情")
    @GetMapping("/queryOrderById/{id}")
    public Result<OrderVos> queryOrderById(@PathVariable("id") Long id) {
        OrderVos orderVos = orderService.queryOrderById(id);
        if (!"".equals(orderVos) && orderVos != null) {
            return Result.success(ResultCode.SUCCESS, orderVos);
        } else {
            return Result.err(ResultCode.FAIL);
        }

    }

    /*创建订单*/
    @Log
    @ApiOperation("添加订单接口")
    @PostMapping("/addOrder")
    public Result addOrder(@RequestBody OrderVo orderVo) {
        int flag = orderService.addOrder(orderVo);
        if (flag > 0) {
            return Result.success(ResultCode.INSERT_SUCCESS);
        } else {
            return Result.err(ResultCode.INSERT_FAIL);
        }
    }

    /*修改订单*/
    @Log
    @ApiOperation("修改订单接口")
    @PutMapping("/updateOrder")
    public Result updateOrder(@RequestBody OrderVo orderVo) {
        int flag = orderService.updateOrder(orderVo);
        if (flag > 0) {
            return Result.success(ResultCode.UPDATE_SUCCESS);
        } else {
            return Result.err(ResultCode.UPDATE_FAIL);
        }
    }

    /*删除订单*/
    @Log
    @ApiOperation("删除订单接口")
    @DeleteMapping("/deleteOrder/{id}")
    public Result deleteOrder(@PathVariable("id") Long id) {
        int flag = orderService.deleteOrder(id);
        if (flag > 0) {
            return Result.success(ResultCode.DELETE_SUCCESS);
        } else {
            return Result.err(ResultCode.DELETE_FAIL);
        }
    }

    @Log
    @ApiOperation("用户订单统计")
    @PostMapping("/queryOrderByUserId/{boundUser}")
    public Result<List<OrderPageVo>> queryOrderByUserId(@PathVariable("boundUser") Long boundUser) {
        List<OrderPageVo> appealVos = orderService.queryOrderByUserId(boundUser);
        System.out.println("查出来的数据" + appealVos);
        return Result.success(ResultCode.SUCCESS, appealVos);
    }

    /*订单结算*/
    @Log
    @ApiOperation("订单结算")
    @PutMapping("/orderSettlement/{berthNumber}")
    public Result orderSettlement(@PathVariable("berthNumber") String berthNumber) {
        int flag = orderService.orderSettlement(berthNumber);
        if (flag > 0) {
            return Result.success(ResultCode.UPDATE_SUCCESS);
        } else {
            return Result.err(ResultCode.UPDATE_FAIL);
        }
    }

    /*订单支付*/
    @Log
    @ApiOperation("订单支付")
    @PutMapping("/payOrder")
    public Result payOrder(@RequestBody OrderPayRequestVo orderPayRequestVo) {
        int flag = orderService.payOrder(orderPayRequestVo);
        if (flag > 0) {
            return Result.success(ResultCode.UPDATE_SUCCESS);
        } else {
            return Result.err(ResultCode.UPDATE_FAIL);
        }
    }

    /*订单申诉*/
    @Log
    @ApiOperation("订单申诉")
    @PutMapping("/updateOrderById")
    public Result updateOrderById(@RequestBody OrderAppealVo orderAppealVo) {
        int flag = orderService.updateOrderById(orderAppealVo);
        if (flag > 0) {
            return Result.success(ResultCode.UPDATE_SUCCESS);
        } else {
            return Result.err(ResultCode.UPDATE_FAIL);
        }
    }

    /*结束订单*/
    @Log
    @ApiOperation("结束订单")
    @PutMapping("/finishOrderById/{id}")
    public Result finishOrderById(@PathVariable("id") Long id) {
        int flag = orderService.finishOrderById(id);
        if (flag > 0) {
            return Result.success(ResultCode.UPDATE_SUCCESS);
        } else {
            return Result.err(ResultCode.UPDATE_FAIL);
        }
    }

    @Log
    @ApiOperation("用户认领")
    @PutMapping("/claimOrder")
    public Result claimOrder(@RequestBody OrderClaimVo orderClaimVo) {
        int flag = orderService.claimOrder(orderClaimVo);
        if (flag > 0) {
            return Result.success(ResultCode.UPDATE_SUCCESS);
        } else {
            return Result.err(ResultCode.UPDATE_FAIL);
        }
    }


}
