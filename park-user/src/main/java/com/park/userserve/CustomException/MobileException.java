package com.park.userserve.CustomException;

public class MobileException extends RuntimeException {

    public MobileException() {
        super();
    }

    public MobileException(String message) {
        super(message);
    }
}
