package com.park.userserve.CustomException;

public class MQBasicAckException extends RuntimeException {

    public MQBasicAckException(String message) {
        super(message);
    }
}
