package com.park.userserve.CustomException;

public class SMSException extends RuntimeException {

    public SMSException() {
        super();
    }

    public SMSException(String message) {
        super(message);
    }
}
