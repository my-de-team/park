package com.park.userserve.CustomException;

public class KongException extends RuntimeException {

    public KongException() {
        super();
    }

    public KongException(String message) {
        super(message);
    }
}
