package com.park.userserve.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * 售后申诉表
 * @TableName tp_appeal_list
 */
@TableName(value ="tp_appeal_list")
@Data
public class Appeal implements Serializable {
    /**
     * 申诉表ID
     */
    @TableId(value = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 订单表ID
     */
    @TableField(value = "order_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long orderId;

    /**
     * 订单编码
     */
    @TableField(value = "order_number")
    private String orderNumber;

    /**
     * 申诉内容
     */
    @TableField(value = "appeal_content")
    private String appealContent;

    /**
     * 申诉时间
     */
    @TableField(value = "appeal_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date appealTime;

    /**
     * 处理情况，0-待处理，1-已退款，2-不通过
     */
    @TableField(value = "appeal_process_state")
    private Integer appealProcessState;

    /**
     * 修改金额
     */
    @TableField(value = "modification_amount")
    private BigDecimal modificationAmount;

    /**
     * 退款金额
     */
    @TableField(value = "refund_amount")
    private BigDecimal refundAmount;

    /**
     * 处理时间
     */
    @TableField(value = "process_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date processTime;

    /**
     * 处理人
     */
    @TableField(value = "process_user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long processUserId;

    /**
     * 留言
     */
    @TableField(value = "leave_words")
    private String leaveWords;

    /**
     * 创建时间
     */
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime;

    /**
     * 逻辑删除，0-未删除（默认），1-已删除
     */
    @TableField(value = "is_deleted")
    @TableLogic
    private Integer isDeleted;

    /**
     * 乐观锁，版本号（默认0）
     */
    @TableField(value = "version")
    private Integer version;

    /**
     * 处理人
     */
    @TableField(value = "process_user")
    private String processUser;

    /**
     * 备用字段二
     */
    @TableField(value = "field2")
    private String field2;

    /**
     * 备用字段三
     */
    @TableField(value = "field3")
    private String field3;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Appeal other = (Appeal) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getOrderId() == null ? other.getOrderId() == null : this.getOrderId().equals(other.getOrderId()))
            && (this.getOrderNumber() == null ? other.getOrderNumber() == null : this.getOrderNumber().equals(other.getOrderNumber()))
            && (this.getAppealContent() == null ? other.getAppealContent() == null : this.getAppealContent().equals(other.getAppealContent()))
            && (this.getAppealTime() == null ? other.getAppealTime() == null : this.getAppealTime().equals(other.getAppealTime()))
            && (this.getAppealProcessState() == null ? other.getAppealProcessState() == null : this.getAppealProcessState().equals(other.getAppealProcessState()))
            && (this.getModificationAmount() == null ? other.getModificationAmount() == null : this.getModificationAmount().equals(other.getModificationAmount()))
            && (this.getRefundAmount() == null ? other.getRefundAmount() == null : this.getRefundAmount().equals(other.getRefundAmount()))
            && (this.getProcessTime() == null ? other.getProcessTime() == null : this.getProcessTime().equals(other.getProcessTime()))
            && (this.getProcessUserId() == null ? other.getProcessUserId() == null : this.getProcessUserId().equals(other.getProcessUserId()))
            && (this.getLeaveWords() == null ? other.getLeaveWords() == null : this.getLeaveWords().equals(other.getLeaveWords()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getIsDeleted() == null ? other.getIsDeleted() == null : this.getIsDeleted().equals(other.getIsDeleted()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()))
            && (this.getProcessUser() == null ? other.getProcessUser() == null : this.getProcessUser().equals(other.getProcessUser()))
            && (this.getField2() == null ? other.getField2() == null : this.getField2().equals(other.getField2()))
            && (this.getField3() == null ? other.getField3() == null : this.getField3().equals(other.getField3()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getOrderId() == null) ? 0 : getOrderId().hashCode());
        result = prime * result + ((getOrderNumber() == null) ? 0 : getOrderNumber().hashCode());
        result = prime * result + ((getAppealContent() == null) ? 0 : getAppealContent().hashCode());
        result = prime * result + ((getAppealTime() == null) ? 0 : getAppealTime().hashCode());
        result = prime * result + ((getAppealProcessState() == null) ? 0 : getAppealProcessState().hashCode());
        result = prime * result + ((getModificationAmount() == null) ? 0 : getModificationAmount().hashCode());
        result = prime * result + ((getRefundAmount() == null) ? 0 : getRefundAmount().hashCode());
        result = prime * result + ((getProcessTime() == null) ? 0 : getProcessTime().hashCode());
        result = prime * result + ((getProcessUserId() == null) ? 0 : getProcessUserId().hashCode());
        result = prime * result + ((getLeaveWords() == null) ? 0 : getLeaveWords().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        result = prime * result + ((getProcessUser() == null) ? 0 : getProcessUser().hashCode());
        result = prime * result + ((getField2() == null) ? 0 : getField2().hashCode());
        result = prime * result + ((getField3() == null) ? 0 : getField3().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", orderId=").append(orderId);
        sb.append(", orderNumber=").append(orderNumber);
        sb.append(", appealContent=").append(appealContent);
        sb.append(", appealTime=").append(appealTime);
        sb.append(", appealProcessState=").append(appealProcessState);
        sb.append(", modificationAmount=").append(modificationAmount);
        sb.append(", refundAmount=").append(refundAmount);
        sb.append(", processTime=").append(processTime);
        sb.append(", processUser=").append(processUserId);
        sb.append(", leaveWords=").append(leaveWords);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", isDeleted=").append(isDeleted);
        sb.append(", version=").append(version);
        sb.append(", field1=").append(processUser);
        sb.append(", field2=").append(field2);
        sb.append(", field3=").append(field3);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}