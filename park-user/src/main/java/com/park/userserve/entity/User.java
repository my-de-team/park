package com.park.userserve.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户表
 *
 * @TableName tp_user
 */
@TableName(value = "tp_user")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class User implements Serializable {
    /**
     * 用户主键id
     */
    @TableId
    private Long id;

    /**
     * 用户名称
     */
    private String name;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 密码 微信一键注册密码与手机号相同
     */
    private String password;

    /**
     * 微信号 微信号 微信注册/登录返回的id
     */
    private String wechat;

    /**
     * 头像
     */
    private String img;

    /**
     * 绑定车辆数量
     */
    private Integer bindNumber;

    /**
     * 是否绑定微信号 0: 绑定 1: 没绑定
     */
    private Integer isBind;

    /**
     * 状态 0：正常 1：异常
     */
    private Integer state;

    /**
     * 最近登录时间
     */
    private Date lastTime;

    /**
     * 创建时间 注册时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 乐观锁 默认：0
     */
    private Integer version;

    /**
     * 是否删除 0:未删除 1:已删除
     */
    @TableLogic
    private Integer isDeleted;

    /**
     * 备用字段1
     */
    private String field1;

    /**
     * 备用字段2
     */
    private String field2;

    /**
     * 备用字段3
     */
    private String field3;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        User other = (User) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
                && (this.getMobile() == null ? other.getMobile() == null : this.getMobile().equals(other.getMobile()))
                && (this.getPassword() == null ? other.getPassword() == null : this.getPassword().equals(other.getPassword()))
                && (this.getWechat() == null ? other.getWechat() == null : this.getWechat().equals(other.getWechat()))
                && (this.getImg() == null ? other.getImg() == null : this.getImg().equals(other.getImg()))
                && (this.getBindNumber() == null ? other.getBindNumber() == null : this.getBindNumber().equals(other.getBindNumber()))
                && (this.getIsBind() == null ? other.getIsBind() == null : this.getIsBind().equals(other.getIsBind()))
                && (this.getState() == null ? other.getState() == null : this.getState().equals(other.getState()))
                && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
                && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
                && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()))
                && (this.getIsDeleted() == null ? other.getIsDeleted() == null : this.getIsDeleted().equals(other.getIsDeleted()))
                && (this.getField1() == null ? other.getField1() == null : this.getField1().equals(other.getField1()))
                && (this.getField2() == null ? other.getField2() == null : this.getField2().equals(other.getField2()))
                && (this.getField3() == null ? other.getField3() == null : this.getField3().equals(other.getField3()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getMobile() == null) ? 0 : getMobile().hashCode());
        result = prime * result + ((getPassword() == null) ? 0 : getPassword().hashCode());
        result = prime * result + ((getWechat() == null) ? 0 : getWechat().hashCode());
        result = prime * result + ((getImg() == null) ? 0 : getImg().hashCode());
        result = prime * result + ((getBindNumber() == null) ? 0 : getBindNumber().hashCode());
        result = prime * result + ((getIsBind() == null) ? 0 : getIsBind().hashCode());
        result = prime * result + ((getState() == null) ? 0 : getState().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
        result = prime * result + ((getField1() == null) ? 0 : getField1().hashCode());
        result = prime * result + ((getField2() == null) ? 0 : getField2().hashCode());
        result = prime * result + ((getField3() == null) ? 0 : getField3().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
//        sb.append("Hash = ").append(hashCode());
        sb.append("id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", mobile=").append(mobile);
        sb.append(", password=").append(password);
        sb.append(", wechat=").append(wechat);
        sb.append(", img=").append(img);
        sb.append(", bindNumber=").append(bindNumber);
        sb.append(", isBind=").append(isBind);
        sb.append(", state=").append(state);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", version=").append(version);
        sb.append(", isDeleted=").append(isDeleted);
        sb.append(", field1=").append(field1);
        sb.append(", field2=").append(field2);
        sb.append(", field3=").append(field3);
//        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}