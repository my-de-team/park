package com.park.userserve.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * 订单表
 * @TableName tp_order_list
 */
@TableName(value ="tp_order_list")
@Data
public class Order implements Serializable {
    /**
     * 订单ID，雪花算法自动生成
     */
    @TableId(value = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 订单编号，前缀【ORN】+ 雪花算法自动生成
     */
    @TableField(value = "order_number")
    private String orderNumber;

    /**
     * 订单提交时间，创建时间
     */
    @TableField(value = "order_submission_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date orderSubmissionTime;

    /**
     * 车牌号码，格式【粤B】+ 英文与字母组合（五位）
     */
    @TableField(value = "plate_number")
    private String plateNumber;

    /**
     * 所属路段
     */
    @TableField(value = "road_segment")
    private String roadSegment;

    /**
     * 泊位编号
     */
    @TableField(value = "berth_number")
    private String berthNumber;

    /**
     * 巡检员,用于关联员工表的巡检员ID
     */
    @TableField(value = "inspector_id")
    private Long inspectorId;

    /**
     * 订单金额，【订单金额】 = 【停车时间】* 【对应时间段单价】
     */
    @TableField(value = "order_amount")
    private BigDecimal orderAmount;

    /**
     * 订单状态，0-进行中，1-待支付，2-已支付，3-已完成
     */
    @TableField(value = "order_state")
    private Integer orderState;

    /**
     * 绑定用户
     */
    @TableField(value = "bound_user")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long boundUser;

    /**
     * 地磁编号
     */
    @TableField(value = "magnetic_number")
    private String magneticNumber;

    /**
     * 驶入时间
     */
    @TableField(value = "drive_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date driveTime;

    /**
     * 驶离时间
     */
    @TableField(value = "leave_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date leaveTime;

    /**
     * 停车时长，【驶离时间】 — 【驶入时间】
     */
    @TableField(value = "park_time")
    private String parkTime;

    /**
     * 订单是否异常，0-正常订单（默认），1-异常订单（用户申诉后订单状态变为异常）
     */
    @TableField(value = "order_abnormal_state")
    private Integer orderAbnormalState;

    /**
     * 实付金额，实际付款金额
     */
    @TableField(value = "order_reality_amount")
    private BigDecimal orderRealityAmount;

    /**
     * 支付方式，1-支付宝支付，2-银联，3-微信支付，4-现金支付
     */
    @TableField(value = "way_pay")
    private Integer wayPay;

    /**
     * 支付时间
     */
    @TableField(value = "pay_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date payTime;

    /**
     * 支付流水号
     */
    @TableField(value = "pay_number")
    private String payNumber;

    /**
     * 创建时间
     */
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime;

    /**
     * 逻辑删除，0-未删除（默认），1-已删除
     */
    @TableField(value = "is_deleted")
    @TableLogic
    private Integer isDeleted;

    /**
     * 乐观锁，版本号（默认0）
     */
    @TableField(value = "version")
    private Integer version;

    /**
     * 备用字段一
     */
    @TableField(value = "field1")
    private String field1;

    /**
     * 备用字段二
     */
    @TableField(value = "field2")
    private String field2;

    /**
     * 备用字段三
     */
    @TableField(value = "field3")
    private String field3;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Order other = (Order) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getOrderNumber() == null ? other.getOrderNumber() == null : this.getOrderNumber().equals(other.getOrderNumber()))
            && (this.getOrderSubmissionTime() == null ? other.getOrderSubmissionTime() == null : this.getOrderSubmissionTime().equals(other.getOrderSubmissionTime()))
            && (this.getPlateNumber() == null ? other.getPlateNumber() == null : this.getPlateNumber().equals(other.getPlateNumber()))
            && (this.getRoadSegment() == null ? other.getRoadSegment() == null : this.getRoadSegment().equals(other.getRoadSegment()))
            && (this.getBerthNumber() == null ? other.getBerthNumber() == null : this.getBerthNumber().equals(other.getBerthNumber()))
            && (this.getInspectorId() == null ? other.getInspectorId() == null : this.getInspectorId().equals(other.getInspectorId()))
            && (this.getOrderAmount() == null ? other.getOrderAmount() == null : this.getOrderAmount().equals(other.getOrderAmount()))
            && (this.getOrderState() == null ? other.getOrderState() == null : this.getOrderState().equals(other.getOrderState()))
            && (this.getBoundUser() == null ? other.getBoundUser() == null : this.getBoundUser().equals(other.getBoundUser()))
            && (this.getMagneticNumber() == null ? other.getMagneticNumber() == null : this.getMagneticNumber().equals(other.getMagneticNumber()))
            && (this.getDriveTime() == null ? other.getDriveTime() == null : this.getDriveTime().equals(other.getDriveTime()))
            && (this.getLeaveTime() == null ? other.getLeaveTime() == null : this.getLeaveTime().equals(other.getLeaveTime()))
            && (this.getParkTime() == null ? other.getParkTime() == null : this.getParkTime().equals(other.getParkTime()))
            && (this.getOrderAbnormalState() == null ? other.getOrderAbnormalState() == null : this.getOrderAbnormalState().equals(other.getOrderAbnormalState()))
            && (this.getOrderRealityAmount() == null ? other.getOrderRealityAmount() == null : this.getOrderRealityAmount().equals(other.getOrderRealityAmount()))
            && (this.getWayPay() == null ? other.getWayPay() == null : this.getWayPay().equals(other.getWayPay()))
            && (this.getPayTime() == null ? other.getPayTime() == null : this.getPayTime().equals(other.getPayTime()))
            && (this.getPayNumber() == null ? other.getPayNumber() == null : this.getPayNumber().equals(other.getPayNumber()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getIsDeleted() == null ? other.getIsDeleted() == null : this.getIsDeleted().equals(other.getIsDeleted()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()))
            && (this.getField1() == null ? other.getField1() == null : this.getField1().equals(other.getField1()))
            && (this.getField2() == null ? other.getField2() == null : this.getField2().equals(other.getField2()))
            && (this.getField3() == null ? other.getField3() == null : this.getField3().equals(other.getField3()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getOrderNumber() == null) ? 0 : getOrderNumber().hashCode());
        result = prime * result + ((getOrderSubmissionTime() == null) ? 0 : getOrderSubmissionTime().hashCode());
        result = prime * result + ((getPlateNumber() == null) ? 0 : getPlateNumber().hashCode());
        result = prime * result + ((getRoadSegment() == null) ? 0 : getRoadSegment().hashCode());
        result = prime * result + ((getBerthNumber() == null) ? 0 : getBerthNumber().hashCode());
        result = prime * result + ((getInspectorId() == null) ? 0 : getInspectorId().hashCode());
        result = prime * result + ((getOrderAmount() == null) ? 0 : getOrderAmount().hashCode());
        result = prime * result + ((getOrderState() == null) ? 0 : getOrderState().hashCode());
        result = prime * result + ((getBoundUser() == null) ? 0 : getBoundUser().hashCode());
        result = prime * result + ((getMagneticNumber() == null) ? 0 : getMagneticNumber().hashCode());
        result = prime * result + ((getDriveTime() == null) ? 0 : getDriveTime().hashCode());
        result = prime * result + ((getLeaveTime() == null) ? 0 : getLeaveTime().hashCode());
        result = prime * result + ((getParkTime() == null) ? 0 : getParkTime().hashCode());
        result = prime * result + ((getOrderAbnormalState() == null) ? 0 : getOrderAbnormalState().hashCode());
        result = prime * result + ((getOrderRealityAmount() == null) ? 0 : getOrderRealityAmount().hashCode());
        result = prime * result + ((getWayPay() == null) ? 0 : getWayPay().hashCode());
        result = prime * result + ((getPayTime() == null) ? 0 : getPayTime().hashCode());
        result = prime * result + ((getPayNumber() == null) ? 0 : getPayNumber().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        result = prime * result + ((getField1() == null) ? 0 : getField1().hashCode());
        result = prime * result + ((getField2() == null) ? 0 : getField2().hashCode());
        result = prime * result + ((getField3() == null) ? 0 : getField3().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", orderNumber=").append(orderNumber);
        sb.append(", orderSubmissionTime=").append(orderSubmissionTime);
        sb.append(", plateNumber=").append(plateNumber);
        sb.append(", roadSegment=").append(roadSegment);
        sb.append(", berthNumber=").append(berthNumber);
        sb.append(", inspector=").append(inspectorId);
        sb.append(", orderAmount=").append(orderAmount);
        sb.append(", orderState=").append(orderState);
        sb.append(", boundUser=").append(boundUser);
        sb.append(", magneticNumber=").append(magneticNumber);
        sb.append(", driveTime=").append(driveTime);
        sb.append(", leaveTime=").append(leaveTime);
        sb.append(", parkTime=").append(parkTime);
        sb.append(", orderAbnormalState=").append(orderAbnormalState);
        sb.append(", orderRealityAmount=").append(orderRealityAmount);
        sb.append(", wayPay=").append(wayPay);
        sb.append(", payTime=").append(payTime);
        sb.append(", payNumber=").append(payNumber);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", isDeleted=").append(isDeleted);
        sb.append(", version=").append(version);
        sb.append(", field1=").append(field1);
        sb.append(", field2=").append(field2);
        sb.append(", field3=").append(field3);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}