package com.park.userserve.service;

import com.park.userserve.entity.Appeal;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.utils.PageUtil;
import com.park.vos.userservevo.*;

import java.util.List;
import java.util.Map;

/**
* @author 86132
* @description 针对表【tp_appeal_list(售后申诉表)】的数据库操作Service
* @createDate 2023-07-13 14:39:02
*/
public interface AppealService extends IService<Appeal> {

    List<AppealVo> queryAppealByOrderId(Long orderId);

    int addAppeal(AppealVo appealVo);

    int updateAppeal(AppealVo appealVo);

    int deleteAppeal(Long id);

    AppealQueryVo queryAlStateByOrId(Long orderId);

    List<AppealQueryVo> queryAplAllState();

    PageUtil<AppealPageVo> queryPageAppeal(Integer pageNum, Integer pageSize, AppealQueryVo appealQueryVo);

    AppealVos queryAppealById(Long id);

    int appealNoPass(AppealManageVo appealManageVo);

    int appealPass(AppealManageVo appealManageVo);
}
