package com.park.userserve.service;

import com.park.userserve.entity.PictureStorage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.utils.PageUtil;
import com.park.vos.userservevo.PictureUrlVos;
import com.park.vos.userservevo.PictureVo;

import java.util.List;

/**
* @author 86132
* @description 针对表【tp_picture_storage(存储驶入图片表)】的数据库操作Service
* @createDate 2023-07-13 14:40:20
*/
public interface PictureStorageService extends IService<PictureStorage> {


    int addPicture(PictureVo pictureVo);

    int addAllPicture(PictureVo pictureVo);

    int deletePicture(Long id);

    PictureVo queryPictureById(Long id);

    int updatePicture(PictureVo pictureVo);

    PageUtil<PictureVo> queryPagePicture(Long pageNum, Long pageSize, PictureVo pictureVo);

    List<PictureUrlVos> queryPictureUrl(Long orderId);

    List<PictureUrlVos> queryAppealPictureUrl(Long orderId);
}
