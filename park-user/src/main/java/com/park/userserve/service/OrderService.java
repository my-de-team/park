package com.park.userserve.service;

import com.park.userserve.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.utils.PageUtil;
import com.park.vos.userservevo.*;

import java.util.List;

/**
* @author 86132
* @description 针对表【tp_order_list(订单表)】的数据库操作Service
* @createDate 2023-07-13 14:35:07
*/
public interface OrderService extends IService<Order> {

    PageUtil<OrderPageVo> queryPageOrder(Long pageNum, Long pageSize, OrderQueryPageVo orderQueryPageVo);

    int deleteOrder(Long id);

    int addOrder(OrderVo orderVo);

    int updateOrder(OrderVo orderVo);

    OrderVos queryOrderById(Long id);

    List<OrderPageVo> queryOrderByUserId(Long boundUser);

    int orderSettlement(String berthNumber);

    int payOrder(OrderPayRequestVo orderPayRequestVo);

    int updateOrderById(OrderAppealVo orderAppealVo);

    int finishOrderById(Long id);

    int claimOrder(OrderClaimVo orderClaimVo);
}
