package com.park.userserve.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.userserve.entity.PictureStorage;
import com.park.userserve.service.PictureStorageService;
import com.park.userserve.mapper.PictureStorageMapper;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import com.park.vos.userservevo.PictureUrlVos;
import com.park.vos.userservevo.PictureVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
* @author 86132
* @description 针对表【tp_picture_storage(存储驶入图片表)】的数据库操作Service实现
* @createDate 2023-07-13 14:40:20
*/
@Service
public class PictureStorageServiceImpl extends ServiceImpl<PictureStorageMapper, PictureStorage>
    implements PictureStorageService{

    @Resource
    private PictureStorageMapper pictureStorageMapper;

    @Override
    public int addPicture(PictureVo pictureVo) {
        PictureStorage pictureStorage = new PictureStorage();
        BeanUtils.copyProperties(pictureVo,pictureStorage);
        int result = pictureStorageMapper.insert(pictureStorage);
        return result;
    }

    @Override
    @Transactional
    public int addAllPicture(PictureVo pictureVo) {
        String[] pictureUrls = pictureVo.getPictureUrl().split(",");
        int count = 0;
        for (String i : pictureUrls){
            pictureVo.setPictureUrl(i);
            addPicture(pictureVo);
            count++;
        }
        System.out.println("count的值" + count);
        System.out.println("pictureUrls.length的值" + pictureUrls.length);
        if (count == pictureUrls.length){
            return 1;
        }else {
            return 0;
        }

    }

    @Override
    public int deletePicture(Long id) {
        int result = pictureStorageMapper.deleteById(id);
        return result;
    }

    @Override
    public PictureVo queryPictureById(Long id) {
        PictureStorage pictureStorage = pictureStorageMapper.selectById(id);
        PictureVo pictureVo = new PictureVo();
        BeanUtils.copyProperties(pictureStorage,pictureVo);
        return pictureVo;
    }

    @Override
    public int updatePicture(PictureVo pictureVo) {
        PictureStorage pictureStorage = new PictureStorage();
        BeanUtils.copyProperties(pictureVo,pictureStorage);
        int result = pictureStorageMapper.updateById(pictureStorage);
        return result;
    }

    @Override
    public PageUtil<PictureVo> queryPagePicture(Long pageNum, Long pageSize, PictureVo pictureVo) {
        IPage<PictureVo> page = new Page<>(pageNum,pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        pictureVo.setIsDeleted(0);
        if (pictureVo != null){
            if (pictureVo.getIsDeleted() != null) {
                queryWrapper.eq("is_deleted",pictureVo.getIsDeleted());
            }
        }
        page = pictureStorageMapper.queryPagePicture(page,queryWrapper);
        PageUtil pageUtil = new PageUtil();
        pageUtil.setPageNum(page.getPages());
        pageUtil.setPageSize(page.getSize());
        pageUtil.setTotal(page.getTotal());
        pageUtil.setPageData(page.getRecords());
        return pageUtil;
    }

    @Override
    public List<PictureUrlVos> queryPictureUrl(Long orderId) {
        List<PictureUrlVos> pictureUrlVos = pictureStorageMapper.queryPictureUrl(orderId);
        return pictureUrlVos;
    }

    @Override
    public List<PictureUrlVos> queryAppealPictureUrl(Long orderId) {
        List<PictureUrlVos> pictureUrlVos = pictureStorageMapper.queryAppealPictureUrl(orderId);
        return pictureUrlVos;
    }
}




