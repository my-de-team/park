package com.park.userserve.service;

import com.park.userserve.entity.Car;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.utils.PageUtil;
import com.park.vos.userservevo.*;
import io.swagger.models.auth.In;

import java.util.List;

/**
 * @author MIN
 * @description 针对表【tp_car(用户绑定车辆表)】的数据库操作Service
 */
public interface CarService extends IService<Car> {

    /**
     * 用户绑定车辆
     *
     * @param userBindCarVo
     * @return
     */
    public boolean bindCar(UserBindCarVo userBindCarVo);

    /**
     * 判断该车是否已经绑定
     * @param carNumber
     * @return
     */
    public boolean isBind(String carNumber);

    /**
     * 平台端 查询全部车辆 车辆管理页面分页条件查询
     *
     * @param pageNum    当前页
     * @param pageSize   每页条数
     * @param carQueryVo 查询条件
     * @return
     */
    public PageUtil<CarInfoVo> getAllCar(Integer pageNum, Integer pageSize, CarQueryVo carQueryVo);

    /**
     * 用户端 根据该用户id查询全部车辆信息 只展示绑定的车辆信息
     *
     * @param userId 用户id
     * @return
     */
    public List<CarInfoVo> getUserCarByUserId(Long userId);

    /**
     * 用户端 根据该用户手机号查询全部车辆信息 只展示绑定的车辆信息
     *
     * @param mobile 用户手机号
     * @return
     */
    public List<CarInfoVo> getUserCarByMobile(String mobile);

    /**
     * 车辆禁用/解绑 用户端
     *
     * @param carUpdateVo 车辆id
     * @return
     */
    public boolean userUpdateCarById(CarUpdateVo carUpdateVo);

    /**
     * 平台端 删除车辆
     *
     * @param id
     * @return
     */
    public boolean deleteCarById(Long id);

    /**
     * 统计用户绑定的车辆数量
     *
     * @param userId 用户id
     * @return
     */
    public int countUserBindCarById(Long userId);


    /**
     * 根据用户id查询该用户绑定的车辆详细信息
     *
     * @param carQueryVo
     * @return
     */
    PageUtil<CarInfoVo> getUserCarById(Integer pageNum, Integer pageSize, CarQueryVo carQueryVo);

    UserOrderCountVo countByUserId(Long id);
}
