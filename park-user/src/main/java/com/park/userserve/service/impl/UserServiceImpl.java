package com.park.userserve.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.http.Header;
import cn.hutool.http.useragent.Browser;
import cn.hutool.http.useragent.OS;
import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;

import java.util.Date;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.annotation.Log;
import com.park.userserve.CustomException.KongException;
import com.park.userserve.CustomException.MobileException;
import com.park.userserve.CustomException.SMSException;
import com.park.userserve.CustomException.UserException;
import com.park.userserve.aop.IpUtils;
import com.park.userserve.entity.User;
import com.park.userserve.esentity.UserLoginLogVo;
import com.park.userserve.service.CarService;
import com.park.userserve.service.UserService;
import com.park.userserve.mapper.UserMapper;
import com.park.userserve.utils.JwtUtils;
import com.park.utils.BCrypt;
import com.park.utils.PageUtil;
import com.park.utils.ResultCode;
import com.park.vos.systemvo.LoginLogVo;
import com.park.vos.userservevo.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author MIN
 * @description 针对表【tp_user(用户表)】的数据库操作Service实现
 * @createDate 2023-07-13 10:39:21
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
        implements UserService {

    @Resource
    private UserMapper userMapper;

    @Autowired
    private CarService carService;

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Resource
    private RabbitTemplate rabbitTemplate;

    /**
     * 用户登录 根据手机号 密码登录
     *
     * @param userLoginVo 登录信息
     * @return token口令
     */
    @Override
    @Transactional
    public String login(UserLoginVo userLoginVo, HttpServletRequest request) {

        if (StrUtil.isBlank(userLoginVo.getWechat())) {
            throw new KongException("微信号");
        }
        String ipAddress = IpUtils.getIpAddress(request);
        System.out.println("ipAddress = " + ipAddress);
        String ipSource = IpUtils.getIpSource(ipAddress);
        System.out.println("ipSource = " + ipSource);
        UserAgent ua = UserAgentUtil.parse(request.getHeader(Header.USER_AGENT.toString()));
        Browser browser = ua.getBrowser();
        System.out.println("browser = " + browser);
        OS os = ua.getOs();
        String s = os.toString();
        System.out.println("os = " + s);
        String osVersion = ua.getOsVersion();
        System.out.println("osVersion = " + osVersion);
        String version = ua.getVersion();
        System.out.println("version = " + version);
        LoginLogVo loginLogVo = new LoginLogVo();
        loginLogVo.setUsername(userLoginVo.getName());
        loginLogVo.setIpAddress(ipAddress);
        if (StrUtil.isBlank(ipSource)) {
            loginLogVo.setIpSource("广东 深圳");
        }
        loginLogVo.setIpSource(ipSource);
        loginLogVo.setBrower(browser.toString() + " " + version);
        loginLogVo.setOs(s);
        loginLogVo.setState(0);
        loginLogVo.setWechat(userLoginVo.getWechat());
        User user = userMapper.login(userLoginVo.getWechat());
        if (user == null) {
            //注册用户 之后提醒用户绑定手机号
            UserRegisterVo userRegister = new UserRegisterVo();
            userRegister.setName(userLoginVo.getName());
            userRegister.setMobile("-1");
            userRegister.setWechat(userLoginVo.getWechat());
            userRegister.setImg(userLoginVo.getImg());
            Long i = addUser(userRegister);
            //返回id 提醒用户绑定手机号
            LogVo logVo = new LogVo();
            logVo.setLoginLogVo(loginLogVo);
            logVo.setReqParam(userLoginVo.toString());
            logVo.setRespData(userLoginVo.getWechat());
            rabbitTemplate.convertAndSend("user-register-exchange", "userRegisterKey", logVo);
//            HashMap<String, Object> tokeMap = new HashMap<>();
//            tokeMap.put("id", i);
//            tokeMap.put("mobile", "-1");
//            tokeMap.put("name", userLoginVo.getName());
//            tokeMap.put("wechat", userLoginVo.getWechat());
//            String token = JwtUtils.getJwtToken(tokeMap);
//            redisTemplate.opsForValue().set(("token:" + token), "张三", 12, TimeUnit.HOURS);
            return "0";
        }
        if ("-1".equals(user.getMobile())) {
            HashMap<String, Object> tokeMap = new HashMap<>();
            tokeMap.put("id", user.getId());
            tokeMap.put("mobile", user.getMobile());
            tokeMap.put("name", user.getName());
            tokeMap.put("wechat", user.getWechat());
            String token = JwtUtils.getJwtToken(tokeMap);
            redisTemplate.opsForValue().set(("token:" + token), "张三", 12, TimeUnit.HOURS);
            return "0";
        }
        HashMap<String, Object> tokeMap = new HashMap<>();
        tokeMap.put("id", user.getId());
        tokeMap.put("mobile", user.getMobile());
        tokeMap.put("name", user.getName());
        tokeMap.put("wechat", user.getWechat());
        String token = JwtUtils.getJwtToken(tokeMap);
        redisTemplate.opsForValue().set(("token:" + token), "张三");

        loginLogVo.setMobile(user.getMobile());
        System.out.println("loginLogVo = " + loginLogVo);
        rabbitTemplate.convertAndSend("user-login-exchange", "userLoginKey", loginLogVo);
        return token;

    }

    /**
     * 用户端 注册
     *
     * @param userRegisterVo
     * @return
     */
    @Override
    @ApiOperation("用户注册")
    @Log
    @Transactional
    public long addUser(UserRegisterVo userRegisterVo) {
//        boolean b = nameIsRepeat(userRegisterVo.getMobile());
//        if (b) {
//            return -2;
//        }
        User user = new User();
        user.setName(userRegisterVo.getName());
        user.setMobile(userRegisterVo.getMobile());
        //加密
        String salt = BCrypt.gensalt(10, new SecureRandom());
        String password = BCrypt.hashpw("123456", salt);
        user.setPassword(password);
        user.setWechat(userRegisterVo.getWechat());
        user.setImg(userRegisterVo.getImg());
        user.setBindNumber(0);
        user.setIsBind(0);
        user.setState(0);
        int insert = userMapper.insert(user);
        if (insert == 1) {
            return user.getId();
        }
        return 0;
    }

    /**
     * 用户绑定手机号
     *
     * @param userBindMobileVo
     * @return
     */
    @Override
    @Transactional
    public boolean bindMobile(UserBindMobileVo userBindMobileVo, HttpServletRequest request) {
        if (StrUtil.isBlank(userBindMobileVo.getMobile())) {
            throw new KongException("绑定手机号");
        }
        User user = new User();
        user.setWechat(userBindMobileVo.getWechat());
        user.setMobile(userBindMobileVo.getMobile());
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("mobile", user.getMobile());
        updateWrapper.eq("wechat", user.getWechat());
        int update = userMapper.update(user, updateWrapper);
        if (update != 1) {
            throw new UserException("UPD");
        }
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("wechat", userBindMobileVo.getWechat());
        User userInfo = userMapper.selectOne(queryWrapper);
        HashMap<String, Object> tokeMap = new HashMap<>();
        tokeMap.put("id", userInfo.getId());
        tokeMap.put("mobile", userInfo.getMobile());
        tokeMap.put("name", userInfo.getName());
        tokeMap.put("wechat", userInfo.getWechat());
        String jwtToken = JwtUtils.getJwtToken(tokeMap);
        redisTemplate.opsForValue().set(("token:" + jwtToken), "张三");
        //异步添加登陆日志
        String ipAddress = IpUtils.getIpAddress(request);
        System.out.println("ipAddress = " + ipAddress);
        String ipSource = IpUtils.getIpSource(ipAddress);
        System.out.println("ipSource = " + ipSource);
        UserAgent ua = UserAgentUtil.parse(request.getHeader(Header.USER_AGENT.toString()));
        Browser browser = ua.getBrowser();
        System.out.println("browser = " + browser);
        OS os = ua.getOs();
        String s = os.toString();
        System.out.println("os = " + s);
        String osVersion = ua.getOsVersion();
        System.out.println("osVersion = " + osVersion);
        String version = ua.getVersion();
        System.out.println("version = " + version);
        LoginLogVo loginLogVo = new LoginLogVo();
        loginLogVo.setUsername(user.getName());
        loginLogVo.setIpAddress(ipAddress);
        if (StrUtil.isBlank(ipSource)) {
            loginLogVo.setIpSource("广东 深圳");
        }
        loginLogVo.setIpSource(ipSource);
        loginLogVo.setBrower(browser.toString() + " " + version);
        loginLogVo.setOs(s);
        loginLogVo.setState(0);
        loginLogVo.setWechat(userInfo.getWechat());
        loginLogVo.setUsername(userInfo.getName());
        loginLogVo.setMobile(userInfo.getMobile());
        System.out.println("loginLogVo = " + loginLogVo);
        rabbitTemplate.convertAndSend("user-login-exchange", "userLoginKey", loginLogVo);
        return true;
    }

    /**
     * 用户端 根据用户id查看个人信息
     *
     * @param id 用户id
     * @return 用户id 用户头像 手机号 用户名称
     */
    @Override
    public UserInfoVo getUserInfoById(Long id) {
        UserInfoVo userInfoVo = userMapper.getUserInfoById(id);
        if (userInfoVo == null) {
            return null;
        }
        return userInfoVo;
    }

    /**
     * 用户端 根据手机号查看个人信息
     *
     * @param mobile 手机号
     * @return 用户id 用户头像 手机号 用户名称
     */
    @Override
    public UserInfoVo getUserInfoByMobile(String mobile) {
        UserInfoVo userInfoVo = userMapper.getUserInfoByMobile(mobile);
        if (userInfoVo == null) {
            return null;
        }
        return userInfoVo;
    }

    /**
     * 平台端 查看用户详细信息
     *
     * @param id 用户id查看
     * @return
     */
    @Override
    public UserAllInfoVo getUserAllInfoById(Long id) {
        //查看用户个人信息
        UserAllInfoVo userAllInfo = userMapper.getUserAllInfoById(id);
        //查看用户最近登录时间
        String wechat = userAllInfo.getWechat();
        userAllInfo.setLastLoginTime(new Date());
        //查看用户订单状态
        int i = carService.countUserBindCarById(userAllInfo.getId());
        userAllInfo.setBindNumber(i);
        return userAllInfo;
    }


    /**
     * 平台分页查询用户信息
     *
     * @param pageNum     当前页
     * @param pageSize    每页条数
     * @param userQueryVo 查询条件
     * @return
     */
    @Override
    public PageUtil<UserAllInfoVo> getUserInfo(Integer pageNum, Integer pageSize, UserQueryVo userQueryVo) {
        Page<UserAllInfoVo> page = new Page<>(pageNum, pageSize);
        QueryWrapper<UserAllInfoVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("create_time");
        page = userMapper.getUserInfo(page, userQueryVo, queryWrapper);
        PageUtil<UserAllInfoVo> userAllInfo = new PageUtil<>();
        userAllInfo.setPageNum(page.getCurrent());
        userAllInfo.setPageSize(page.getSize());
        userAllInfo.setTotal(page.getTotal());
        userAllInfo.setPageData(page.getRecords());

        return userAllInfo;
    }

    /**
     * 绑车成功后 用户表绑定车辆数量+1
     * 解绑后 用户绑定车辆数量-1;
     *
     * @param id
     * @return
     */
    @Override
    public boolean updateBindCarNumber(Long id, int number) {
        int bind = userMapper.updateBindCarNumber(id, number);
        if (bind == 1) {
            return true;
        }
        return false;
    }

    /**
     * 用户修改绑定的手机号
     *
     * @param userUpdateVo
     * @return
     */
    @Override
    public boolean updateUserMobile(UserUpdateVo userUpdateVo) {
        //验证短信验证码是否正确
        //修改绑定的手机号
        return false;
    }

    @Override
    public boolean updateUserName(UserUpdateVo userUpdateVo) {
        return false;
    }

    /**
     * 平台端 根据id禁用用户
     *
     * @param id 用户id
     * @return
     */
    @Override
    public boolean disableUserById(Long id) {
        int disable = userMapper.disableUserById(id);
        if (disable == 1) {
            return true;
        }
        return false;
    }

    /**
     * 根据手机号查询用户id
     *
     * @param mobile 用户手机号
     * @return
     */
    @Override
    public Long getUserId(String mobile) {
        Long id = userMapper.getUserId(mobile);
        if (id == null || id == 0) {
            return null;
        }
        return id;
    }

    @Override
    public String getUserMobile(Long id) {
        String mobile = userMapper.getUserMobile(id);
        if (StrUtil.isBlank(mobile)) {
            return null;
        }
        return mobile;
    }

    /**
     * 根据id逻辑删除用户
     *
     * @param id
     * @return
     */
    @Override
    @Transactional
    public boolean deleteUserById(Long id) {
        int delete = userMapper.deleteById(id);
        if (delete == 1) {
            return true;
        }
        return false;
    }

    /**
     * 修改用户基本信息
     *
     * @param userUpdateVo
     * @return
     */
    @Override
    public boolean updateUserInfoById(UserUpdateVo userUpdateVo) {
        User user = new User();
        user.setId(userUpdateVo.getId());
        user.setName(userUpdateVo.getName());
        user.setPassword(userUpdateVo.getPassword());
        user.setImg(userUpdateVo.getImg());
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("name", user.getName());
        updateWrapper.set("password", user.getPassword());
        updateWrapper.set("img", user.getImg());
        updateWrapper.eq("id", user.getId());
        int update = userMapper.updateUserInfoById(user);
        if (update == 1) {
            return true;
        }
        throw new UserException("UPD");
    }

    /**
     * 统计当天注册人员数量
     *
     * @return
     */
    @Override
    public Integer getUserByCreateTime() {
        String today = DateUtil.today();
        Integer count = (Integer) redisTemplate.opsForValue().get("userCount:" + today);
        return count;
    }

    /**
     * 根据手机号判断是否已使用
     *
     * @param mobile 手机号
     * @return
     */
    @Override
    public boolean mobileIsRepeat(String mobile) {
        List<Long> ids = userMapper.mobileIsRepeat(mobile);
        if (ids.size() == 0) {
            return false;
        }
        throw new MobileException("手机号重复");
    }

    /**
     * 判断用户名是否已使用
     *
     * @param name
     * @return
     */
    @Override
    public boolean nameIsRepeat(String name) {
        List<Long> ids = userMapper.nameIsRepeat(name);
        if (ids.size() == 0) {
            return false;
        }
        throw new UserException(ResultCode.REGISTER_USERNAME_FAIL.getMessage());
    }

    /**
     * 根据id修改手机号
     *
     * @param userMobileVo
     * @return
     */
    @Override
    @Transactional
    public boolean updateMobileById(UserMobileVo userMobileVo) {
        //校验手机号是否重复
        mobileIsRepeat(userMobileVo.getMobile());
        //判断redis中验证码受否正确
        String code = (String) redisTemplate.opsForValue().get("SMSCode:" + userMobileVo.getMobile());
        if (!code.equals(userMobileVo.getMessageCode())) {
            throw new SMSException();
        }
        User user = new User();
        user.setId(userMobileVo.getId());
        user.setMobile(userMobileVo.getMobile());
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("mobile", user.getMobile());
        updateWrapper.eq("id", user.getId());
        int update = userMapper.update(user, updateWrapper);
        if (update == 1) {
            return true;
        }
        throw new UserException("UPD");
    }
}




