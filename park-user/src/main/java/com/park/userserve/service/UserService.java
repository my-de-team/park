package com.park.userserve.service;

import com.park.userserve.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.utils.PageUtil;
import com.park.vos.userservevo.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author MIN
 * @description 针对表【tp_user(用户表)】的数据库操作Service
 * @createDate 2023-07-13 10:39:21
 */
public interface UserService extends IService<User> {

    /**
     * 用户登录 根据手机号 密码登录
     */
    public String login(UserLoginVo userLoginVo, HttpServletRequest request);

    /**
     * 用户注册
     */
    public long addUser(UserRegisterVo userRegisterVo);

    /**
     * 用户绑定手机号
     *
     * @param userBindMobileVo
     * @return
     */
    boolean bindMobile(UserBindMobileVo userBindMobileVo, HttpServletRequest request);

    /**
     * 用户端 根据手机号查询个人数据
     *
     * @param id 用户id
     * @return UserInfoVo
     */
    public UserInfoVo getUserInfoById(Long id);

    /**
     * 用户端 根据手机号查询个人数据
     *
     * @param mobile 手机号
     * @return UserInfoVo
     */
    public UserInfoVo getUserInfoByMobile(String mobile);

    /**
     * 平台端查询个人详细数据 根据id
     */
    public UserAllInfoVo getUserAllInfoById(Long id);

    /**
     * 分页全部用户的信息
     */
    public PageUtil<UserAllInfoVo> getUserInfo(Integer pageNum, Integer pageSize, UserQueryVo userQueryVo);

    /**
     * 绑定车辆修改 数量
     *
     * @param id     用户id
     * @param number 修改后的数量
     * @return
     */
    public boolean updateBindCarNumber(Long id, int number);

    /**
     * 用户修改手机号
     */
    public boolean updateUserMobile(UserUpdateVo userUpdateVo);

    /**
     * 用户修改个人信息
     *
     * @param userUpdateVo
     * @return
     */
    public boolean updateUserName(UserUpdateVo userUpdateVo);

    /**
     * pc端 根据id禁用用户
     *
     * @param id 用户id
     * @return
     */
    public boolean disableUserById(Long id);

    /**
     * 根据手机号查询用户id
     *
     * @param mobile 用户手机号
     * @return
     */
    Long getUserId(String mobile);

    /**
     * 根据id查询用户手机号
     *
     * @param id 用户id
     * @return
     */
    String getUserMobile(Long id);

    /**
     * 逻辑删除用户
     *
     * @param id 用户id
     * @return
     */
    boolean deleteUserById(Long id);

    /**
     * 根据id修改用户基本信息
     *
     * @param userUpdateVo
     * @return
     */
    boolean updateUserInfoById(UserUpdateVo userUpdateVo);

    /**
     * 统计当天注册人员数量
     *
     * @return
     */
    Integer getUserByCreateTime();

    /**
     * 判断手机号是否重复
     *
     * @param mobile
     * @return
     */
    boolean mobileIsRepeat(String mobile);

    /**
     * 判断用户名是否重复
     *
     * @param name
     * @return
     */
    boolean nameIsRepeat(String name);

    /**
     * 用户更改手机号
     *
     * @param userMobileVo
     * @return
     */
    boolean updateMobileById(UserMobileVo userMobileVo);

}
