package com.park.userserve.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.api.PayControllerApi;
import com.park.userserve.entity.Order;
import com.park.userserve.service.AppealService;
import com.park.userserve.service.OrderService;
import com.park.userserve.mapper.OrderMapper;
import com.park.userserve.service.PictureStorageService;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.vos.payservevo.TradeLogVo;
import com.park.vos.userservevo.*;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
* @author 86132
* @description 针对表【tp_order_list(订单表)】的数据库操作Service实现
* @createDate 2023-07-13 14:35:07
*/
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order>
    implements OrderService{

    @Resource
    private OrderMapper orderMapper;

    @Resource
    private PayControllerApi payControllerApi;

    @Resource
    private PictureStorageService pictureStorageService;

    @Resource
    private AppealService appealService;

    @Override
    public PageUtil<OrderPageVo> queryPageOrder(Long pageNum, Long pageSize, OrderQueryPageVo orderQueryPageVo) {
        /*用来接数据*/
        IPage<OrderPageVo> page = new Page<>(pageNum,pageSize);
        /*查询条件*/
        QueryWrapper queryWrapper = new QueryWrapper();
        /*为了避免传过来的查询条件为空时，数据库查询语句出错*/
        orderQueryPageVo.setIsDeleted(0);
        if (orderQueryPageVo != null){
            /*删除*/
            if (orderQueryPageVo.getIsDeleted() != null){
                queryWrapper.eq("tol.is_deleted",orderQueryPageVo.getIsDeleted());
            }
            /*订单编号*/
            if (orderQueryPageVo.getOrderNumber() != null && !"".equals(orderQueryPageVo.getOrderNumber())){
                queryWrapper.eq("tol.order_number",orderQueryPageVo.getOrderNumber());
            }
            /*车牌号码*/
            if (orderQueryPageVo.getPlateNumber() != null && !"".equals(orderQueryPageVo.getPlateNumber())){
                queryWrapper.eq("tol.plate_number",orderQueryPageVo.getPlateNumber());
            }
            /*所属路段*/
            if (orderQueryPageVo.getRoadSegment() != null && !"".equals(orderQueryPageVo.getRoadSegment())){
                queryWrapper.like("road_segment",orderQueryPageVo.getRoadSegment());
            }
            /*泊位编号*/
            if (orderQueryPageVo.getBerthNumber() != null && !"".equals(orderQueryPageVo.getBerthNumber())){
                queryWrapper.eq("tol.berth_number",orderQueryPageVo.getBerthNumber());
            }
            /*订单状态*/
            if (orderQueryPageVo.getOrderState() != null){
                queryWrapper.eq("tol.order_state",orderQueryPageVo.getOrderState());
            }
            /*订单提交时间,范围查询开始时间*/
            if (orderQueryPageVo.getOrderSubStartTime() != null){
                /*接时间数组*/
                queryWrapper.ge("tol.order_submission_time",orderQueryPageVo.getOrderSubStartTime());

            }
            /*订单提交时间,范围查询结束时间*/
            if (orderQueryPageVo.getOrderSubEndTime() != null){
                queryWrapper.le("tol.order_submission_time",orderQueryPageVo.getOrderSubEndTime());
            }
            /*根据用户查订单*/
            if (orderQueryPageVo.getBoundUser() != null){
                queryWrapper.eq("tol.bound_user",orderQueryPageVo.getBoundUser());
            }
            /*订单为异常订单时*/
            if (orderQueryPageVo.getOrderAbnormalState() != null){
                /*当管理员进入异常订单管理页面时候，订单状态默认为异常*/
                if (orderQueryPageVo.getOrderAbnormalState() == 1){
                    queryWrapper.eq("tol.order_abnormal_state",orderQueryPageVo.getOrderAbnormalState());
//                    String str = "tol.id";
//                    queryWrapper.eq("tal.order_id",str);
                }
            }

        }
        /*如果订单状态是已支付查询支付方式*/
        /*如果订单状态为异常订单，查询订单异常状态*/
        /*分页查询*/
        page = orderMapper.queryPageOrder(page, queryWrapper);
        System.out.println("查询到的数据" + page);
        /*创建对象获取结果*/
        PageUtil pageUtil = new PageUtil();
        /*总记录数*/
        pageUtil.setTotal(page.getTotal());
        /*当前页*/
        pageUtil.setPageNum(page.getPages());
        /*每页条数*/
        pageUtil.setPageSize(page.getSize());
        /*本页数据集合*/
        pageUtil.setPageData(page.getRecords());
        System.out.println("本页数据" + page.getRecords());
        return pageUtil;
    }

    @Override
    public int deleteOrder(Long id) {
        int result = orderMapper.deleteById(id);
        return result;
    }

    @Override
    public int addOrder(OrderVo orderVo) {
        Order order = new Order();
        BeanUtils.copyProperties(orderVo,order);
        int result = orderMapper.insert(order);
        return result;
    }

    @Override
    public int updateOrder(OrderVo orderVo) {
        Order order = new Order();
        BeanUtils.copyProperties(orderVo,order);
        int result = orderMapper.updateById(order);
        return result;
    }

    /*此处在根据ID多表联查出订单详情后，调用图片存储表的方法查询订单驶入图片，将其遍历循环展示出来*/
    @Override
    public OrderVos queryOrderById(Long id) {
        OrderVos orderVos = orderMapper.queryOrderById(id);
        List<PictureUrlVos> pictureUrlVos = pictureStorageService.queryPictureUrl(id);
        ArrayList<String> pictures = new ArrayList<>();
        for (int i = 0; i < pictureUrlVos.size(); i++){
            String picture = pictureUrlVos.get(i).getPictureUrl();
            pictures.add(picture);
        }
        orderVos.setPictureUrl(pictures);
        return orderVos;
    }

    @Override
    public List<OrderPageVo> queryOrderByUserId(Long boundUser) {
        List<OrderPageVo> appealVos = orderMapper.queryOrderByUserId(boundUser);
        return appealVos;
    }

    @Override
    public int orderSettlement(String berthNumber) {
        /*1、根据泊位编号查询出状态为进行中订单信息*/
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("berth_number",berthNumber);
        queryWrapper.eq("order_state",0);
        queryWrapper.eq("is_deleted",0);
        Order order = orderMapper.selectOne(queryWrapper);
        /*2、求出订单停车时长*/
        /*先获取此订单的驶离时间，为当前时间*/
        order.setLeaveTime(new Date());
        /*根据驶入时间和驶离时间计算出时间差值*/
        Long parkTimeMILLISECONDS = Math.abs(order.getDriveTime().getTime() - order.getLeaveTime().getTime());
        Long parkTimeMINUTES = TimeUnit.MINUTES.convert(parkTimeMILLISECONDS,TimeUnit.MILLISECONDS);
        /*更新停车时长*/
        order.setParkTime(parkTimeMINUTES.toString());
        System.out.println("停车时长" + order.getParkTime());
        /*3、计算订单金额*/
        /*转换类型*/
        int a = Integer.parseInt(order.getParkTime());
        /*求出停了多少小时,每30分收两元，不到30的按照免费算*/
        int b = a / 30;
        /*订单金额 = 停车时长 * 单价*/
        int money = b * 2;
        BigDecimal bigDecimal = new BigDecimal(money);
        /*更新订单金额*/
        order.setOrderAmount(bigDecimal);
        System.out.println("订单金额" + order.getOrderAmount());
        /*4、更新数据*/
        /*更新此订单的订单状态为待支付*/
        order.setOrderState(1);
        System.out.println("此时订单的信息" + order);
        int result = orderMapper.updateById(order);
        return result;
    }

    @Override
    @Transactional
    public int payOrder(OrderPayRequestVo orderPayRequestVo) {
        /*通过订单id查询出订单信息*/
        OrderVos orderVos = queryOrderById(orderPayRequestVo.getId());
        /*订单状态为待支付时，才可以支付*/
        if (orderVos.getOrderState() == 1){
            /*用户支付订单的数据*/
            TradeLogVo tradeLogVoNew = new TradeLogVo();
            /*交易标题*/
            tradeLogVoNew.setTitle("停车费");
            /*订单表ID*/
            tradeLogVoNew.setOrderId(orderVos.getId());
            /*---------------支付方式--------------------*/
            tradeLogVoNew.setPayPlatform(orderPayRequestVo.getWayPay());
            /*交易类型*/
            tradeLogVoNew.setTradeType(1);
            /*交易金额*/
            tradeLogVoNew.setAmount(orderVos.getOrderAmount());
            /*调用支付接口*/
            /*1支付宝,2银联,3微信,4现金*/
            if (orderPayRequestVo.getWayPay() == 1){
                Result<Object> tradeLog = payControllerApi.placeOrderForPCWeb(tradeLogVoNew);
                if (tradeLog.getCode() == "200"){
                    System.out.println("退款成功");
                }else {
                    System.out.println("抛出一个全局异常，银联退款失败");
                }
            }
            if (orderPayRequestVo.getWayPay() == 2){
                Result<Object> tradeLog = payControllerApi.addOrder(tradeLogVoNew);
                if (tradeLog.getCode() == "200"){
                    System.out.println("退款成功");
                }else {
                    System.out.println("抛出一个全局异常，银联退款失败");
                }
            }
            if (orderPayRequestVo.getWayPay() == 3 || orderPayRequestVo.getWayPay() == 4){
                Result<Object> tradeLog = payControllerApi.addTradeLog(tradeLogVoNew);
                if (tradeLog.getCode() == "200"){
                    System.out.println("退款成功");
                }else {
                    System.out.println("抛出一个全局异常，银联退款失败");
                }
            }
            /*通过订单表ID查询交易记录表中支付记录数据*/
            Result<TradeLogVo> tradeLogVo = payControllerApi.getTradeLogByOrderId(orderPayRequestVo.getId());
            /*更新订单表数据*/
            /*更新实付金额*/
            orderVos.setOrderRealityAmount(tradeLogVo.getData().getAmount());
            /*更新支付方式*/
            orderVos.setWayPay(tradeLogVo.getData().getPayPlatform());
            /*更新支付时间*/
            orderVos.setPayTime(new Date());
            /*更新支付流水*/
            orderVos.setPayNumber(tradeLogVo.getData().getReqFlow());
            /*将订单状态更新为已支付*/
            orderVos.setOrderState(2);
            OrderVo orderVo = new OrderVo();
            BeanUtils.copyProperties(orderVos,orderVo);
            System.out.println("订单更新后的数据" + orderVo);
            /*更新数据*/
            int result = updateOrder(orderVo);
            return result;
        }
        return 0;
    }

    @Override
    @Transactional
    public int updateOrderById(OrderAppealVo orderAppealVo) {

        /*后期可添加判断条件已支付、已完成状态下才可申诉   前台进行判断*/
        /*添加修改金额的判断，不能大于订单金额*/
        OrderVos orderVos = queryOrderById(orderAppealVo.getOrderId());
        if (orderVos.getOrderState() == 2 || orderVos.getOrderState() == 3){
            OrderVo orderVo = new OrderVo();
            orderVo.setId(orderAppealVo.getOrderId());
            orderVo.setOrderAbnormalState(1);
            int result1 = updateOrder(orderVo);
            System.out.println("订单表修改结果" + result1);

            AppealVo appealVo = new AppealVo();
            appealVo.setOrderId(orderAppealVo.getOrderId());
            appealVo.setOrderNumber(orderAppealVo.getOrderNumber());
            appealVo.setAppealContent(orderAppealVo.getAppealContent());
            appealVo.setAppealTime(new Date());
            appealVo.setAppealProcessState(0);

            if (orderAppealVo.getModificationAmount().compareTo(orderVos.getOrderRealityAmount()) <= 0){
                appealVo.setModificationAmount(orderAppealVo.getModificationAmount());
            }else {
                System.out.println("修改金额不得大于实付金额");
            }

            int result2 = appealService.addAppeal(appealVo);


            /*添加申诉图片 --------------*/
            PictureVo pictureVo = new PictureVo();
            pictureVo.setOrderId(orderAppealVo.getOrderId());
            pictureVo.setOrderNumber(orderAppealVo.getOrderNumber());
            pictureVo.setPictureUrl(orderAppealVo.getPictureUrl());
            pictureVo.setPictureAttribute(1);
            int result3 = pictureStorageService.addAllPicture(pictureVo);

            if (result1 == 1 && result2 == 1 && result3 == 1){
                return 1;
            }
        }

        return 0;
    }

    @Override
    public int finishOrderById(Long id) {
        OrderVo orderVo = new OrderVo();
        orderVo.setId(id);
        /*更新订单状态为已完成*/
        orderVo.setOrderState(3);
        /*更新订单是否异常状态为直接结束订单*/
        orderVo.setOrderAbnormalState(2);
        int result = updateOrder(orderVo);
        return result;
    }

    @Override
    public int claimOrder(OrderClaimVo orderClaimVo) {
        Order order = new Order();
        UpdateWrapper updateWrapper = new UpdateWrapper();
        updateWrapper.eq("berth_number",orderClaimVo.getBerthNumber());
        order.setBoundUser(orderClaimVo.getId());
        int result = orderMapper.update(order,updateWrapper);
        return result;
    }

}




