package com.park.userserve.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.api.PayControllerApi;
import com.park.userserve.entity.Appeal;
import com.park.userserve.entity.Order;
import com.park.userserve.service.AppealService;
import com.park.userserve.mapper.AppealMapper;
import com.park.userserve.service.OrderService;
import com.park.userserve.service.PictureStorageService;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.vos.payservevo.TradeLogVo;
import com.park.vos.userservevo.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

import javax.annotation.Resource;
import java.util.*;

/**
* @author 86132
* @description 针对表【tp_appeal_list(售后申诉表)】的数据库操作Service实现
* @createDate 2023-07-13 14:39:02
*/
@Service
public class AppealServiceImpl extends ServiceImpl<AppealMapper, Appeal>
    implements AppealService{

    @Resource
    private AppealMapper appealMapper;

    @Resource
    private OrderService orderService;

    @Resource
    private PictureStorageService pictureStorageService;

    @Resource
    private PayControllerApi payControllerApi;

    @Override
    public List<AppealVo> queryAppealByOrderId(Long orderId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("order_id",orderId);
        List<AppealVo> appealVos = appealMapper.selectList(queryWrapper);
        return appealVos;
    }

    @Override
    public int addAppeal(AppealVo appealVo) {
        Appeal appeal = new Appeal();
        BeanUtils.copyProperties(appealVo,appeal);
        int result = appealMapper.insert(appeal);
        return result;
    }

    @Override
    public int updateAppeal(AppealVo appealVo) {
        Appeal appeal = new Appeal();
        BeanUtils.copyProperties(appealVo,appeal);
        int result = appealMapper.updateById(appeal);
        return result;
    }

    @Override
    public int deleteAppeal(Long id) {
        int result = appealMapper.deleteById(id);
        return result;
    }

    @Override
    public AppealQueryVo queryAlStateByOrId(Long orderId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("order_id",orderId);
        Appeal appeal = appealMapper.selectOne(queryWrapper);
        AppealQueryVo appealQueryVo = new AppealQueryVo();
        BeanUtils.copyProperties(appeal,appealQueryVo);
        return appealQueryVo;
    }

    @Override
    public List<AppealQueryVo> queryAplAllState() {
        List<AppealQueryVo> appealQueryVo = appealMapper.queryAplAllState();
        return appealQueryVo;
    }

    @Override
    public PageUtil<AppealPageVo> queryPageAppeal(Integer pageNum, Integer pageSize, AppealQueryVo appealQueryVo) {

        IPage<AppealPageVo> page = new Page<>(pageNum,pageSize);

        QueryWrapper queryWrapper = new QueryWrapper();

        /*逻辑删除为未删除*/
        appealQueryVo.setIsDeleted(0);
        if (appealQueryVo != null){
            if (appealQueryVo.getIsDeleted() != null){
                queryWrapper.eq("tal.is_deleted",appealQueryVo.getIsDeleted());
            }
            if (appealQueryVo.getAppealProcessState() != null){
                queryWrapper.eq("tal.appeal_process_state",appealQueryVo.getAppealProcessState());
            }
            /*创建时间范围-处理时间*/
            if (appealQueryVo.getStartTime() != null){
                queryWrapper.ge("tal.process_time",appealQueryVo.getStartTime());
            }
            if (appealQueryVo.getEndTime() != null){
                queryWrapper.le("tal.process_time",appealQueryVo.getEndTime());
            }
        }

        page = appealMapper.queryPageAppeal(page,queryWrapper);
        PageUtil pageUtil = new PageUtil();
        pageUtil.setPageNum(page.getPages());
        pageUtil.setPageSize(page.getSize());
        pageUtil.setTotal(page.getTotal());
        pageUtil.setPageData(page.getRecords());
        System.out.println("本页数据" + page.getRecords());
        return pageUtil;
    }

    @Override
    public AppealVos queryAppealById(Long id) {
        /*查申诉表的数据*/
        AppealVos appealVos = appealMapper.queryAppealById(id);
        /*通过调用订单表的接口来查询订单表数据，以及在订单表中多表联查出的数据*/
        OrderVos orderVos = orderService.queryOrderById(appealVos.getOrderId());
        System.out.println("从order表获取到的值" + orderVos);
        BeanUtils.copyProperties(orderVos,appealVos);
        /*获取申诉图片路径*/
        List<PictureUrlVos> pictureUrlVos = pictureStorageService.queryAppealPictureUrl(appealVos.getOrderId());
        ArrayList<String> appealPictureUrl = new ArrayList<>();
        for (int i = 0; i < pictureUrlVos.size(); i++){
            appealPictureUrl.add(pictureUrlVos.get(i).getPictureUrl());
        }
        appealVos.setAppealPictureUrl(appealPictureUrl);
        return appealVos;
    }

    @Override
    public int appealNoPass(AppealManageVo appealManageVo) {
        AppealVo appealVo = new AppealVo();
        /*根据id进行修改*/
        appealVo.setId(appealManageVo.getId());
        /*更新处理情况为不通过*/
        appealVo.setAppealProcessState(2);
        /*处理时间为现在*/
        appealVo.setProcessTime(new Date());
        /*处理人为当前登录的管理员*/
        appealVo.setProcessUserId(appealManageVo.getProcessUserId());
        /*更新处理人留言*/
        appealVo.setLeaveWords(appealManageVo.getLeaveWords());
        int result = updateAppeal(appealVo);
        return result;
    }

    @Override
    @Transactional
    public int appealPass(AppealManageVo appealManageVo) {

        AppealVos appealVos = queryAppealById(appealManageVo.getId());
        System.out.println("查询到的此售后订单详情" + appealVos);

        /*如果异常订单状态为待处理时才进行接下来操作*/
        if (appealVos.getAppealProcessState() == 0){
            AppealVo appealVo = new AppealVo();
            /*根据id进行修改*/
            appealVo.setId(appealManageVo.getId());
            /*状态改为已退款*/
            appealVo.setAppealProcessState(1);
            /*此处有两种情况*/
            /*退款金额为【实付金额】 - 【修改金额】*/
            /*退款金额打电话和用户协商后进行退款*/
            /*调用支付接口给用户退款*/
            /*判断用户是那种途径支付的，原路径退回钱*/
            /*订单状态为已支付或已完成才可以退款*/
            /*退款订单的数据*/
            TradeLogVo tradeLogVo = new TradeLogVo();
            if (appealVos.getOrderState() == 2 || appealVos.getOrderState() == 3){

                /*交易标题*/
                tradeLogVo.setTitle("停车费退费");
                /*订单表ID*/
                tradeLogVo.setOrderId(appealVos.getOrderId());
                /*支付方式*/
                tradeLogVo.setPayPlatform(appealVos.getWayPay());
                /*交易类型为退款*/
                tradeLogVo.setTradeType(2);
                /*交易金额*/
            /*使用compareTo方法来比较两个BigDecimal对象的大小
              如果返回值小于0，则表示前一个BigDecimal对象小于后一个
            * 如果返回值等于0，则表示两个BigDecimal对象相等
            * 如果返回值大于0，则表示前一个BigDecimal对象大于后一个*/
                /*subtract方法进行减法运算*/
                if (appealManageVo.getRefundAmount() != null && appealManageVo.getRefundAmount().compareTo(appealVos.getOrderRealityAmount()) <= 0){
                    tradeLogVo.setAmount(appealManageVo.getRefundAmount());
                    /*更新退款金额*/
                    appealVo.setRefundAmount(tradeLogVo.getAmount());
                }else if (appealManageVo.getRefundAmount() == null){
                    tradeLogVo.setAmount(appealVos.getOrderRealityAmount().subtract(appealVos.getModificationAmount()));
                    /*更新退款金额*/
                    appealVo.setRefundAmount(tradeLogVo.getAmount());
                }else {
                    System.out.println("抛出一个错误" + "退款金额大于实付金额");
                }

                if (appealVos.getWayPay() == 1){
                    Result<Object> tradeLog = payControllerApi.placeOrderForPCWeb(tradeLogVo);
                    if (tradeLog.getCode() == "200"){
                        System.out.println("退款成功");
                    }else {
                        System.out.println("抛出一个全局异常，支付宝退款失败");
                    }
                }else if (appealVos.getWayPay() == 2){
                    Result<Object> tradeLog = payControllerApi.addOrder(tradeLogVo);
                    if (tradeLog.getCode() == "200"){
                        System.out.println("退款成功");
                    }else {
                        System.out.println("抛出一个全局异常，银联退款失败");
                    }
                }else if (appealVos.getWayPay() == 3 || appealVos.getWayPay() == 4){
                    Result<Object> tradeLog = payControllerApi.addTradeLog(tradeLogVo);
                    if (tradeLog.getCode() == "200"){
                        System.out.println("退款成功");
                    }else {
                        System.out.println("抛出一个全局异常，其他支付方式退款失败");
                    }
                }
            }else {
                System.out.println("抛个全局异常，退款失败，用户没交钱退个毛线");
            }
            /*更新处理时间*/
            appealVo.setProcessTime(new Date());
            /*更新处理人id*/
            appealVo.setProcessUserId(appealManageVo.getProcessUserId());
            /*更新处理人留言*/
            appealVo.setLeaveWords(appealManageVo.getLeaveWords());
            int result1 = updateAppeal(appealVo);

            /*修改订单表状态和实付金额*/
            OrderVo orderVo = new OrderVo();
            orderVo.setId(appealManageVo.getOrderId());
            orderVo.setOrderState(4);
            /*【实付金额】 = 【实付金额】 - 【退款金额】  subtract减*/
            orderVo.setOrderRealityAmount(appealVos.getOrderRealityAmount().subtract(appealVo.getRefundAmount()));
            int result2 = orderService.updateOrder(orderVo);

            if (result1 > 0 && result2 >0){
                return 1;
            }
        }

        return 0;
    }

}




