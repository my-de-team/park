package com.park.userserve.service.mqconsumer;

import java.util.Date;

import cn.hutool.core.date.DateUtil;
import com.park.api.SystemControllerApi;
import com.park.userserve.CustomException.MQBasicAckException;
import com.park.utils.Result;
import com.park.vos.systemvo.LoginLogVo;
import com.park.vos.systemvo.OptLogVo;
import com.park.vos.userservevo.LogVo;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * 用户模块消费者
 */
@Component
public class UserMQ {

    @Resource
    private SystemControllerApi systemApi;

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 登录消费者
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "user-login-queue"),
            exchange = @Exchange(value = "user-login-exchange", type = ExchangeTypes.DIRECT),
            key = "userLoginKey"
    ))
    public void userLoginLog(Message message, Channel channel, @Payload LoginLogVo loginLogVo) {

        System.out.println("登录: loginLogVo = " + loginLogVo);
        Result result = systemApi.addLoginLog(loginLogVo);
        System.out.println("result = " + result);

        if ("200".equals(result.getCode())) {
//            String today = DateUtil.today();
//            redisTemplate.opsForValue().increment(("userCount:" + today), 1);
            //手动确认消息
            try {
                channel.basicAck(message.getMessageProperties().getDeliveryTag(), true);

            } catch (IOException e) {
                //MQException
                throw new MQBasicAckException("UserMQ userLoginLog channel.basicAck出现异常");
            }
        }


    }

    /**
     * 用户注册消费者
     *
     * @param
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "user-register-queue"),
            exchange = @Exchange(value = "user-register-exchange", type = ExchangeTypes.DIRECT),
            key = "userRegisterKey"
    ))
    public void userRegister(Message message, Channel channel, @Payload LogVo logVo) {

        System.out.println("logvo" + logVo);
        //往操作日志中添加
        OptLogVo optLogVo = new OptLogVo();
        optLogVo.setTitle("系统新用户注册");
        optLogVo.setBusinessType(1);
        optLogVo.setMethodName("新用户注册");
        optLogVo.setMethodUrl("com.park.userserve.service.impl.UserServiceImpl.addUser");
        optLogVo.setReqType("POST");
        optLogVo.setOptName("system");
        optLogVo.setOptUrl("/user/register");
        optLogVo.setOptIp(logVo.getLoginLogVo().getIpAddress());
        optLogVo.setOptAddress(logVo.getLoginLogVo().getIpSource());
        optLogVo.setReqParam(logVo.getReqParam());
        optLogVo.setRespData(logVo.getRespData());
        optLogVo.setOptTime(new Date());
        Result result = systemApi.addOptLog(optLogVo);

        //redis自增
        if (result.getCode().equals("200")) {
            String today = DateUtil.today();
            redisTemplate.opsForValue().increment("userCount:" + today, 1);
            //发送短信

        } else {
            System.out.println("logVo = " + logVo);
        }
        //手动确认消息
        try {
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), true);

        } catch (IOException e) {
            //MQException
            throw new MQBasicAckException("UserMQ userRegister channel.basicAck出现异常");
        }

    }
}
