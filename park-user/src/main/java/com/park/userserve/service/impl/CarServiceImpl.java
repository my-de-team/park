package com.park.userserve.service.impl;

import java.math.BigDecimal;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Date;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.userserve.CustomException.*;
import com.park.userserve.entity.Car;
import com.park.userserve.service.CarService;
import com.park.userserve.mapper.CarMapper;
import com.park.userserve.service.OrderService;
import com.park.userserve.service.UserService;
import com.park.utils.PageUtil;
import com.park.vos.userservevo.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author MIN
 * @description 针对表【tp_car(用户绑定车辆表)】的数据库操作Service实现
 */
@Service
public class CarServiceImpl extends ServiceImpl<CarMapper, Car>
        implements CarService {

    @Resource
    private CarMapper carMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private OrderService orderService;


    /**
     * 用户绑定车辆 (最多五个)
     *
     * @param userBindCarVo
     * @return true/false
     */
    @Override
    @Transactional
    public boolean bindCar(UserBindCarVo userBindCarVo) {
        //根据id/手机号查找当前用户绑定了几辆车 >=5 添加失败
        int number = countUserBindCarById(userBindCarVo.getUserId());
        if (number >= 5) {
            throw new CarNumberException();
        }
        boolean bind = isBind(userBindCarVo.getNumber());
        if (bind) {
            throw new IsBindException();
        }

        Car car = new Car();
        car.setNumber(userBindCarVo.getNumber());
        car.setType(userBindCarVo.getType());
        car.setUserId(userBindCarVo.getUserId());
        car.setBindTime(new Date());
        car.setState(0);
        car.setVersion(0);
        car.setIsDeleted(0);
        int insert = carMapper.insert(car);
        if (insert == 1) {
            userService.updateBindCarNumber(car.getUserId(), number + 1);
            return true;
        }

        throw new BindCarException();
    }

    @Override
    public boolean isBind(String carNumber) {
        List<Long> carList = carMapper.isBind(carNumber);
        if (carList.size() > 0) {
            return true;
        }
        return false;
    }

    /**
     * 车辆管理页面分页条件查询
     *
     * @param pageNum    当前页
     * @param pageSize   每页条数
     * @param carQueryVo 查询条件
     * @return
     */
    @Override
    public PageUtil<CarInfoVo> getAllCar(Integer pageNum, Integer pageSize, CarQueryVo carQueryVo) {

        Page<CarInfoVo> carPage = new Page<>(pageNum, pageSize);
        QueryWrapper<CarInfoVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("bind_time");
        if (carQueryVo.getState() == null) {
            carQueryVo.setState(-1);
        }
        if (carQueryVo.getState() == 0 || carQueryVo.getState() == 1) {
            queryWrapper.eq("tc.state", carQueryVo.getState());
        } else {
            queryWrapper.eq("tc.state", 0);
        }

        if (StrUtil.isNotBlank(carQueryVo.getNumber())) {
            queryWrapper.eq("number", carQueryVo.getNumber());
        }

        if (StrUtil.isNotBlank(carQueryVo.getUserName())) {
            queryWrapper.eq("username", carQueryVo.getUserName());
        }

        if (carQueryVo.getType() != null) {
            queryWrapper.eq("type", carQueryVo.getType());
        }
        carPage = carMapper.getAllCar(carPage, queryWrapper);
        PageUtil<CarInfoVo> carInfoPage = new PageUtil<>();
        carInfoPage.setPageNum(carPage.getCurrent());
        carInfoPage.setPageSize(carPage.getSize());
        carInfoPage.setTotal(carPage.getTotal());
        carInfoPage.setPageData(carPage.getRecords());

        return carInfoPage;
    }

    /**
     * 用户端 根据用户id查询绑定车辆
     *
     * @param userId 用户id
     * @return
     */
    @Override
    public List<CarInfoVo> getUserCarByUserId(Long userId) {
        List<CarInfoVo> carInfoList = carMapper.getUserCarByUserId(userId);
        if (carInfoList.size() > 0) {
            return carInfoList;
        }
        throw new CarNullException();
    }

    /**
     * 用户端 根据手机号查询绑定车辆
     *
     * @param mobile 用户手机号
     * @return
     */
    @Override
    public List<CarInfoVo> getUserCarByMobile(String mobile) {
        Long userId = userService.getUserId(mobile);
        if (userId == null) {
            return null;
        }
        return getUserCarByUserId(userId);
    }

    /**
     * 用户端 解绑/修改车辆
     *
     * @param carUpdateVo 车辆id
     * @return
     */
    @Override
    @Transactional
    public boolean userUpdateCarById(CarUpdateVo carUpdateVo) {
        int number = countUserBindCarById(carUpdateVo.getUserId());
        int disableCar = carMapper.updateCarById(carUpdateVo);
        if (carUpdateVo.getState() != null && carUpdateVo.getState() == 1) {

            userService.updateBindCarNumber(carUpdateVo.getUserId(), number - 1);
        }
        if (disableCar == 1) {
            return true;
        }
        throw new CarDisableException();
    }

    /**
     * 平台 根据id逻辑删除车辆
     *
     * @param id
     * @return
     */
    @Override
    @Transactional
    public boolean deleteCarById(Long id) {
        int deleteCar = carMapper.deleteById(id);
        if (deleteCar == 1) {
            return true;
        }
        throw new CarDeleteException();
    }


    /**
     * 根据用户id统计用户绑定车辆数量
     *
     * @param userId 用户id
     * @return
     */
    @Override
    public int countUserBindCarById(Long userId) {
        int car = carMapper.countUserBindCarById(userId);
        return car;
    }

    /**
     * 根据用户id查询该用户绑定的车辆详细信息
     *
     * @return
     */
    @Override
    public PageUtil<CarInfoVo> getUserCarById(Integer pageNum, Integer pageSize, CarQueryVo carQueryVo) {

        Page<Car> carPage = new Page<>(pageNum, pageSize);
        QueryWrapper<Car> queryWrapper = new QueryWrapper<>();
        if (StrUtil.isNotBlank(carQueryVo.getNumber())) {
            queryWrapper.like("number", carQueryVo.getNumber());
        }
        if (carQueryVo.getState() == null) {
            carQueryVo.setState(-1);
        }
        if (carQueryVo.getState() == 0 || carQueryVo.getState() == 1) {
            queryWrapper.eq("state", carQueryVo.getState());
        } else {
            queryWrapper.eq("state", 0);
        }
        queryWrapper.orderByDesc("bind_time");
        carPage = carMapper.getUserCarById(carPage, queryWrapper, carQueryVo);
        List<Car> carList = carPage.getRecords();
        ArrayList<CarInfoVo> carInfoList = new ArrayList<>();
        for (Car car : carList) {
            CarInfoVo carInfoVo = new CarInfoVo();
            BeanUtils.copyProperties(car, carInfoVo);
            carInfoList.add(carInfoVo);
        }
        PageUtil<CarInfoVo> carInfoPageUtil = new PageUtil<>();
        carInfoPageUtil.setPageNum(carPage.getCurrent());
        carInfoPageUtil.setPageSize(carPage.getSize());
        carInfoPageUtil.setTotal(carPage.getTotal());
        carInfoPageUtil.setPageData(carInfoList);

        return carInfoPageUtil;
    }

    @Override
    public UserOrderCountVo countByUserId(Long id) {
        List<OrderPageVo> orders = orderService.queryOrderByUserId(id);
        UserOrderCountVo userOrderCount = new UserOrderCountVo();
        BigDecimal moneySum = new BigDecimal("0");
        BigDecimal noPayMoneySum = new BigDecimal("0");
        int noPaySum = 0;
        for (OrderPageVo order : orders) {
            if (order.getOrderState() == 1) {
                noPaySum++;
                BigDecimal orderAmount = order.getOrderAmount();
                noPayMoneySum = noPayMoneySum.add(orderAmount);
            }
            if (order.getOrderState() == 3) {
                BigDecimal orderRealityAmount = order.getOrderRealityAmount();
                moneySum = moneySum.add(orderRealityAmount);
            }
        }
        userOrderCount.setSum(orders.size());
        userOrderCount.setMoneySum(moneySum);
        userOrderCount.setNoPayMoney(noPayMoneySum);
        userOrderCount.setNoPaySum(0);

        return userOrderCount;
    }
}




