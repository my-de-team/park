package com.park.userserve.globalexception;

import com.park.userserve.CustomException.*;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.text.SimpleDateFormat;
import java.util.Date;

@RestControllerAdvice
public class GlobalExceptionController {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @ExceptionHandler
    public Result execute(Exception e) {
        System.out.println("出现异常！！！已进入全局异常处理");
        String formatDate = dateFormat.format(new Date());

        //自定义 空异常
        if (e instanceof KongException) {
            System.err.println(formatDate + "错误信息是: " + e.getMessage() + ResultCode.KONG.getMessage());
            return Result.err(ResultCode.KONG);
        }

        //未绑定手机号错误异常
        if (e instanceof MobileException) {
            if ("手机号重复".equals(e.getMessage())) {
                System.err.println(formatDate + "错误信息是: " + e.getMessage());
                return Result.err(ResultCode.PHONE_FAIL);
            }
            System.err.println(formatDate + "错误信息是: " + ResultCode.MOBILE_FAIL.getMessage());

            return Result.err(ResultCode.MOBILE_FAIL);
        }

        //用户异常
        if (e instanceof UserException) {
            if ("ADD".equals(e.getMessage())) {
                System.err.println(formatDate + "错误信息是: " + ResultCode.INSERT_USER_FAIL.getMessage());
                return Result.err(ResultCode.INSERT_USER_FAIL);
            }
            if ("UPD".equals(e.getMessage())) {
                System.err.println(formatDate + "错误信息是: " + ResultCode.UPDATE_USER_FAIL.getMessage());
                return Result.err(ResultCode.INSERT_USER_FAIL);
            }
            if ("DEL".equals(e.getMessage())) {
                System.err.println(formatDate + "错误信息是: " + ResultCode.DELETE_USER_FAIL.getMessage());
                return Result.err(ResultCode.INSERT_USER_FAIL);
            }
            System.err.println(formatDate + "错误信息是: " + ResultCode.OTHER_USER_FAIL.getMessage());
            return Result.err(ResultCode.OTHER_USER_FAIL);
        }

        if (e instanceof IsBindException) {
            System.err.println(formatDate + "错误信息是: " + "车辆已绑定");
            return Result.send("6001", "车辆已绑定", null);
        }

        if (e instanceof CarNumberException) {
            System.err.println(formatDate + "错误信息是: " + "车辆绑定数量>5");
            return Result.send("6002", "车辆绑定数量>5", null);
        }

        //出现其他错误
        e.printStackTrace();
        return Result.err(ResultCode.EXCEPTION);
    }
}
