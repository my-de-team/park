package com.park.dataserve.config;

import com.park.utils.SnowflakeIdWorker;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;


@Configuration
public class BeanConfig {

    @Bean
    public RestTemplate newRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public MessageConverter newMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public SnowflakeIdWorker initSnowflakeIdWorker(){
        return new SnowflakeIdWorker(1,1,1);
    }
}
