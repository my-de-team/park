package com.park.dataserve.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.dataserve.entity.Magnetic;
import com.park.dataserve.entity.RoadSegment;
import com.park.dataserve.service.MagneticService;
import com.park.dataserve.mapper.MagneticMapper;
import com.park.dataserve.service.RoadSegmentService;
import com.park.expection.MagneticExpection;
import com.park.utils.PageUtil;
import com.park.vos.datavo.MagneticVo;
import com.park.vos.datavo.QueryMagneticVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
* @author blank
* @description 针对表【tp_magnetic】的数据库操作Service实现
* @createDate 2023-07-13 11:38:15
*/
@Service
public class MagneticServiceImpl extends ServiceImpl<MagneticMapper, Magnetic> implements MagneticService{
    @Resource
    private MagneticMapper magneticMapper;

    @Resource
    private RoadSegmentService roadSegmentService;
    //新增地磁管理
    @Override
    public int magneticAdd(MagneticVo magneticVo) throws MagneticExpection {
        if (StringUtils.isEmpty(magneticVo.getMagneticNumber())) {
            throw new MagneticExpection();
        }
        if (StringUtils.isEmpty(magneticVo.getMagneticName())) {
            throw new MagneticExpection();
        }
        if (magneticVo.getRoadId() == null) {
            throw new MagneticExpection();
        }
        Magnetic magnetic = new Magnetic();
        BeanUtils.copyProperties(magneticVo, magnetic);
        int flag = magneticMapper.insert(magnetic);
        //int flag = magneticMapper.magneticAdd(queryMagneticVo);
        return flag;
    }

    //根据id删除地磁管理
    @Override
    public int deleteMagneticById(Long id) {
        int flag = magneticMapper.deleteById(id);
        return flag;
    }

    //修改地磁管理
    @Override
    public int magneticPut(MagneticVo magneticVo) throws MagneticExpection {
        if (StringUtils.isEmpty(magneticVo.getMagneticNumber())) {
            throw new MagneticExpection();
        }
        if (StringUtils.isEmpty(magneticVo.getMagneticName())) {
            throw new MagneticExpection();
        }
        if (magneticVo.getRoadId() == null) {
            throw new MagneticExpection();
        }
        int flag = magneticMapper.magneticPut(magneticVo);
        return flag;
    }

    //分页查询地磁管理、条件查询
    @Override
    public PageUtil queryMagneticAll(Integer pageNum, Integer pageSize, QueryMagneticVo queryMagneticVo) {
        IPage<Magnetic> page = new Page<>(pageNum, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        if (!StringUtils.isEmpty(queryMagneticVo.getMagneticNumber())) {
            queryWrapper.eq("magnetic_number", queryMagneticVo.getMagneticNumber());
        }
        if (!StringUtils.isEmpty(queryMagneticVo.getMagneticName())) {
            queryWrapper.like("magnetic_name", queryMagneticVo.getMagneticName());
        }
        if (queryMagneticVo.getRoadId() != null) {
            queryWrapper.eq("road_id",queryMagneticVo.getRoadId());
        }
        page = magneticMapper.selectPage(page, queryWrapper);
        PageUtil pageUtil = new PageUtil<>();
        pageUtil.setTotal(page.getTotal());
        pageUtil.setPageData(page.getRecords());
        pageUtil.setPageNum(page.getCurrent());
        pageUtil.setPageSize(page.getSize());
        List<MagneticVo> magneticVoList = new ArrayList<>();
        List<Magnetic> magneticList = page.getRecords();
        for (Magnetic magnetic : magneticList) {
            MagneticVo magneticVo = new MagneticVo();
            BeanUtils.copyProperties(magnetic, magneticVo);
            Long roadId = magnetic.getRoadId();
            //根据roadId查询路段名称，调用路段接口
            RoadSegment roadSegment = roadSegmentService.queryRoadSegmentById(roadId);
            if (roadSegment != null) {
                String segmentName = roadSegmentService.queryRoadSegmentById(roadId).getSegmentName();
                magneticVo.setSegmentName(segmentName);
            }

            magneticVoList.add(magneticVo);
        }
        pageUtil.setPageData(magneticVoList);
        return pageUtil;
    }
}




