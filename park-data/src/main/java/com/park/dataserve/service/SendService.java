package com.park.dataserve.service;

import org.springframework.stereotype.Service;

@Service
public interface SendService {
    public boolean sendCode(String mobile, String code) throws Exception;
}
