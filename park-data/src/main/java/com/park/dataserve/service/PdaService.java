package com.park.dataserve.service;

import com.park.dataserve.entity.Pda;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.utils.PageUtil;
import com.park.vos.datavo.PdaQueryVo;
import com.park.vos.datavo.PdaVo;

/**
* @author blank
* @description 针对表【tp_pda(手持PDA表)】的数据库操作Service
* @createDate 2023-07-18 11:14:46
*/
public interface PdaService extends IService<Pda> {

    PageUtil queryPdaAll(Integer pageNum, Integer pageSize, PdaQueryVo pdaQueryVo);

    int pdaAdd(PdaVo pdaVo);

    int deletePdaById(Long id);

    int pdaPut(PdaVo pdaVo);
}
