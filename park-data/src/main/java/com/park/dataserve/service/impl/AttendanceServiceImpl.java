package com.park.dataserve.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.annotation.IdempotentAnnotation;
import com.park.dataserve.entity.Attendance;
import com.park.dataserve.mapper.AttendanceMapper;
import com.park.dataserve.service.AttendanceService;
import com.park.vos.datavo.AddAttendanceQueryVo;
import com.park.vos.datavo.AddAttendanceVo;
import com.park.vos.datavo.AttendanceVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 考勤表 服务实现类
 * </p>
 *
 * @author lxc
 * @since 2023-07-13
 */
@Service
public class AttendanceServiceImpl extends ServiceImpl<AttendanceMapper, Attendance> implements AttendanceService {
    @Autowired
    private AttendanceMapper attendanceMapper;

    @Override
    @IdempotentAnnotation
    @Transactional
    public int AddAttendance(AddAttendanceVo addAttendanceVo) {
        Attendance attendance = new Attendance();
        BeanUtils.copyProperties(addAttendanceVo, attendance);
        int flag = attendanceMapper.insert(attendance);
        return flag;
    }

    @Override
    public List<AttendanceVo> queryAttendanceRecords(AddAttendanceQueryVo addAttendanceQueryVo) {
        QueryWrapper<Attendance> queryWrapper = new QueryWrapper<>();
        if (Objects.nonNull(addAttendanceQueryVo)) {
            // 判断条件是否有巡检员id
            if (Objects.nonNull(addAttendanceQueryVo.getInspectorId())) {
                queryWrapper.eq("inspector_id", addAttendanceQueryVo.getInspectorId());
            }
            // 判断查询条件是否有开始时间
            if (Objects.nonNull(addAttendanceQueryVo.getClockTimeStart())) {
                queryWrapper.ge("clock_time", addAttendanceQueryVo.getClockTimeStart());
            }
            // 判断查询条件是否有结束时间
            if (Objects.nonNull(addAttendanceQueryVo.getClockTimeEnd())) {
                queryWrapper.le("clock_time", addAttendanceQueryVo.getClockTimeEnd());

            }

        }


        List<Attendance> attendanceList = attendanceMapper.selectList(queryWrapper);
        List<AttendanceVo> attendanceVoList = new ArrayList<>();

        for (Attendance attendance : attendanceList) {
            AttendanceVo attendanceVo = new AttendanceVo();
            BeanUtils.copyProperties(attendance, attendanceVo);
            attendanceVoList.add(attendanceVo);
        }
        return attendanceVoList;
    }
}
