package com.park.dataserve.service;

import com.park.dataserve.entity.InspectorFault;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.utils.PageUtil;
import com.park.vos.datavo.InspectorFaultAddVo;
import com.park.vos.datavo.InspectorFaultVo;
import com.park.vos.datavo.InspectorFaultQueryVo;

/**
 * <p>
 * 巡检员反馈故障表 服务类
 * </p>
 *
 * @author lxc
 * @since 2023-07-13
 */
public interface InspectorFaultService extends IService<InspectorFault> {

    /**
     * 添加巡检员故障反馈记录
     * @return
     */
    int addInspectorFault(InspectorFaultAddVo inspectorFaultAddVo);

    /**
     * 删除巡检员故障反馈记录
     */
    int deleteInspectorFaultById(Long id);

    /**
     * 分页查询
     */
    PageUtil<InspectorFaultVo> queryInspectorFault(Long pageNum, Long pageSize, InspectorFaultQueryVo inspectorFaultQueryVo);

    /**
     * 根据id查询
     */
    InspectorFaultVo queryInspectorFaultById(Long id);

    /**
     * 更新反馈单
     */
    int updateInspectorFaultById(InspectorFaultVo inspectorFaultVo);
}
