package com.park.dataserve.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.dataserve.entity.Pda;
import com.park.dataserve.entity.PdaLog;
import com.park.dataserve.service.PdaLogService;
import com.park.dataserve.mapper.PdaLogMapper;
import com.park.utils.PageUtil;
import com.park.vos.datavo.PdaLogAddVo;
import com.park.vos.datavo.PdaLogQueryVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
* @author blank
* @description 针对表【tp_pda_log(PDA登录日志表)】的数据库操作Service实现
* @createDate 2023-07-20 16:21:57
*/
@Service
public class PdaLogServiceImpl extends ServiceImpl<PdaLogMapper, PdaLog> implements PdaLogService{
    @Resource
    private PdaLogMapper pdaLogMapper;

    public int addPdaLog(PdaLogAddVo pdaLogAddVo) {
        PdaLog pdaLog = new PdaLog();
        BeanUtils.copyProperties(pdaLogAddVo, pdaLog);
        int flag = pdaLogMapper.insert(pdaLog);
        return flag;
    }

    @Override
    public PageUtil queryPdaLogAll(Integer pageNum, Integer pageSize, PdaLogQueryVo pdaLogQueryVo) {
        IPage<PdaLog> page = new Page<>(pageNum, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        page = pdaLogMapper.selectPage(page, queryWrapper);
        PageUtil pageUtil = new PageUtil<>();
        pageUtil.setTotal(page.getTotal());
        pageUtil.setPageData(page.getRecords());
        pageUtil.setPageNum(page.getCurrent());
        pageUtil.setPageSize(page.getSize());
        return pageUtil;
    }
}




