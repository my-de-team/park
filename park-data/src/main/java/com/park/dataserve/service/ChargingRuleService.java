package com.park.dataserve.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.park.dataserve.entity.ChargingRule;
import com.park.utils.PageUtil;
import com.park.vos.datavo.ChargingRuleQueryVo;
import com.park.vos.datavo.ChargingRuleVo;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 收费规则表 服务类
 * </p>
 *
 * @author lauxiaoyang
 * @since 2023-07-18
 */
public interface ChargingRuleService extends IService<ChargingRule> {

    /*
    分页查询
     */
    PageUtil<ChargingRuleVo> queryPageChargingRule(Integer pageNum, Integer pageSize, ChargingRuleVo chargingRuleVo);

    /*
    新增
     */
    boolean addChargingRule(ChargingRuleVo chargingRuleVo);

    /*
    修改
     */
    void updateChargingRule(ChargingRule chargingRule);

    /*
    删除
     */
    boolean deleteChargingRule(Long id);

    /*
     根据车辆驶入和驶出时间计算收费
     */
    BigDecimal calculateCharge(ChargingRuleQueryVo chargingRuleQueryVo);

    /*
    根据泊位id查询收费信息 泊位ID：parking_position_id
     */
    List<ChargingRuleVo> getChargingByParkingPositionId(Long parkingPositionId);

    BigDecimal sum(ChargingRuleQueryVo chargingRuleQueryVo);

}
