package com.park.dataserve.service;

import com.park.dataserve.entity.RoadSegment;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.utils.PageUtil;
import com.park.vos.datavo.RoadSegmentAddVo;
import com.park.vos.datavo.RoadSegmentQueryVo;
import com.park.vos.datavo.RoadSegmentVo;

import java.util.List;

/**
 * <p>
 * 路段信息 服务类
 * </p>
 *
 * @author lauxiaoyang
 * @since 2023-07-13
 */
public interface RoadSegmentService extends IService<RoadSegment> {

    /*
    分页查询
     */
    PageUtil queryPageRoadSegment(Integer pageNum, Integer pageSize, RoadSegmentQueryVo roadSegmentQueryVo);


    /*
        根据路段id查询路段名称
         */
    RoadSegment queryRoadSegmentById(Long id);

    /*
    新增
     */
    boolean addRoadSegment(RoadSegmentAddVo roadSegmentAddVo);

    /*
    修改
     */
    int updateRoadSegment(RoadSegment roadSegment);

    /*
    删除
     */
    boolean deleteRoadSegmentById(Long id);

    /*
    根据经纬度查询路段信息
     */
//    List<RoadSegmentVo> queryRoadSegmentByCoordinates(String longitude, String latitude);
}
