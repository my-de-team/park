package com.park.dataserve.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.dataserve.entity.Magnetic;
import com.park.dataserve.entity.MagneticLog;
import com.park.dataserve.service.MagneticLogService;
import com.park.dataserve.mapper.MagneticLogMapper;
import com.park.utils.PageUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
* @author blank
* @description 针对表【tp_magnetic_log(地磁日志表)】的数据库操作Service实现
* @createDate 2023-07-15 15:47:41
*/
@Service
public class MagneticLogServiceImpl extends ServiceImpl<MagneticLogMapper, MagneticLog> implements MagneticLogService{

    @Resource
    private MagneticLogMapper magneticLogMapper;

    //分页查询
    @Override
    public PageUtil queryMagneticLogAll(Integer pageNum, Integer pageSize, String magneticNumber) {
        IPage<MagneticLog> page = new Page<>(pageNum, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        if (magneticNumber != null) {
            queryWrapper.eq("magnetic_number", magneticNumber);
        }
        page = magneticLogMapper.selectPage(page,queryWrapper);
        PageUtil pageUtil = new PageUtil<>();
        pageUtil.setTotal(page.getTotal());
        pageUtil.setPageNum(page.getCurrent());
        pageUtil.setPageSize(page.getSize());
        pageUtil.setPageData(page.getRecords());
        return pageUtil;
    }
}




