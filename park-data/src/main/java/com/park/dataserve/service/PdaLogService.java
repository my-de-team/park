package com.park.dataserve.service;

import com.park.dataserve.entity.PdaLog;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.utils.PageUtil;
import com.park.vos.datavo.PdaLogAddVo;
import com.park.vos.datavo.PdaLogQueryVo;

/**
* @author blank
* @description 针对表【tp_pda_log(PDA登录日志表)】的数据库操作Service
* @createDate 2023-07-20 16:21:57
*/
public interface PdaLogService extends IService<PdaLog> {
    int addPdaLog(PdaLogAddVo pdaLogAddVo);

    PageUtil queryPdaLogAll(Integer pageNum, Integer pageSize, PdaLogQueryVo pdaLogQueryVo);
}
