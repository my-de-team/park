package com.park.dataserve.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.dataserve.entity.InspectorFaultImg;
import com.park.dataserve.mapper.InspectorFaultImgMapper;
import com.park.dataserve.service.InspectorFaultImgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 巡检员故障反馈表图片 服务实现类
 * </p>
 *
 * @author lxc
 * @since 2023-07-13
 */
@Service
public class InspectorFaultImgServiceImpl extends ServiceImpl<InspectorFaultImgMapper, InspectorFaultImg> implements InspectorFaultImgService {
    @Autowired
    private InspectorFaultImgMapper inspectorFaultImgMapper;

    /**
     * 保存巡检员反馈单图片
     */
    @Override
    public int addInspectorFaultImg(String imgUrl, Long inspectorFaultId) {
        InspectorFaultImg inspectorFaultImg = new InspectorFaultImg();
        inspectorFaultImg.setImgUrl(imgUrl);
        inspectorFaultImg.setInspectorFaultId(inspectorFaultId);
        int flag = inspectorFaultImgMapper.insert(inspectorFaultImg);
        return flag;
    }

    /**
     * 删除巡检员反馈记录的图片
     * @param inspectorFaultId
     * @return
     */
    @Override
    public int deleteFaultImgByInspectorFaultId(Long inspectorFaultId) {
        QueryWrapper<InspectorFaultImg> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("inspector_fault_id", inspectorFaultId);
        int flag = inspectorFaultImgMapper.delete(queryWrapper);
        return flag;
    }

    /**
     * 根据巡检员反馈单查询相应图片
     * @return
     */
    @Override
    public List<String> queryFaultImgByInspectorFaultId(Long inspectorFaultId) {
        List<String> imgUrlList = inspectorFaultImgMapper.queryImgByInspectorFaultId(inspectorFaultId);
        return imgUrlList;
    }
}
