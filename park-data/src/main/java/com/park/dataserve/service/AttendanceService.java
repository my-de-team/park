package com.park.dataserve.service;

import com.park.dataserve.entity.Attendance;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.vos.datavo.AddAttendanceQueryVo;
import com.park.vos.datavo.AddAttendanceVo;
import com.park.vos.datavo.AttendanceVo;

import java.util.List;

/**
 * <p>
 * 考勤表 服务类
 * </p>
 *
 * @author lxc
 * @since 2023-07-13
 */
public interface AttendanceService extends IService<Attendance> {

    /**
     * 上下班打卡
     */
    int AddAttendance(AddAttendanceVo addAttendanceVo) ;

    /**
     * 条件查询所有打卡记录
     */
    List<AttendanceVo> queryAttendanceRecords(AddAttendanceQueryVo addAttendanceQueryVo);

}
