package com.park.dataserve.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.park.annotation.IdempotentAnnotation;
import com.park.dataserve.entity.InspectorFault;
import com.park.dataserve.mapper.InspectorFaultMapper;
import com.park.dataserve.service.InspectorFaultImgService;
import com.park.dataserve.service.InspectorFaultService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.utils.AssertUtils;
import com.park.utils.CustomUtil;
import com.park.utils.PageUtil;
import com.park.utils.SnowflakeIdWorker;
import com.park.vos.datavo.InspectorFaultAddVo;
import com.park.vos.datavo.InspectorFaultVo;
import com.park.vos.datavo.InspectorFaultQueryVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 巡检员反馈故障表 服务实现类
 * </p>
 *
 * @author lxc
 * @since 2023-07-13
 */
@Service
public class InspectorFaultServiceImpl extends ServiceImpl<InspectorFaultMapper, InspectorFault> implements InspectorFaultService {
    @Autowired
    private InspectorFaultMapper inspectorFaultMapper;
    @Autowired
    InspectorFaultImgService inspectorFaultImgService;

    /**
     * 添加巡检员故障反馈记录
     *
     * @return
     */
    @Override
    @IdempotentAnnotation
    @Transactional
    public int addInspectorFault(InspectorFaultAddVo inspectorFaultAddVo) {
        // 添加故障反馈
        InspectorFault inspectorFault = new InspectorFault();
        BeanUtils.copyProperties(inspectorFaultAddVo, inspectorFault);
        inspectorFault.setFeedbackNo(CustomUtil.getCodeNumber("Fault"));
        inspectorFault.setFeedbackTime(new Date());
        inspectorFault.setState(0);
        if (AssertUtils.isNotEmpty(inspectorFaultAddVo.getImgUrlList())) {
            inspectorFault.setImgCount(inspectorFaultAddVo.getImgUrlList().size());
        }
        int flag = inspectorFaultMapper.insert(inspectorFault);
        // 保存图片
        if (inspectorFaultAddVo.getImgUrlList() != null) {
            for (String imgUrl : inspectorFaultAddVo.getImgUrlList()) {
                inspectorFaultImgService.addInspectorFaultImg(imgUrl, inspectorFault.getId());
            }

        }
        return flag;
    }

    /**
     * 删除巡检员故障反馈记录
     *
     * @param id 巡检员id
     * @return
     */
    @Override
    public int deleteInspectorFaultById(Long id) {
        // 查询图片数量
        Integer imgCount = inspectorFaultMapper.queryImgCountById(id);
        // 删除反馈记录
        int flag = inspectorFaultMapper.deleteById(id);
        // 删除反馈记录的图片
        if (imgCount != null) {
            inspectorFaultImgService.deleteFaultImgByInspectorFaultId(id);
        }
        return flag;
    }

    /**
     * 分页查询
     */
    @Override
    public PageUtil<InspectorFaultVo> queryInspectorFault(Long pageNum, Long pageSize, InspectorFaultQueryVo inspectorFaultQueryVo) {
        IPage<InspectorFaultVo> page = new Page<>(pageNum, pageSize);

        page = inspectorFaultMapper.queryInspectorFaultList(page, inspectorFaultQueryVo);
        PageUtil<InspectorFaultVo> pageUtil = new PageUtil<>();
        pageUtil.setPageData(page.getRecords());
        pageUtil.setTotal(page.getTotal());
        return pageUtil;
    }

    /**
     * 根据id查询
     */
    @Override
    public InspectorFaultVo queryInspectorFaultById(Long id) {
        InspectorFault inspectorFault = inspectorFaultMapper.selectById(id);
        InspectorFaultVo inspectorFaultVo = new InspectorFaultVo();
        BeanUtils.copyProperties(inspectorFault, inspectorFaultVo);
        //查询姓名和手机号

        //查询图片
        if (inspectorFault.getImgCount() != null || !inspectorFault.getImgCount().equals(0)) {
            List<String> imgUrlList = inspectorFaultImgService.queryFaultImgByInspectorFaultId(id);
            inspectorFaultVo.setImgUrlList(imgUrlList);
        }
        return inspectorFaultVo;
    }

    @Override
    public int updateInspectorFaultById(InspectorFaultVo inspectorFaultVo) {
        InspectorFault inspectorFault = new InspectorFault();
        BeanUtils.copyProperties(inspectorFaultVo, inspectorFault);
        int flag = inspectorFaultMapper.updateById(inspectorFault);
        return flag;
    }
}
