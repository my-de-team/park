package com.park.dataserve.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.park.dataserve.entity.InspectorFaultImg;

import java.util.List;

/**
 * <p>
 * 巡检员故障反馈表图片 服务类
 * </p>
 *
 * @author lxc
 * @since 2023-07-13
 */
public interface InspectorFaultImgService extends IService<InspectorFaultImg> {
    /**
     * 保存巡检员反馈单图片
     */
    int addInspectorFaultImg(String imgUrl, Long inspectorFaultId);

    /**
     * 删除巡检员反馈记录的图片
     * @param inspectorFaultId
     * @return
     */
    int deleteFaultImgByInspectorFaultId(Long inspectorFaultId);

    /**
     * 根据巡检员反馈单查询相应图片
     * @return
     */
    List<String> queryFaultImgByInspectorFaultId(Long inspectorFaultId);
}
