package com.park.dataserve.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.park.dataserve.entity.ChargingRule;
import com.park.dataserve.mapper.ChargingRuleMapper;
import com.park.dataserve.service.ChargingRuleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.utils.PageUtil;
import com.park.vos.datavo.ChargingRuleQueryVo;
import com.park.vos.datavo.ChargingRuleVo;
import netscape.javascript.JSUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * <p>
 * 收费规则表 服务实现类
 * </p>
 *
 * @author lauxiaoyang
 * @since 2023-07-18
 */
@Service
public class ChargingRuleServiceImpl extends ServiceImpl<ChargingRuleMapper, ChargingRule> implements ChargingRuleService {

    @Resource
    private ChargingRuleMapper chargingRuleMapper;

    /*
    分页查询
     */
    @Override
    public PageUtil<ChargingRuleVo> queryPageChargingRule(Integer pageNum, Integer pageSize, ChargingRuleVo chargingRuleVo) {
        Page<ChargingRule> page = new Page<>();
        QueryWrapper<ChargingRule> queryWrapper = new QueryWrapper<>();
        page = chargingRuleMapper.selectPage(page, queryWrapper);
        PageUtil pageUtil = new PageUtil<>();
        pageUtil.setTotal(page.getTotal());
        pageUtil.setPageData(page.getRecords());
        return pageUtil;
    }

    /*
    新增
     */
    @Override
    public boolean addChargingRule(ChargingRuleVo chargingRuleVo) {

        ChargingRule chargingRule = new ChargingRule();
        BeanUtils.copyProperties(chargingRuleVo, chargingRule);
        int flag = chargingRuleMapper.insert(chargingRule);
        if (flag == 1) {
            return true;
        }
        return false;
    }

    /*
    修改
     */
    @Override
    public void updateChargingRule(ChargingRule chargingRule) {
        int update = chargingRuleMapper.updateById(chargingRule);
        if (update == 1) {
            return;
        }
        System.out.println("updatePageChargingRule 出错");
    }

    /*
    删除
     */
    @Override
    public boolean deleteChargingRule(Long id) {
        int flag = chargingRuleMapper.deleteById(id);
        if (flag == 1) {
            return true;
        }
        return false;
    }


    /*
    根据车辆驶入和驶出时间计算收费
     */
    @Override
    public BigDecimal calculateCharge(ChargingRuleQueryVo chargingRuleQueryVo) {

        // 获取车辆计费起始时间和计费结束时间
        Date entryTime = chargingRuleQueryVo.getStartTime();
        Date exitTime = chargingRuleQueryVo.getEndTime();

        long parkingDuration = DateUtil.between(entryTime, exitTime, DateUnit.MINUTE);
        // 输出结果
        System.out.println("停车时长（分钟）：" + parkingDuration);

        // 一天的总分钟数
        long minutesInOneDay = 24 * 60;

        // 判断停车是否在同一天
        boolean isSameDay = parkingDuration < minutesInOneDay;

        // 输出结果
        System.out.println("停车是否在同一天：" + isSameDay);

        // 判断时间差是否在30分钟以内
        if (parkingDuration <= 30) {
            return BigDecimal.ZERO;
            //return new BigDecimal("0");
        }
        //


        // 判断是否是工作日
        //boolean isWeekday = isWeekday(entryTime);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        int dayofWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int hourofDay = calendar.get(Calendar.HOUR_OF_DAY);
        System.out.println("hourofDay" + hourofDay);
        if (dayofWeek >= Calendar.MONDAY && dayofWeek <= Calendar.FRIDAY) {
            //星期一到星期五是工作日


//            if (hourofDay >= 9 && hourofDay <= 21) {
//                //繁忙时间（工作日）
//                BigDecimal bigDecimal = BigDecimal.valueOf(((double) parkingDuration - 30D) / 30D * 5D);
//                if (bigDecimal.compareTo(new BigDecimal(50)) > 0){
//                    return BigDecimal.valueOf(50);
//                }
//                return bigDecimal;
//            } else {
//                //非繁忙时间（工作日）
//                BigDecimal bigDecimal = BigDecimal.valueOf(((double) parkingDuration - 30D) / 30D * 2D);
//                if (bigDecimal.compareTo(new BigDecimal(10)) > 0){
//                    return BigDecimal.valueOf(10);
//                }
//                return bigDecimal;
//            }


            if (hourofDay > 9 && hourofDay <= 21) {

                //繁忙时间（工作日）
                BigDecimal bigDecimal = BigDecimal.valueOf(((double) parkingDuration - 30D) / 30D * 5D);
                if (bigDecimal.compareTo(new BigDecimal(50)) > 0) {
                    return BigDecimal.valueOf(50);
                }
                return bigDecimal;
            } else if ((hourofDay > 21 && hourofDay <= 24) || (hourofDay >= 0 && hourofDay <= 9)) {

                //非繁忙时间（工作日）
                BigDecimal bigDecimal = BigDecimal.valueOf(((double) parkingDuration - 30D) / 30D * 2D);
                if (bigDecimal.compareTo(new BigDecimal(10)) > 0) {
                    return BigDecimal.valueOf(10);
                }
                return bigDecimal;
            }

        } else {
            //星期六和星期六是非工作日
            return BigDecimal.valueOf(0);
        }

        //判断是否是繁忙时间段
        //boolean isBusyTime = isBusyTime(entryTime);

        //BigDecimal bigDecimal = BigDecimal.valueOf(((double) parkingDuration - 30D) / 30D * 2D);
        //return bigDecimal;

        //数值类型

        //获取对应的收费规则
        //ChargingRule chargingRule = getChargingRule(entryTime);//根据计费开始时间（对应车辆驶入时间）
        /*
        if (chargingRule == null) {
            return BigDecimal.ZERO;// 没有找到对应的收费规则，不收费
        }
        */


        //计算收费金额
        //BigDecimal charge = calculateChargeAmount(parkingDuration, chargingRule, isWeekday, isBusyTime);
        //return charge;
        //return new BigDecimal("1");
        return new BigDecimal("123");

    }


    // 根据车辆驶入时间获取对应收费规则
//    private ChargingRule getChargingRule(Date entryTime) {
//        QueryWrapper<ChargingRule> queryWrapper = new QueryWrapper<>();
//        queryWrapper.le("enter_time", entryTime)//计费起始时间
//                .ge("exit_time", entryTime)//计费结束时间
//                .eq("is_weekday", isWeekday(entryTime));
//        return chargingRuleMapper.selectOne(queryWrapper);
//    }

    // 判断是否是工作日
//    private boolean isWeekday(Date date) {
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(date);
//        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
//        return dayOfWeek >= Calendar.MONDAY && dayOfWeek <= Calendar.FRIDAY;
//    }

    // 判断是否是繁忙时间段
//    private boolean isBusyTime(Date date) {
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(date);
//        int hour = calendar.get(Calendar.HOUR_OF_DAY);
//        return hour >= 9 && hour < 21;
//    }


    // 计算收费金额
//    private BigDecimal calculateChargeAmount(long parkingDuration, ChargingRule chargingRule, boolean isWeekday, boolean isBusyTime) {
//        BigDecimal charge;
//        //BigDecimal charge = BigDecimal.ZERO;
//        int freeDuration = chargingRule.getFreeDuration();
//        int maxDuration = chargingRule.getMaxDuration();
//        BigDecimal ratePerHour = chargingRule.getRatePerHour();
//        BigDecimal ceilingPrice = chargingRule.getCeilingPrice();
//
//        if (isWeekday) {
//            if (isBusyTime) {
//                charge = calculateBusyTimeCharge(parkingDuration, freeDuration, ratePerHour, ceilingPrice);
//            } else {
//                charge = calculateNonBusyTimeCharge(parkingDuration, freeDuration, ratePerHour, ceilingPrice);
//            }
//        } else {
//            charge = BigDecimal.ZERO; // 非工作日不收费
//        }
//
//        return charge;
//    }

    // 计算繁忙时间段的收费
//    private BigDecimal calculateBusyTimeCharge(long parkingDuration, int freeDuration, BigDecimal ratePerHour, BigDecimal ceilingPrice) {
//        if (parkingDuration <= freeDuration) {
//            return BigDecimal.ZERO; // 免费时段内不收费
//        } else {
//            // 超过免费时长，按每30分钟5元收费，考虑封顶金额
//            int additionalMinutes = (int) (parkingDuration - freeDuration);
//            BigDecimal chargingAmount = ratePerHour.multiply(BigDecimal.valueOf((additionalMinutes + 29) / 30));
//            return chargingAmount.min(ceilingPrice);
//        }
//    }

    // 计算非繁忙时间段的收费
//    private BigDecimal calculateNonBusyTimeCharge(long parkingDuration, int freeDuration, BigDecimal ratePerHour, BigDecimal ceilingPrice) {
//        if (parkingDuration <= freeDuration) {
//            return BigDecimal.ZERO; // 免费时段内不收费
//        } else {
//            // 超过免费时长，按每60分钟2元收费，考虑封顶金额
//            int additionalMinutes = (int) (parkingDuration - freeDuration);
//            BigDecimal chargingAmount = ratePerHour.multiply(BigDecimal.valueOf((additionalMinutes + 59) / 60));
//            return chargingAmount.min(ceilingPrice);
//        }
//    }


    /*
    根据泊位id查询收费信息 泊位ID：parking_position_id
     */
    @Override
    public List<ChargingRuleVo> getChargingByParkingPositionId(Long roadSegmentId) {
        // 创建显示收费信息（ChargingRuleVoVo）的list集合
        List<ChargingRuleVo> chargingRuleVoList = new ArrayList<>();
        // 根据路段id查询所有的泊位信息
        List<ChargingRule> chargingRuleList = chargingRuleMapper.getChargingByParkingPositionId(roadSegmentId);
        // 转换成vo的list集合
        for (ChargingRule chargingRule : chargingRuleList) {
            ChargingRuleVo chargingRuleVo = new ChargingRuleVo();
            BeanUtils.copyProperties(chargingRule, chargingRuleVo);
            chargingRuleVoList.add(chargingRuleVo);
        }
        return chargingRuleVoList;//返回收费信息列表
    }

    @Override
    public BigDecimal sum(ChargingRuleQueryVo chargingRuleQueryVo) {
        //30分钟免费
        // 获取车辆计费起始时间和计费结束时间
        Date entryTime = chargingRuleQueryVo.getStartTime();
        Date exitTime = chargingRuleQueryVo.getEndTime();

        long parkingDuration = DateUtil.between(entryTime, exitTime, DateUnit.MINUTE);

        if (parkingDuration <= 30) {
            BigDecimal bigDecimal = new BigDecimal("0");
            return bigDecimal;
        }

        //大于30分钟每30分钟0.1元

        Double d = (parkingDuration - 30) * 0.1;
        return new BigDecimal(d);
    }


}
