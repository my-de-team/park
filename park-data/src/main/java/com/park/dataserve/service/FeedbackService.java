package com.park.dataserve.service;

import com.park.dataserve.entity.Feedback;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.utils.PageUtil;
import com.park.vos.datavo.FeedbackAddVo;
import com.park.vos.datavo.FeedbackQueryVo;
import com.park.vos.datavo.FeedbackVo;

/**
 * <p>
 * 用户反馈表 服务类
 * </p>
 *
 * @author lxc
 * @since 2023-07-15
 */
public interface FeedbackService extends IService<Feedback> {

    /**
     * 添加用户反馈记录
     * @return
     */
    int addFeedback(FeedbackAddVo feedbackAddVo);

    /**
     * 删除用户反馈记录
     */
    int deleteFeedbackById(Long id);

    /**
     * 分页查询
     */
    PageUtil<FeedbackVo> queryFeedback(Long pageNum, Long pageSize, FeedbackQueryVo feedbackQueryVo);

    /**
     * 根据id查询
     */
    FeedbackVo queryFeedbackById(Long id);

    /**
     * 更新反馈单
     */
    int updateFeedbackById(FeedbackVo feedbackVo);
}
