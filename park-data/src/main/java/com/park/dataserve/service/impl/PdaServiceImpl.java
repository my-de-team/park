package com.park.dataserve.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.dataserve.entity.Pda;
import com.park.dataserve.entity.RoadSegment;
import com.park.dataserve.service.PdaService;
import com.park.dataserve.mapper.PdaMapper;
import com.park.dataserve.service.RoadSegmentService;
import com.park.utils.PageUtil;
import com.park.vos.datavo.PdaQueryVo;
import com.park.vos.datavo.PdaVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
* @author blank
* @description 针对表【tp_pda(手持PDA表)】的数据库操作Service实现
* @createDate 2023-07-18 11:14:46
*/
@Service
public class PdaServiceImpl extends ServiceImpl<PdaMapper, Pda> implements PdaService{
    @Resource
    private PdaMapper pdaMapper;
    @Resource
    private RoadSegmentService roadSegmentService;

    //新增PDA
    @Override
    public int pdaAdd(PdaVo pdaVo) {
        if (StringUtils.isEmpty(pdaVo.getDeviceNumber())) {
            return 0;
        }
        if (StringUtils.isEmpty(pdaVo.getDeviceName())) {
            return 0;
        }
        if (pdaVo.getRoadId() == null) {
            return 0;
        }
        Pda pda = new Pda();
        BeanUtils.copyProperties(pdaVo, pda);
        int flag = pdaMapper.insert(pda);
        return flag;
    }

    //删除PDA
    @Override
    public int deletePdaById(Long id) {
       int flag = pdaMapper.deleteById(id);
       return flag;
    }

    //修改PDA
    @Override
    public int pdaPut(PdaVo pdaVo) {
        if (StringUtils.isEmpty(pdaVo.getDeviceNumber())) {
            return 0;
        }
        if (StringUtils.isEmpty(pdaVo.getDeviceName())) {
            return 0;
        }
        if (pdaVo.getRoadId() == null) {
            return 0;
        }
        int flag = pdaMapper.pdaPut(pdaVo);
        return flag;
    }

    //分页查询
    @Override
    public PageUtil queryPdaAll(Integer pageNum, Integer pageSize, PdaQueryVo pdaQueryVo) {
        IPage<Pda> page = new Page<>(pageNum, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        if (!StringUtils.isEmpty(pdaQueryVo.getDeviceNumber())) {
            queryWrapper.eq("set_number", pdaQueryVo.getDeviceNumber());
        }
        if (!StringUtils.isEmpty(pdaQueryVo.getDeviceName())) {
            queryWrapper.like("set_name", pdaQueryVo.getDeviceName());
        }
        if (pdaQueryVo.getRoadId() != null) {
            queryWrapper.eq("road_id",pdaQueryVo.getRoadId());
        }
        page = pdaMapper.selectPage(page, queryWrapper);
        PageUtil pageUtil = new PageUtil<>();
        pageUtil.setTotal(page.getTotal());
        pageUtil.setPageData(page.getRecords());
        pageUtil.setPageNum(page.getCurrent());
        pageUtil.setPageSize(page.getSize());
        List<PdaVo> pdaVoList = new ArrayList<>();
        List<Pda> pdaList = page.getRecords();
        for (Pda pda : pdaList) {
            PdaVo pdaVo = new PdaVo();
            BeanUtils.copyProperties(pda, pdaVo);
            Long roadId = pda.getRoadId();
            RoadSegment roadSegment = roadSegmentService.queryRoadSegmentById(roadId);
            if (roadSegment != null) {
                String segmentName = roadSegmentService.queryRoadSegmentById(roadId).getSegmentName();
                pdaVo.setSegmentName(segmentName);
            }
            pdaVoList.add(pdaVo);
        }
        pageUtil.setPageData(pdaVoList);
        return pageUtil;
    }


}




