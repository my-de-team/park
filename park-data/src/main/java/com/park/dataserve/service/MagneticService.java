package com.park.dataserve.service;

import com.park.dataserve.entity.Magnetic;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.expection.MagneticExpection;
import com.park.utils.PageUtil;
import com.park.vos.datavo.MagneticVo;
import com.park.vos.datavo.QueryMagneticVo;

/**
* @author blank
* @description 针对表【tp_magnetic】的数据库操作Service
* @createDate 2023-07-13 11:38:15
*/
public interface MagneticService extends IService<Magnetic> {

    int magneticAdd(MagneticVo magneticVo) throws MagneticExpection;

    int deleteMagneticById(Long id);

    int magneticPut(MagneticVo magneticVo) throws MagneticExpection;

    PageUtil queryMagneticAll(Integer pageNum, Integer pageSize, QueryMagneticVo queryMagneticVo);
}
