package com.park.dataserve.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.park.annotation.IdempotentAnnotation;
import com.park.dataserve.entity.Feedback;
import com.park.dataserve.entity.InspectorFault;
import com.park.dataserve.mapper.FeedbackMapper;
import com.park.dataserve.service.FeedbackService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.dataserve.service.InspectorFaultImgService;
import com.park.utils.AssertUtils;
import com.park.utils.CustomUtil;
import com.park.utils.PageUtil;
import com.park.vos.datavo.FeedbackAddVo;
import com.park.vos.datavo.FeedbackQueryVo;
import com.park.vos.datavo.FeedbackVo;
import com.park.vos.datavo.InspectorFaultVo;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 用户反馈表 服务实现类
 * </p>
 *
 * @author lxc
 * @since 2023-07-15
 */
@Service
@RequiredArgsConstructor
public class FeedbackServiceImpl extends ServiceImpl<FeedbackMapper, Feedback> implements FeedbackService {

    private final FeedbackMapper feedbackMapper;
    private final InspectorFaultImgService inspectorFaultImgService;

    @Override
    @IdempotentAnnotation
    @Transactional
    public int addFeedback(FeedbackAddVo feedbackAddVo) {
        // 添加反馈
        Feedback feedback = new Feedback();
        BeanUtils.copyProperties(feedbackAddVo, feedback);
        feedback.setFeedbackNo(CustomUtil.getCodeNumber("Feedback"));
        feedback.setCreateTime(new Date());
        if (AssertUtils.isNotEmpty(feedbackAddVo.getImgUrlList())) {
            feedback.setImgCount(feedbackAddVo.getImgUrlList().size());
        }
        int flag = feedbackMapper.insert(feedback);
        // 插入图片
        if (feedbackAddVo.getImgUrlList() != null) {
            for (String imgUrl : feedbackAddVo.getImgUrlList()) {
                inspectorFaultImgService.addInspectorFaultImg(imgUrl, feedback.getId());
            }

        }

        return flag;
    }

    @Override
    public int deleteFeedbackById(Long id) {
        // 查询图片数量
        Integer imgCount = feedbackMapper.queryImgCountById(id);
        // 删除反馈记录
        int flag = feedbackMapper.deleteById(id);
        // 删除反馈记录的图片
        if (imgCount != null || !imgCount.equals(0)) {
            inspectorFaultImgService.deleteFaultImgByInspectorFaultId(id);
        }
        return flag;
    }

    @Override
    public PageUtil<FeedbackVo> queryFeedback(Long pageNum, Long pageSize, FeedbackQueryVo feedbackQueryVo) {
        IPage<FeedbackVo> page = new Page<>(pageNum, pageSize);

        page = feedbackMapper.queryFeedbackPage(page, feedbackQueryVo);
        PageUtil<FeedbackVo> pageUtil = new PageUtil<>();
        pageUtil.setPageData(page.getRecords());
        pageUtil.setTotal(page.getTotal());
        return pageUtil;
    }

    @Override
    public FeedbackVo queryFeedbackById(Long id) {
        Feedback feedback = feedbackMapper.selectById(id);
        FeedbackVo feedbackVo = new FeedbackVo();
        BeanUtils.copyProperties(feedback, feedbackVo);
        //查询姓名和手机号

        //查询图片
        if (feedback.getImgCount() != null) {
            List<String> imgUrlList = inspectorFaultImgService.queryFaultImgByInspectorFaultId(id);
            feedbackVo.setImgUrlList(imgUrlList);
        }
        return feedbackVo;
    }

    @Override
    public int updateFeedbackById(FeedbackVo feedbackVo) {
        Feedback feedback = new Feedback();
        BeanUtils.copyProperties(feedbackVo, feedback);
        int flag = feedbackMapper.updateById(feedback);

        return flag;
    }
}
