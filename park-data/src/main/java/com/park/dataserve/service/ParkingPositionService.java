package com.park.dataserve.service;

import com.park.dataserve.entity.ParkingPosition;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.vos.datavo.ParkingPositionQueryVo;
import com.park.vos.datavo.ParkingPositionVo;

import java.util.List;

/**
 * <p>
 * 泊位信息 服务类
 * </p>
 *
 * @author lauxiaoyang
 * @since 2023-07-14
 */
public interface ParkingPositionService extends IService<ParkingPosition> {

    /*
    分页查询
    */
    PageUtil queryPageParkingPosition(Integer pageNum, Integer pageSize, ParkingPositionVo parkingPositionVo);

    /*
    新增
     */
    boolean addPageParkingPosition(ParkingPositionVo parkingPositionVo);

    /*
    修改
     */
    void updatePageParkingPosition(ParkingPosition position);

    /*
    删除
     */
    boolean deletePageParkingPosition(Long id);

    /*
    根据泊位编号查询吃泊位id
     */
    Long getByBerthNumber(String berthNumber);

    /*
    根据路段id查询泊位信息 路段ID：road_segment_id
     */
    List<ParkingPositionQueryVo> getParkingByRoadSegmentId(Long roadSegmentId);
}
