package com.park.dataserve.service;

import com.park.dataserve.entity.MagneticLog;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.utils.PageUtil;

/**
* @author blank
* @description 针对表【tp_magnetic_log(地磁日志表)】的数据库操作Service
* @createDate 2023-07-15 15:47:41
*/
public interface MagneticLogService extends IService<MagneticLog> {

    PageUtil queryMagneticLogAll(Integer pageNum, Integer pageSize, String magneticNumber);
}
