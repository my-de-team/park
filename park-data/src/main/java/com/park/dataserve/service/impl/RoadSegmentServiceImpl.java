package com.park.dataserve.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.park.dataserve.entity.RoadSegment;
import com.park.dataserve.mapper.RoadSegmentMapper;
import com.park.dataserve.service.PeopleService;
import com.park.dataserve.service.RoadSegmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.utils.PageUtil;
import com.park.vos.datavo.PeopleVo;
import com.park.vos.datavo.RoadSegmentAddVo;
import com.park.vos.datavo.RoadSegmentQueryVo;
import com.park.vos.datavo.RoadSegmentVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 路段信息 服务实现类
 * </p>
 *
 * @author lauxiaoyang
 * @since 2023-07-13
 */
@Service
public class RoadSegmentServiceImpl extends ServiceImpl<RoadSegmentMapper, RoadSegment> implements RoadSegmentService {


    @Resource
    private RoadSegmentMapper roadSegmentMapper;

    @Resource
    private PeopleService peopleService;



    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    public RoadSegmentServiceImpl(RoadSegmentMapper roadSegmentMapper, RedisTemplate<String, Object> redisTemplate) {
        this.roadSegmentMapper = roadSegmentMapper;

        this.redisTemplate = redisTemplate;
    }


    /*
    分页查询
     */
    @Override
    public PageUtil<RoadSegmentVo> queryPageRoadSegment(Integer pageNum, Integer pageSize, RoadSegmentQueryVo roadSegmentQueryVo) {

        IPage<RoadSegment> page = new Page<>(pageNum, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        //根据巡检员id查询巡检员姓名
        //queryWrapper.eq("inspector_id",1);
        page = roadSegmentMapper.selectPage(page, queryWrapper);
        PageUtil<RoadSegmentVo> pageUtil = new PageUtil();
        pageUtil.setTotal(page.getTotal());
        pageUtil.setPageNum(page.getCurrent());
        pageUtil.setPageSize(page.getSize());

        List<RoadSegment> roadSegmentList = page.getRecords();
        List<RoadSegmentVo> roadSegmentVoList = new ArrayList<>();
        for (RoadSegment roadSegment : roadSegmentList) {
            RoadSegmentVo roadSegmentVo = new RoadSegmentVo();
            BeanUtils.copyProperties(roadSegment, roadSegmentVo);
            Long id = roadSegment.getId();
            // 查询路段对应的运维人员id
            List<String> maintenanceList = new ArrayList<>();
            // 查询路段对应着巡检员id
            List<String> employeeNameList = new ArrayList<>();
            List<Long> inspectorIdList = roadSegmentMapper.queryInspectorId(id);
            for (Long inspectorId : inspectorIdList) {
                //调用方法，根据巡检员id查询巡检员name
                PeopleVo employee = peopleService.queryPeopleNameById(inspectorId, 0);
                if (employee != null) {
                    String employeeName = employee.getEmployeeName();
                    employeeNameList.add(employeeName);
                }

                PeopleVo maintenance = peopleService.queryPeopleNameById(inspectorId, 1);
                if (maintenance != null) {
                    String maintenanceName = maintenance.getEmployeeName();
                    maintenanceList.add(maintenanceName);
                }


            }
            roadSegmentVo.setEmployeeName(employeeNameList);
            roadSegmentVo.setMaintenanceNameList(maintenanceList);
            roadSegmentVoList.add(roadSegmentVo);
        }


        pageUtil.setPageData(roadSegmentVoList);
        return pageUtil;
    }


    /*
    根据路段id查询路段名称
     */
    @Override
    public RoadSegment queryRoadSegmentById(Long id) {
        RoadSegment roadSegment = roadSegmentMapper.queryRoadSegmentById(id);
        return roadSegment;
    }

    /*
    新增
     */
    @Override
    public boolean addRoadSegment(RoadSegmentAddVo roadSegmentAddVo) {
        RoadSegment roadSegment = new RoadSegment();
        BeanUtils.copyProperties(roadSegmentAddVo, roadSegment);
        int flag = roadSegmentMapper.insert(roadSegment);//
        return true;
    }

    /*
    修改
     */
    @Override
    public int updateRoadSegment(RoadSegment roadSegment) {
        int flag = roadSegmentMapper.updateRoadSegment(roadSegment);

        return flag;
    }

    /*
    删除

     */
    @Override
    public boolean deleteRoadSegmentById(Long id) {
        //RoadSegment roadSegment = new RoadSegment();
        //roadSegment.getId();
        int falg = roadSegmentMapper.deleteRoadSegmentById(id);
        if (falg > 0) {
            return true;
        }
        return false;
    }


    /*
    根据经纬度查询路段信息
     */
//    @Override
//    public List<RoadSegmentVo> queryRoadSegmentByCoordinates(String longitude, String latitude) {
//
//        return roadSegmentMapper.queryRoadSegmentByCoordinates(longitude, latitude);
//    }
//
//    public List<RoadSegment> queryRoadSegmentByCoordinatesWithCache(String longitude, String latitude) {
//        String cacheKey = generateCacheKey(longitude, latitude);
//        ListOperations<String, RoadSegment> listOperations = redisTemplate.opsForList();
//        // 检查Redis中是否已经缓存了数据
//        List<RoadSegment> roadSegmentList = listOperations.range(cacheKey, 0, 1);
//        if (roadSegmentList != null && !roadSegmentList.isEmpty()) {
//            // 如果数据已缓存，则返回
//            return roadSegmentList;
//        }else {
//            //如果没有缓存数据，则从数据库中查询
//
//            List<RoadSegmentVo> roadSegments = roadSegmentMapper.queryRoadSegmentByCoordinates(longitude, latitude);
//            if (roadSegments != null && !roadSegments.isEmpty()){
//                listOperations.rightPushAll(cacheKey, (RoadSegment) roadSegments);
//            }
//            return roadSegmentList;
//        }
//    }
//
//    //根据经度和纬度生成缓存键的 Helper 方法
//    private String generateCacheKey(String longitude, String latitude) {
//        return "road_segments" + longitude + "_" + latitude;
//    }


}
