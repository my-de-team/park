package com.park.dataserve.service;

import com.park.dataserve.entity.People;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.expection.MobileCodeErrorException;
import com.park.expection.MobileOrPasswordException;
import com.park.expection.RegisterMobileRepeatException;
import com.park.utils.PageUtil;
import com.park.vos.datavo.PeopleLoginVo;
import com.park.vos.datavo.PeopleVo;
import com.park.vos.datavo.QueryPeopleVo;
import com.park.vos.datavo.RegisterVo;

/**
* @author blank
* @description 针对表【tp_people(员工管理表)】的数据库操作Service
* @createDate 2023-07-14 09:42:50
*/
public interface PeopleService extends IService<People> {

    PageUtil queryPeopleAll(Integer pageNum, Integer pageSize, QueryPeopleVo queryPeopleVo);

    PageUtil queryMaintenanceAll(Integer pageNum, Integer pageSize, QueryPeopleVo queryPeopleVo);

    PeopleVo queryPeopleNameById(Long id, Integer roleState);

    int addInspector(PeopleVo peopleVo);

    int putInspector(PeopleVo peopleVo);

    int deletePeopleById(Long id);

    String peopleLogin(PeopleLoginVo peopleLoginVo) throws MobileOrPasswordException;

    int register(RegisterVo registerVo) throws MobileCodeErrorException, RegisterMobileRepeatException, InterruptedException;

    int isRegisterMobile(String mobile);
}
