package com.park.dataserve.service.impl;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.park.dataserve.entity.ParkingPosition;
import com.park.dataserve.mapper.ParkingPositionMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.dataserve.service.ParkingPositionService;
import com.park.utils.CustomUtil;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.vos.datavo.ParkingPositionQueryVo;
import com.park.vos.datavo.ParkingPositionVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 泊位信息 服务实现类
 * </p>
 *
 * @author lauxiaoyang
 * @since 2023-07-14
 */
@Service
public class ParkingPositionServiceImpl extends ServiceImpl<ParkingPositionMapper, ParkingPosition> implements ParkingPositionService {

    @Resource
    private ParkingPositionMapper parkingPositionMapper;


    /*
    分页查询
     */
    @Override
    public PageUtil queryPageParkingPosition(Integer pageNum, Integer pageSize, ParkingPositionVo parkingPositionVo) {

        Page<ParkingPosition> page = new Page<>(pageNum, pageSize);
        QueryWrapper<ParkingPosition> queryWrapper = new QueryWrapper<>();
        page = parkingPositionMapper.selectPage(page,queryWrapper);
        PageUtil pageUtil = new PageUtil<>();
        pageUtil.setTotal(page.getTotal());//setTotals(page.getTotal())
        pageUtil.setPageData(page.getRecords());//setResult(page.getRecords());
        return pageUtil;
    }

    /*
    新增
     */
    @Override
    public boolean addPageParkingPosition(ParkingPositionVo parkingPositionVo) {

        ParkingPosition parkingPosition =new ParkingPosition();

        parkingPosition.setRoadSegmentId(parkingPositionVo.getRoadSegmentId());
        parkingPosition.setParkingName(parkingPositionVo.getParkingName());
        parkingPosition.setBerthNumber(CustomUtil.getCodeNumber("DI"));
        parkingPosition.setMagneticNumber(parkingPositionVo.getMagneticNumber());
        parkingPosition.setState(0);
        parkingPosition.setVersion(0);
        parkingPosition.setIsDeleted(0);

        int flag = parkingPositionMapper.insert(parkingPosition);
        if (flag == 1){
            return true;
        }
        return false;
    }

    /*
    修改
     */
    @Override
    public void updatePageParkingPosition(ParkingPosition position) {
        int update = parkingPositionMapper.updateById(position);
        if (update == 1){
            return;
        }
        System.out.println("updatePageParkingPosition 出错");
    }


    /*
    删除
     */
    @Override
    public boolean deletePageParkingPosition(Long id) {

        int flag = parkingPositionMapper.deleteById(id);
        if (flag == 1){
            return true;
        }
        return false;
    }

    /*
    根据泊位编号查询吃泊位id
     */
    @Override
    public Long getByBerthNumber(String berthNumber) {
        Long id = parkingPositionMapper.selectByBerthNumber(berthNumber);
        return id;
    }

    /*
    根据路段id查询泊位信息 路段ID：road_segment_id
     */
    @Override
    public List<ParkingPositionQueryVo> getParkingByRoadSegmentId(Long roadSegmentId) {
        // 创建显示泊位信息（ParkingPositionQueryVo）的list集合
        List<ParkingPositionQueryVo> parkingPositionQueryVoList = new ArrayList<>();
        // 根据路段id查询所有的泊位信息
        List<ParkingPosition> parkingList = parkingPositionMapper.getParkingByRoadSegmentId(roadSegmentId);
        // 转换成vo的list集合
        for (ParkingPosition position : parkingList) {
            ParkingPositionQueryVo parkingPositionQueryVo = new ParkingPositionQueryVo();
            BeanUtils.copyProperties(position, parkingPositionQueryVo);
            parkingPositionQueryVoList.add(parkingPositionQueryVo);
        }
        return parkingPositionQueryVoList;//返回泊位信息列表
    }


}
