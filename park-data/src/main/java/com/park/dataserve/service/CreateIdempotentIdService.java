package com.park.dataserve.service;

public interface CreateIdempotentIdService {
    public String create();
}
