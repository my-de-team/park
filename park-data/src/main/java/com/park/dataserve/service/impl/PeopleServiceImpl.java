package com.park.dataserve.service.impl;

import cn.hutool.jwt.JWTUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.api.DataControllerApi;
import com.park.api.SystemControllerApi;
import com.park.dataserve.entity.People;
import com.park.dataserve.entity.RoadSegment;
import com.park.dataserve.service.PdaService;
import com.park.dataserve.service.PeopleService;
import com.park.dataserve.mapper.PeopleMapper;
import com.park.dataserve.service.RoadSegmentService;
import com.park.expection.MobileCodeErrorException;
import com.park.expection.MobileOrPasswordException;
import com.park.expection.RegisterMobileRepeatException;
import com.park.utils.BCrypt;
import com.park.utils.JwtUtils;
import com.park.utils.PageUtil;
import com.park.vos.datavo.*;
import com.park.vos.systemvo.LoginLogVo;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.security.SecureRandom;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author blank
 * @description 针对表【tp_people(员工管理表)】的数据库操作Service实现
 * @createDate 2023-07-14 09:42:50
 */
@Service
public class PeopleServiceImpl extends ServiceImpl<PeopleMapper, People> implements PeopleService {

    @Resource
    private PeopleMapper peopleMapper;

    @Resource
    private RoadSegmentService roadSegmentService;

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private RabbitTemplate rabbitTemplate;

    @Resource
    private SystemControllerApi systemControllerApi;

    //分页查询
    @Override
    public PageUtil<PeopleVo> queryPeopleAll(Integer pageNum, Integer pageSize, QueryPeopleVo queryPeopleVo) {
        IPage<People> page = new Page<>(pageNum, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("role_state", 0);
        page = peopleMapper.selectPage(page, queryWrapper);
        PageUtil<PeopleVo> pageUtil = new PageUtil<>();
        pageUtil.setTotal(page.getTotal());
        pageUtil.setPageNum(page.getCurrent());
        pageUtil.setPageSize(page.getSize());
        List<PeopleVo> peopleVoList = new ArrayList<>();
        List<People> peopleList = page.getRecords();
        for (People people : peopleList) {
            PeopleVo peopleVo = new PeopleVo();
            List<String> segmentNameList = new ArrayList<>();
            BeanUtils.copyProperties(people, peopleVo);
            Long id = people.getId();
            List<Long> roadIdList = peopleMapper.queryRoadId(id);
            for (Long roadId : roadIdList) {
                // 调方法获取路段名
                RoadSegment roadSegment = roadSegmentService.queryRoadSegmentById(roadId);
                if (roadSegment != null) {
                    String segmentName = roadSegment.getSegmentName();
                    segmentNameList.add(segmentName);
                }
            }
            peopleVo.setSegmentNameList(segmentNameList);
            peopleVoList.add(peopleVo);
        }
        pageUtil.setPageData(peopleVoList);
        return pageUtil;
    }

    //分页查询运维人员
    @Override
    public PageUtil queryMaintenanceAll(Integer pageNum, Integer pageSize, QueryPeopleVo queryPeopleVo) {
        IPage<People> page = new Page<>(pageNum, pageSize);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("role_state", 1);
        page = peopleMapper.selectPage(page, queryWrapper);
        PageUtil<PeopleVo> pageUtil = new PageUtil<>();
        pageUtil.setTotal(page.getTotal());
        pageUtil.setPageNum(page.getCurrent());
        pageUtil.setPageSize(page.getSize());
        List<PeopleVo> peopleVoList = new ArrayList<>();
        List<People> peopleList = page.getRecords();
        for (People people : peopleList) {
            PeopleVo peopleVo = new PeopleVo();
            List<String> segmentNameList = new ArrayList<>();
            BeanUtils.copyProperties(people, peopleVo);
            Long id = people.getId();
            List<Long> roadIdList = peopleMapper.queryRoadId(id);
            for (Long roadId : roadIdList) {
                // 调方法获取路段名
                RoadSegment roadSegment = roadSegmentService.queryRoadSegmentById(roadId);
                if (roadSegment != null) {
                    String segmentName = roadSegment.getSegmentName();
                    segmentNameList.add(segmentName);
                }
            }
            peopleVo.setSegmentNameList(segmentNameList);
            peopleVoList.add(peopleVo);

        }
        pageUtil.setPageData(peopleVoList);
        return pageUtil;
    }

    //根据id查询巡检员名
    @Override
    public PeopleVo queryPeopleNameById(Long id, Integer roleState) {
        PeopleVo peopleVo = peopleMapper.queryPeopleNameById(id, roleState);
        return peopleVo;
    }

    //新增员工
    @Override
    public int addInspector(PeopleVo peopleVo) {
        People people = new People();
        BeanUtils.copyProperties(peopleVo, people);
        int flag = peopleMapper.insert(people);
        List<Long> roadIdList = peopleVo.getRoadId();
        if (roadIdList != null) {
            for (Long roadId : roadIdList) {
                peopleMapper.addInspectorRoad(people.getId(), roadId);
            }
        }
        return flag;
    }

    //删除员工
    @Override
    public int deletePeopleById(Long id) {
        int flag = peopleMapper.deleteById(id);
        return flag;
    }

    @Override
    public String peopleLogin(PeopleLoginVo peopleLoginVo) throws MobileOrPasswordException {
        // 获取用户名字和密码
        People people = peopleMapper.login(peopleLoginVo.getPhone());
        // 比较密码是否正确
        boolean flag = BCrypt.matches(peopleLoginVo.getPassword(), people.getPassword());
        // 不正确抛名字或者密码异常
        if (!flag) {
            throw new MobileOrPasswordException();
        }
        // 创建一个载荷map，放入手机号、用户名
        Map<String, Object> claim = new HashMap<>();
        claim.put("phone", people.getPhone());
        claim.put("roleState", people.getRoleState().toString());
        claim.put("employeeName", people.getEmployeeName());
        // 生成token
        String token = JwtUtils.getJwtToken(claim);
        // 放入redis
        redisTemplate.opsForValue().set("token:" + token, 1, 12, TimeUnit.HOURS);
        return token;
    }

    @Override
    public int register(RegisterVo registerVo) throws MobileCodeErrorException, RegisterMobileRepeatException, InterruptedException {
        int result = 0;
        /**
         * 1、验证验证码是否正确
         */
        Object objectRedisCode = redisTemplate.opsForValue().get("mobileCode:" + registerVo.getMobile());
        String strRedisCode = null;
        if (objectRedisCode == null) {
            throw new MobileCodeErrorException();
        }
        strRedisCode = (String) objectRedisCode;
        if (!strRedisCode.equals(registerVo.getMobilecode())) {
            throw new MobileCodeErrorException();
        }
        /**
         * 2、查询手机号码是重复
         */
        int flag = this.isRegisterMobile(registerVo.getMobile());
        if (flag > 0) {
            //说明该手机号已经被注册
            throw new RegisterMobileRepeatException();
        }
        /**
         * 3、注册信息插入到数据库中
         */
        People register = new People();
        register.setPhone(registerVo.getMobile());
        /**
         * 获取密码的密文:
         *      String salt = gensalt(10, new SecureRandom());
         *      String mm = hashpw("123456", salt);
         */
        String salt = BCrypt.gensalt(10, new SecureRandom());
        String bCryptPassword = BCrypt.hashpw(registerVo.getPassword(), salt);
        register.setPhone(registerVo.getMobile());
        register.setRoleState(registerVo.getRoleState());
        register.setPassword(bCryptPassword);
        register.setIsDeleted(0);
        register.setEmployeeName(registerVo.getEmployeeName());
        result = peopleMapper.insert(register);
        /**
         * 4、不用删除redis里面的验证码,因为默认5分钟以后会失效
         */
        redisTemplate.delete("mobileCode:" + registerVo.getMobile());
        /**
         * 5、通过mq给用户发送注册成功的短信提示
         */
        rabbitTemplate.convertAndSend("register_success_code_exchange", "success", register.getPhone());

        return result;
    }

    @Override
    public int isRegisterMobile(String mobile) {
        int count = peopleMapper.isRegisterMobile(mobile);
        return count;
    }

    //修改员工
    @Override
    public int putInspector(PeopleVo peopleVo) {
        People people = new People();
        BeanUtils.copyProperties(peopleVo, people);
        int flag = peopleMapper.updateById(people);
        List<Long> roadIdList = peopleVo.getRoadId();
        if (roadIdList != null) {
            for (Long roadId : roadIdList) {
                peopleMapper.putInspectorRoad(people.getId(), roadId);
            }
        }
        return flag;
    }


}




