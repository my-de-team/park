package com.park.dataserve.service.impl;

import com.park.dataserve.service.CreateIdempotentIdService;
import com.park.utils.SnowflakeIdWorker;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Service
public class CreateIdempotentIdServiceImpl implements CreateIdempotentIdService {
    @Resource
    private RedisTemplate<String, Object> redisTemplate;
    @Resource
    private SnowflakeIdWorker sw;
    @Override
    public String create() {
        /**
         * 1 生成id
         */
        String id = sw.nextId() + "";
        /**
         * 2 存到redis中
         */
        redisTemplate.opsForValue().set(id, id, 10, TimeUnit.MINUTES);
        return id;
    }
}
