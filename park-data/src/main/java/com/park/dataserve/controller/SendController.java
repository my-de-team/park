package com.park.dataserve.controller;

import com.park.dataserve.service.SendService;
import com.park.utils.CreateCodeUtil;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/send")
@Api(tags = "短信收发模块")
public class SendController {
    @Resource
    private SendService sendService;
    @Resource
    private RedisTemplate<String, Object> rt;

    /**
     * 我们自己的发送验证码接口
     */
    @ApiOperation("自己的发送验证码接口")
    @GetMapping("/sendCode/{mobile}")
    public Result sendCode(@PathVariable("mobile") String mobile) throws Exception {
        /**
         * 1、先去redis里面查，看有没有该用户的验证码，如果有说明已经发过了
         */
        Object redisCode = rt.opsForValue().get("mobileCode:" + mobile);//moblieCode:12718413397
        if (redisCode != null) {
            //说明已经发送验证码，并且没有过期
            return Result.send("50006", "验证码5分钟内有效，不需要重新获取", null);
        }
        /**
         * 2、调用service完成短信发送
         */
        String code = CreateCodeUtil.getCode();
        //boolean flag = sendService.sendCode(mobile, code);
        /*用于测试的不发送短信*/
        code = "1234";
        boolean flag = true;
        if (flag) {
            /**
             * 3、把生成的code存在redis 中，并且设置过期时间为5分钟
             */
            rt.opsForValue().set("mobileCode:" + mobile, code, 100, TimeUnit.MINUTES);
            return Result.success(ResultCode.SMS_SUCCESS);
        } else {
            return Result.send("50007", "短信发送失败", null);
        }
    }
}
