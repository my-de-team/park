package com.park.dataserve.controller;

import com.park.annotation.Log;
import com.park.dataserve.entity.RoadSegment;
import com.park.dataserve.service.RoadSegmentService;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import com.park.vos.datavo.RoadSegmentAddVo;
import com.park.vos.datavo.RoadSegmentCoordinateVo;
import com.park.vos.datavo.RoadSegmentQueryVo;
import com.park.vos.datavo.RoadSegmentVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 路段信息 前端控制器
 * </p>
 *
 * @author lauxiaoyang
 * @since 2023-07-13
 */
@Api(tags = "路段管理处理模块")
@RestController
@RequestMapping("/road-segment")
public class RoadSegmentController {

    @Resource
    private RoadSegmentService roadSegmentService;



    /*
    分页查询
     */
    @Log
    @ApiOperation("路段管理模块分页查询")
    @PostMapping("/page/{pageNum}/{pageSize}")
    public Result<PageUtil<RoadSegmentVo>> queryPageRoadSegment(
            @PathVariable("pageNum") Integer pageNum,
            @PathVariable("pageSize") Integer pageSize,
            @RequestBody RoadSegmentQueryVo roadSegmentQueryVo
            ) {
        PageUtil<RoadSegmentVo> pageUtil = roadSegmentService.queryPageRoadSegment(pageNum, pageSize, roadSegmentQueryVo);
        return Result.success(ResultCode.SUCCESS, pageUtil);
    }

    /*
    根据路段id查询路段名称
     */
    @Log
    @ApiOperation("根据路段id查询路段名称")
    @GetMapping("/queryRoadSegmentById/{id}")
    public Result queryRoadSegmentById(@PathVariable Long id){
        RoadSegment roadSegment = roadSegmentService.queryRoadSegmentById(id);
        return Result.success(ResultCode.SUCCESS,roadSegment);
    }

    /*
    新增
     */
    @Log
    @ApiOperation("新增路段信息")
    @PostMapping("/addRoadSegment")
    public Result<RoadSegmentAddVo> addRoadSegment(@RequestBody RoadSegmentAddVo roadSegmentAddVo){
        boolean flag = roadSegmentService.addRoadSegment(roadSegmentAddVo);
        if (flag){
            return Result.success(ResultCode.SUCCESS);
            //return Result.success(ResultCode.SUCCESS,roadSegmentAddVo);添加不用返回值 加就加了
        }
        return Result.err(ResultCode.FAIL);
    }

    /*
    修改
     */
    @Log
    @ApiOperation("修改路段信息")
    @PostMapping("/updateRoadSegment")
    public Result updateRoadSegment(@RequestBody RoadSegment roadSegment){
        int flag = roadSegmentService.updateRoadSegment(roadSegment);
        if (flag >0){
            return Result.success(ResultCode.UPDATE_SUCCESS);
        }
        return Result.err(ResultCode.UPDATE_FAIL);
    }

    /*
    删除
     */
    @Log
    @ApiOperation("删除路段信息")
    @PostMapping("/deleteRoadSegmentById/{id}")
    public Result deleteRoadSegmentById(@PathVariable Long id){
        boolean flag = roadSegmentService.deleteRoadSegmentById(id);
        if (flag){
            return Result.success(ResultCode.SUCCESS);
        }else {
            return Result.err(ResultCode.DELETE_FAIL);
        }
    }

    /*
    根据经纬度查询路段信息
     */
//    @ApiOperation("根据经纬度查询路段信息")
//    @PostMapping("/queryRoadSegmentByCoordinates")
//    public Result<List<RoadSegmentVo>> queryRoadSegmentByCoordinates(@RequestBody RoadSegmentCoordinateVo roadSegmentCoordinateVo) {
//        List<RoadSegmentVo> roadSegments = roadSegmentService.queryRoadSegmentByCoordinates(roadSegmentCoordinateVo.getLongitude(), roadSegmentCoordinateVo.getLatitude());
//        return Result.success(roadSegments);
//    }

}

