package com.park.dataserve.controller;

import com.park.expection.MobileOrPasswordException;
import com.park.expection.NoIdempotentException;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestControllerAdvice
public class GlobalExceptionController {
    @ExceptionHandler
    public Result exception(Exception e) {
        e.printStackTrace();
        if (e instanceof MobileOrPasswordException) {
            return Result.send("50020","用户名或者密码错误",null);
        }
        if (e instanceof MethodArgumentNotValidException) {
            /*强转为MethodArgumentNotValidException*/
            MethodArgumentNotValidException mn = (MethodArgumentNotValidException) e;
            /*获取报错的message*/
            List<FieldError> errors =  mn.getBindingResult().getFieldErrors();
            /*把错误封装成Map集合返回*/
            //Map<String, Object> errorMap = errors.stream().collect(Collectors.toMap(item -> item.getField(), item -> item.getDefaultMessage()));
            HashMap<String, Object> map = new HashMap<>();
            for (FieldError error : errors) {
                map.put(error.getField(),error.getDefaultMessage());
            }
            return Result.send("50030","校验错误!!",map);
        }
        if (e instanceof NoIdempotentException) {
            return Result.send("50040","不允许重复提交", null);
        }

        return Result.err(ResultCode.EXCEPTION);
    }
}
