package com.park.dataserve.controller;


import com.park.annotation.Log;
import com.park.dataserve.entity.ChargingRule;
import com.park.dataserve.entity.ParkingPosition;
import com.park.dataserve.service.ChargingRuleService;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import com.park.vos.datavo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 收费规则表 前端控制器
 * </p>
 *
 * @author lauxiaoyang
 * @since 2023-07-18
 */
@Api(tags = "收费规则管理模块")
@RestController
@RequestMapping("/charging-rule")
public class ChargingRuleController {

    @Resource
    private ChargingRuleService chargingRuleService;


    /*
    分页查询
     */
    @Log
    @ApiOperation("收费规则管理模块分页查询")
    @PostMapping("/page/{pageNum}/{pageSize}")
    public Result<PageUtil<ChargingRuleVo>> queryPageChargingRule(
            @PathVariable("pageNum") Integer pageNum,
            @PathVariable("pageSize") Integer pageSize,
            @RequestBody ChargingRuleVo chargingRuleVo
    ) {
        PageUtil<ChargingRuleVo> pageUtil = chargingRuleService.queryPageChargingRule(pageNum, pageSize, chargingRuleVo);
        return Result.success(ResultCode.SUCCESS, pageUtil);
    }

    /*
    新增
     */
    @Log
    @ApiOperation("新增收费规则信息")
    @PostMapping("/addChargingRule")
    public Result addChargingRule(@RequestBody ChargingRuleVo chargingRuleVo){
        boolean flag = chargingRuleService.addChargingRule(chargingRuleVo);
        if (flag) {
            return Result.success(ResultCode.SUCCESS);
        }
        return Result.err(ResultCode.FAIL);
    }

    /*
    修改
     */
    @Log
    @ApiOperation("修改收费规则信息")
    @PutMapping("/updateChargingRule")
    public Result updateChargingRule(@RequestBody ChargingRule chargingRule){
        chargingRuleService.updateChargingRule(chargingRule);
        return Result.success(ResultCode.SUCCESS);
    }

    /*
    删除
     */
    @Log
    @ApiOperation("删除收费规则信息")
    @DeleteMapping("/deleteChargingRule/{id}")
    public Result<Boolean> deleteChargingRule(@PathVariable("id") Long id){
        boolean flag = chargingRuleService.deleteChargingRule(id);
        if(flag){
            return Result.success(ResultCode.SUCCESS, flag);
        }
        return Result.err(ResultCode.FAIL);
    }

    /*
     根据车辆驶入和驶出时间计算收费
     */
    @Log
    @ApiOperation(" 根据车辆驶入和驶出时间计算收费")
    @PostMapping("/calculateCharge")
    public Result<BigDecimal> calculateCharge(@RequestBody ChargingRuleQueryVo chargingRuleQueryVo){
        BigDecimal charge = chargingRuleService.calculateCharge(chargingRuleQueryVo);
        return Result.success(ResultCode.SUCCESS, charge);
    }

    /*
    根据泊位id查询收费信息 泊位ID：parking_position_id
     */
    @Log
    @ApiOperation("根据路段id查询收费信息")
    @PostMapping("/parkingPosition/{parkingPositionId}")
    public Result getChargingByParkingPositionId(@PathVariable("parkingPositionId") Long parkingPositionId) {
        List<ChargingRuleVo> chargingRuleVos = chargingRuleService.getChargingByParkingPositionId(parkingPositionId);
        return Result.success(chargingRuleVos);
    }

    /*
     根据车辆驶入和驶出时间计算收费
     */
    @Log
    @ApiOperation(" 30分钟一毛钱")
    @PostMapping("/sum")
    public Result<BigDecimal> sum(@RequestBody ChargingRuleQueryVo chargingRuleQueryVo){
        BigDecimal charge = chargingRuleService.sum(chargingRuleQueryVo);
        return Result.success(ResultCode.SUCCESS, charge);
    }
}

