package com.park.dataserve.controller;

import com.park.dataserve.service.PdaService;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import com.park.vos.datavo.PdaQueryVo;
import com.park.vos.datavo.PdaVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api(tags = "PDA模块")
@RestController
@RequestMapping("/pda")
public class PdaController {
    @Resource
    private PdaService pdaService;

    @ApiOperation("新增PDA")
    @PostMapping("/add")
    public Result pdaAdd(@RequestBody PdaVo pdaVo) {
       int flag = pdaService.pdaAdd(pdaVo);
       return Result.success(ResultCode.INSERT_SUCCESS, flag);
    }

    @ApiOperation("删除PDA")
    @DeleteMapping("/delete/{id}")
    public Result deletePdaById(@PathVariable("id") Long id) {
        int flag = pdaService.deletePdaById(id);
        return Result.success(ResultCode.DELETE_SUCCESS, flag);
    }

    @ApiOperation("修改PDA")
    @PutMapping("/put")
    public Result pdaPut(@RequestBody PdaVo pdaVo) {
        int flag = pdaService.pdaPut(pdaVo);
        return Result.success(ResultCode.UPDATE_SUCCESS, flag);
    }

    @ApiOperation("分页查询PDA")
    @PostMapping("/page/{pageNum}/{pageSize}")
    public Result queryPdaAll(@PathVariable("pageNum") Integer pageNum,
                              @PathVariable("pageSize") Integer pageSize,
                              @RequestBody PdaQueryVo pdaQueryVo) {
        PageUtil pageUtil = pdaService.queryPdaAll(pageNum, pageSize, pdaQueryVo);
        return Result.success(ResultCode.SUCCESS, pageUtil);
    }
}
