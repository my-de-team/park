package com.park.dataserve.controller;


import com.park.annotation.Log;
import com.park.dataserve.service.InspectorFaultService;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import com.park.vos.datavo.InspectorFaultAddVo;
import com.park.vos.datavo.InspectorFaultQueryVo;
import com.park.vos.datavo.InspectorFaultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 巡检员反馈故障表 前端控制器
 * </p>
 *
 * @author lxc
 * @since 2023-07-13
 */
@RestController
@RequestMapping("/data/inspector-fault")
@Api(tags = "巡检员反馈故障")
public class InspectorFaultController {
    @Autowired
    private InspectorFaultService inspectorFaultService;

    @PostMapping("/{pageNum}/{pageSize}")
    @ApiOperation("分页查询所有故障反馈")
    @Log
    public Result queryInspectorFault(@PathVariable("pageNum") Long pageNum,
                                      @PathVariable("pageSize") Long pageSize,
                                      @RequestBody InspectorFaultQueryVo inspectorFaultQueryVo) {
        PageUtil<InspectorFaultVo> pageUtil = inspectorFaultService.queryInspectorFault(pageNum, pageSize, inspectorFaultQueryVo);
        return Result.success(pageUtil);
    }

    @GetMapping("/{id}")
    @ApiOperation("根据id查询")
    @Log
    public Result queryInspectorFaultById(@PathVariable("id") Long id) {
        InspectorFaultVo inspectorFaultVo = inspectorFaultService.queryInspectorFaultById(id);
        return Result.success(inspectorFaultVo);
    }

    @PostMapping
    @ApiOperation("添加故障反馈")
    @Log
    public Result addInspectorFault(@RequestBody InspectorFaultAddVo inspectorFaultAddVo) {
        int flag = inspectorFaultService.addInspectorFault(inspectorFaultAddVo);
        return Result.success(ResultCode.SUCCESS);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("删除故障反馈")
    @Log
    public Result deleteInspectorFaultById(@PathVariable("id") Long id) {
        int flag = inspectorFaultService.deleteInspectorFaultById(id);
        return Result.success(ResultCode.SUCCESS);
    }

    @PutMapping
    @ApiOperation("更新故障反馈")
    @Log
    public Result updateInspectorFaultById(@RequestBody InspectorFaultVo inspectorFaultVo) {
        int flag = inspectorFaultService.updateInspectorFaultById(inspectorFaultVo);
        return Result.success(ResultCode.SUCCESS);
    }


}

