package com.park.dataserve.controller;

import com.park.dataserve.service.MagneticLogService;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(tags = "地磁日志模块")
@RestController
@RequestMapping("/magneticLog")
public class MagneticLogController {
    @Resource
    private MagneticLogService magneticLogService;

    //分页查询
    @ApiOperation("地磁日志分页查询")
    @PostMapping("/page/{pageNum}/{pageSize}/{magneticNumber}")
    public Result queryMagneticLogAll(@PathVariable("pageNum") Integer pageNum,
                                      @PathVariable("pageSize") Integer pageSize,
                                      @PathVariable("magneticNumber") String magneticNumber) {
        PageUtil pageUtil = magneticLogService.queryMagneticLogAll(pageNum, pageSize, magneticNumber);
        return Result.success(ResultCode.SUCCESS, pageUtil);
    }
}
