package com.park.dataserve.controller;

import com.park.annotation.IdempotentAnnotation;
import com.park.annotation.Log;
import com.park.api.DataControllerApi;
import com.park.dataserve.service.PeopleService;
import com.park.expection.MobileCodeErrorException;
import com.park.expection.MobileOrPasswordException;
import com.park.expection.RegisterMobileRepeatException;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import com.park.vos.datavo.PeopleLoginVo;
import com.park.vos.datavo.PeopleVo;
import com.park.vos.datavo.QueryPeopleVo;
import com.park.vos.datavo.RegisterVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

@Api(tags = "员工管理模块")
@RestController
@RequestMapping("/people")
public class PeopleController {

    @Resource
    private PeopleService peopleService;


    //分页查询
    @ApiOperation("巡检员分页查询")
    @PostMapping("/page/inspector/{pageNum}/{pageSize}")
    @Log
    public Result queryPeopleAll(@PathVariable("pageNum") Integer pageNum,
                                 @PathVariable("pageSize") Integer pageSize,
                                 @RequestBody QueryPeopleVo queryPeopleVo) {
        PageUtil pageUtil = peopleService.queryPeopleAll(pageNum, pageSize, queryPeopleVo);
        return Result.success(ResultCode.SUCCESS, pageUtil);
    }

    @ApiOperation("运维人员分页查询")
    @PostMapping("/page/maintenance/{pageNum}/{pageSize}")
    @Log
    public Result queryMaintenanceAll(@PathVariable("pageNum") Integer pageNum,
                                      @PathVariable("pageSize") Integer pageSize,
                                      @RequestBody QueryPeopleVo queryPeopleVo) {
        PageUtil pageUtil = peopleService.queryMaintenanceAll(pageNum, pageSize, queryPeopleVo);
        return Result.success(ResultCode.SUCCESS, pageUtil);
    }

    //根据id查询巡检员名
    @ApiOperation("根据id查询巡检员名")
    @PostMapping("/select/{id}")
    @Log
    public Result queryPeoPleNameById(@PathVariable("id") Long id) {
        PeopleVo peopleVo = peopleService.queryPeopleNameById(id , 0);
        return Result.success(ResultCode.SUCCESS, peopleVo);
    }

    @ApiOperation("新增员工")
    @PostMapping("/inspector/add")
    @Log
    public Result addInspector(@RequestBody @Valid PeopleVo peopleVo) {
        int flag = peopleService.addInspector(peopleVo);
        return Result.success(ResultCode.SUCCESS, flag);
    }

    @ApiOperation("删除员工")
    @DeleteMapping("/delete/{id}")
    @Log
    public Result deletePeopleById(@PathVariable("id") Long id) {
        int flag = peopleService.deletePeopleById(id);
        return Result.success(ResultCode.DELETE_SUCCESS, flag);
    }

    @ApiOperation("修改员工")
    @PutMapping("/inspector/put")
    @Log
    public Result putInspector(@RequestBody @Valid PeopleVo peopleVo) {
        int flag = peopleService.putInspector(peopleVo);
        return Result.success(ResultCode.UPDATE_SUCCESS, flag);
    }

    /**
     * 注册接口
     */
    @PostMapping("/register")
    @IdempotentAnnotation
    @ApiOperation("注册")
    @Log
    public Result register(@RequestBody @Valid RegisterVo registerVo) throws RegisterMobileRepeatException, MobileCodeErrorException, InterruptedException, MobileCodeErrorException {
        /**
         * 注册
         */
        int flag = peopleService.register(registerVo);
        if (flag > 0) {
            return Result.success(ResultCode.SUCCESS);
        } else {
            return Result.err(ResultCode.FAIL);
        }
    }

    @ApiOperation("员工登录")
    @PostMapping("/login")
    public Result<String> peopleLogin(@RequestBody PeopleLoginVo peopleLoginVo) throws MobileOrPasswordException {
        String token = peopleService.peopleLogin(peopleLoginVo);
        Result<String> result = Result.success(token);
        System.out.println("result = " + result);
        return result;
    }
}
