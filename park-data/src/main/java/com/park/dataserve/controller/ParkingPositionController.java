package com.park.dataserve.controller;
import com.park.annotation.Log;
import com.park.dataserve.entity.ParkingPosition;
import com.park.dataserve.entity.RoadSegment;
import com.park.dataserve.service.ParkingPositionService;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import com.park.vos.datavo.ParkingPositionQueryVo;
import com.park.vos.datavo.ParkingPositionVo;
import com.park.vos.datavo.RoadSegmentCoordinateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 泊位信息 前端控制器
 * </p>
 *
 * @author lauxiaoyang
 * @since 2023-07-14
 */
@Api(tags = "泊位管理处理模块")
@RestController
@RequestMapping("/parking-position")
public class ParkingPositionController {

    @Resource
    private ParkingPositionService parkingPositionService;

    /*
    分页查询
    */
    @Log
    @ApiOperation("泊位管理模块分页查询")
    @PostMapping("/page/{pageNum}/{pageSize}")
    public Result<PageUtil<ParkingPositionVo>> queryPageParkingPosition(
            @PathVariable("pageNum") Integer pageNum,
            @PathVariable("pageSize") Integer pageSize,
            @RequestBody ParkingPositionVo parkingPositionVo){
        PageUtil<ParkingPositionVo> pageUtil = parkingPositionService.queryPageParkingPosition(pageNum, pageSize, parkingPositionVo);
        return Result.success(ResultCode.SUCCESS,pageUtil);
    }


    /*
     根据泊位编号查询泊位id
      */
    @Log
    @ApiOperation("根据泊位编号查询泊位id")
    @PostMapping("/queryBerthNumber")
    public Result queryBerthNumber(@RequestParam("berthNumber") String berthNumber){
        Long id = parkingPositionService.getByBerthNumber(berthNumber);
        return Result.success(ResultCode.SUCCESS, id.toString());
    }

    /*
    新增
     */
    @Log
    @ApiOperation("新增泊位信息")
    @PostMapping("/addPageParkingPosition")
    public Result addPageParkingPosition(@RequestBody ParkingPositionVo parkingPositionVo){
        boolean flag = parkingPositionService.addPageParkingPosition(parkingPositionVo);
        if (flag) {
            return Result.success(ResultCode.SUCCESS);
        }
        return Result.err(ResultCode.FAIL);
    }

    /*
    修改
     */
    @Log
    @ApiOperation("修改泊位信息")
    @PutMapping("/updatePageParkingPosition")
    public Result updatePageParkingPosition(@RequestBody ParkingPosition position){
        parkingPositionService.updatePageParkingPosition(position);
        return Result.success(ResultCode.SUCCESS);
    }

    /*
    删除
     */
    @Log
    @ApiOperation("删除泊位信息")
    @DeleteMapping("/deletePageParkingPosition/{id}")
    public Result<Boolean> deletePageParkingPosition(@PathVariable("id") Long id){
        boolean flag = parkingPositionService.deletePageParkingPosition(id);
        if(flag){
            return Result.success(ResultCode.SUCCESS, flag);
        }
        return Result.err(ResultCode.FAIL);
    }


    /*
    根据路段id查询泊位信息 路段ID：road_segment_id
     */
    @Log
    @ApiOperation("根据路段id查询泊位信息")
    @PostMapping("/roadSegment/{roadSegmentId}")
    public Result getParkingByRoadSegmentId(@PathVariable("roadSegmentId") Long roadSegmentId) {
        List<ParkingPositionQueryVo>  parkingPositionQueryVoList = parkingPositionService.getParkingByRoadSegmentId(roadSegmentId);
        return Result.success(parkingPositionQueryVoList);
    }


}

