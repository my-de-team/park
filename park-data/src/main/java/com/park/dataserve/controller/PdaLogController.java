package com.park.dataserve.controller;

import com.park.dataserve.service.PdaLogService;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import com.park.vos.datavo.PdaLogQueryVo;
import com.park.vos.datavo.PdaQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/pdaLog")
@Api(tags = "pda日志模块")
public class PdaLogController {
    @Resource
    private PdaLogService pdaLogService;

    @ApiOperation("分页查询PDA登录日志")
    @PostMapping("/page/{pageNum}/{pageSize}")
    public Result queryPdaAll(@PathVariable("pageNum") Integer pageNum,
                              @PathVariable("pageSize") Integer pageSize,
                              @RequestBody PdaLogQueryVo pdaLogQueryVo) {
        PageUtil pageUtil = pdaLogService.queryPdaLogAll(pageNum, pageSize, pdaLogQueryVo);
        return Result.success(ResultCode.SUCCESS, pageUtil);
    }

}
