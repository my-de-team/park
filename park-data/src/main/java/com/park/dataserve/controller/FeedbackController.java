package com.park.dataserve.controller;


import com.park.annotation.Log;
import com.park.dataserve.service.FeedbackService;
import com.park.dataserve.service.InspectorFaultService;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import com.park.vos.datavo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.bind.BindResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 用户反馈表 前端控制器
 * </p>
 *
 * @author lxc
 * @since 2023-07-15
 */
@RestController
@RequestMapping("/feedback")
@Api(tags = "用户反馈")
public class FeedbackController {
    @Autowired
    private FeedbackService feedbackService;

    @PostMapping("/{pageNum}/{pageSize}")
    @ApiOperation("分页查询所有用户反馈")
    @Log
    public Result queryFeedback(@PathVariable("pageNum") Long pageNum,
                                      @PathVariable("pageSize") Long pageSize,
                                      @RequestBody FeedbackQueryVo feedbackQueryVo) {
        PageUtil<FeedbackVo> pageUtil = feedbackService.queryFeedback(pageNum, pageSize, feedbackQueryVo);
        return Result.success(pageUtil);
    }

    @GetMapping("/{id}")
    @ApiOperation("根据id查询")
    @Log
    public Result queryFeedbackById(@PathVariable("id") Long id) {
        FeedbackVo feedbackVo = feedbackService.queryFeedbackById(id);
        return Result.success(feedbackVo);
    }

    @PostMapping
    @ApiOperation("添加用户反馈")
    @Log
    public Result addFeedback(@RequestBody @Valid FeedbackAddVo feedbackAddVo) {
        int flag = feedbackService.addFeedback(feedbackAddVo);
        return Result.success(ResultCode.SUCCESS);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("删除用户反馈")
    @Log
    public Result deleteFeedbackById(@PathVariable("id") Long id) {
        int flag = feedbackService.deleteFeedbackById(id);
        return Result.success(ResultCode.SUCCESS);
    }

    @PutMapping
    @ApiOperation("更新用户反馈")
    @Log
    public Result updateFeedbackById(@RequestBody FeedbackVo feedbackVo) {
        int flag = feedbackService.updateFeedbackById(feedbackVo);
        return Result.success(ResultCode.SUCCESS);
    }


}

