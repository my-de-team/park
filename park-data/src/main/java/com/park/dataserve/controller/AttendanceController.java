package com.park.dataserve.controller;


import com.park.annotation.Log;
import com.park.dataserve.service.AttendanceService;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import com.park.vos.datavo.AddAttendanceQueryVo;
import com.park.vos.datavo.AddAttendanceVo;
import com.park.vos.datavo.AttendanceVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.bind.BindResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 考勤表 前端控制器
 * </p>
 *
 * @author lxc
 * @since 2023-07-13
 */
@RestController
@RequestMapping("/data/attendance")
@Api(tags = "打卡")
public class AttendanceController {
    @Autowired
    private AttendanceService attendanceService;

    @PostMapping("/clock")
    @ApiOperation("打卡接口")
    @Log
    public Result clock(@RequestBody @Valid AddAttendanceVo addAttendanceVo) {
        int flag = attendanceService.AddAttendance(addAttendanceVo);
        return Result.success(ResultCode.SUCCESS, null);
    }

    @PostMapping("/query")
    @ApiOperation("根据条件查询打卡")
    @Log
    public Result queryAllAttendanceRecords(@RequestBody AddAttendanceQueryVo addAttendanceQueryVo) {
        List<AttendanceVo> attendanceVoList = attendanceService.queryAttendanceRecords(addAttendanceQueryVo);
        return Result.success(attendanceVoList);
    }

}

