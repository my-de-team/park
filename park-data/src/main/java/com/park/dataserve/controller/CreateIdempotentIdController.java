package com.park.dataserve.controller;

import com.park.dataserve.service.CreateIdempotentIdService;
import com.park.utils.Result;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Api(tags = "创建接口幂等id")
public class CreateIdempotentIdController {
    @Resource
    private CreateIdempotentIdService idempotentIdService;

    @GetMapping("/idempotent/create")
    public Result create() {
        String id = idempotentIdService.create();
        return Result.success(id);
    }
}
