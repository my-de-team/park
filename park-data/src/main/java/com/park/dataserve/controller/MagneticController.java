package com.park.dataserve.controller;

import com.park.dataserve.service.MagneticService;
import com.park.expection.MagneticExpection;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import com.park.vos.datavo.MagneticVo;
import com.park.vos.datavo.QueryMagneticVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api(tags = "地磁管理模块")
@RestController
@RequestMapping("/magnetic")
public class MagneticController {
    @Resource
    private MagneticService magneticService;

    //新增地磁管理
    @ApiOperation("新增地磁")
    @PostMapping("/add")
    public Result magneticAdd(@RequestBody MagneticVo magneticVo) throws MagneticExpection {
        int flag = magneticService.magneticAdd(magneticVo);
        if (flag > 0) {
            return Result.success(ResultCode.INSERT_SUCCESS);
        } else {
            return Result.err(ResultCode.INSERT_FAIL);
        }
    }

    //根据id删除地磁管理
    @ApiOperation("根据id删除地磁")
    @DeleteMapping("/delete/{id}")
    public Result deleteMagneticById(@PathVariable("id") Long id) {
        int flag = magneticService.deleteMagneticById(id);
        if (flag > 0) {
            return Result.success(ResultCode.DELETE_SUCCESS);
        } else {
            return Result.err(ResultCode.DELETE_FAIL);
        }
    }

    //修改地磁管理
    @ApiOperation("修改地磁管理")
    @PutMapping("/put")
    public Result magneticPut(@RequestBody MagneticVo magneticVo) throws MagneticExpection {
        int flag = magneticService.magneticPut(magneticVo);
        if (flag > 0) {
            return Result.success(ResultCode.UPDATE_SUCCESS);
        } else {
            return Result.err(ResultCode.UPDATE_FAIL);
        }
    }

    //分页查询地磁管理、条件查询
    @ApiOperation("分页查询、条件查询")
    @PostMapping("/page/{pageNum}/{pageSize}")
    public Result queryMagneticAll(@PathVariable("pageNum") Integer pageNum,
                                   @PathVariable("pageSize") Integer pageSize,
                                   @RequestBody QueryMagneticVo queryMagneticVo) {
        PageUtil pageUtil = magneticService.queryMagneticAll(pageNum, pageSize, queryMagneticVo);
        return Result.success(ResultCode.SUCCESS, pageUtil);
    }
}
