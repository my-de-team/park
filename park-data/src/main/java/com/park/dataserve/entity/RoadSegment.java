package com.park.dataserve.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import jdk.nashorn.internal.objects.annotations.Getter;
import jdk.nashorn.internal.objects.annotations.Setter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * <p>
 * 路段信息
 * </p>
 *
 * @author lauxiaoyang
 * @since 2023-07-13
 */
@TableName("tp_road_segment")
@NoArgsConstructor
@AllArgsConstructor
@Data
@ApiModel(value = "RoadSegment对象", description = "路段信息")
public class RoadSegment implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 路段id
     */
    @ApiModelProperty("路段id")
    @TableId //(value = "id", type = IdType.AUTO)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 泊位路段名称
     */
    @ApiModelProperty("泊位路段名称")
    private String segmentName;

    /**
     * 路段类型
     */
    @ApiModelProperty("路段类型")
    private String segmentType;

    /**
     * 路段编号 road_segment
     */
    @ApiModelProperty("路段类型")
    private String roadSegment;


    /**
     * 所属地区
     */
    @ApiModelProperty("所属地区")
    private String region;

    /**
     * 泊位数量
     */
    @ApiModelProperty("泊位数量")
    private Integer parkingSpots;

    /**
     * 限制泊位数
     */
    @ApiModelProperty("限制泊位数")
    private Integer restrictedSpots;

    /**
     * 巡检员
     */
    @ApiModelProperty("巡检员")
    private String inspector;

    /**
     * 运维人员ID
     */
    @ApiModelProperty("运维人员ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long maintenanceStaff;


    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @ApiModelProperty("更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    @ApiModelProperty("备用字段1")
    private String Field1;

    @ApiModelProperty("备用字段2")
    private String Field2;

    @ApiModelProperty("备用字段3")
    private String Field3;

    @ApiModelProperty("乐观锁")
    private Integer version;

    @ApiModelProperty("逻辑删除")
    @TableLogic
    private Integer isDeleted;

    /**
     * 经度
     */
    @ApiModelProperty("经度")
    private String longitude;

    /**
     * 纬度
     */
    @ApiModelProperty("纬度")
    private String latitude;


/*    @JsonSerialize(using = ToStringSerializer.class)
    private Long inspectorId;*/

}
