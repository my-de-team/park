package com.park.dataserve.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @TableName tp_magnetic
 */
@TableName(value ="tp_magnetic")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Magnetic implements Serializable {
    /**
     * 主键
     */
    @TableId
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 地磁编号
     */
    private String magneticNumber;

    /**
     * 地磁名称
     */
    private String magneticName;

    /**
     * 所属路段
     */
    private String road;

    /**
     * 绑定泊位
     */
    private String berthageBind;

    /**
     * 泊位情况,0:有车1:无车
     */
    private Integer berthageCondition;

    /**
     * 地磁状态,0:未激活1:在线2:离线
     */
    private Integer state;

    /**
     * 状态更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private Date updateTime;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 1表示删除，0表示没有删除,逻辑删除
     */
    private Integer isDeleted;

    /**
     * 乐观锁
     */
    private Integer version;

    /**
     * 备用字段1
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long roadId;

    /**
     * 备用字段2
     */
    private String filed2;

    /**
     * 备用字段3
     */
    private String filed3;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Magnetic other = (Magnetic) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getMagneticNumber() == null ? other.getMagneticNumber() == null : this.getMagneticNumber().equals(other.getMagneticNumber()))
            && (this.getMagneticName() == null ? other.getMagneticName() == null : this.getMagneticName().equals(other.getMagneticName()))
            && (this.getRoad() == null ? other.getRoad() == null : this.getRoad().equals(other.getRoad()))
            && (this.getBerthageBind() == null ? other.getBerthageBind() == null : this.getBerthageBind().equals(other.getBerthageBind()))
            && (this.getBerthageCondition() == null ? other.getBerthageCondition() == null : this.getBerthageCondition().equals(other.getBerthageCondition()))
            && (this.getState() == null ? other.getState() == null : this.getState().equals(other.getState()))
            && (this.getUpdateTime() == null ? other.getUpdateTime() == null : this.getUpdateTime().equals(other.getUpdateTime()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getIsDeleted() == null ? other.getIsDeleted() == null : this.getIsDeleted().equals(other.getIsDeleted()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()))
            && (this.getRoadId() == null ? other.getRoadId() == null : this.getRoadId().equals(other.getRoadId()))
            && (this.getFiled2() == null ? other.getFiled2() == null : this.getFiled2().equals(other.getFiled2()))
            && (this.getFiled3() == null ? other.getFiled3() == null : this.getFiled3().equals(other.getFiled3()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getMagneticNumber() == null) ? 0 : getMagneticNumber().hashCode());
        result = prime * result + ((getMagneticName() == null) ? 0 : getMagneticName().hashCode());
        result = prime * result + ((getRoad() == null) ? 0 : getRoad().hashCode());
        result = prime * result + ((getBerthageBind() == null) ? 0 : getBerthageBind().hashCode());
        result = prime * result + ((getBerthageCondition() == null) ? 0 : getBerthageCondition().hashCode());
        result = prime * result + ((getState() == null) ? 0 : getState().hashCode());
        result = prime * result + ((getUpdateTime() == null) ? 0 : getUpdateTime().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        result = prime * result + ((getRoadId() == null) ? 0 : getRoadId().hashCode());
        result = prime * result + ((getFiled2() == null) ? 0 : getFiled2().hashCode());
        result = prime * result + ((getFiled3() == null) ? 0 : getFiled3().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", magneticNumber=").append(magneticNumber);
        sb.append(", magneticName=").append(magneticName);
        sb.append(", road=").append(road);
        sb.append(", berthageBind=").append(berthageBind);
        sb.append(", berthageCondition=").append(berthageCondition);
        sb.append(", state=").append(state);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", createTime=").append(createTime);
        sb.append(", isDeleted=").append(isDeleted);
        sb.append(", version=").append(version);
        sb.append(", roadId=").append(roadId);
        sb.append(", filed2=").append(filed2);
        sb.append(", filed3=").append(filed3);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}