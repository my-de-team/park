package com.park.dataserve.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * PDA登录日志表
 *
 * @TableName tp_pda_log
 */
@TableName(value = "tp_pda_log")
@Data
public class PdaLog implements Serializable {
    /**
     * 主键
     */
    @TableId
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 设备编号
     */
    private String deviceNumber;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 登录人
     */
    private String login;

    /**
     * 路段id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long roadId;

    /**
     * 1表示删除，0表示没有删除,逻辑删除
     */
    @TableField(fill = FieldFill.INSERT)
    private Integer isDeleted;

    /**
     * 乐观锁
     */
    private Integer version;

    /**
     * 备用字段1
     */
    private String parkFiled1;

    /**
     * 备用字段2
     */
    private String parkFiled2;

    /**
     * 备用字段3
     */
    private String parkFiled3;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        PdaLog other = (PdaLog) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getDeviceNumber() == null ? other.getDeviceNumber() == null : this.getDeviceNumber().equals(other.getDeviceNumber()))
                && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
                && (this.getLogin() == null ? other.getLogin() == null : this.getLogin().equals(other.getLogin()))
                && (this.getRoadId() == null ? other.getRoadId() == null : this.getRoadId().equals(other.getRoadId()))
                && (this.getIsDeleted() == null ? other.getIsDeleted() == null : this.getIsDeleted().equals(other.getIsDeleted()))
                && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()))
                && (this.getParkFiled1() == null ? other.getParkFiled1() == null : this.getParkFiled1().equals(other.getParkFiled1()))
                && (this.getParkFiled2() == null ? other.getParkFiled2() == null : this.getParkFiled2().equals(other.getParkFiled2()))
                && (this.getParkFiled3() == null ? other.getParkFiled3() == null : this.getParkFiled3().equals(other.getParkFiled3()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getDeviceNumber() == null) ? 0 : getDeviceNumber().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getLogin() == null) ? 0 : getLogin().hashCode());
        result = prime * result + ((getRoadId() == null) ? 0 : getRoadId().hashCode());
        result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        result = prime * result + ((getParkFiled1() == null) ? 0 : getParkFiled1().hashCode());
        result = prime * result + ((getParkFiled2() == null) ? 0 : getParkFiled2().hashCode());
        result = prime * result + ((getParkFiled3() == null) ? 0 : getParkFiled3().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", deviceNumber=").append(deviceNumber);
        sb.append(", createTime=").append(createTime);
        sb.append(", login=").append(login);
        sb.append(", roadId=").append(roadId);
        sb.append(", isDeleted=").append(isDeleted);
        sb.append(", version=").append(version);
        sb.append(", parkFiled1=").append(parkFiled1);
        sb.append(", parkFiled2=").append(parkFiled2);
        sb.append(", parkFiled3=").append(parkFiled3);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}