package com.park.dataserve.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * <p>
 * 泊位信息
 * </p>
 *
 * @author lauxiaoyang
 * @since 2023-07-14
 */
@TableName("tp_parking_position")
@NoArgsConstructor
@AllArgsConstructor
@Data
@ApiModel(value = "ParkingPosition对象", description = "泊位信息")
public class ParkingPosition implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 泊位id
     */
    @ApiModelProperty("泊位id")
    @TableId
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 泊车路段ID
     */
    @ApiModelProperty("泊车路段ID")
    private Long roadSegmentId;

    /**
     * 泊位名称
     */
    @ApiModelProperty("泊位名称")
    private String parkingName;

    /**
     * 泊位编号
     */
    @ApiModelProperty("泊位编号")
    private String berthNumber;

    /**
     * 地磁编号
     */
    @ApiModelProperty("地磁编号")
    private String magneticNumber;

    /**
     * 泊位状态：1.有车 2.无车 3.未激活
     */
    @ApiModelProperty("泊位状态：1.有车 2.无车 3.未激活")
    private Integer state;

    /**
     * 激活时间
     */
    @ApiModelProperty("激活时间")
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date activationTime;


    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @ApiModelProperty("备用字段1")
    private String Field1;

    @ApiModelProperty("备用字段2")
    private String Field2;

    @ApiModelProperty("备用字段3")
    private String Field3;

    @ApiModelProperty("乐观锁,默认1")
    private Integer version;

    @ApiModelProperty("逻辑删除")
    @TableLogic
    private Integer isDeleted;

    @ApiModelProperty("更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;



}
