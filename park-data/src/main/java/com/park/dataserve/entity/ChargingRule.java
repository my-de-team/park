package com.park.dataserve.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * <p>
 * 收费规则表
 * </p>
 *
 * @author lauxiaoyang
 * @since 2023-07-18
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@TableName("tp_charging_rule")
@ApiModel(value = "ChargingRule对象", description = "收费规则表")
public class ChargingRule implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("收费规则表ID")
    @TableId//(value = "id", type = IdType.AUTO)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty("路段ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long roadSegmentId;

    @ApiModelProperty("泊位ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parkingPositionId;

    @ApiModelProperty("是否工作日：1-是，0-否")
    private Integer isWeekday;

    @ApiModelProperty("是否繁忙时间段：1-是，0-否")
    private Integer isBusyTime;

    @ApiModelProperty("起始时间")
    private String enterTime;

    @ApiModelProperty("结束时间")
    private String exitTime;

    @ApiModelProperty("每小时收费率（收费标准）")
    private BigDecimal ratePerHour;

    @ApiModelProperty("最低计费时长（分钟）")
    private Integer minDuration;

    @ApiModelProperty("最高计费时长（分钟）")
    private Integer maxDuration;

    @ApiModelProperty("免费时长（分钟）")
    private Integer freeDuration;

    @ApiModelProperty("计费是否包含免费时长状态：1-是，2-否")
    private Integer state;

    @ApiModelProperty("封顶金额")
    private BigDecimal ceilingPrice;

    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @ApiModelProperty("更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    @ApiModelProperty("备用字段1")
    private String field1;

    @ApiModelProperty("备用字段2")
    private String field2;

    @ApiModelProperty("备用字段3")
    private String field3;

    @ApiModelProperty("乐观锁，默认1")
    private Integer version;

    @ApiModelProperty("逻辑删除")
    @TableLogic
    private Integer isDeleted;


}
