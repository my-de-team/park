package com.park.dataserve.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 员工管理表
 * @TableName tp_people
 */
@TableName(value ="tp_people")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class People implements Serializable {
    /**
     * 主键
     */
    @TableId
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 员工名称
     */
    private String employeeName;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 密码
     */
    private String password;

    /**
     * 管理路段
     */
    private String road;

    /**
     * 执勤时间
     */
    private String dutyTime;

    /**
     * 订单完成率
     */
    private String orderRate;

    /**
     * 所属分组
     */
    private String owningGroup;

    /**
     * 角色状态,0:巡检员1:运维人员
     */
    private Integer roleState;

    /**
     * 状态,0:正常
     */
    private Integer state;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private String createTime;

    /**
     * 1表示删除，0表示没有删除,逻辑删除
     */
    private Integer isDeleted;

    /**
     * 乐观锁
     */
    private Integer version;

    /**
     * 备用字段1
     */
    private String filed1;

    /**
     * 备用字段2
     */
    private String filed2;

    /**
     * 备用字段3
     */
    private String filed3;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        People other = (People) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getEmployeeName() == null ? other.getEmployeeName() == null : this.getEmployeeName().equals(other.getEmployeeName()))
            && (this.getPhone() == null ? other.getPhone() == null : this.getPhone().equals(other.getPhone()))
            && (this.getPassword() == null ? other.getPassword() == null : this.getPassword().equals(other.getPassword()))
            && (this.getRoad() == null ? other.getRoad() == null : this.getRoad().equals(other.getRoad()))
            && (this.getDutyTime() == null ? other.getDutyTime() == null : this.getDutyTime().equals(other.getDutyTime()))
            && (this.getOrderRate() == null ? other.getOrderRate() == null : this.getOrderRate().equals(other.getOrderRate()))
            && (this.getOwningGroup() == null ? other.getOwningGroup() == null : this.getOwningGroup().equals(other.getOwningGroup()))
            && (this.getRoleState() == null ? other.getRoleState() == null : this.getRoleState().equals(other.getRoleState()))
            && (this.getState() == null ? other.getState() == null : this.getState().equals(other.getState()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getIsDeleted() == null ? other.getIsDeleted() == null : this.getIsDeleted().equals(other.getIsDeleted()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()))
            && (this.getFiled1() == null ? other.getFiled1() == null : this.getFiled1().equals(other.getFiled1()))
            && (this.getFiled2() == null ? other.getFiled2() == null : this.getFiled2().equals(other.getFiled2()))
            && (this.getFiled3() == null ? other.getFiled3() == null : this.getFiled3().equals(other.getFiled3()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getEmployeeName() == null) ? 0 : getEmployeeName().hashCode());
        result = prime * result + ((getPhone() == null) ? 0 : getPhone().hashCode());
        result = prime * result + ((getPassword() == null) ? 0 : getPassword().hashCode());
        result = prime * result + ((getRoad() == null) ? 0 : getRoad().hashCode());
        result = prime * result + ((getDutyTime() == null) ? 0 : getDutyTime().hashCode());
        result = prime * result + ((getOrderRate() == null) ? 0 : getOrderRate().hashCode());
        result = prime * result + ((getOwningGroup() == null) ? 0 : getOwningGroup().hashCode());
        result = prime * result + ((getRoleState() == null) ? 0 : getRoleState().hashCode());
        result = prime * result + ((getState() == null) ? 0 : getState().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        result = prime * result + ((getFiled1() == null) ? 0 : getFiled1().hashCode());
        result = prime * result + ((getFiled2() == null) ? 0 : getFiled2().hashCode());
        result = prime * result + ((getFiled3() == null) ? 0 : getFiled3().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", employeeName=").append(employeeName);
        sb.append(", phone=").append(phone);
        sb.append(", password=").append(password);
        sb.append(", road=").append(road);
        sb.append(", dutyTime=").append(dutyTime);
        sb.append(", orderRate=").append(orderRate);
        sb.append(", owningGroup=").append(owningGroup);
        sb.append(", roleState=").append(roleState);
        sb.append(", state=").append(state);
        sb.append(", createTime=").append(createTime);
        sb.append(", isDeleted=").append(isDeleted);
        sb.append(", version=").append(version);
        sb.append(", filed1=").append(filed1);
        sb.append(", filed2=").append(filed2);
        sb.append(", filed3=").append(filed3);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}