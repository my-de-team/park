package com.park.dataserve.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 地磁日志表
 * @TableName tp_magnetic_log
 */
@TableName(value ="tp_magnetic_log")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class MagneticLog implements Serializable {
    /**
     * 主键
     */
    @TableId
    private Long id;

    /**
     * 地磁编号
     */
    private String magneticNumber;

    /**
     * 时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 泊位状态,0:驶入1:驶出2:故障
     */
    private Integer parkState;

    /**
     * 地磁状态,0:在线1：离线
     */
    private Integer magneticStatus;

    /**
     * 1表示删除，0表示没有删除,逻辑删除
     */
    private Integer isDeleted;

    /**
     * 乐观锁
     */
    private Integer version;

    /**
     * 备用字段1
     */
    private String filed1;

    /**
     * 备用字段2
     */
    private String filed2;

    /**
     * 备用字段3
     */
    private String filed3;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        MagneticLog other = (MagneticLog) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getMagneticNumber() == null ? other.getMagneticNumber() == null : this.getMagneticNumber().equals(other.getMagneticNumber()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getParkState() == null ? other.getParkState() == null : this.getParkState().equals(other.getParkState()))
            && (this.getMagneticStatus() == null ? other.getMagneticStatus() == null : this.getMagneticStatus().equals(other.getMagneticStatus()))
            && (this.getIsDeleted() == null ? other.getIsDeleted() == null : this.getIsDeleted().equals(other.getIsDeleted()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()))
            && (this.getFiled1() == null ? other.getFiled1() == null : this.getFiled1().equals(other.getFiled1()))
            && (this.getFiled2() == null ? other.getFiled2() == null : this.getFiled2().equals(other.getFiled2()))
            && (this.getFiled3() == null ? other.getFiled3() == null : this.getFiled3().equals(other.getFiled3()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getMagneticNumber() == null) ? 0 : getMagneticNumber().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getParkState() == null) ? 0 : getParkState().hashCode());
        result = prime * result + ((getMagneticStatus() == null) ? 0 : getMagneticStatus().hashCode());
        result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        result = prime * result + ((getFiled1() == null) ? 0 : getFiled1().hashCode());
        result = prime * result + ((getFiled2() == null) ? 0 : getFiled2().hashCode());
        result = prime * result + ((getFiled3() == null) ? 0 : getFiled3().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", magneticNumber=").append(magneticNumber);
        sb.append(", createTime=").append(createTime);
        sb.append(", parkState=").append(parkState);
        sb.append(", magneticStatus=").append(magneticStatus);
        sb.append(", isDeleted=").append(isDeleted);
        sb.append(", version=").append(version);
        sb.append(", filed1=").append(filed1);
        sb.append(", filed2=").append(filed2);
        sb.append(", filed3=").append(filed3);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}