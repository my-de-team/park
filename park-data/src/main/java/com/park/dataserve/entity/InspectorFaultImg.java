package com.park.dataserve.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * <p>
 * 巡检员故障反馈表图片
 * </p>
 *
 * @author lxc
 * @since 2023-07-13
 */
@TableName("tp_inspector_fault_img")
@NoArgsConstructor
@AllArgsConstructor
@Data
@ApiModel(value = "InspectorFaultImg对象", description = "巡检员故障反馈表图片")
public class InspectorFaultImg implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty("图片路径")
    private String imgUrl;

    @ApiModelProperty("故障反馈表id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long inspectorFaultId;

    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty("修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @ApiModelProperty("备用字段1")
    private String filed1;

    @ApiModelProperty("备用字段2")
    private String filed2;

    @ApiModelProperty("备用字段3")
    private String filed3;

    @ApiModelProperty("乐观锁版本号")
    private Integer version;

    @ApiModelProperty("逻辑删除")
    @TableLogic
    private Integer isDeleted;


}
