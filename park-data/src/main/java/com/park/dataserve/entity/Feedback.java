package com.park.dataserve.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.*;

/**
 * <p>
 * 用户反馈表
 * </p>
 *
 * @author lxc
 * @since 2023-07-15
 */
@TableName("tp_feedback")
@NoArgsConstructor
@AllArgsConstructor
@Data
@ApiModel(value = "Feedback对象", description = "用户反馈表")
public class Feedback implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @JsonSerialize(using = ToStringSerializer.class)
      private Long id;

    @ApiModelProperty("反馈单号")
    private String feedbackNo;

    @ApiModelProperty("反馈时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date feedbackTime;

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty("处理结果")
    private String processingResult;

    @ApiModelProperty("反馈内容")
    private String feedbackContent;

    @ApiModelProperty("图片数量")
    private Integer imgCount;

    @ApiModelProperty("处理时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date processingTime;

    @ApiModelProperty("0：待处理， 1：已处理")
    private Integer state;

    @ApiModelProperty("创建时间")
      @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @ApiModelProperty("创建时间")
      @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @ApiModelProperty("备用字段1")
    private String filed1;

    @ApiModelProperty("备用字段2")
    private String filed2;

    @ApiModelProperty("备用字段3")
    private String filed3;

    @ApiModelProperty("乐观锁版本号")
    private Integer version;

    @ApiModelProperty("逻辑删除")
    @TableLogic
    private Integer isDeleted;


}
