package com.park.dataserve.mqConsumer;

import com.park.dataserve.service.PdaLogService;
import com.park.dataserve.service.PdaService;
import com.park.vos.datavo.PdaLogAddVo;
import com.rabbitmq.client.Channel;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;

@Component
public class PdaLogConsumer {
    @Resource
    private PdaLogService pdaLogService;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "pda_log_add_queue"),
            exchange = @Exchange(name = "pda_log_add_exchange"),
            key = "success"
    ))
    public void addPdaLog(Message message, Channel channel, @Payload PdaLogAddVo pdaLogAddVo) {
        try {
            pdaLogService.addPdaLog(pdaLogAddVo);
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                channel.basicAck(message.getMessageProperties().getDeliveryTag(),true);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        }

    }


}
