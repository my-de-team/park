package com.park.dataserve.mqConsumer;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class MobileCodeSuccessConsumer {
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "register_success_code_queue"),
            exchange = @Exchange(name = "register_success_code_exchange"),
            key = "success"
    ))
    public void success(Message message, Channel channel) {
        try {
            /**
             * 手机号码
             */
            String strMessage = new String(message.getBody());
            System.out.println("MQ消费者调用短信接口发送注册成功短信!!!");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                channel.basicAck(message.getMessageProperties().getDeliveryTag(), true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
