package com.park.dataserve;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

@SpringBootApplication
@MapperScan("com.park.dataserve.mapper")
@EnableKnife4j
@EnableSwagger2WebMvc
@EnableFeignClients(basePackages = {"com.park.api"})
@EnableCaching
@EnableScheduling
@EnableTransactionManagement
public class DataApplication8084 {

    public static void main(String[] args) {
        SpringApplication.run(DataApplication8084.class, args);
    }
}
