package com.park.dataserve.aop;

import cn.hutool.http.Header;
import cn.hutool.http.useragent.*;
import com.park.api.SystemControllerApi;
import com.park.utils.JwtUtils;
import com.park.utils.Result;
import com.park.vos.datavo.PdaLogAddVo;
import com.park.vos.datavo.PeopleLoginVo;
import com.park.vos.systemvo.LoginLogVo;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@Component
@Aspect
public class PeopleLoginLogAspect {
    @Resource
    private SystemControllerApi systemControllerApi;
    @Resource
    private RabbitTemplate rabbitTemplate;

    @Around("execution(public com.park.utils.Result com.park.dataserve.controller.PeopleController.peopleLogin(com.park.vos.datavo.PeopleLoginVo))")
    public Object loginLog(ProceedingJoinPoint jp) throws Throwable {
        // 获取RequestAttributes
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        // 从获取RequestAttributes中获取HttpServletRequest的信息
        HttpServletRequest request = (HttpServletRequest) Objects.requireNonNull(requestAttributes).resolveReference(RequestAttributes.REFERENCE_REQUEST);

        // 获取用户登录信息
        String ipAddress = IpUtils.getIpAddress(request);
        System.out.println("ipAddress = " + ipAddress);
        String ipSource = IpUtils.getIpSource(ipAddress);
        System.out.println("ipSource = " + ipSource);
        UserAgent ua = UserAgentUtil.parse(request.getHeader(Header.USER_AGENT.toString()));
        System.out.println(ua.getPlatform().toString());
        boolean isMobile = ua.isMobile();
        System.out.println("isMobile = " + isMobile);
        Browser browser = ua.getBrowser();
        System.out.println("browser = " + browser.toString());
        Platform platform = ua.getPlatform();
        System.out.println("platform = " + platform);
        OS os = ua.getOs();
        String s = os.toString();
        System.out.println("s = " + s);
        String osVersion = ua.getOsVersion();
        System.out.println("osVersion = " + osVersion);
        Engine engine = ua.getEngine();
        System.out.println("engine = " + engine);
        String version = ua.getVersion();
        System.out.println("version = " + version);
        String engineVersion = ua.getEngineVersion();
        System.out.println("engineVersion = " + engineVersion);

        // 获取方法参数
        Object[] args = jp.getArgs();
        PeopleLoginVo peopleLoginVo = (PeopleLoginVo) args[0];
        System.out.println(peopleLoginVo.toString());

        // 封装登录日志
        LoginLogVo loginLogVo = new LoginLogVo();
        //loginLogVo.setUsername();
        loginLogVo.setMobile(peopleLoginVo.getPhone());
        loginLogVo.setWechat(os.toString());
        loginLogVo.setOs(os.toString());
        //loginLogVo.setState();
        loginLogVo.setBrower(browser.toString() + version);
        loginLogVo.setIpAddress(ipAddress);
        loginLogVo.setIpSource(ipSource);

        Object obj = null;
        try {
            obj = jp.proceed();
        } catch (Throwable e) {
            e.printStackTrace();
            // 调用登录日志
            loginLogVo.setState(1);
            systemControllerApi.addLoginLog(loginLogVo);
            throw e;
        }

        // 调用登录日志
        Result result = (Result) obj;
        String token = (String) result.getData();
        loginLogVo.setUsername(JwtUtils.getClaimByJwtToken(token, "employeeName"));
        loginLogVo.setState(0);
        systemControllerApi.addLoginLog(loginLogVo);

        // 如果是巡检员，再插入pda登录日志
//        Integer roleState = new Integer(JwtUtils.getClaimByJwtToken(token,"roleState"));
        String roleState = JwtUtils.getClaimByJwtToken(token, "roleState");

        if (roleState.equals("0")) {
            PdaLogAddVo pdaLogAddVo = new PdaLogAddVo();
            pdaLogAddVo.setDeviceNumber(peopleLoginVo.getDeviceNumber());
            pdaLogAddVo.setRoadId(peopleLoginVo.getRoadId());
            pdaLogAddVo.setLogin(JwtUtils.getClaimByJwtToken(token, "employeeName"));
            rabbitTemplate.convertAndSend("pda_log_add_exchange", "success", pdaLogAddVo);
        }

        return obj;
    }
}
