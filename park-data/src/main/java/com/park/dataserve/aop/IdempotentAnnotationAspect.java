package com.park.dataserve.aop;

import com.park.annotation.IdempotentAnnotation;
import com.park.expection.NoIdempotentException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class IdempotentAnnotationAspect {
    @Resource
    private RedisTemplate<String, Object> redisTemplate;
    /**
     * 通过环绕通知解析指定的注解,把该环绕通知织入到指定的方法里面,完成接口幂等性校验的功能
     */
    @Around("@annotation(idempotentAnnotation)")
    public Object before(ProceedingJoinPoint jp, IdempotentAnnotation idempotentAnnotation) throws Throwable {
        /**
         * 1. 在环绕通知中获取request对象
         */
        ServletRequestAttributes  servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        /**
         * 2. 从request中获取放在header里面的idempotentId
         */
        String idempotentId = request.getHeader("idempotentId");
        /**
         * 3. 直接从redis中id删除对应的key,如果删除成功表示第一次操作,否则就是重复操作
         */
        boolean flag = redisTemplate.delete(idempotentId);
        Object obj = null;
        if (flag) {
            //表示允许操作,放行
            try {
                obj = jp.proceed();
            } catch (Throwable e) {
                e.printStackTrace();
                throw e;
            }
        } else {
            // 抛一个异常
            throw new NoIdempotentException();
        }
        return obj;
    }
}
