package com.park.dataserve.mapper;

import com.park.dataserve.entity.Pda;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.park.vos.datavo.PdaVo;
import org.apache.ibatis.annotations.Update;

/**
* @author blank
* @description 针对表【tp_pda(手持PDA表)】的数据库操作Mapper
* @createDate 2023-07-18 11:14:46
* @Entity com.park.dataserve.entity.Pda
*/
public interface PdaMapper extends BaseMapper<Pda> {

    @Update("update tp_pda set device_number = #{deviceNumber}, device_name = #{deviceName}, road_id = #{roadId} where id = #{id}")
    int pdaPut(PdaVo pdaVo);
}




