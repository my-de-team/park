package com.park.dataserve.mapper;

import com.park.dataserve.entity.MagneticLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author blank
* @description 针对表【tp_magnetic_log(地磁日志表)】的数据库操作Mapper
* @createDate 2023-07-15 15:47:41
* @Entity com.park.dataserve.entity.MagneticLog
*/
public interface MagneticLogMapper extends BaseMapper<MagneticLog> {

}




