package com.park.dataserve.mapper;

import com.park.dataserve.entity.People;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.park.vos.datavo.PeopleVo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
* @author blank
* @description 针对表【tp_people(员工管理表)】的数据库操作Mapper
* @createDate 2023-07-14 09:42:50
* @Entity com.park.dataserve.entity.People
*/
public interface PeopleMapper extends BaseMapper<People> {
    @Select("select road_id from tp_people_road where inspector_id = #{id}")
    List<Long> queryRoadId(Long id);

    @Select("select employee_name from tp_people where role_state = #{roleState} and id = #{id}")
    PeopleVo queryPeopleNameById(@Param("id") Long id, @Param("roleState") Integer roleState);



    @Insert("insert into tp_people_road (road_id,inspector_id) values (#{roadId}, #{inspectorId})")
    int addInspectorRoad(@Param("inspectorId") Long inspectorId, @Param("roadId") Long roadId);

    @Update("update tp_people_road set inspector_id = #{inspectorId}, road_id = #{roadId} where inspector_id = #{inspectorId}")
    void putInspectorRoad(@Param("inspectorId") Long inspectorId, @Param("roadId") Long roadId);

    @Select("select employee_name, password, role_state from tp_people where phone = #{phone} and is_deleted = 0")
    People login(String phone);

    @Select("select count(1) from tp_people where phone = #{phone} and is_deleted = 0")
    int isRegisterMobile(String phone);

    @Select("select road_id from tp_people_road where inspector_id = #{id} and is_deleted = 0")
    List<Long> queryAllRoadIdById(Long id);
}




