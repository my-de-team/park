package com.park.dataserve.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.park.dataserve.entity.InspectorFaultImg;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 巡检员故障反馈表图片 Mapper 接口
 * </p>
 *
 * @author lxc
 * @since 2023-07-13
 */
public interface InspectorFaultImgMapper extends BaseMapper<InspectorFaultImg> {

    @Select("select img_url from tp_inspector_fault_img where inspector_fault_id = #{id} and is_deleted=0")
    List<String> queryImgByInspectorFaultId(Long id);
}
