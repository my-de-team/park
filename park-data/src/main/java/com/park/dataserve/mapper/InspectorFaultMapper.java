package com.park.dataserve.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.park.dataserve.entity.InspectorFault;
import com.park.vos.datavo.InspectorFaultVo;
import com.park.vos.datavo.InspectorFaultQueryVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>['[
 * 巡检员反馈故障表 Mapper 接口
 * </p>
 *
 * @author lxc
 * @since 2023-07-13
 */
public interface InspectorFaultMapper extends BaseMapper<InspectorFault> {

    @Select("select img_count from tp_inspector_fault where id = #{id} and is_deleted=0")
    Integer queryImgCountById(Long id);


    IPage<InspectorFaultVo> queryInspectorFaultList(@Param("page") IPage<InspectorFaultVo> page, @Param("inspectorFaultQueryVo") InspectorFaultQueryVo inspectorFaultQueryVo);

}
