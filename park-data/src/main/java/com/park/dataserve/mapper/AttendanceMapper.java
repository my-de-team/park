package com.park.dataserve.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.park.dataserve.entity.Attendance;

/**
 * <p>
 * 考勤表 Mapper 接口
 * </p>
 *
 * @author lxc
 * @since 2023-07-13
 */
public interface AttendanceMapper extends BaseMapper<Attendance> {

}
