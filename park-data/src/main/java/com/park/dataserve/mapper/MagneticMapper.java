package com.park.dataserve.mapper;

import com.park.dataserve.entity.Magnetic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.park.vos.datavo.MagneticVo;
import com.park.vos.datavo.QueryMagneticVo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

/**
* @author blank
* @description 针对表【tp_magnetic】的数据库操作Mapper
* @createDate 2023-07-13 11:38:15
* @Entity com.park.dataserve.entity.Magnetic
*/
public interface MagneticMapper extends BaseMapper<Magnetic> {
    //新增地磁管理
//    @Insert("INSERT INTO tp_magnetic(id, magnetic_number, magnetic_name, road_id)" +
//            "values (#{id}, #{magneticNumber}, #{magneticName}, #{roadId})")
//    int magneticAdd(MagneticVo magneticVo);

    //修改地磁管理
    @Update("update tp_magnetic " +
            "set magnetic_number = #{magneticNumber}, " +
            "magnetic_name = #{magneticName}, " +
            "road_id = #{roadId} " +
            "where id = #{id}")
    int magneticPut(MagneticVo magneticVo);
}




