package com.park.dataserve.mapper;

import com.park.dataserve.entity.ParkingPosition;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.park.utils.Result;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 泊位信息 Mapper 接口
 * </p>
 *
 * @author lauxiaoyang
 * @since 2023-07-14
 */
public interface ParkingPositionMapper extends BaseMapper<ParkingPosition> {


    /*
    根据泊位编号查询泊位id
     */
    @Select("select road_segment_id from tp_parking_position where berth_number = #{berthNumber};")
    Long selectByBerthNumber(String berthNumber);

    /*
    根据路段id查询泊位信息 路段ID：road_segment_id
     */
    @Select("select * from tp_parking_position where road_segment_id = #{roadSegmentId}")
    List<ParkingPosition> getParkingByRoadSegmentId(Long roadSegmentId);
}
