package com.park.dataserve.mapper;

import com.park.dataserve.entity.PdaLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author blank
* @description 针对表【tp_pda_log(PDA登录日志表)】的数据库操作Mapper
* @createDate 2023-07-20 16:21:57
* @Entity com.park.dataserve.entity.PdaLog
*/
public interface PdaLogMapper extends BaseMapper<PdaLog> {

}




