package com.park.dataserve.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.park.dataserve.entity.Feedback;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.park.vos.datavo.FeedbackQueryVo;
import com.park.vos.datavo.FeedbackVo;
import com.park.vos.datavo.InspectorFaultVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 用户反馈表 Mapper 接口
 * </p>
 *
 * @author lxc
 * @since 2023-07-15
 */
public interface FeedbackMapper extends BaseMapper<Feedback> {

    @Select("select img_count from tp_feedback where id = #{id} and is_deleted=0")
    Integer queryImgCountById(Long id);

    IPage<FeedbackVo> queryFeedbackPage(@Param("page") IPage<FeedbackVo> page, @Param("feedbackQueryVo") FeedbackQueryVo feedbackQueryVo);
}
