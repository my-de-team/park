package com.park.dataserve.mapper;

import com.park.dataserve.entity.RoadSegment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.park.vos.datavo.RoadSegmentVo;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 * 路段信息 Mapper 接口
 * </p>
 *
 * @author lauxiaoyang
 * @since 2023-07-13
 */
@Mapper
public interface RoadSegmentMapper extends BaseMapper<RoadSegment> {

    /*
    分页查询
     */
    @Select("select inspector_id from tp_people_road where road_id = #{id} and is_deleted = 0")
    List<Long> queryInspectorId(Long id);

    /*
    根据路段id查询路段名称
     */
    @Select("select segment_name from tp_road_segment where id = #{id} and is_deleted = 0")
    RoadSegment queryRoadSegmentById(Long id);

    /*
    新增
     */
    @Insert("insert into tp_road_segment (segment_name,road_segment,segment_type,region,parking_spots,restricted_spots,inspector,maintenance_staff,create_time,update_time,field1,field2,field3,version,is_deleted,longitude,latitude) values (#{segmentName},#{roadSegment},#{segmentType},#{region},#{parkingSpots},#{restrictedSpots},#{inspector},#{maintenanceStaff},#{createTime},#{updateTime},#{Field1},#{Field2},#{Field3},#{version},#{isDeleted},#{longitude},#{latitude})")
    boolean addRoadSegment(RoadSegment roadSegment);

    /*
    删除
     */
    @Update("update tp_road_segment set is_deleted = 1 where id = #{id} and is_deleted = 0")
    int deleteRoadSegmentById(Long id);

    /*
    修改
     */
    int updateRoadSegment(RoadSegment roadSegment);

    /*
    根据经纬度查询路段信息
     */
//    @Select("select * from tp_road_segment where longitude = #{longitude} and latitude = #{latitude}")
//    List<RoadSegmentVo> queryRoadSegmentByCoordinates(@Param("longitude") String longitude, @Param("latitude") String latitude);
}
