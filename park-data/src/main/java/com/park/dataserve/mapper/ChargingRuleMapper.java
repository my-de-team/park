package com.park.dataserve.mapper;

import com.park.dataserve.entity.ChargingRule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * 收费规则表 Mapper 接口
 * </p>
 *
 * @author lauxiaoyang
 * @since 2023-07-18
 */
public interface ChargingRuleMapper extends BaseMapper<ChargingRule> {
    // 根据路段ID查询收费规则
    @Select("select * from tp_charging_rule where road_segment_id = {roadSegmentId}")
    ChargingRule selectByRoadSegmentId(@Param("roadSegmentId") Long roadSegmentId);

    /*
    根据泊位id查询收费信息 泊位ID：parking_position_id
     */
    @Select("select * from tp_charging_rule where road_segment_id = #{roadSegmentId}")
    List<ChargingRule> getChargingByParkingPositionId(Long roadSegmentId);
}
