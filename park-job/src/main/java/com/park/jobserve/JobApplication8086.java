package com.park.jobserve;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients(basePackages = {"com.park.api"})
public class JobApplication8086 {
    public static void main(String[] args) {
        SpringApplication.run(JobApplication8086.class,args);
    }
}
