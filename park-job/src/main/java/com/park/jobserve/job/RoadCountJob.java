package com.park.jobserve.job;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.park.api.DataControllerApi;
import com.park.api.SystemControllerApi;
import com.park.api.UserControllerApi;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.vos.datavo.RoadSegmentQueryVo;
import com.park.vos.datavo.RoadSegmentVo;
import com.park.vos.systemvo.RoadCountVo;
import com.park.vos.userservevo.AppealQueryVo;
import com.park.vos.userservevo.AppealVo;
import com.park.vos.userservevo.OrderPageVo;
import com.park.vos.userservevo.OrderQueryPageVo;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.data.geo.Point;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class RoadCountJob extends IJobHandler {
    @Resource
    private SystemControllerApi systemControllerApi;

    @Resource
    private DataControllerApi dataControllerApi;

    @Resource
    private UserControllerApi userControllerApi;

    @Resource
    private RedisTemplate<String,Object> redisTemplate;

    @XxlJob("roadCountJob")
    @Override
    public void execute() throws Exception {
        System.out.println("------定时任务启动,进行路段统计----");
        //1.所有路段的集合,远程调用路段模块
        Result<PageUtil<RoadSegmentVo>> pageUtilResult = dataControllerApi.queryPageRoadSegment(1, -1, new RoadSegmentQueryVo());
        List<RoadSegmentVo> roadSegmentVoList = pageUtilResult.getData().getPageData();
        System.out.println("查询到的路段信息: " + roadSegmentVoList);
        //2.遍历路段集合取出id,然后根据id在订单中查询
        for (RoadSegmentVo roadSegmentVo : roadSegmentVoList) {
            Point point = new Point(new Double(roadSegmentVo.getLongitude()), new Double(roadSegmentVo.getLatitude()));
            redisTemplate.opsForGeo().add("roadSegmentVoList",point,roadSegmentVo.getId());


            RoadCountVo roadCountVo = new RoadCountVo();
            roadCountVo.setCountTime(new Date());
            roadCountVo.setRoadCountNum(roadSegmentVoList.size());
            roadCountVo.setRoadId(roadSegmentVo.getId());
            /* ----根据id统计订单数据----*/
            Date date = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DATE, -1);
            /*设置查询条件,查询前一天的订单数据*/
            OrderQueryPageVo orderQueryPageVo = new OrderQueryPageVo();
            orderQueryPageVo.setOrderSubStartTime(calendar.getTime());
            orderQueryPageVo.setOrderSubEndTime(date);
            orderQueryPageVo.setOrderState(2);//已支付
            orderQueryPageVo.setIsDeleted(0);
            orderQueryPageVo.setRoadSegment(roadSegmentVo.getSegmentName());
            Result orderCountResult = userControllerApi.queryPageOrder(1L, -1L, orderQueryPageVo);
            /*--转换数据集合*/
            ObjectMapper om = new ObjectMapper();
            PageUtil pageUtil = om.convertValue(orderCountResult.getData(), PageUtil.class);
            List list = pageUtil.getPageData();
            System.out.println("根据路段名称查询到的对应订单集合 : " + list);
            //只有当日有数据才进行另外操作
            if (list != null && list.size() > 0) {
                List<OrderPageVo> orderPageVoList = new ArrayList<>();
                for (Object o : list) {
                    OrderPageVo orderPageVo = om.convertValue(o, OrderPageVo.class);
                    orderPageVoList.add(orderPageVo);
                }
                // -当日订单数总数
                roadCountVo.setOrderCountNum(orderPageVoList.size());
                //-当日营收总额
                BigDecimal revenue = new BigDecimal(0);
                //初始化
                Integer refundOrder = 0;
                //初始化
                BigDecimal refundAmount = new BigDecimal(0);
                for (OrderPageVo orderPageVo : orderPageVoList) {
                    revenue = orderPageVo.getOrderAmount().add(revenue);

                    AppealQueryVo appealQueryVo = new AppealQueryVo();
                    appealQueryVo.setAppealProcessState(1);//已退款
                    appealQueryVo.setIsDeleted(0);
                    appealQueryVo.setOrderId(orderPageVo.getId());
                    appealQueryVo.setStartTime(calendar.getTime());
                    appealQueryVo.setEndTime(date);

                    Result<PageUtil<AppealVo>> appealResult = userControllerApi.queryPageAppeal(1, -1, appealQueryVo);
                    List<AppealVo> appealVoList = appealResult.getData().getPageData();
                    if (appealVoList != null && appealVoList.size() > 0) {
                        refundOrder++;
                        refundAmount = refundAmount.add(appealVoList.get(0).getRefundAmount());
                    }
                }
                roadCountVo.setRevenueAmount(revenue);
                // -该路段当日退款订单总数
                roadCountVo.setRefundOrder(refundOrder);
                //-该路段当日退款订单总金额
                roadCountVo.setRefundAmount(refundAmount);
            }
            //添加统计记录
            System.out.println("添加统计记录: " + roadCountVo);
            systemControllerApi.addRoadCount(roadCountVo);
        }

    }
}
