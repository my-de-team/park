package com.park.jobserve.job;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.park.api.PayControllerApi;
import com.park.api.SystemControllerApi;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.vos.payservevo.TradeLogVo;
import com.park.vos.systemvo.PayCountVo;
import com.park.vos.systemvo.QueryTradeLogVo;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class PayCountJob extends IJobHandler {
    @Resource
    private PayControllerApi payControllerApi;
    @Resource
    private SystemControllerApi systemControllerApi;

    @XxlJob("payCountJob")
    @Override
    public void execute() throws Exception {
        System.out.println("------定时任务启动,进行支付统计----");
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, -1);
        //远程调用交易记录表，查询前一天的交易记录进行统计
        //查询条件
        QueryTradeLogVo queryVo = new QueryTradeLogVo();
        queryVo.setStartTime(calendar.getTime());
        queryVo.setEndTime(date);
        queryVo.setState(1);
        //获取查询到的数据
        Result<PageUtil<TradeLogVo>> pageUtilResult = payControllerApi.queryPageTradeLog(1, -1, queryVo);
        List<TradeLogVo> tradeLogVoList = pageUtilResult.getData().getPageData();
        System.out.println("查询到的交易记录数据： " + tradeLogVoList);
        //封装支付统计对象
        PayCountVo payCountVo = new PayCountVo();
        payCountVo.setCountTime(new Date());
        if (tradeLogVoList != null && tradeLogVoList.size() > 0){
            //主动支付，巡检员字段为null则为主动支付
            Integer activePay = 0;
            Integer cashPay = 0;
            Integer codePay = 0;
            BigDecimal wechatPayAmount = new BigDecimal("0");
            BigDecimal alipayPayAmount = new BigDecimal("0");
            BigDecimal cashPayAmount = new BigDecimal("0");
            BigDecimal unionPayAmount = new BigDecimal("0");
            for (TradeLogVo tradeLogVo : tradeLogVoList) {

                if (tradeLogVo.getPayPlatform() == 1){
                    //支付宝付款
                    activePay++;
                    alipayPayAmount = alipayPayAmount.add(tradeLogVo.getAmount());
                }
                if (tradeLogVo.getPayPlatform() == 2){
                    //银联付款
                    codePay++;
                    unionPayAmount = unionPayAmount.add(tradeLogVo.getAmount());
                }
                if (tradeLogVo.getPayPlatform() == 3){
                    //微信付款
                    activePay++;
                    wechatPayAmount = wechatPayAmount.add(tradeLogVo.getAmount());
                }
                if (tradeLogVo.getPayPlatform() == 4){
                    //现金付款
                    cashPay++;
                    cashPayAmount = cashPayAmount.add(tradeLogVo.getAmount());
                }
            }
            payCountVo.setActivePay(activePay);
            payCountVo.setCashPay(cashPay);
            payCountVo.setCodePay(codePay);
            payCountVo.setWechatPayAmount(wechatPayAmount);
            payCountVo.setAlipayPayAmount(alipayPayAmount);
            payCountVo.setCashPayAmount(cashPayAmount);
            payCountVo.setUnionPayAmount(unionPayAmount);
        }
        //添加统计数据
        systemControllerApi.addPayCount(payCountVo);
        System.out.println("添加的数据: " + payCountVo);
    }
}
