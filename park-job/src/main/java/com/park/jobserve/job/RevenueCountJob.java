package com.park.jobserve.job;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.park.api.SystemControllerApi;
import com.park.api.UserControllerApi;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.vos.systemvo.RevenueCountVo;
import com.park.vos.userservevo.AppealQueryVo;
import com.park.vos.userservevo.AppealVo;
import com.park.vos.userservevo.OrderPageVo;
import com.park.vos.userservevo.OrderQueryPageVo;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class RevenueCountJob extends IJobHandler {
    @Resource
    private UserControllerApi userControllerApi;
    @Resource
    private SystemControllerApi systemControllerApi;

    @XxlJob("revenueCountJob")
    @Override
    public void execute() throws Exception {
        System.out.println("------定时任务启动,进行营收统计----");
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, -1);
        /*设置查询条件,查询前一天的订单数据*/
        OrderQueryPageVo orderQueryPageVo = new OrderQueryPageVo();
        orderQueryPageVo.setOrderSubStartTime(calendar.getTime());
        orderQueryPageVo.setOrderSubEndTime(date);
        orderQueryPageVo.setOrderState(2);//已支付
        orderQueryPageVo.setIsDeleted(0);

        Result orderCountResult = userControllerApi.queryPageOrder(1L, -1L, orderQueryPageVo);
        /*--需要统计的数据如下:*/
        ObjectMapper om = new ObjectMapper();
        PageUtil pageUtil = om.convertValue(orderCountResult.getData(), PageUtil.class);
        List list = pageUtil.getPageData();
        RevenueCountVo revenueCountVo = new RevenueCountVo();
        revenueCountVo.setCountTime(new Date());
        System.out.println("---查询的订单数据" + orderCountResult);
        //只有当日有数据才进行另外操作
        if (list != null && list.size() > 0) {
            List<OrderPageVo> orderPageVoList = new ArrayList<>();
            for (Object o : list) {
                OrderPageVo orderPageVo = om.convertValue(o, OrderPageVo.class);
                orderPageVoList.add(orderPageVo);
            }
            // -当日订单数总数
            revenueCountVo.setOrderCount(orderPageVoList.size());
            //-当日营收总额
            BigDecimal revenue = new BigDecimal(0);
            for (OrderPageVo orderPageVo : orderPageVoList) {
                revenue = orderPageVo.getOrderAmount().add(revenue);
            }
            revenueCountVo.setRevenue(revenue);

            AppealQueryVo appealQueryVo = new AppealQueryVo();
            appealQueryVo.setAppealProcessState(1);//已退款
            appealQueryVo.setIsDeleted(0);
            appealQueryVo.setStartTime(calendar.getTime());
            appealQueryVo.setEndTime(date);

            Result<PageUtil<AppealVo>> appealResult = userControllerApi.queryPageAppeal(1, -1, appealQueryVo);
            List<AppealVo> appealVoList = appealResult.getData().getPageData();
            // -当日退款订单总数
            revenueCountVo.setRefundOrder(appealVoList.size());
            // -当日退款总金额
            BigDecimal refundAmount = new BigDecimal(0);
            for (AppealVo appealVo : appealVoList) {
                refundAmount = appealVo.getRefundAmount().add(refundAmount);
            }
            revenueCountVo.setRefundAmount(refundAmount);
            // -当日新注册用户数量
            revenueCountVo.setNewUser(userControllerApi.getUserByCreateTime().getData());

        }
        System.out.println(revenueCountVo);
        //添加营收统计
        systemControllerApi.addRevenueCount(revenueCountVo);
    }
}
