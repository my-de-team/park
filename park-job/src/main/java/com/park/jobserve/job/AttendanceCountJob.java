package com.park.jobserve.job;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.park.api.DataControllerApi;
import com.park.api.SystemControllerApi;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.vos.datavo.AddAttendanceQueryVo;
import com.park.vos.datavo.PeopleVo;
import com.park.vos.datavo.QueryPeopleVo;
import com.park.vos.systemvo.AttendanceCountVo;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class AttendanceCountJob extends IJobHandler {
    @Resource
    private SystemControllerApi systemControllerApi;
    @Resource
    private DataControllerApi dataControllerApi;

    @XxlJob("attendanceCountJob")
    @Override
    public void execute() throws Exception {

        System.out.println("------定时任务启动,进行考勤统计----");
        //1.查询所有员工集合(巡检员)

        Result result = dataControllerApi.queryPeopleAll(1, -1, new QueryPeopleVo());
        ObjectMapper om = new ObjectMapper();
        PageUtil pageUtil = om.convertValue(result.getData(), PageUtil.class);
        List list = pageUtil.getPageData();
        System.out.println("查询到的员工集合: " + list);
        //设置当月月份
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        //日历对象
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        String countMonth = format.format(new Date()).split("-")[1];
        Date clockTimeStart = calendar.getTime();
        if (list != null && list.size() > 0) {
            for (Object o : list) {
                PeopleVo peopleVo = om.convertValue(o, PeopleVo.class);
                //2.根据巡检员id在考勤表中统计数据
                //构建查询条件对象
                AddAttendanceQueryVo addAttendanceQueryVo = new AddAttendanceQueryVo();
                addAttendanceQueryVo.setInspectorId(peopleVo.getId());
                addAttendanceQueryVo.setClockTimeStart(clockTimeStart);
                addAttendanceQueryVo.setClockTimeEnd(new Date());
                //3.远程调用查询该员工id当月的所有打卡记录
                Result attendanceLog = dataControllerApi.queryAllAttendanceRecords(addAttendanceQueryVo);
                System.out.println("该员工id当月的所有打卡记录"+attendanceLog);
                List attendanceList = new ArrayList<>();
                if (attendanceLog.getData() != null){
                    attendanceList = om.convertValue(attendanceLog.getData(), ArrayList.class);
                }
                //4.封装统计数据
                AttendanceCountVo attendanceCountVo = new AttendanceCountVo();
                attendanceCountVo.setCountMonth(countMonth);
                attendanceCountVo.setCountTime(new Date());
                attendanceCountVo.setInspectorId(peopleVo.getId());
                attendanceCountVo.setInspectorName(peopleVo.getEmployeeName());
                attendanceCountVo.setScheduleDays(22);
                attendanceCountVo.setNormalDays(attendanceList != null ?
                        attendanceList.size() : 0);
                attendanceCountVo.setAbnormalDays(0);
                //把考勤统计数据插入数据库
                System.out.println("把考勤统计数据插入数据库 : " + attendanceCountVo);
                systemControllerApi.addAttendanceCount(attendanceCountVo);
            }

        }
    }

    /*public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(format.format(calendar.getTime()));
    }*/
}
