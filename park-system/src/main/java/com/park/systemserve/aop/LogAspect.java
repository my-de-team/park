package com.park.systemserve.aop;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.park.annotation.Log;
import com.park.constant.ServiceErrorConst;
import com.park.systemserve.entity.OptLog;
import com.park.systemserve.service.OptLogService;
import com.park.utils.AssertUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Aspect
@Component
public class LogAspect {


    @Resource
    private OptLogService optLogService;

    /**
     * 日志切入点
     */
    @Pointcut("@annotation(com.park.annotation.Log)")
    public void logPointCut() {
    }

    @AfterReturning(value = "logPointCut()", returning = "response")
    public void saveOptLog(JoinPoint joinPoint, Object response) throws Exception {
        // 获取RequestAttributes
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        // 从获取RequestAttributes中获取HttpServletRequest的信息
        HttpServletRequest request = (HttpServletRequest) Objects.requireNonNull(requestAttributes).resolveReference(RequestAttributes.REFERENCE_REQUEST);
        // 从切面织入点处通过反射机制获取织入点处的方法
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        // 获取切入点所在的方法
        Method method = signature.getMethod();
        // 获取操作
        Api api = (Api) signature.getDeclaringType().getAnnotation(Api.class);
        ApiOperation apiOperation = method.getAnnotation(ApiOperation.class);
        Log log = method.getAnnotation(Log.class);

        // 获取类名
        String className = joinPoint.getTarget().getClass().getName();
        String name = method.getName();
        String methodName = className + "." + name;

        /*------------------------  数据封装  -----------------------------*/
        ObjectMapper objectMapper = new ObjectMapper();
        OptLog optLog = new OptLog();
        optLog.setTitle(api.tags()[0]);
        optLog.setBusinessType(log.logType());
        optLog.setMethodName(apiOperation.value());
        optLog.setMethodUrl(methodName);
        optLog.setReqType(Objects.requireNonNull(request).getMethod());
        optLog.setOptName(!ObjectUtils.isEmpty(request.getHeader("name"))
                ? request.getHeader("name") : "system");
        optLog.setOptUrl(request.getRequestURI());
        optLog.setOptIp(IpUtils.getIpAddress(request));
        List<Object> list = new ArrayList<>();
        for (Object arg : joinPoint.getArgs()) {
            if (!arg.getClass().getName().contains("org.apache.catalina.connector")){
                list.add(arg);
            }
        }
        optLog.setOptAddress(IpUtils.getIpSource(IpUtils.getIpAddress(request)));
        optLog.setReqParam(objectMapper.writeValueAsString(list));
        optLog.setRespData(objectMapper.writeValueAsString(response));
        optLog.setOptTime(new Date());

        int insert = optLogService.addOptLog(optLog);
        AssertUtils.sysIsError(insert == 0, ServiceErrorConst.SAVE_DATA_FAIL);
    }

}
