package com.park.systemserve.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.systemserve.entity.PayCount;
import com.park.systemserve.service.PayCountService;
import com.park.systemserve.mapper.PayCountMapper;
import com.park.utils.PageUtil;
import com.park.vos.systemvo.PayCountVo;
import com.park.vos.systemvo.QueryCountCommonVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;

/**
* @author zdx
* @description 针对表【tp_pay_count(ade-支付统计表)】的数据库操作Service实现
* @createDate 2023-07-14 11:18:44
*/
@Service
public class PayCountServiceImpl extends ServiceImpl<PayCountMapper, PayCount>
    implements PayCountService{
    @Resource
    private PayCountMapper mapper;
    @Override
    public PageUtil<PayCount> queryPagePayCount(Integer pageNum, Integer pageSize, QueryCountCommonVo queryVo) {
        //1.构建分页查询对象,传入分页设置参数
        IPage<PayCount> countIPage = new Page<>(pageNum,pageSize);
        //2.构建查询条件
        QueryWrapper<PayCount> wrapper = new QueryWrapper<>();
        if (!ObjectUtils.isEmpty(queryVo)){
            if (!ObjectUtils.isEmpty(queryVo.getStartTime())){
                wrapper.ge("count_time",queryVo.getStartTime());
            }
            if (!ObjectUtils.isEmpty(queryVo.getEndTime())){
                wrapper.le("count_time",queryVo.getEndTime());
            }
        }
        wrapper.orderByDesc("count_time");
        IPage<PayCount> resultPage = mapper.selectPage(countIPage, wrapper);

        //3.封装分页结果集
        PageUtil<PayCount> pageUtil = new PageUtil<>();
        pageUtil.setPageData(resultPage.getRecords());
        pageUtil.setTotal(resultPage.getTotal());
        return pageUtil;
    }

    @Override
    public int addPayCount(PayCountVo countVo) {
        PayCount payCount = new PayCount();
        if (countVo == null){
            return 0;
        }
        BeanUtils.copyProperties(countVo,payCount);
        return mapper.insert(payCount);
    }
}




