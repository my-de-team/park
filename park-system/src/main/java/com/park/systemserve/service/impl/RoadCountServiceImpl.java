package com.park.systemserve.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.systemserve.entity.PayCount;
import com.park.systemserve.entity.RoadCount;
import com.park.systemserve.service.RoadCountService;
import com.park.systemserve.mapper.RoadCountMapper;
import com.park.utils.PageUtil;
import com.park.vos.systemvo.QueryCountCommonVo;
import com.park.vos.systemvo.RoadCountVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;

/**
* @author zdx
* @description 针对表【tp_road_count(ade-路段统计表)】的数据库操作Service实现
* @createDate 2023-07-14 14:31:17
*/
@Service
public class RoadCountServiceImpl extends ServiceImpl<RoadCountMapper, RoadCount>
    implements RoadCountService{

    @Resource
    private RoadCountMapper mapper;
    @Override
    public PageUtil<RoadCount> queryPageRoadCount(Integer pageNum, Integer pageSize, QueryCountCommonVo queryVo) {
        //1.构建分页查询对象,传入分页设置参数
        IPage<RoadCount> countIPage = new Page<>(pageNum,pageSize);
        //2.构建查询条件
        QueryWrapper<RoadCount> wrapper = new QueryWrapper<>();
        if (!ObjectUtils.isEmpty(queryVo)){
            if (!ObjectUtils.isEmpty(queryVo.getStartTime())){
                wrapper.ge("count_time",queryVo.getStartTime());
            }
            if (!ObjectUtils.isEmpty(queryVo.getEndTime())){
                wrapper.le("count_time",queryVo.getEndTime());
            }
            if (!ObjectUtils.isEmpty(queryVo.getRoadId())){
                wrapper.eq("road_id",queryVo.getRoadId());
            }
        }
        wrapper.orderByDesc("count_time");
        IPage<RoadCount> resultPage = mapper.selectPage(countIPage, wrapper);

        //3.封装分页结果集
        PageUtil<RoadCount> pageUtil = new PageUtil<>();
        pageUtil.setPageData(resultPage.getRecords());
        pageUtil.setTotal(resultPage.getTotal());
        return pageUtil;
    }

    @Override
    public int addRoadCount(RoadCountVo countVo) {
        RoadCount roadCount = new RoadCount();
        if (countVo == null){
            return 0;
        }
        BeanUtils.copyProperties(countVo,roadCount);
        return mapper.insert(roadCount);
    }
}




