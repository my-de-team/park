package com.park.systemserve.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.systemserve.entity.AttendanceCount;
import com.park.systemserve.service.AttendanceCountService;
import com.park.systemserve.mapper.AttendanceCountMapper;
import com.park.systemserve.utils.BeanCopyUtils;
import com.park.utils.PageUtil;
import com.park.vos.systemvo.AttendanceCountVo;
import com.park.vos.systemvo.QueryCountCommonVo;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;

/**
* @author zdx
* @description 针对表【tp_attendance_count(ade-考勤统计表)】的数据库操作Service实现
* @createDate 2023-07-14 11:18:45
*/
@Service
public class AttendanceCountServiceImpl extends ServiceImpl<AttendanceCountMapper, AttendanceCount>
    implements AttendanceCountService{
    @Resource
    private AttendanceCountMapper mapper;
    @Override
    public PageUtil<AttendanceCount> queryPageAttendanceCount(Integer pageNum, Integer pageSize, QueryCountCommonVo queryVo) {
        //1.构建分页查询对象,传入分页设置参数
        IPage<AttendanceCount> countIPage = new Page<>(pageNum,pageSize);
        //2.构建查询条件
        QueryWrapper<AttendanceCount> wrapper = new QueryWrapper<>();
        if (!ObjectUtils.isEmpty(queryVo)){
            if (!ObjectUtils.isEmpty(queryVo.getStartTime())){
                wrapper.ge("count_time",queryVo.getStartTime());
            }
            if (!ObjectUtils.isEmpty(queryVo.getEndTime())){
                wrapper.le("count_time",queryVo.getEndTime());
            }
            if (!ObjectUtils.isEmpty(queryVo.getInspectorName())){
                wrapper.like("inspector_name",queryVo.getInspectorName());
            }
        }
        wrapper.orderByDesc("count_time");
        IPage<AttendanceCount> resultPage = mapper.selectPage(countIPage, wrapper);

        //3.封装分页结果集
        PageUtil<AttendanceCount> pageUtil = new PageUtil<>();
        pageUtil.setPageData(resultPage.getRecords());
        pageUtil.setTotal(resultPage.getTotal());
        return pageUtil;
    }

    @Override
    public int addAttendanceCount(AttendanceCountVo countVo) {
        if (countVo == null){
            return 0;
        }
        return mapper.insert(BeanCopyUtils.copyObject(countVo,AttendanceCount.class));
    }
}




