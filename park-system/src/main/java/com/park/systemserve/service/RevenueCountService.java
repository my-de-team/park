package com.park.systemserve.service;

import com.park.systemserve.entity.RevenueCount;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.utils.PageUtil;
import com.park.vos.systemvo.QueryCountCommonVo;
import com.park.vos.systemvo.RevenueCountVo;

/**
* @author zdx
* @description 针对表【tp_revenue_count(ade-营收统计表)】的数据库操作Service
* @createDate 2023-07-14 14:31:17
*/
public interface RevenueCountService extends IService<RevenueCount> {

    PageUtil<RevenueCount> queryPageRevenueCount(Integer pageNum, Integer pageSize, QueryCountCommonVo queryVo);

    int addRevenueCount(RevenueCountVo countVo);
}
