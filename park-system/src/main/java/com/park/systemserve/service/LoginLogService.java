package com.park.systemserve.service;

import com.park.systemserve.entity.LoginLog;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.utils.PageUtil;
import com.park.vos.systemvo.LoginLogVo;
import com.park.vos.systemvo.QueryCountCommonVo;

import java.util.Date;

/**
* @author zdx
* @description 针对表【tp_login_log(ade-登录日志表)】的数据库操作Service
* @createDate 2023-07-14 11:18:45
*/
public interface LoginLogService extends IService<LoginLog> {

    int addLoginLog(LoginLogVo logVo);

    PageUtil<LoginLog> queryPageLoginLog(Integer pageNum, Integer pageSize, QueryCountCommonVo queryVo);

    Date getLastLoginTime(String mobile);
}
