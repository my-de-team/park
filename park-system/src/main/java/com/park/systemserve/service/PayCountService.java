package com.park.systemserve.service;

import com.park.systemserve.entity.PayCount;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.utils.PageUtil;
import com.park.vos.systemvo.PayCountVo;
import com.park.vos.systemvo.QueryCountCommonVo;

/**
* @author zdx
* @description 针对表【tp_pay_count(ade-支付统计表)】的数据库操作Service
* @createDate 2023-07-14 11:18:44
*/
public interface PayCountService extends IService<PayCount> {

    PageUtil<PayCount> queryPagePayCount(Integer pageNum, Integer pageSize, QueryCountCommonVo queryVo);

    int addPayCount(PayCountVo countVo);
}
