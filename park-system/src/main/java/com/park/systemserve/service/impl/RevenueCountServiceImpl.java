package com.park.systemserve.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.systemserve.entity.RevenueCount;
import com.park.systemserve.service.RevenueCountService;
import com.park.systemserve.mapper.RevenueCountMapper;
import com.park.utils.PageUtil;
import com.park.vos.systemvo.QueryCountCommonVo;
import com.park.vos.systemvo.RevenueCountVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;

/**
* @author zdx
* @description 针对表【tp_revenue_count(ade-营收统计表)】的数据库操作Service实现
* @createDate 2023-07-14 14:31:17
*/
@Service
public class RevenueCountServiceImpl extends ServiceImpl<RevenueCountMapper, RevenueCount>
    implements RevenueCountService{

    @Resource
    private RevenueCountMapper mapper;

    /**
     * 分页条件查询
     * @param pageNum
     * @param pageSize
     * @param queryVo
     * @return
     */
    @Override
    public PageUtil<RevenueCount> queryPageRevenueCount(Integer pageNum, Integer pageSize, QueryCountCommonVo queryVo) {

        //1.构建分页查询对象,传入分页设置参数
        IPage<RevenueCount> countIPage = new Page<>(pageNum,pageSize);
        //2.构建查询条件
        QueryWrapper<RevenueCount> wrapper = new QueryWrapper<>();
        if (!ObjectUtils.isEmpty(queryVo)){
            if (!ObjectUtils.isEmpty(queryVo.getStartTime())){
                wrapper.ge("count_time",queryVo.getStartTime());
            }
            if (!ObjectUtils.isEmpty(queryVo.getEndTime())){
                wrapper.le("count_time",queryVo.getEndTime());
            }
        }
        wrapper.orderByDesc("count_time");
        IPage<RevenueCount> resultPage = mapper.selectPage(countIPage, wrapper);

        //3.封装分页结果集
        PageUtil<RevenueCount> pageUtil = new PageUtil<>();
        pageUtil.setPageData(resultPage.getRecords());
        pageUtil.setTotal(resultPage.getTotal());
        return pageUtil;
    }

    @Override
    public int addRevenueCount(RevenueCountVo countVo) {
        RevenueCount revenueCount = new RevenueCount();
        BeanUtils.copyProperties(countVo,revenueCount);
        return mapper.insert(revenueCount);
    }
}




