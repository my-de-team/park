package com.park.systemserve.service;

import com.park.systemserve.entity.RoadCount;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.utils.PageUtil;
import com.park.vos.systemvo.QueryCountCommonVo;
import com.park.vos.systemvo.RoadCountVo;

/**
* @author zdx
* @description 针对表【tp_road_count(ade-路段统计表)】的数据库操作Service
* @createDate 2023-07-14 14:31:17
*/
public interface RoadCountService extends IService<RoadCount> {

    PageUtil<RoadCount> queryPageRoadCount(Integer pageNum, Integer pageSize, QueryCountCommonVo queryVo);

    int addRoadCount(RoadCountVo countVo);
}
