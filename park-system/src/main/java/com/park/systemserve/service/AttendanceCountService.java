package com.park.systemserve.service;

import com.park.systemserve.entity.AttendanceCount;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.utils.PageUtil;
import com.park.vos.systemvo.AttendanceCountVo;
import com.park.vos.systemvo.QueryCountCommonVo;

/**
* @author zdx
* @description 针对表【tp_attendance_count(ade-考勤统计表)】的数据库操作Service
* @createDate 2023-07-14 11:18:45
*/
public interface AttendanceCountService extends IService<AttendanceCount> {

    PageUtil<AttendanceCount> queryPageAttendanceCount(Integer pageNum, Integer pageSize, QueryCountCommonVo queryVo);

    int addAttendanceCount(AttendanceCountVo countVo);
}
