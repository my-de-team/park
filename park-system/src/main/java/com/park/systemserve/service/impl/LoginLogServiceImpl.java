package com.park.systemserve.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.systemserve.entity.LoginLog;
import com.park.systemserve.service.LoginLogService;
import com.park.systemserve.mapper.LoginLogMapper;
import com.park.utils.PageUtil;
import com.park.vos.systemvo.LoginLogVo;
import com.park.vos.systemvo.QueryCountCommonVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
* @author zdx
* @description 针对表【tp_login_log(ade-登录日志表)】的数据库操作Service实现
* @createDate 2023-07-14 11:18:45
*/
@Service
public class LoginLogServiceImpl extends ServiceImpl<LoginLogMapper, LoginLog>
    implements LoginLogService{
    @Resource
    private LoginLogMapper mapper;

    /**
     * 添加登录日志的方法
     * @param logVo 登录日志Vo
     * @return 返回大于0添加成功
     */
    @Override
    public int addLoginLog(LoginLogVo logVo) {
        LoginLog loginLog = new LoginLog();
        BeanUtils.copyProperties(logVo,loginLog);
        return mapper.insert(loginLog);
    }

    @Override
    public PageUtil<LoginLog> queryPageLoginLog(Integer pageNum, Integer pageSize, QueryCountCommonVo queryVo) {
        //1.构建分页查询对象,传入分页设置参数
        IPage<LoginLog> countIPage = new Page<>(pageNum,pageSize);
        //2.构建查询条件
        QueryWrapper<LoginLog> wrapper = new QueryWrapper<>();
        if (!ObjectUtils.isEmpty(queryVo)){
            if (!ObjectUtils.isEmpty(queryVo.getStartTime())){
                wrapper.ge("create_time",queryVo.getStartTime());
            }
            if (!ObjectUtils.isEmpty(queryVo.getEndTime())){
                wrapper.le("create_time",queryVo.getEndTime());
            }
        }
        wrapper.orderByDesc("create_time");
        IPage<LoginLog> resultPage = mapper.selectPage(countIPage, wrapper);

        //3.封装分页结果集
        PageUtil<LoginLog> pageUtil = new PageUtil<>();
        pageUtil.setPageData(resultPage.getRecords());
        pageUtil.setTotal(resultPage.getTotal());
        return pageUtil;
    }

    @Override
    public Date getLastLoginTime(String mobile) {
        List<Date> dateList = mapper.getLastLoginTime(mobile);
        return dateList.get(0);
    }
}




