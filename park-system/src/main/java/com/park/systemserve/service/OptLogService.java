package com.park.systemserve.service;

import com.park.systemserve.entity.OptLog;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.utils.PageUtil;
import com.park.vos.systemvo.QueryCountCommonVo;

/**
* @author zdx
* @description 针对表【tp_opt_log(ade-操作日志表)】的数据库操作Service
* @createDate 2023-07-14 11:18:44
*/
public interface OptLogService extends IService<OptLog> {

    int addOptLog(OptLog optLog);

    PageUtil<OptLog> queryPageOptLog(Integer pageNum, Integer pageSize, QueryCountCommonVo queryVo);
}
