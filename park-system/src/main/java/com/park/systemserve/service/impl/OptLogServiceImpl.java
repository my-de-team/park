package com.park.systemserve.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.systemserve.entity.OptLog;
import com.park.systemserve.service.OptLogService;
import com.park.systemserve.mapper.OptLogMapper;
import com.park.utils.PageUtil;
import com.park.vos.systemvo.QueryCountCommonVo;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;

/**
* @author zdx
* @description 针对表【tp_opt_log(ade-操作日志表)】的数据库操作Service实现
* @createDate 2023-07-14 11:18:44
*/
@Service
public class OptLogServiceImpl extends ServiceImpl<OptLogMapper, OptLog>
    implements OptLogService{
    @Resource
    private OptLogMapper mapper;
    @Override
    public int addOptLog(OptLog optLog) {

        return mapper.insert(optLog);
    }

    @Override
    public PageUtil<OptLog> queryPageOptLog(Integer pageNum, Integer pageSize, QueryCountCommonVo queryVo) {
        //1.构建分页查询对象,传入分页设置参数
        IPage<OptLog> countIPage = new Page<>(pageNum,pageSize);
        //2.构建查询条件
        QueryWrapper<OptLog> wrapper = new QueryWrapper<>();
        if (!ObjectUtils.isEmpty(queryVo)){
            if (!ObjectUtils.isEmpty(queryVo.getStartTime())){
                wrapper.ge("opt_time",queryVo.getStartTime());
            }
            if (!ObjectUtils.isEmpty(queryVo.getEndTime())){
                wrapper.le("opt_time",queryVo.getEndTime());
            }
        }
        wrapper.orderByDesc("opt_time");
        IPage<OptLog> resultPage = mapper.selectPage(countIPage, wrapper);

        //3.封装分页结果集
        PageUtil<OptLog> pageUtil = new PageUtil<>();
        pageUtil.setPageData(resultPage.getRecords());
        pageUtil.setTotal(resultPage.getTotal());
        return pageUtil;
    }
}




