package com.park.systemserve.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ade-考勤统计表
 *
 * @TableName tp_attendance_count
 */
@TableName(value = "tp_attendance_count")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class AttendanceCount implements Serializable {
    /**
     * 考勤统计主键id
     */
    @Excel(name = "统计id", orderNum = "1", width = 30)
    @JsonSerialize(using = ToStringSerializer.class)
    @TableId(value = "id")
    private Long id;

    /**
     * 统计月份
     */
    @Excel(name = "统计月份", orderNum = "2", width = 30)
    @TableField(value = "count_month")
    private String countMonth;

    /**
     * 统计日期
     */
    @Excel(name = "统计日期", orderNum = "3", width = 30,exportFormat = "yyyy-MM-dd")
    @TableField(value = "count_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date countTime;

    /**
     * 巡检员id
     */
    @Excel(name = "巡检员id", orderNum = "4", width = 30)
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "inspector_id")
    private Long inspectorId;

    /**
     * 巡检员姓名
     */
    @Excel(name = "巡检员姓名", orderNum = "5", width = 30)
    @TableField(value = "inspector_name")
    private String inspectorName;

    /**
     * 排班天数
     */
    @Excel(name = "排班天数", orderNum = "6", width = 30)
    @TableField(value = "schedule_days")
    private Integer scheduleDays;

    /**
     * 正常天数
     */
    @Excel(name = "正常天数", orderNum = "7", width = 30)
    @TableField(value = "normal_days")
    private Integer normalDays;

    /**
     * 异常天数
     */
    @Excel(name = "异常天数", orderNum = "8", width = 30)
    @TableField(value = "abnormal_days")
    private Integer abnormalDays;

    /**
     * 生成该条数据的时间戳
     */
    @Excel(name = "生成该条数据的时间戳", orderNum = "9", width = 30,exportFormat = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}