package com.park.systemserve.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ade-登录日志表
 * @TableName tp_login_log
 */
@TableName(value ="tp_login_log")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class LoginLog implements Serializable {
    /**
     * 主键
     */
    @Excel(name = "日志id", orderNum = "1", width = 30)
    @JsonSerialize(using = ToStringSerializer.class)
    @TableId(value = "id")
    private Long id;

    /**
     * 用户名
     */
    @Excel(name = "用户名", orderNum = "2", width = 30)
    @TableField(value = "username")
    private String username;

    /**
     * 用户ip
     */
    @Excel(name = "登录ip地址", orderNum = "3", width = 30)
    @TableField(value = "ip_address")
    private String ipAddress;

    /**
     * 用户地址
     */
    @Excel(name = "地址", orderNum = "4", width = 30)
    @TableField(value = "ip_source")
    private String ipSource;

    /**
     * 浏览器
     */
    @Excel(name = "浏览器", orderNum = "5", width = 30)
    @TableField(value = "brower")
    private String brower;

    /**
     * 操作系统
     */
    @Excel(name = "操作系统", orderNum = "6", width = 30)
    @TableField(value = "os")
    private String os;

    /**
     * 登陆状态, 0登录成功,1登录失败
     */
    @Excel(name = "登陆状态, 0登录成功,1登录失败", orderNum = "7", width = 30)
    @TableField(value = "state")
    private Integer state;

    /**
     * 创建时间
     */
    @Excel(name = "登录时间", orderNum = "8", width = 30,exportFormat = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**
     * 备注信息
     */
    @Excel(name = "登录微信标识", orderNum = "9", width = 30)
    @TableField(value = "wechat")
    private String wechat;

    /**
     * 手机号码
     */
    @Excel(name = "绑定手机号码", orderNum = "10", width = 30)
    @TableField(value = "mobile")
    private String mobile;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}