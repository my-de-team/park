package com.park.systemserve.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ade-支付统计表
 * @TableName tp_pay_count
 */
@TableName(value ="tp_pay_count")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class PayCount implements Serializable {
    /**
     * 支付统计主键id
     */
    @Excel(name = "支付统计id", orderNum = "1", width = 30)
    @JsonSerialize(using = ToStringSerializer.class)
    @TableId(value = "id")
    private Long id;

    /**
     * 统计日期
     */
    @Excel(name = "统计日期", orderNum = "2", width = 30,exportFormat = "yyyy-MM-dd")
    @TableField(value = "count_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date countTime;

    /**
     * 当日主动支付次数统计（次）
     */
    @Excel(name = "当日主动支付次数统计（次）", orderNum = "3", width = 30)
    @TableField(value = "active_pay")
    private Integer activePay;

    /**
     * 当日现金支付次数统计（次）
     */
    @Excel(name = "当日现金支付次数统计（次）", orderNum = "4", width = 30)
    @TableField(value = "cash_pay")
    private Integer cashPay;

    /**
     * 当日二维码收款次数统计（次）
     */
    @Excel(name = "当日二维码收款次数统计（次）", orderNum = "5", width = 30)
    @TableField(value = "code_pay")
    private Integer codePay;

    /**
     * 当日微信支付金额统计（元）
     */
    @Excel(name = "当日微信支付金额统计（元）", orderNum = "6", width = 30)
    @TableField(value = "wechat_pay_amount")
    private BigDecimal wechatPayAmount;

    /**
     * 当日支付宝支付金额统计（元）
     */
    @Excel(name = "当日支付宝支付金额统计（元）", orderNum = "7", width = 30)
    @TableField(value = "alipay_pay_amount")
    private BigDecimal alipayPayAmount;

    /**
     * 当日现金支付金额统计（元）
     */
    @Excel(name = "当日现金支付金额统计（元）", orderNum = "8", width = 30)
    @TableField(value = "cash_pay_amount")
    private BigDecimal cashPayAmount;

    /**
     * 当日银联支付金额统计（元）
     */
    @Excel(name = "日银联支付金额统计（元）", orderNum = "9", width = 30)
    @TableField(value = "union_pay_amount")
    private BigDecimal unionPayAmount;

    /**
     * 生成该条数据的时间戳
     */
    @Excel(name = "生成该条数据的时间戳", orderNum = "10", width = 30,exportFormat = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}