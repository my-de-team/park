package com.park.systemserve.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ade-路段统计表
 *
 * @TableName tp_road_count
 */
@TableName(value = "tp_road_count")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class RoadCount implements Serializable {
    /**
     * 路段统计主键id
     */
    @Excel(name = "路段统计id", orderNum = "1", width = 30)
    @JsonSerialize(using = ToStringSerializer.class)
    @TableId(value = "id")
    private Long id;

    /**
     * 统计日期
     */
    @Excel(name = "统计日期", orderNum = "2", width = 30,exportFormat = "yyyy-MM-dd")
    @TableField(value = "count_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date countTime;

    /**
     * 路段总数（个）
     */
    @Excel(name = "路段总数（个）", orderNum = "3", width = 30)
    @TableField(value = "road_count_num")
    private Integer roadCountNum;

    /**
     * 统计的路段id
     */
    @Excel(name = "统计的路段id", orderNum = "4", width = 30)
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "road_id")
    private Long roadId;

    /**
     * 当日该路段订单总数
     */
    @Excel(name = "当日该路段订单总数", orderNum = "5", width = 30)
    @TableField(value = "order_count_num")
    private Integer orderCountNum;

    /**
     * 当日该路段营收金额（元）
     */
    @Excel(name = "当日该路段营收金额（元）", orderNum = "6", width = 30)
    @TableField(value = "revenue_amount")
    private BigDecimal revenueAmount;

    /**
     * 统计当日退款订单总数
     */
    @Excel(name = "统计当日退款订单总数", orderNum = "7", width = 30)
    @TableField(value = "refund_order")
    private Integer refundOrder;

    /**
     * 统计当日退款金额总数
     */
    @Excel(name = "统计当日退款金额总数", orderNum = "8", width = 30)
    @TableField(value = "refund_amount")
    private BigDecimal refundAmount;

    /**
     * 当日无订单的路段数（个）
     */
    @Excel(name = "当日无订单的路段数（个）", orderNum = "9", width = 30)
    @TableField(value = "zero_order_num")
    private Integer zeroOrderNum;

    /**
     * 生成该条数据的时间戳
     */
    @Excel(name = "生成该条数据的时间戳", orderNum = "10", width = 30,exportFormat = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}