package com.park.systemserve.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ade-营收统计表
 * @TableName tp_revenue_count
 */
@TableName(value ="tp_revenue_count")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class RevenueCount implements Serializable {
    /**
     * 营收统计主键id
     */
    @Excel(name = "营收统计id", orderNum = "1", width = 30)
    @JsonSerialize(using = ToStringSerializer.class)
    @TableId(value = "id")
    private Long id;

    /**
     * 统计日期
     */
    @Excel(name = "统计日期", orderNum = "2", width = 30,exportFormat = "yyyy-MM-dd")
    @TableField(value = "count_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date countTime;

    /**
     * 统计当日的订单总数（包含异常订单）
     */
    @Excel(name = "统计当日的订单总数（包含异常订单）", orderNum = "3", width = 30)
    @TableField(value = "order_count")
    private Integer orderCount;

    /**
     * 当日营收额总数（收入减去退款金额）
     */
    @Excel(name = "当日营收额总数（收入减去退款金额）", orderNum = "4", width = 30)
    @TableField(value = "revenue")
    private BigDecimal revenue;

    /**
     * 统计当日退款订单总数
     */
    @Excel(name = "统计当日退款订单总数", orderNum = "5", width = 30)
    @TableField(value = "refund_order")
    private Integer refundOrder;

    /**
     * 统计当日退款金额总数
     */
    @Excel(name = "统计当日退款金额总数", orderNum = "6", width = 30)
    @TableField(value = "refund_amount")
    private BigDecimal refundAmount;

    /**
     * 统计当日新注册用户数
     */
    @Excel(name = "统计当日新注册用户数", orderNum = "7", width = 30)
    @TableField(value = "new_user")
    private Integer newUser;

    /**
     * 生成该条数据的时间戳
     */
    @Excel(name = "生成该条数据的时间戳", orderNum = "8", width = 30,exportFormat = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}