package com.park.systemserve.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelIgnore;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ade-操作日志表
 * @TableName tp_opt_log
 */
@TableName(value ="tp_opt_log")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class OptLog implements Serializable {
    /**
     * 日志id
     */
    @Excel(name = "日志id", orderNum = "1", width = 30)
    @JsonSerialize(using = ToStringSerializer.class)
    @TableId(value = "id")
    private Long id;

    /**
     * 模块标题
     */
    @Excel(name = "模块标题", orderNum = "2", width = 30)
    @TableField(value = "title")
    private String title;

    /**
     * 业务类型（1 新增 2 修改 3 删除 ）
     */
    @Excel(name = "业务类型", orderNum = "3", width = 30)
    @TableField(value = "business_type")
    private Integer businessType;

    /**
     * 方法名称
     */
    @Excel(name = "方法名称", orderNum = "4", width = 30)
    @TableField(value = "method_name")
    private String methodName;

    /**
     * 方法路径
     */
    @Excel(name = "方法路径", orderNum = "5", width = 30)
    @TableField(value = "method_url")
    private String methodUrl;

    /**
     * 请求方式
     */
    @Excel(name = "请求方式", orderNum = "6", width = 30)
    @TableField(value = "req_type")
    private String reqType;

    /**
     * 操作者
     */
    @Excel(name = "操作人", orderNum = "7", width = 30)
    @TableField(value = "opt_name")
    private String optName;

    /**
     * 请求路径
     */
    @Excel(name = "请求路径", orderNum = "8", width = 30)
    @TableField(value = "opt_url")
    private String optUrl;

    /**
     * 操作者ip
     */
    @Excel(name = "操作者ip", orderNum = "9", width = 30)
    @TableField(value = "opt_ip")
    private String optIp;

    /**
     * 操作者地址
     */
    @Excel(name = "操作者地址", orderNum = "10", width = 30)
    @TableField(value = "opt_address")
    private String optAddress;

    /**
     * 请求参数
     */
    @ExcelIgnore
    @TableField(value = "req_param")
    private String reqParam;

    /**
     * 返回信息
     */
    @ExcelIgnore
    @TableField(value = "resp_data")
    private String respData;

    /**
     * 操作日期
     */
    @Excel(name = "操作时间", orderNum = "11", width = 30,exportFormat = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "opt_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date optTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}