package com.park.systemserve.mapper;

import com.park.systemserve.entity.LoginLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

/**
* @author zdx
* @description 针对表【tp_login_log(ade-登录日志表)】的数据库操作Mapper
* @createDate 2023-07-14 11:18:44
* @Entity com.park.systemserve.entity.LoginLog
*/
public interface LoginLogMapper extends BaseMapper<LoginLog> {
    @Select("select create_time from tp_login_log where mobile = #{mobile} and state = 0 order by create_time desc ")
    List<Date> getLastLoginTime(String mobile);
}




