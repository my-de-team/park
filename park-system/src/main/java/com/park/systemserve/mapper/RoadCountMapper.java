package com.park.systemserve.mapper;

import com.park.systemserve.entity.RoadCount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author zdx
* @description 针对表【tp_road_count(ade-路段统计表)】的数据库操作Mapper
* @createDate 2023-07-14 14:31:17
* @Entity com.park.systemserve.entity.RoadCount
*/
public interface RoadCountMapper extends BaseMapper<RoadCount> {

}




