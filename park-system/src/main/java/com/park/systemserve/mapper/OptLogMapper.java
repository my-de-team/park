package com.park.systemserve.mapper;

import com.park.systemserve.entity.OptLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author zdx
* @description 针对表【tp_opt_log(ade-操作日志表)】的数据库操作Mapper
* @createDate 2023-07-14 11:18:44
* @Entity com.park.systemserve.entity.OptLog
*/
public interface OptLogMapper extends BaseMapper<OptLog> {

}




