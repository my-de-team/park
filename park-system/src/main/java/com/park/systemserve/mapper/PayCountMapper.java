package com.park.systemserve.mapper;

import com.park.systemserve.entity.PayCount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author zdx
* @description 针对表【tp_pay_count(ade-支付统计表)】的数据库操作Mapper
* @createDate 2023-07-14 11:18:44
* @Entity com.park.systemserve.entity.PayCount
*/
public interface PayCountMapper extends BaseMapper<PayCount> {

}




