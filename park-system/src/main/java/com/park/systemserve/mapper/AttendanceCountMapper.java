package com.park.systemserve.mapper;

import com.park.systemserve.entity.AttendanceCount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author zdx
* @description 针对表【tp_attendance_count(ade-考勤统计表)】的数据库操作Mapper
* @createDate 2023-07-14 11:18:45
* @Entity com.park.systemserve.entity.AttendanceCount
*/
public interface AttendanceCountMapper extends BaseMapper<AttendanceCount> {

}




