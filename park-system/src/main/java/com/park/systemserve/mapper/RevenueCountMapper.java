package com.park.systemserve.mapper;

import com.park.systemserve.entity.RevenueCount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author zdx
* @description 针对表【tp_revenue_count(ade-营收统计表)】的数据库操作Mapper
* @createDate 2023-07-14 14:31:17
* @Entity com.park.systemserve.entity.RevenueCount
*/
public interface RevenueCountMapper extends BaseMapper<RevenueCount> {

}




