package com.park.systemserve.controller;

import com.park.annotation.Log;
import com.park.systemserve.entity.LoginLog;
import com.park.systemserve.entity.OptLog;
import com.park.systemserve.entity.OptLog;
import com.park.systemserve.service.OptLogService;
import com.park.systemserve.utils.ExcelUtils;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.vos.systemvo.OptLogVo;
import com.park.vos.systemvo.QueryCountCommonVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/system/opt")
@Api(tags = "系统微服务: 操作日志管理")
public class OptLogController {
    @Resource
    private OptLogService optLogService;

    @PostMapping
    @ApiOperation("添加操作日志")
    public Result addOptLog(@RequestBody OptLogVo optLogVo){
        OptLog optLog = new OptLog();
        BeanUtils.copyProperties(optLogVo,optLog);
        int flag = optLogService.addOptLog(optLog);
        return Result.success(flag);
    }

    /**
     * 操作日志分页条件查询
     * @param pageNum 起始页
     * @param pageSize 大小
     * @param queryVo 操作时间
     * @return 返回分页结果集
     */
    @Log
    @PostMapping("/page/{pageNum}/{pageSize}")
    @ApiOperation("分页条件查询")
    public Result queryPageOptLog(@PathVariable("pageNum")Integer pageNum,
                                           @PathVariable("pageSize")Integer pageSize,
                                           @RequestBody QueryCountCommonVo queryVo){
        PageUtil<OptLog> optLogPageUtil = optLogService.queryPageOptLog(pageNum,pageSize,queryVo);
        return Result.success(optLogPageUtil);
    }

    /** --导出操作日志--*/
    @ApiOperation("导出excel文件")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response) {
        try {
            // 查询所有店铺信息
            String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            List<OptLog> list = optLogService.list();
            // List<?> list, String title, String sheetName, Class<?> pojoClass, String fileName, HttpServletResponse response
            ExcelUtils.exportExcel(list, "登录日志表", "登录日志数据", OptLog.class, date+"操作日志.xls", response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** --导入操作日志--*/
    @ApiOperation("导入excel文件")
    @PostMapping("/importExcel")
    public void importExcel(@RequestPart("file") MultipartFile file) {
        List<OptLog> optLogList = ExcelUtils.importExcel(file, 1, 1, OptLog.class);
        System.out.println("导入数据共" + optLogList.size() + "行");
        optLogList.forEach(System.out::println);
        // 通过批量添加数据到数据库
        // shopService.saveBatch(shops, shops.size());
    }
}
