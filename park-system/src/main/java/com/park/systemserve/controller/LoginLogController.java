package com.park.systemserve.controller;

import com.park.annotation.Log;
import com.park.systemserve.entity.LoginLog;
import com.park.systemserve.service.LoginLogService;
import com.park.systemserve.utils.ExcelUtils;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.vos.systemvo.LoginLogVo;
import com.park.vos.systemvo.QueryCountCommonVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/system/login/Log")
@Api(tags = "系统微服务: 登录日志管理")
public class LoginLogController {
    @Resource
    private LoginLogService loginLogService;

    /**
     * 添加登录日志的接口
     * @param logVo 登录日志vo
     * @return 返回值大于0添加成功
     */
    @ApiOperation("添加登录日志")
    @PostMapping
    public Result addLoginLog(@RequestBody LoginLogVo logVo){
        int flag = loginLogService.addLoginLog(logVo);
        return Result.success(flag);
    }

    /**
     * 登录日志分页条件查询vo
     * @param pageNum 页码
     * @param pageSize 大小
     * @param queryVo 条件
     * @return 返回分页结果集
     */
    @Log
    @PostMapping("/page/{pageNum}/{pageSize}")
    @ApiOperation("分页条件查询")
    public Result queryPageLoginLog(@PathVariable("pageNum")Integer pageNum,
                                  @PathVariable("pageSize")Integer pageSize,
                                  @RequestBody QueryCountCommonVo queryVo){
        PageUtil<LoginLog> loginLogPageUtil = loginLogService.queryPageLoginLog(pageNum,pageSize,queryVo);
        return Result.success(loginLogPageUtil);
    }

    /**根据手机号码查询最近一次登录时间*/
    @GetMapping("/last/{mobile}")
    @Log
    @ApiOperation("根据手机号码查询最近一次登录时间")
    public Result<Date> getLastLoginTime(@PathVariable("mobile") String mobile){
        Date lastLoginTime = loginLogService.getLastLoginTime(mobile);
        return Result.success(lastLoginTime);
    }
    /** --导出登陆日志--*/
    @ApiOperation("导出excel文件")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response) {
        try {
            // 查询所有店铺信息
            String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            List<LoginLog> list = loginLogService.list();
            // List<?> list, String title, String sheetName, Class<?> pojoClass, String fileName, HttpServletResponse response
            ExcelUtils.exportExcel(list, "登录日志表", "登录日志数据", LoginLog.class, date+"登录日志.xls", response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** --导入登陆日志--*/
    @ApiOperation("导入excel文件")
    @PostMapping("/importExcel")
    public void importExcel(@RequestPart("file") MultipartFile file) {
        List<LoginLog> tradeLogList = ExcelUtils.importExcel(file, 1, 1, LoginLog.class);
        System.out.println("导入数据共" + tradeLogList.size() + "行");
        tradeLogList.forEach(System.out::println);
        // 通过批量添加数据到数据库
        // shopService.saveBatch(shops, shops.size());
    }
}
