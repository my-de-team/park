package com.park.systemserve.controller;

import com.park.annotation.Log;
import com.park.systemserve.entity.AttendanceCount;
import com.park.systemserve.service.AttendanceCountService;
import com.park.systemserve.utils.ExcelUtils;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.vos.systemvo.AttendanceCountVo;
import com.park.vos.systemvo.QueryCountCommonVo;
import com.park.vos.systemvo.RevenueCountVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/system/attendance")
@Api(tags = "系统微服务: 考勤统计")
public class AttendanceCountController {
    @Resource
    private AttendanceCountService attendanceService;

    /**
     * 条件分页查询的方法
     * @param pageNum 前台传入的页码
     * @param pageSize 前台传入的每页条数
     * @param queryVo 前台传入的条件vo,起始时间/结束时间/路段id
     * @return 分页结果,包含total总条数/data数据
     */
    @Log
    @PostMapping("/page/{pageNum}/{pageSize}")
    @ApiOperation("分页条件查询")
    public Result queryPageAttendanceCount(@PathVariable("pageNum")Integer pageNum,
                                        @PathVariable("pageSize")Integer pageSize,
                                        @RequestBody QueryCountCommonVo queryVo){
        PageUtil<AttendanceCount> revenuePageUtil = attendanceService.queryPageAttendanceCount(pageNum,pageSize,queryVo);
        return Result.success(revenuePageUtil);
    }

    /** 添加考勤统计信息*/
    @Log
    @PostMapping
    @ApiOperation("添加考勤统计信息")
    public Result addAttendanceCount(@RequestBody AttendanceCountVo countVo){
        int flag = attendanceService.addAttendanceCount(countVo);
        return Result.success(flag);
    }

    /** 导出excel文件*/
    @ApiOperation("导出excel文件")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response) {
        try {
            // 查询所有店铺信息
            String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            List<AttendanceCount> list = attendanceService.list();
            // List<?> list, String title, String sheetName, Class<?> pojoClass, String fileName, HttpServletResponse response
            ExcelUtils.exportExcel(list, "考勤统计表", "考勤数据", AttendanceCount.class, date+"考勤统计.xls", response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** 导入excel文件*/
    @ApiOperation("导入excel文件")
    @PostMapping("/importExcel")
    public void importExcel(@RequestPart("file") MultipartFile file) {
        List<AttendanceCount> attendanceCountList = ExcelUtils.importExcel(file, 1, 1, AttendanceCount.class);
        System.out.println("导入数据共" + attendanceCountList.size() + "行");
        attendanceCountList.forEach(System.out::println);
        // 通过批量添加数据到数据库
//        shopService.saveBatch(shops, shops.size());
    }
}
