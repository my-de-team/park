package com.park.systemserve.controller;

import com.park.annotation.Log;
import com.park.systemserve.entity.PayCount;
import com.park.systemserve.entity.RevenueCount;
import com.park.systemserve.entity.RoadCount;
import com.park.systemserve.service.RoadCountService;
import com.park.systemserve.utils.ExcelUtils;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.vos.systemvo.PayCountVo;
import com.park.vos.systemvo.QueryCountCommonVo;
import com.park.vos.systemvo.RoadCountVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/system/road")
@Api(tags = "系统微服务: 路段统计")
public class RoadCountController {
    @Resource
    private RoadCountService roadService;

    /**
     * 条件分页查询的方法
     *
     * @param pageNum  前台传入的页码
     * @param pageSize 前台传入的每页条数
     * @param queryVo  前台传入的条件vo,起始时间/结束时间/路段id
     * @return 分页结果, 包含total总条数/data数据
     */
    @Log
    @PostMapping("/page/{pageNum}/{pageSize}")
    @ApiOperation("分页条件查询")
    public Result queryPageRoadCount(@PathVariable("pageNum") Integer pageNum,
                                     @PathVariable("pageSize") Integer pageSize,
                                     @RequestBody QueryCountCommonVo queryVo) {
        PageUtil<RoadCount> roadPageUtil = roadService.queryPageRoadCount(pageNum, pageSize, queryVo);
        return Result.success(roadPageUtil);
    }

    /**添加路段统计记录*/
    @Log
    @PostMapping
    @ApiOperation("添加路段统计记录")
    public Result addRoadCount(@RequestBody RoadCountVo countVo){
        int flag = roadService.addRoadCount(countVo);
        return Result.success(flag);
    }

    /** --导出路段统计--*/
    @ApiOperation("导出excel文件")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response) {
        try {
            // 查询所有店铺信息
            String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            List<RoadCount> list = roadService.list();
            // List<?> list, String title, String sheetName, Class<?> pojoClass, String fileName, HttpServletResponse response
            ExcelUtils.exportExcel(list, "路段统计表", "路段统计数据", RoadCount.class, date+"路段统计.xls", response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** --导入路段统计--*/
    @ApiOperation("导入excel文件")
    @PostMapping("/importExcel")
    public void importExcel(@RequestPart("file") MultipartFile file) {
        List<RoadCount> RoadCount = ExcelUtils.importExcel(file, 1, 1, RoadCount.class);
        System.out.println("导入数据共" + RoadCount.size() + "行");
        RoadCount.forEach(System.out::println);
        // 通过批量添加数据到数据库
        // shopService.saveBatch(shops, shops.size());
    }
}
