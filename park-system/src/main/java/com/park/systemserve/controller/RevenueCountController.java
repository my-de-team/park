package com.park.systemserve.controller;

import com.park.annotation.Log;
import com.park.systemserve.entity.PayCount;
import com.park.systemserve.entity.RevenueCount;
import com.park.systemserve.service.RevenueCountService;
import com.park.systemserve.utils.ExcelUtils;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.vos.systemvo.QueryCountCommonVo;
import com.park.vos.systemvo.RevenueCountVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/system/revenue")
@Api(tags = "系统微服务: 营收统计")
public class RevenueCountController {
    @Resource
    private RevenueCountService revenueService;

    /**
     * 条件分页查询的方法
     * @param pageNum 前台传入的页码
     * @param pageSize 前台传入的每页条数
     * @param queryVo 前台传入的条件vo,起始时间/结束时间/路段id
     * @return 分页结果,包含total总条数/data数据
     */
    @Log
    @PostMapping("/page/{pageNum}/{pageSize}")
    @ApiOperation("分页条件查询")
    public Result queryPageRevenueCount(@PathVariable("pageNum")Integer pageNum,
                                        @PathVariable("pageSize")Integer pageSize,
                                        @RequestBody QueryCountCommonVo queryVo){
        PageUtil<RevenueCount> revenuePageUtil = revenueService.queryPageRevenueCount(pageNum,pageSize,queryVo);
        return Result.success(revenuePageUtil);
    }

    /**
     * 添加营收记录
     * @param countVo 数据vo
     * @return 返回大于0添加成功
     */
    @Log
    @PostMapping
    @ApiOperation("添加营收统计信息")
    public Result addRevenueCount(@RequestBody RevenueCountVo countVo){
        int flag = revenueService.addRevenueCount(countVo);
        return Result.success(flag);
    }

    /** --导出营收统计--*/
    @ApiOperation("导出excel文件")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response) {
        try {
            // 查询所有店铺信息
            String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            List<RevenueCount> list = revenueService.list();
            // List<?> list, String title, String sheetName, Class<?> pojoClass, String fileName, HttpServletResponse response
            ExcelUtils.exportExcel(list, "营收统计表", "营收统计数据", RevenueCount.class, date+"营收统计.xls", response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** --导入营收统计--*/
    @ApiOperation("导入excel文件")
    @PostMapping("/importExcel")
    public void importExcel(@RequestPart("file") MultipartFile file) {
        List<RevenueCount> revenueCountList = ExcelUtils.importExcel(file, 1, 1, RevenueCount.class);
        System.out.println("导入数据共" + revenueCountList.size() + "行");
        revenueCountList.forEach(System.out::println);
        // 通过批量添加数据到数据库
        // shopService.saveBatch(shops, shops.size());
    }
}
