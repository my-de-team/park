package com.park.api;

import com.park.annotation.Log;
import com.park.utils.Result;
import com.park.vos.systemvo.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Date;

/**
 * 系统微服务的feign客户端
 */
@FeignClient("park-system")
public interface SystemControllerApi {
    /**
     * 添加操作日志的远程调用接口方法
     *
     * @param optLogvo 操作日志vo
     */
    @PostMapping("/system/opt")
    public Result addOptLog(@RequestBody OptLogVo optLogvo);

    /**
     * 添加登录日志的远程调用接口方法
     *
     * @param logVo 登录日志vo
     */
    @PostMapping("/system/login/Log")
    public Result addLoginLog(@RequestBody LoginLogVo logVo);

    /**
     * 添加营收记录
     */
    @PostMapping("/system/revenue")
    public Result addRevenueCount(@RequestBody RevenueCountVo countVo);

    /**
     * 添加支付统计记录
     */
    @PostMapping("/system/pay")
    public Result addPayCount(@RequestBody PayCountVo countVo);

    /**
     * 添加路段统计记录
     */
    @PostMapping("/system/road")
    public Result addRoadCount(@RequestBody RoadCountVo countVo);

    /** 添加考勤统计信息*/
    @PostMapping("/system/attendance")
    public Result addAttendanceCount(@RequestBody AttendanceCountVo countVo);
    /**根据手机号码查询最近一次登录时间*/
    @GetMapping("/system/login/Log/last/{mobile}")
    public Result<Date> getLastLoginTime(@PathVariable("mobile") String mobile);
}
