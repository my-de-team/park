package com.park.api;

import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.vos.userservevo.AppealQueryVo;
import com.park.vos.userservevo.AppealVo;
import com.park.vos.userservevo.OrderPageVo;
import com.park.vos.userservevo.OrderQueryPageVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("park-user")
public interface UserControllerApi {

    @PostMapping("/order/queryPageOrder/{pageNum}/{pageSize}")
    public Result<PageUtil<OrderPageVo>> queryPageOrder(@PathVariable("pageNum") Long pageNum,
                                                        @PathVariable("pageSize") Long pageSize,
                                                        @RequestBody OrderQueryPageVo orderQueryPageVo);

    @PostMapping("/appeal/queryPageAppeal/{pageNum}/{pageSize}")
    public Result<PageUtil<AppealVo>> queryPageAppeal(@PathVariable("pageNum") Integer pageNum,
                                                      @PathVariable("pageSize") Integer pageSize,
                                                      @RequestBody AppealQueryVo appealQueryVo);

    @GetMapping("/user/userCount")
    public Result<Integer> getUserByCreateTime();
}
