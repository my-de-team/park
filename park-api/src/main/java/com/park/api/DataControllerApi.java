package com.park.api;

import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.vos.datavo.QueryPeopleVo;
import com.park.vos.datavo.RoadSegmentQueryVo;
import com.park.vos.datavo.RoadSegmentVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("park-data")
public interface DataControllerApi {

    /**
    路段管理模块分页查询
     */
    @PostMapping("/road-segment/page/{pageNum}/{pageSize}")
    public Result<PageUtil<RoadSegmentVo>> queryPageRoadSegment(
            @PathVariable("pageNum") Integer pageNum,
            @PathVariable("pageSize") Integer pageSize,
            @RequestBody RoadSegmentQueryVo roadSegmentQueryVo
    );
    /**巡检员分页查询*/
    @PostMapping("/people/page/inspector/{pageNum}/{pageSize}")
    public Result queryPeopleAll(@PathVariable("pageNum") Integer pageNum,
                                 @PathVariable("pageSize") Integer pageSize,
                                 @RequestBody QueryPeopleVo queryPeopleVo);
}
