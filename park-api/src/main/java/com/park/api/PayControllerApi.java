package com.park.api;

import com.park.annotation.Log;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.vos.payservevo.TradeLogVo;
import com.park.vos.systemvo.QueryTradeLogVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("park-pay")
public interface PayControllerApi {
    /*支付宝接口*/
    @PostMapping("/pay/alipay/order")
    public Result<Object> placeOrderForPCWeb(@RequestBody TradeLogVo logVo);

    /*银联支付接口*/
    @PostMapping("/pay/union/order")
    public Result<Object> addOrder(@RequestBody TradeLogVo logVo);

    /*其他支付接口*/
    @PostMapping("/pay")
    public Result addTradeLog(@RequestBody TradeLogVo logVo);

    /*根据订单id查询支付记录*/
    @GetMapping("/pay/{orderId}")
    public Result<TradeLogVo> getTradeLogByOrderId(@PathVariable("orderId") Long orderId);
    /**交易记录分页条件查询*/
    @PostMapping("/pay/page/{pageNum}/{pageSize}")
    public Result<PageUtil<TradeLogVo>> queryPageTradeLog(@PathVariable("pageNum")Integer pageNum,
                                                          @PathVariable("pageSize")Integer pageSize,
                                                          @RequestBody QueryTradeLogVo queryVo);
}
