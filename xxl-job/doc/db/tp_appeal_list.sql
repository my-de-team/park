/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80032 (8.0.32)
 Source Host           : localhost:3306
 Source Schema         : park

 Target Server Type    : MySQL
 Target Server Version : 80032 (8.0.32)
 File Encoding         : 65001

 Date: 18/07/2023 20:04:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tp_appeal_list
-- ----------------------------
DROP TABLE IF EXISTS `tp_appeal_list`;
CREATE TABLE `tp_appeal_list`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '申诉表ID',
  `order_id` bigint NOT NULL COMMENT '订单表ID',
  `order_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单编码',
  `appeal_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '申诉内容',
  `appeal_time` datetime NULL DEFAULT NULL COMMENT '申诉时间',
  `appeal_process_state` int NOT NULL DEFAULT 0 COMMENT '处理情况，0-待处理，1-已退款，2-不通过',
  `modification_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '修改金额',
  `refund_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '退款金额',
  `process_time` datetime NULL DEFAULT NULL COMMENT '处理时间',
  `process_user_id` bigint NULL DEFAULT NULL COMMENT '处理人',
  `leave_words` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '留言',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_deleted` int NOT NULL DEFAULT 0 COMMENT '逻辑删除，0-未删除（默认），1-已删除',
  `version` int NOT NULL DEFAULT 0 COMMENT '乐观锁，版本号（默认0）',
  `field1` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段一',
  `field2` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段二',
  `field3` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段三',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `order_id`(`order_id` ASC) USING BTREE,
  INDEX `order_number`(`order_number` ASC) USING BTREE,
  INDEX `appeal_process_state`(`appeal_process_state` ASC) USING BTREE,
  INDEX `process_user`(`process_user_id` ASC) USING BTREE,
  INDEX `create_time`(`create_time` ASC) USING BTREE,
  INDEX `update_time`(`update_time` ASC) USING BTREE,
  INDEX `is_deleted`(`is_deleted` ASC) USING BTREE,
  INDEX `version`(`version` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1679784962879959043 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '售后申诉表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tp_appeal_list
-- ----------------------------
INSERT INTO `tp_appeal_list` VALUES (1679784856130727938, 1, '111', '储扒皮太黑了', '2023-07-14 01:22:01', 1, 20.00, 30.00, '2023-07-14 01:22:01', NULL, '给我尽快处理', '2023-07-14 17:28:13', '2023-07-17 10:15:34', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_appeal_list` VALUES (1679784934572601346, 2, '111', '储扒皮太黑了', '2023-07-14 01:22:01', 2, 20.00, 30.00, '2023-07-14 01:22:01', NULL, '给我尽快处理', '2023-07-14 17:28:31', '2023-07-15 16:12:25', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_appeal_list` VALUES (1679784962879959042, 3, '111', '储扒皮黑的一匹', '2023-07-14 01:22:01', 1, 20.00, 30.00, '2023-07-14 01:22:01', NULL, '给我尽快处理', '2023-07-14 17:28:38', '2023-07-15 16:12:27', 0, 0, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
