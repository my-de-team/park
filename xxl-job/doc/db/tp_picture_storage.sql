/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80032 (8.0.32)
 Source Host           : localhost:3306
 Source Schema         : park

 Target Server Type    : MySQL
 Target Server Version : 80032 (8.0.32)
 File Encoding         : 65001

 Date: 18/07/2023 20:04:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tp_picture_storage
-- ----------------------------
DROP TABLE IF EXISTS `tp_picture_storage`;
CREATE TABLE `tp_picture_storage`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '图片表ID',
  `order_id` bigint NOT NULL COMMENT '订单表ID，与订单表关联的字段',
  `order_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单编号，与订单表关联的字段',
  `picture_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '图片路径',
  `picture_attribute` int NOT NULL DEFAULT 0 COMMENT '图片属性，0-驶入图片，1-申诉图片',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_deleted` int NOT NULL DEFAULT 0 COMMENT '逻辑删除，0-未删除（默认），1-已删除',
  `version` int NOT NULL DEFAULT 0 COMMENT '乐观锁，版本号（默认0）',
  `field1` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段一',
  `field2` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段二',
  `field3` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段三',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `order_id`(`order_id` ASC) USING BTREE,
  INDEX `order_number`(`order_number` ASC) USING BTREE,
  INDEX `picture_attribute`(`picture_attribute` ASC) USING BTREE,
  INDEX `create_time`(`create_time` ASC) USING BTREE,
  INDEX `update_time`(`update_time` ASC) USING BTREE,
  INDEX `is_deleted`(`is_deleted` ASC) USING BTREE,
  INDEX `version`(`version` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1681226360117719042 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '存储驶入图片表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tp_picture_storage
-- ----------------------------
INSERT INTO `tp_picture_storage` VALUES (1, 1, 'ppppp', 'lll1', 0, '2023-07-18 08:45:17', '2023-07-18 08:45:17', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_picture_storage` VALUES (1679761398470578177, 1, '111', 'dawiodjawidjawdij', 0, '2023-07-14 15:55:00', '2023-07-14 15:55:00', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_picture_storage` VALUES (1679761487880556545, 2, '111', 'dawiodjawidjawdij', 0, '2023-07-14 15:55:21', '2023-07-14 15:55:21', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_picture_storage` VALUES (1679761522114465793, 3, '111', 'dawiodjawidjawdij', 0, '2023-07-14 15:55:30', '2023-07-14 15:55:59', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_picture_storage` VALUES (1679764588171685889, 666, 'ooooooooo', 'first1', 0, '2023-07-14 16:07:41', '2023-07-14 16:07:41', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_picture_storage` VALUES (1679764589480308737, 666, 'ooooooooo', 'first2', 0, '2023-07-14 16:07:41', '2023-07-14 16:07:41', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_picture_storage` VALUES (1679764589480308738, 666, 'ooooooooo', 'first3', 0, '2023-07-14 16:07:41', '2023-07-14 16:07:41', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_picture_storage` VALUES (1679764589547417602, 666, 'ooooooooo', 'first4', 0, '2023-07-14 16:07:41', '2023-07-14 16:07:41', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_picture_storage` VALUES (1679764821467262977, 787878, '111', 'dawiodjawidjawdij', 0, '2023-07-14 16:08:36', '2023-07-14 16:09:17', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_picture_storage` VALUES (1681226360117719041, 1, 'ORN1', 'field', 0, '2023-07-18 16:56:15', '2023-07-18 16:56:15', 0, 0, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
