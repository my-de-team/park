/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80032 (8.0.32)
 Source Host           : localhost:3306
 Source Schema         : park

 Target Server Type    : MySQL
 Target Server Version : 80032 (8.0.32)
 File Encoding         : 65001

 Date: 18/07/2023 20:05:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tp_order_list
-- ----------------------------
DROP TABLE IF EXISTS `tp_order_list`;
CREATE TABLE `tp_order_list`  (
  `id` bigint NOT NULL COMMENT '订单ID，雪花算法自动生成',
  `order_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单编号，前缀【ORN】+ 雪花算法自动生成',
  `order_submission_time` datetime NOT NULL COMMENT '订单提交时间，创建时间',
  `plate_number` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '车牌号码，格式【粤B】+ 英文与字母组合（五位）',
  `road_segment` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所属路段',
  `berth_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '泊位编号',
  `inspector_id` bigint NULL DEFAULT NULL COMMENT '巡检员,用于关联员工表的巡检员ID',
  `order_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '订单金额，【订单金额】 = 【停车时间】* 【对应时间段单价】',
  `order_state` int NOT NULL DEFAULT 0 COMMENT '订单状态，0-进行中，1-待支付，2-已支付，3-已完成',
  `bound_user` bigint NULL DEFAULT NULL COMMENT '绑定用户,绑定用户表的id',
  `magnetic_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '地磁编号',
  `drive_time` datetime NULL DEFAULT NULL COMMENT '驶入时间',
  `leave_time` datetime NULL DEFAULT NULL COMMENT '驶离时间',
  `park_time` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '停车时长，【驶离时间】 — 【驶入时间】',
  `order_abnormal_state` int NOT NULL DEFAULT 0 COMMENT '订单是否异常，0-正常订单（默认），1-异常订单（用户申诉后订单状态变为异常）',
  `order_reality_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '实付金额，实际付款金额',
  `way_pay` int NULL DEFAULT NULL COMMENT '支付方式，1-支付宝支付，2-银联，3-微信支付，4-现金支付',
  `pay_time` datetime NULL DEFAULT NULL COMMENT '支付时间',
  `pay_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '支付流水号',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_deleted` int NOT NULL DEFAULT 0 COMMENT '逻辑删除，0-未删除（默认），1-已删除',
  `version` int NOT NULL DEFAULT 0 COMMENT '乐观锁，版本号（默认0）',
  `field1` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段一',
  `field2` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段二',
  `field3` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段三',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `order_number`(`order_number` ASC) USING BTREE,
  INDEX `plate_number`(`plate_number` ASC) USING BTREE,
  INDEX `magnetic_number`(`magnetic_number` ASC) USING BTREE,
  INDEX `order_state`(`order_state` ASC) USING BTREE,
  INDEX `order_abnormal_state`(`order_abnormal_state` ASC) USING BTREE,
  INDEX `way_pay`(`way_pay` ASC) USING BTREE,
  INDEX `create_time`(`create_time` ASC) USING BTREE,
  INDEX `update_time`(`update_time` ASC) USING BTREE,
  INDEX `is_deleted`(`is_deleted` ASC) USING BTREE,
  INDEX `version`(`version` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '订单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tp_order_list
-- ----------------------------
INSERT INTO `tp_order_list` VALUES (1, 'ORN1', '2023-07-17 00:00:00', '粤B1234', 'Road 1', 'Berth 1', 1, 304.00, 2, 1, 'Mag 1', '2023-07-14 08:00:00', '2023-07-17 12:05:07', '4565', 0, 304.00, 1, '2023-07-18 08:52:55', 'PAYMENT7080089106377740288', '2023-07-17 09:00:00', '2023-07-18 17:57:53', 0, 0, 'Field 1', 'Field 2', 'Field 3');
INSERT INTO `tp_order_list` VALUES (2, 'ORN2', '2023-07-17 01:00:00', '粤B2345', 'Road 2', 'Berth 2', 1, 20.00, 1, 2, 'Mag 2', '2023-07-14 10:00:00', '2023-07-14 11:30:00', '1.5 hours', 0, 20.00, 2, '2023-07-14 11:30:00', 'Pay 2', '2023-07-17 11:30:00', '2023-07-18 17:57:54', 0, 0, 'Field 1', 'Field 2', 'Field 3');
INSERT INTO `tp_order_list` VALUES (3, 'ORN3', '2023-07-17 02:00:00', '粤B3456', 'Road 3', 'Berth 3', 2, 15.00, 2, 3, 'Mag 3', '2023-07-14 12:00:00', '2023-07-14 14:00:00', '2 hours', 0, 15.00, 3, '2023-07-14 14:00:00', 'Pay 3', '2023-07-17 14:00:00', '2023-07-18 17:57:54', 0, 0, 'Field 1', 'Field 2', 'Field 3');
INSERT INTO `tp_order_list` VALUES (4, 'ORN4', '2023-07-17 03:00:00', '粤B4567', 'Road 4', 'Berth 4', 2, 18.50, 3, 4, 'Mag 4', '2023-07-14 16:00:00', '2023-07-14 18:30:00', '2.5 hours', 0, 18.50, 1, '2023-07-14 18:30:00', 'Pay 4', '2023-07-17 18:30:00', '2023-07-18 17:57:56', 0, 0, 'Field 1', 'Field 2', 'Field 3');
INSERT INTO `tp_order_list` VALUES (5, 'ORN5', '2023-07-17 04:00:00', '粤B5678', 'Road 5', 'Berth 5', 1, 12.80, 2, 5, 'Mag 5', '2023-07-14 20:00:00', '2023-07-14 21:15:00', '1.25 hours', 0, 12.80, 2, '2023-07-14 21:15:00', 'Pay 5', '2023-07-17 21:15:00', '2023-07-18 17:57:57', 0, 0, 'Field 1', 'Field 2', 'Field 3');
INSERT INTO `tp_order_list` VALUES (1679716725026836482, 'ORN6', '2023-07-17 20:18:34', 'dawdaw', '个人饿', '324234', 1, 3333.00, 2, 1, '3123123', '2023-07-13 20:18:34', '2023-07-13 20:18:34', '1小时', 0, 9999.00, 1, '2023-07-13 20:18:34', '3423', '2023-07-17 20:18:34', '2023-07-18 17:57:58', 0, 0, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
