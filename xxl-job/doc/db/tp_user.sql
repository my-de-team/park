/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80032 (8.0.32)
 Source Host           : localhost:3306
 Source Schema         : park

 Target Server Type    : MySQL
 Target Server Version : 80032 (8.0.32)
 File Encoding         : 65001

 Date: 18/07/2023 20:02:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tp_user
-- ----------------------------
DROP TABLE IF EXISTS `tp_user`;
CREATE TABLE `tp_user`  (
  `id` bigint NOT NULL COMMENT '用户主键id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户名称',
  `mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '手机号',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码',
  `wechat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '微信号 微信注册/登录返回的id',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像',
  `bind_number` int NULL DEFAULT NULL COMMENT '绑定车辆数量',
  `is_bind` int NULL DEFAULT NULL COMMENT '是否绑定微信号 0: 绑定 1: 没绑定',
  `state` int NULL DEFAULT NULL COMMENT '状态 0：正常 1：异常',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间 注册时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `version` int NULL DEFAULT 0 COMMENT '乐观锁 默认：0',
  `is_deleted` int NULL DEFAULT 0 COMMENT '是否删除 0:未删除 1:已删除',
  `field1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段1',
  `field2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段2',
  `field3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段3',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tp_user
-- ----------------------------
INSERT INTO `tp_user` VALUES (1234, '张三四', '12344444', NULL, '666', '43', NULL, NULL, NULL, '2023-07-06 15:25:53', NULL, 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_user` VALUES (123456789, '李四', '1333445', '$2a$10$pokEB7IapvBovFBXqDYVEO9h1/PxOd2J8/XTY.hU1mESfEpdC.VHq', '777', 'jpg', 0, 0, 0, '2023-07-14 08:37:53', '2023-07-14 08:37:53', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_user` VALUES (1681211882737696769, '王五2', '12345678999', '$2a$10$N.v2s2gLkOB6axO.UkZCrOMbuPzYdWD8/P03l1T4QZO6jzapD.Eha', '45ijg40r9gk4', 'img1', 0, 0, 0, '2023-07-18 07:58:43', '2023-07-18 07:58:43', 0, 0, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
