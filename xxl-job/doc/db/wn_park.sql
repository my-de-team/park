/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.3.5
 Source Server Type    : MySQL
 Source Server Version : 80026 (8.0.26)
 Source Host           : 192.168.3.5:3306
 Source Schema         : wn_park

 Target Server Type    : MySQL
 Target Server Version : 80026 (8.0.26)
 File Encoding         : 65001

 Date: 18/07/2023 20:07:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tp_appeal_list
-- ----------------------------
DROP TABLE IF EXISTS `tp_appeal_list`;
CREATE TABLE `tp_appeal_list`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '申诉表ID',
  `order_id` bigint NOT NULL COMMENT '订单表ID',
  `order_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单编码',
  `appeal_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '申诉内容',
  `appeal_time` datetime NULL DEFAULT NULL COMMENT '申诉时间',
  `appeal_process_state` int NOT NULL DEFAULT 0 COMMENT '处理情况，0-待处理，1-已退款，2-不通过',
  `modification_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '修改金额',
  `refund_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '退款金额',
  `process_time` datetime NULL DEFAULT NULL COMMENT '处理时间',
  `process_user_id` bigint NULL DEFAULT NULL COMMENT '处理人',
  `leave_words` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '留言',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_deleted` int NOT NULL DEFAULT 0 COMMENT '逻辑删除，0-未删除（默认），1-已删除',
  `version` int NOT NULL DEFAULT 0 COMMENT '乐观锁，版本号（默认0）',
  `field1` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段一',
  `field2` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段二',
  `field3` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段三',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `order_id`(`order_id` ASC) USING BTREE,
  INDEX `order_number`(`order_number` ASC) USING BTREE,
  INDEX `appeal_process_state`(`appeal_process_state` ASC) USING BTREE,
  INDEX `process_user`(`process_user_id` ASC) USING BTREE,
  INDEX `create_time`(`create_time` ASC) USING BTREE,
  INDEX `update_time`(`update_time` ASC) USING BTREE,
  INDEX `is_deleted`(`is_deleted` ASC) USING BTREE,
  INDEX `version`(`version` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1679784962879959043 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '售后申诉表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_appeal_list
-- ----------------------------
INSERT INTO `tp_appeal_list` VALUES (1679784856130727938, 1, '111', '储扒皮太黑了', '2023-07-14 01:22:01', 1, 20.00, 30.00, '2023-07-14 01:22:01', NULL, '给我尽快处理', '2023-07-14 17:28:13', '2023-07-17 10:15:34', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_appeal_list` VALUES (1679784934572601346, 2, '111', '储扒皮太黑了', '2023-07-14 01:22:01', 2, 20.00, 30.00, '2023-07-14 01:22:01', NULL, '给我尽快处理', '2023-07-14 17:28:31', '2023-07-15 16:12:25', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_appeal_list` VALUES (1679784962879959042, 3, '111', '储扒皮黑的一匹', '2023-07-14 01:22:01', 1, 20.00, 30.00, '2023-07-14 01:22:01', NULL, '给我尽快处理', '2023-07-14 17:28:38', '2023-07-15 16:12:27', 0, 0, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tp_attendance
-- ----------------------------
DROP TABLE IF EXISTS `tp_attendance`;
CREATE TABLE `tp_attendance`  (
  `id` bigint NOT NULL COMMENT '主键id',
  `inspector_id` bigint NULL DEFAULT NULL COMMENT '巡检员id',
  `clock_time` datetime NULL DEFAULT NULL COMMENT '打卡时间',
  `clock_date` date NULL DEFAULT NULL COMMENT '打卡日期',
  `clock_type` tinyint(1) NULL DEFAULT NULL COMMENT '打卡类型  0:上班 1：下班',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `filed1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段1',
  `filed2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段2',
  `filed3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段3',
  `version` int NOT NULL DEFAULT 1 COMMENT '乐观锁版本号',
  `is_deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_inspector_id`(`inspector_id` ASC) USING BTREE,
  INDEX `idx_clock_time`(`clock_time` ASC) USING BTREE,
  INDEX `idx_clock_date`(`clock_date` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '考勤表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_attendance
-- ----------------------------
INSERT INTO `tp_attendance` VALUES (1680869924168966145, 234, '2023-07-20 09:00:00', '2023-07-19', 0, '2023-07-17 09:19:54', '2023-07-17 09:19:54', NULL, NULL, NULL, 1, 0);
INSERT INTO `tp_attendance` VALUES (1681186610378080257, 234, '2023-07-20 09:00:00', '2023-07-19', 0, '2023-07-18 06:18:18', '2023-07-18 06:18:18', NULL, NULL, NULL, 1, 0);

-- ----------------------------
-- Table structure for tp_attendance_count
-- ----------------------------
DROP TABLE IF EXISTS `tp_attendance_count`;
CREATE TABLE `tp_attendance_count`  (
  `id` bigint NOT NULL COMMENT '营收统计主键id',
  `count_time` datetime NULL DEFAULT NULL COMMENT '统计日期',
  `count_month` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '统计月份',
  `inspector_id` bigint NULL DEFAULT NULL COMMENT '巡检员id',
  `inspector_name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '巡检员姓名',
  `schedule_days` int NULL DEFAULT NULL COMMENT '排班天数',
  `normal_days` int NULL DEFAULT NULL COMMENT '正常天数',
  `abnormal_days` int NULL DEFAULT NULL COMMENT '异常天数',
  `create_time` datetime NULL DEFAULT NULL COMMENT '生成该条数据的时间戳',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = 'ade-考勤统计表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_attendance_count
-- ----------------------------
INSERT INTO `tp_attendance_count` VALUES (1, '2004-12-04 23:55:27', 'tq5yu', 215, '譚俊宇', 323, 233, 540, '2011-04-05 21:31:44');
INSERT INTO `tp_attendance_count` VALUES (2, '2002-04-21 19:27:22', 'gUxKz', 290, '陈睿', 456, 757, 472, '2020-12-07 18:06:58');
INSERT INTO `tp_attendance_count` VALUES (3, '2010-01-29 10:46:40', '1glx4', 492, '尹晓明', 9, 183, 875, '2016-08-09 19:51:13');
INSERT INTO `tp_attendance_count` VALUES (4, '2001-04-04 10:25:21', 'jdFo3', 995, '樊志遠', 669, 606, 986, '2006-06-29 15:36:41');
INSERT INTO `tp_attendance_count` VALUES (5, '2012-04-19 01:12:52', 'K1yfu', 730, '叶睿', 677, 929, 73, '2002-01-26 14:47:42');
INSERT INTO `tp_attendance_count` VALUES (6, '2009-06-14 06:14:08', 'P4tZi', 945, '胡梓軒', 39, 987, 628, '2023-05-10 08:34:38');
INSERT INTO `tp_attendance_count` VALUES (7, '2003-10-17 03:36:28', 'wIfuN', 303, '陶秀文', 685, 559, 702, '2013-05-13 08:02:53');
INSERT INTO `tp_attendance_count` VALUES (8, '2004-05-14 03:30:42', 'mA6Xy', 677, '任岚', 173, 712, 520, '2001-08-19 03:39:39');
INSERT INTO `tp_attendance_count` VALUES (9, '2017-03-03 22:23:05', '2Ogml', 60, '石嘉伦', 147, 104, 814, '2022-04-15 01:19:41');
INSERT INTO `tp_attendance_count` VALUES (10, '2001-02-06 01:23:21', 'GKUL8', 431, '山本海斗', 960, 869, 635, '2018-07-11 06:10:04');
INSERT INTO `tp_attendance_count` VALUES (11, '2002-03-16 21:31:48', 'IngTY', 750, '前田舞', 713, 97, 114, '2017-08-19 07:26:12');
INSERT INTO `tp_attendance_count` VALUES (12, '2002-08-06 10:59:26', 'z68ai', 450, '周云熙', 501, 290, 644, '2023-01-05 11:13:35');
INSERT INTO `tp_attendance_count` VALUES (13, '2000-09-20 20:45:08', 'xb39o', 732, '西村蒼士', 426, 559, 849, '2009-01-01 08:36:25');
INSERT INTO `tp_attendance_count` VALUES (14, '2022-08-03 20:45:20', 'pbOF1', 307, '程宇宁', 670, 835, 754, '2002-03-05 21:12:20');
INSERT INTO `tp_attendance_count` VALUES (15, '2003-06-21 20:26:35', 'Afq2m', 629, '蔡詩涵', 439, 650, 899, '2002-06-04 17:21:12');
INSERT INTO `tp_attendance_count` VALUES (16, '2006-08-29 08:19:40', 'iGZ84', 131, '上野優奈', 890, 755, 410, '2023-04-04 04:49:37');
INSERT INTO `tp_attendance_count` VALUES (17, '2004-09-07 13:47:20', '4bB72', 490, '野村樹', 101, 67, 304, '2007-05-10 21:42:37');
INSERT INTO `tp_attendance_count` VALUES (18, '2006-08-01 00:40:42', 'ZEPbY', 182, '阿部凛', 935, 662, 94, '2000-08-13 17:21:47');
INSERT INTO `tp_attendance_count` VALUES (19, '2012-03-14 13:22:01', 'BeCmp', 567, '余云熙', 167, 853, 564, '2005-03-02 02:48:39');
INSERT INTO `tp_attendance_count` VALUES (20, '2004-04-30 22:52:48', 'eysje', 271, '鈴木陽菜', 897, 875, 255, '2005-01-22 13:19:57');

-- ----------------------------
-- Table structure for tp_car
-- ----------------------------
DROP TABLE IF EXISTS `tp_car`;
CREATE TABLE `tp_car`  (
  `id` bigint NOT NULL COMMENT '主键id',
  `number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '车牌号',
  `type` int NULL DEFAULT NULL COMMENT '车牌类型',
  `user_id` bigint NULL DEFAULT NULL COMMENT '用户id',
  `bind_time` datetime NULL DEFAULT NULL COMMENT '绑定时间',
  `state` int NULL DEFAULT NULL COMMENT '绑定状态 0：绑定 1：未绑定',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `version` int NULL DEFAULT 0 COMMENT '乐观锁 默认：0',
  `is_deleted` int NULL DEFAULT 0 COMMENT '是否删除 0:未删除 1:已删除',
  `field1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段1',
  `field2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段2',
  `field3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段3',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户绑定车辆表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_car
-- ----------------------------

-- ----------------------------
-- Table structure for tp_charging_rule
-- ----------------------------
DROP TABLE IF EXISTS `tp_charging_rule`;
CREATE TABLE `tp_charging_rule`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '收费规则表ID',
  `road_segment_id` bigint NULL DEFAULT NULL COMMENT '泊车路段ID',
  `is_weekday` tinyint(1) NOT NULL COMMENT '是否工作日：1-是，0-否',
  `is_busy_time` tinyint(1) NOT NULL COMMENT '是否繁忙时间段：1-是，0-否',
  `start_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '起始时间',
  `end_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '结束时间',
  `rate_per_hour` decimal(10, 2) NOT NULL COMMENT '每小时收费率（收费标准）',
  `min_duration` int NOT NULL COMMENT '最低计费时长（分钟）',
  `max_duration` int NOT NULL COMMENT '最高计费时长（分钟）',
  `free_duration` int NOT NULL COMMENT '免费时长（分钟）',
  `state` int NULL DEFAULT 1 COMMENT '计费是否包含免费时长状态：1-是，2-否',
  `ceiling_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '封顶金额',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `field1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段1',
  `field2` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段2',
  `field3` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段3',
  `version` int NOT NULL DEFAULT 0 COMMENT '乐观锁，默认1',
  `is_deleted` int NOT NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `road_segment_id`(`road_segment_id` ASC) USING BTREE,
  CONSTRAINT `tp_charging_rule_ibfk_1` FOREIGN KEY (`road_segment_id`) REFERENCES `tp_road_segment` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1681207747076411395 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '收费规则表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_charging_rule
-- ----------------------------
INSERT INTO `tp_charging_rule` VALUES (1, 1, 1, 1, '2023-07-17 16:38:45', '2023-07-17 19:38:50', 0.26, 30, 30, 30, 1, 50.00, '2023-07-11 16:42:36', '2023-07-14 16:42:45', NULL, NULL, NULL, 0, 0);
INSERT INTO `tp_charging_rule` VALUES (2, 2, 0, 0, '2023-07-18 03:09:40', '2023-07-18 03:09:40', 0.00, 30, 120, 30, 1, 50.00, '2023-07-18 03:09:40', '2023-07-18 03:09:40', '', '', '', 0, 0);
INSERT INTO `tp_charging_rule` VALUES (3, 6, 1, 1, '2023-07-18 03:09:58', '2023-07-18 03:09:58', 0.90, 36, 88, 39, 0, 166.00, '2023-07-18 03:09:58', '2023-07-18 03:19:12', '', '', '', 0, 1);
INSERT INTO `tp_charging_rule` VALUES (4, 3, 0, 0, '2023-07-18 07:28:42', '2023-07-18 07:28:42', 0.00, 30, 120, 30, 1, 50.00, '2023-07-18 07:28:42', '2023-07-18 07:28:42', '', '', '', 0, 0);
INSERT INTO `tp_charging_rule` VALUES (1681206014833061890, 1, 1, 1, '2023-07-18 15:35:23', '2023-07-18 15:35:23', 0.99, 36, 88, 31, 0, 898.00, '2023-07-18 07:35:24', '2023-07-18 07:35:24', NULL, NULL, NULL, 0, 0);
INSERT INTO `tp_charging_rule` VALUES (1681207747076411394, 1, 1, 1, '2023-07-18 15:42:16', '2023-07-18 15:42:16', 0.18, 36, 8899, 31, 0, 898686.00, '2023-07-18 07:42:17', '2023-07-18 07:42:17', NULL, NULL, NULL, 0, 0);

-- ----------------------------
-- Table structure for tp_feedback
-- ----------------------------
DROP TABLE IF EXISTS `tp_feedback`;
CREATE TABLE `tp_feedback`  (
  `id` bigint NOT NULL COMMENT '主键id',
  `feedback_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '反馈单号',
  `feedback_time` datetime NULL DEFAULT NULL COMMENT '反馈时间',
  `user_id` bigint NULL DEFAULT NULL COMMENT '用户id',
  `processing_result` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '处理结果',
  `feedback_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '反馈内容',
  `img_count` int NULL DEFAULT NULL COMMENT '图片数量',
  `processing_time` datetime NULL DEFAULT NULL COMMENT '处理时间',
  `state` tinyint(1) NULL DEFAULT NULL COMMENT '0：待处理， 1：已处理',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `filed1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段1',
  `filed2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段2',
  `filed3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段3',
  `version` int NOT NULL DEFAULT 1 COMMENT '乐观锁版本号',
  `is_deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户反馈表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tp_feedback
-- ----------------------------
INSERT INTO `tp_feedback` VALUES (1680897338836176898, '', '2023-07-14 11:33:21', 0, '', '', 0, NULL, 0, '2023-07-17 11:08:50', '2023-07-17 11:27:06', NULL, NULL, NULL, 1, 1);

-- ----------------------------
-- Table structure for tp_inspector_fault
-- ----------------------------
DROP TABLE IF EXISTS `tp_inspector_fault`;
CREATE TABLE `tp_inspector_fault`  (
  `id` bigint NOT NULL COMMENT '主键id',
  `feedback_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '反馈单号',
  `feedback_time` datetime NULL DEFAULT NULL COMMENT '反馈时间',
  `inspector_id` bigint NULL DEFAULT NULL COMMENT '巡检员id',
  `magnetic_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '地磁编号',
  `road_berth` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所属泊位',
  `feedback_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '反馈内容',
  `processing_result` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '处理结果',
  `processing_time` datetime NULL DEFAULT NULL COMMENT '处理时间',
  `img_count` int NULL DEFAULT NULL COMMENT '图片数量',
  `fault_type` tinyint NULL DEFAULT NULL COMMENT '0：故障，\r\n1：其他',
  `state` tinyint(1) NULL DEFAULT NULL COMMENT '0：未处理， 1：已处理',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `filed1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段1',
  `filed2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段2',
  `filed3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段3',
  `version` int NOT NULL DEFAULT 1 COMMENT '乐观锁版本号',
  `is_deleted` tinyint(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '巡检员反馈故障表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_inspector_fault
-- ----------------------------
INSERT INTO `tp_inspector_fault` VALUES (1, '234', '2023-07-14 11:33:21', 234, '234', '234', '243', '234', '2023-07-11 11:33:38', 2, 1, 0, '2023-07-14 11:33:50', '2023-07-12 11:33:54', NULL, NULL, NULL, 1, 0);
INSERT INTO `tp_inspector_fault` VALUES (1680878856685883393, 'Fault7079742450985734144', '2023-07-17 09:55:23', 1, NULL, '田安路', '地磁坏了', NULL, NULL, 2, 0, 0, '2023-07-17 09:55:23', '2023-07-17 09:55:23', NULL, NULL, NULL, 1, 0);

-- ----------------------------
-- Table structure for tp_inspector_fault_img
-- ----------------------------
DROP TABLE IF EXISTS `tp_inspector_fault_img`;
CREATE TABLE `tp_inspector_fault_img`  (
  `id` bigint NOT NULL COMMENT '主键id\r\n',
  `img_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图片路径',
  `inspector_fault_id` bigint NULL DEFAULT NULL COMMENT '故障反馈表id',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `filed1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段1',
  `filed2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段2',
  `filed3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段3',
  `version` int NOT NULL DEFAULT 1 COMMENT '乐观锁版本号',
  `is_deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '巡检员故障反馈表图片' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_inspector_fault_img
-- ----------------------------
INSERT INTO `tp_inspector_fault_img` VALUES (1, '234234', 1, '2023-07-19 11:34:08', '2023-07-12 11:34:11', NULL, NULL, NULL, 1, 0);
INSERT INTO `tp_inspector_fault_img` VALUES (1680878858355216386, '334', 1680878856685883393, '2023-07-17 09:55:24', '2023-07-17 09:55:24', NULL, NULL, NULL, 1, 0);
INSERT INTO `tp_inspector_fault_img` VALUES (1680878858413936641, '15523', 1680878856685883393, '2023-07-17 09:55:24', '2023-07-17 09:55:24', NULL, NULL, NULL, 1, 0);
INSERT INTO `tp_inspector_fault_img` VALUES (1680897340455178242, '4213421', 1680897338836176898, '2023-07-17 11:08:50', '2023-07-17 11:08:50', NULL, NULL, NULL, 1, 1);
INSERT INTO `tp_inspector_fault_img` VALUES (1680897340455178243, '3412', 1680897338836176898, '2023-07-17 11:08:50', '2023-07-17 11:08:50', NULL, NULL, NULL, 1, 1);
INSERT INTO `tp_inspector_fault_img` VALUES (1680897340455178244, '213412', 1680897338836176898, '2023-07-17 11:08:50', '2023-07-17 11:08:50', NULL, NULL, NULL, 1, 1);

-- ----------------------------
-- Table structure for tp_login_log
-- ----------------------------
DROP TABLE IF EXISTS `tp_login_log`;
CREATE TABLE `tp_login_log`  (
  `id` bigint NOT NULL COMMENT '主键',
  `username` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `ip_address` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户ip',
  `ip_source` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户地址',
  `brower` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '浏览器',
  `os` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作系统',
  `state` int NULL DEFAULT 1 COMMENT '登陆状态, 0登录成功,1登录失败',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `wechat` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '微信小程序端用户标识',
  `mobile` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'ade-登录日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_login_log
-- ----------------------------
INSERT INTO `tp_login_log` VALUES (1681263859221925890, '王五2', '0:0:0:0:0:0:0:1', '', 'Chrome 114.0.0.0', 'Windows 10 or Windows Server 2016', 0, '2023-07-18 10:55:14', '45ijg40r9gk4', '12345678999');
INSERT INTO `tp_login_log` VALUES (1681263859427446786, '王五2', '0:0:0:0:0:0:0:1', '', 'Chrome 114.0.0.0', 'Windows 10 or Windows Server 2016', 0, '2023-07-18 10:59:34', '45ijg40r9gk4', '12345678999');
INSERT INTO `tp_login_log` VALUES (1681267085035966465, '王五2', '0:0:0:0:0:0:0:1', '', 'Chrome 114.0.0.0', 'Windows 10 or Windows Server 2016', 0, '2023-07-18 11:38:03', '45ijg40r9gk4', '12345678999');
INSERT INTO `tp_login_log` VALUES (1681267471910178817, '王五2', '0:0:0:0:0:0:0:1', '', 'Chrome 114.0.0.0', 'Windows 10 or Windows Server 2016', 0, '2023-07-18 11:39:36', '45ijg40r9gk4', '12345678999');
INSERT INTO `tp_login_log` VALUES (1681267866942312450, '王五2', '0:0:0:0:0:0:0:1', '', 'Chrome 114.0.0.0', 'Windows 10 or Windows Server 2016', 0, '2023-07-18 11:41:10', '45ijg40r9gk4', '12345678999');
INSERT INTO `tp_login_log` VALUES (1681269129041940482, '王五2', '192.168.3.4', '', 'Chrome 114.0.0.0', 'Windows 10 or Windows Server 2016', 0, '2023-07-18 11:46:11', '45ijg40r9gk4', '12345678999');

-- ----------------------------
-- Table structure for tp_magnetic
-- ----------------------------
DROP TABLE IF EXISTS `tp_magnetic`;
CREATE TABLE `tp_magnetic`  (
  `id` bigint NOT NULL DEFAULT 0 COMMENT '主键',
  `magnetic_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '地磁编号',
  `magnetic_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '地磁名称',
  `road` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所属路段',
  `berthage_bind` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '绑定泊位',
  `berthage_condition` int NULL DEFAULT NULL COMMENT '泊位情况,0:有车1:无车',
  `state` int NULL DEFAULT NULL COMMENT '地磁状态,0:未激活1:在线2:离线',
  `update_time` datetime NULL DEFAULT NULL COMMENT '状态更新时间',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `is_deleted` int NULL DEFAULT 0 COMMENT '1表示删除，0表示没有删除,逻辑删除',
  `version` int(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '乐观锁',
  `road_id` bigint NULL DEFAULT NULL COMMENT '路段id',
  `filed2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段2',
  `filed3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段3',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '地磁管理表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_magnetic
-- ----------------------------
INSERT INTO `tp_magnetic` VALUES (1, '4123213', 'A设备', '天安1街', 'A-112', 0, 1, '2023-07-12 10:10:16', '2023-07-12 10:10:20', 0, 1, 1, NULL, NULL);
INSERT INTO `tp_magnetic` VALUES (2, '4123213', 'A设备', '天安1街', 'A-112', 0, 1, '2023-07-12 10:10:54', '2023-07-12 10:10:58', 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_magnetic` VALUES (3, '4123213', 'A设备', '天安1街', 'A-112', 1, 2, '2023-07-12 10:11:51', '2023-07-12 10:11:54', 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_magnetic` VALUES (4, '4123213', 'A设备', '天安1街', 'A-112', 1, 0, '2023-07-12 10:12:51', '2023-07-12 10:12:54', 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_magnetic` VALUES (5, '4123214', 'A设备', '天安1街', 'A-113', 1, 1, '2023-07-12 10:13:41', '2023-07-12 10:13:43', 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_magnetic` VALUES (6, '4123214', 'A设备', '天安1街', 'A-113', 0, 1, '2023-07-12 10:14:28', '2023-07-12 10:14:31', 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_magnetic` VALUES (7, '4123214', 'A设备', '天安1街', 'A-113', 0, 2, '2023-07-12 10:15:15', '2023-07-12 10:15:18', 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_magnetic` VALUES (8, '4123214', 'A设备', '天安1街', 'A-113', 1, 2, '2023-07-12 10:15:51', '2023-07-12 10:15:53', 0, 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tp_magnetic_log
-- ----------------------------
DROP TABLE IF EXISTS `tp_magnetic_log`;
CREATE TABLE `tp_magnetic_log`  (
  `id` bigint NOT NULL COMMENT '主键',
  `magnetic_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '地磁编号',
  `create_time` datetime NULL DEFAULT NULL COMMENT '时间',
  `park_state` int NULL DEFAULT NULL COMMENT '泊位状态,0:驶入1:驶出2:故障',
  `magnetic_status` int NULL DEFAULT NULL COMMENT '地磁状态,0:在线1：离线',
  `is_deleted` int NULL DEFAULT 0 COMMENT '1表示删除，0表示没有删除,逻辑删除',
  `version` int(1) UNSIGNED ZEROFILL NULL DEFAULT 1 COMMENT '乐观锁',
  `filed1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段1',
  `filed2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段2',
  `filed3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段3',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '地磁日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_magnetic_log
-- ----------------------------
INSERT INTO `tp_magnetic_log` VALUES (1, '4123213', '2023-07-12 10:16:19', 0, 0, 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_magnetic_log` VALUES (2, '4123213', '2023-07-12 10:17:06', 1, 1, 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_magnetic_log` VALUES (3, '4123213', '2023-07-12 10:18:01', 2, 1, 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_magnetic_log` VALUES (4, '4123213', '2023-07-12 10:18:15', 0, 1, 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_magnetic_log` VALUES (5, '4123214', '2023-07-12 10:18:29', 1, 0, 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_magnetic_log` VALUES (6, '4123214', '2023-07-12 10:19:33', 2, 1, 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_magnetic_log` VALUES (7, '4123214', '2023-07-12 10:19:44', 0, 0, 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_magnetic_log` VALUES (8, '4123214', '2023-07-12 10:19:55', 1, 1, 0, 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tp_opt_log
-- ----------------------------
DROP TABLE IF EXISTS `tp_opt_log`;
CREATE TABLE `tp_opt_log`  (
  `id` bigint NOT NULL COMMENT '日志id',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模块标题',
  `business_type` int NULL DEFAULT NULL COMMENT '业务类型（1 新增 2 修改 3 删除 ）',
  `method_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '方法名称',
  `method_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '方法路径',
  `req_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求方式',
  `opt_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作者',
  `opt_url` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求路径',
  `opt_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作者ip',
  `opt_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作者地址',
  `req_param` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '请求参数',
  `resp_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '返回信息',
  `opt_time` datetime NULL DEFAULT NULL COMMENT '操作日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'ade-操作日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_opt_log
-- ----------------------------
INSERT INTO `tp_opt_log` VALUES (1679788350122242049, '系统微服务: 营收统计', 1, '分页条件查询', 'com.park.systemserve.controller.RevenueCountController.queryPageRevenueCount', 'POST', NULL, '/system/revenue/page/1/5', '0:0:0:0:0:0:0:1', '', '[1,5,{\"startTime\":null,\"endTime\":null,\"roadId\":null,\"inspectorName\":\"\"}]', '{\"code\":\"200\",\"message\":\"请求成功\",\"data\":{\"pageNum\":null,\"pageSize\":null,\"total\":20,\"pageData\":[{\"id\":\"2\",\"countTime\":1687501577000,\"orderCount\":764,\"revenue\":575.72,\"refundOrder\":279,\"refundAmount\":747.01,\"newUser\":616,\"createTime\":1451166333000},{\"id\":\"19\",\"countTime\":1686653816000,\"orderCount\":44,\"revenue\":970.21,\"refundOrder\":51,\"refundAmount\":27.73,\"newUser\":28,\"createTime\":1334867965000},{\"id\":\"9\",\"countTime\":1585404486000,\"orderCount\":24,\"revenue\":679.75,\"refundOrder\":916,\"refundAmount\":183.03,\"newUser\":142,\"createTime\":1335276831000},{\"id\":\"10\",\"countTime\":1574730532000,\"orderCount\":456,\"revenue\":988.84,\"refundOrder\":600,\"refundAmount\":414.76,\"newUser\":381,\"createTime\":1328030954000},{\"id\":\"4\",\"countTime\":1545864704000,\"orderCount\":770,\"revenue\":4.43,\"refundOrder\":379,\"refundAmount\":576.06,\"newUser\":1,\"createTime\":1582136419000}]}}', '2023-07-14 09:42:06');
INSERT INTO `tp_opt_log` VALUES (1680154728322904066, '系统微服务: 操作日志管理', 1, '分页条件查询', 'com.park.systemserve.controller.OptLogController.queryPageOptLog', 'POST', 'system', '/system/opt/page/1/5', '0:0:0:0:0:0:0:1', '', '[1,5,{\"startTime\":null,\"endTime\":null,\"roadId\":\"0\",\"inspectorName\":\"\"}]', '{\"code\":\"200\",\"message\":\"请求成功\",\"data\":{\"pageNum\":null,\"pageSize\":null,\"total\":1,\"pageData\":[{\"id\":\"1679788350122242049\",\"title\":\"系统微服务: 营收统计\",\"businessType\":1,\"methodName\":\"分页条件查询\",\"methodUrl\":\"com.park.systemserve.controller.RevenueCountController.queryPageRevenueCount\",\"reqType\":\"POST\",\"optName\":null,\"optUrl\":\"/system/revenue/page/1/5\",\"optIp\":\"0:0:0:0:0:0:0:1\",\"optAddress\":\"\",\"reqParam\":\"[1,5,{\\\"startTime\\\":null,\\\"endTime\\\":null,\\\"roadId\\\":null,\\\"inspectorName\\\":\\\"\\\"}]\",\"respData\":\"{\\\"code\\\":\\\"200\\\",\\\"message\\\":\\\"请求成功\\\",\\\"data\\\":{\\\"pageNum\\\":null,\\\"pageSize\\\":null,\\\"total\\\":20,\\\"pageData\\\":[{\\\"id\\\":\\\"2\\\",\\\"countTime\\\":1687501577000,\\\"orderCount\\\":764,\\\"revenue\\\":575.72,\\\"refundOrder\\\":279,\\\"refundAmount\\\":747.01,\\\"newUser\\\":616,\\\"createTime\\\":1451166333000},{\\\"id\\\":\\\"19\\\",\\\"countTime\\\":1686653816000,\\\"orderCount\\\":44,\\\"revenue\\\":970.21,\\\"refundOrder\\\":51,\\\"refundAmount\\\":27.73,\\\"newUser\\\":28,\\\"createTime\\\":1334867965000},{\\\"id\\\":\\\"9\\\",\\\"countTime\\\":1585404486000,\\\"orderCount\\\":24,\\\"revenue\\\":679.75,\\\"refundOrder\\\":916,\\\"refundAmount\\\":183.03,\\\"newUser\\\":142,\\\"createTime\\\":1335276831000},{\\\"id\\\":\\\"10\\\",\\\"countTime\\\":1574730532000,\\\"orderCount\\\":456,\\\"revenue\\\":988.84,\\\"refundOrder\\\":600,\\\"refundAmount\\\":414.76,\\\"newUser\\\":381,\\\"createTime\\\":1328030954000},{\\\"id\\\":\\\"4\\\",\\\"countTime\\\":1545864704000,\\\"orderCount\\\":770,\\\"revenue\\\":4.43,\\\"refundOrder\\\":379,\\\"refundAmount\\\":576.06,\\\"newUser\\\":1,\\\"createTime\\\":1582136419000}]}}\",\"optTime\":1689327726000}]}}', '2023-07-15 09:57:58');
INSERT INTO `tp_opt_log` VALUES (1680154894870327297, '系统微服务: 登录日志管理', 1, '分页条件查询', 'com.park.systemserve.controller.LoginLogController.queryPageLoginLog', 'POST', 'system', '/system/login/Log/page/1/5', '0:0:0:0:0:0:0:1', '', '[1,5,{\"startTime\":null,\"endTime\":null,\"roadId\":\"0\",\"inspectorName\":\"\"}]', '{\"code\":\"200\",\"message\":\"请求成功\",\"data\":{\"pageNum\":null,\"pageSize\":null,\"total\":4,\"pageData\":[{\"id\":\"4\",\"username\":\"Nanjustar\",\"ipAddress\":\"127.0.0.1\",\"ipSource\":\"\",\"brower\":\"Firefox 11\",\"os\":\"Windows 10\",\"state\":1,\"createTime\":1685728915000,\"wechat\":\"登陆成功！\",\"mobile\":null},{\"id\":\"3\",\"username\":\"Nanjustar\",\"ipAddress\":\"127.0.0.1\",\"ipSource\":\"\",\"brower\":\"Chrome 11\",\"os\":\"Windows 10\",\"state\":1,\"createTime\":1684771742000,\"wechat\":\"登陆成功！\",\"mobile\":null},{\"id\":\"2\",\"username\":\"admin\",\"ipAddress\":\"127.0.0.1\",\"ipSource\":\"\",\"brower\":\"Firefox 11\",\"os\":\"Windows 10\",\"state\":1,\"createTime\":1684755937000,\"wechat\":\"登陆成功！\",\"mobile\":null},{\"id\":\"1\",\"username\":\"Nanjustar\",\"ipAddress\":\"127.0.0.1\",\"ipSource\":\"\",\"brower\":\"Firefox 11\",\"os\":\"Windows 10\",\"state\":1,\"createTime\":1684755835000,\"wechat\":\"登陆成功！\",\"mobile\":null}]}}', '2023-07-15 09:58:37');
INSERT INTO `tp_opt_log` VALUES (1680155173313392641, '系统微服务: 操作日志管理', 1, '分页条件查询', 'com.park.systemserve.controller.OptLogController.queryPageOptLog', 'POST', 'system', '/system/opt/page/1/5', '0:0:0:0:0:0:0:1', '', '[1,5,{\"startTime\":null,\"endTime\":null,\"roadId\":\"0\",\"inspectorName\":\"\"}]', '{\"code\":\"200\",\"message\":\"请求成功\",\"data\":{\"pageNum\":null,\"pageSize\":null,\"total\":3,\"pageData\":[{\"id\":\"1680154894870327297\",\"title\":\"系统微服务: 登录日志管理\",\"businessType\":1,\"methodName\":\"分页条件查询\",\"methodUrl\":\"com.park.systemserve.controller.LoginLogController.queryPageLoginLog\",\"reqType\":\"POST\",\"optName\":\"system\",\"optUrl\":\"/system/login/Log/page/1/5\",\"optIp\":\"0:0:0:0:0:0:0:1\",\"optAddress\":\"\",\"reqParam\":\"[1,5,{\\\"startTime\\\":null,\\\"endTime\\\":null,\\\"roadId\\\":\\\"0\\\",\\\"inspectorName\\\":\\\"\\\"}]\",\"respData\":\"{\\\"code\\\":\\\"200\\\",\\\"message\\\":\\\"请求成功\\\",\\\"data\\\":{\\\"pageNum\\\":null,\\\"pageSize\\\":null,\\\"total\\\":4,\\\"pageData\\\":[{\\\"id\\\":\\\"4\\\",\\\"username\\\":\\\"Nanjustar\\\",\\\"ipAddress\\\":\\\"127.0.0.1\\\",\\\"ipSource\\\":\\\"\\\",\\\"brower\\\":\\\"Firefox 11\\\",\\\"os\\\":\\\"Windows 10\\\",\\\"state\\\":1,\\\"createTime\\\":1685728915000,\\\"wechat\\\":\\\"登陆成功！\\\",\\\"mobile\\\":null},{\\\"id\\\":\\\"3\\\",\\\"username\\\":\\\"Nanjustar\\\",\\\"ipAddress\\\":\\\"127.0.0.1\\\",\\\"ipSource\\\":\\\"\\\",\\\"brower\\\":\\\"Chrome 11\\\",\\\"os\\\":\\\"Windows 10\\\",\\\"state\\\":1,\\\"createTime\\\":1684771742000,\\\"wechat\\\":\\\"登陆成功！\\\",\\\"mobile\\\":null},{\\\"id\\\":\\\"2\\\",\\\"username\\\":\\\"admin\\\",\\\"ipAddress\\\":\\\"127.0.0.1\\\",\\\"ipSource\\\":\\\"\\\",\\\"brower\\\":\\\"Firefox 11\\\",\\\"os\\\":\\\"Windows 10\\\",\\\"state\\\":1,\\\"createTime\\\":1684755937000,\\\"wechat\\\":\\\"登陆成功！\\\",\\\"mobile\\\":null},{\\\"id\\\":\\\"1\\\",\\\"username\\\":\\\"Nanjustar\\\",\\\"ipAddress\\\":\\\"127.0.0.1\\\",\\\"ipSource\\\":\\\"\\\",\\\"brower\\\":\\\"Firefox 11\\\",\\\"os\\\":\\\"Windows 10\\\",\\\"state\\\":1,\\\"createTime\\\":1684755835000,\\\"wechat\\\":\\\"登陆成功！\\\",\\\"mobile\\\":null}]}}\",\"optTime\":1689415117000},{\"id\":\"1680154728322904066\",\"title\":\"系统微服务: 操作日志管理\",\"businessType\":1,\"methodName\":\"分页条件查询\",\"methodUrl\":\"com.park.systemserve.controller.OptLogController.queryPageOptLog\",\"reqType\":\"POST\",\"optName\":\"system\",\"optUrl\":\"/system/opt/page/1/5\",\"optIp\":\"0:0:0:0:0:0:0:1\",\"optAddress\":\"\",\"reqParam\":\"[1,5,{\\\"startTime\\\":null,\\\"endTime\\\":null,\\\"roadId\\\":\\\"0\\\",\\\"inspectorName\\\":\\\"\\\"}]\",\"respData\":\"{\\\"code\\\":\\\"200\\\",\\\"message\\\":\\\"请求成功\\\",\\\"data\\\":{\\\"pageNum\\\":null,\\\"pageSize\\\":null,\\\"total\\\":1,\\\"pageData\\\":[{\\\"id\\\":\\\"1679788350122242049\\\",\\\"title\\\":\\\"系统微服务: 营收统计\\\",\\\"businessType\\\":1,\\\"methodName\\\":\\\"分页条件查询\\\",\\\"methodUrl\\\":\\\"com.park.systemserve.controller.RevenueCountController.queryPageRevenueCount\\\",\\\"reqType\\\":\\\"POST\\\",\\\"optName\\\":null,\\\"optUrl\\\":\\\"/system/revenue/page/1/5\\\",\\\"optIp\\\":\\\"0:0:0:0:0:0:0:1\\\",\\\"optAddress\\\":\\\"\\\",\\\"reqParam\\\":\\\"[1,5,{\\\\\\\"startTime\\\\\\\":null,\\\\\\\"endTime\\\\\\\":null,\\\\\\\"roadId\\\\\\\":null,\\\\\\\"inspectorName\\\\\\\":\\\\\\\"\\\\\\\"}]\\\",\\\"respData\\\":\\\"{\\\\\\\"code\\\\\\\":\\\\\\\"200\\\\\\\",\\\\\\\"message\\\\\\\":\\\\\\\"请求成功\\\\\\\",\\\\\\\"data\\\\\\\":{\\\\\\\"pageNum\\\\\\\":null,\\\\\\\"pageSize\\\\\\\":null,\\\\\\\"total\\\\\\\":20,\\\\\\\"pageData\\\\\\\":[{\\\\\\\"id\\\\\\\":\\\\\\\"2\\\\\\\",\\\\\\\"countTime\\\\\\\":1687501577000,\\\\\\\"orderCount\\\\\\\":764,\\\\\\\"revenue\\\\\\\":575.72,\\\\\\\"refundOrder\\\\\\\":279,\\\\\\\"refundAmount\\\\\\\":747.01,\\\\\\\"newUser\\\\\\\":616,\\\\\\\"createTime\\\\\\\":1451166333000},{\\\\\\\"id\\\\\\\":\\\\\\\"19\\\\\\\",\\\\\\\"countTime\\\\\\\":1686653816000,\\\\\\\"orderCount\\\\\\\":44,\\\\\\\"revenue\\\\\\\":970.21,\\\\\\\"refundOrder\\\\\\\":51,\\\\\\\"refundAmount\\\\\\\":27.73,\\\\\\\"newUser\\\\\\\":28,\\\\\\\"createTime\\\\\\\":1334867965000},{\\\\\\\"id\\\\\\\":\\\\\\\"9\\\\\\\",\\\\\\\"countTime\\\\\\\":1585404486000,\\\\\\\"orderCount\\\\\\\":24,\\\\\\\"revenue\\\\\\\":679.75,\\\\\\\"refundOrder\\\\\\\":916,\\\\\\\"refundAmount\\\\\\\":183.03,\\\\\\\"newUser\\\\\\\":142,\\\\\\\"createTime\\\\\\\":1335276831000},{\\\\\\\"id\\\\\\\":\\\\\\\"10\\\\\\\",\\\\\\\"countTime\\\\\\\":1574730532000,\\\\\\\"orderCount\\\\\\\":456,\\\\\\\"revenue\\\\\\\":988.84,\\\\\\\"refundOrder\\\\\\\":600,\\\\\\\"refundAmount\\\\\\\":414.76,\\\\\\\"newUser\\\\\\\":381,\\\\\\\"createTime\\\\\\\":1328030954000},{\\\\\\\"id\\\\\\\":\\\\\\\"4\\\\\\\",\\\\\\\"countTime\\\\\\\":1545864704000,\\\\\\\"orderCount\\\\\\\":770,\\\\\\\"revenue\\\\\\\":4.43,\\\\\\\"refundOrder\\\\\\\":379,\\\\\\\"refundAmount\\\\\\\":576.06,\\\\\\\"newUser\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1582136419000}]}}\\\",\\\"optTime\\\":1689327726000}]}}\",\"optTime\":1689415078000},{\"id\":\"1679788350122242049\",\"title\":\"系统微服务: 营收统计\",\"businessType\":1,\"methodName\":\"分页条件查询\",\"methodUrl\":\"com.park.systemserve.controller.RevenueCountController.queryPageRevenueCount\",\"reqType\":\"POST\",\"optName\":null,\"optUrl\":\"/system/revenue/page/1/5\",\"optIp\":\"0:0:0:0:0:0:0:1\",\"optAddress\":\"\",\"reqParam\":\"[1,5,{\\\"startTime\\\":null,\\\"endTime\\\":null,\\\"roadId\\\":null,\\\"inspectorName\\\":\\\"\\\"}]\",\"respData\":\"{\\\"code\\\":\\\"200\\\",\\\"message\\\":\\\"请求成功\\\",\\\"data\\\":{\\\"pageNum\\\":null,\\\"pageSize\\\":null,\\\"total\\\":20,\\\"pageData\\\":[{\\\"id\\\":\\\"2\\\",\\\"countTime\\\":1687501577000,\\\"orderCount\\\":764,\\\"revenue\\\":575.72,\\\"refundOrder\\\":279,\\\"refundAmount\\\":747.01,\\\"newUser\\\":616,\\\"createTime\\\":1451166333000},{\\\"id\\\":\\\"19\\\",\\\"countTime\\\":1686653816000,\\\"orderCount\\\":44,\\\"revenue\\\":970.21,\\\"refundOrder\\\":51,\\\"refundAmount\\\":27.73,\\\"newUser\\\":28,\\\"createTime\\\":1334867965000},{\\\"id\\\":\\\"9\\\",\\\"countTime\\\":1585404486000,\\\"orderCount\\\":24,\\\"revenue\\\":679.75,\\\"refundOrder\\\":916,\\\"refundAmount\\\":183.03,\\\"newUser\\\":142,\\\"createTime\\\":1335276831000},{\\\"id\\\":\\\"10\\\",\\\"countTime\\\":1574730532000,\\\"orderCount\\\":456,\\\"revenue\\\":988.84,\\\"refundOrder\\\":600,\\\"refundAmount\\\":414.76,\\\"newUser\\\":381,\\\"createTime\\\":1328030954000},{\\\"id\\\":\\\"4\\\",\\\"countTime\\\":1545864704000,\\\"orderCount\\\":770,\\\"revenue\\\":4.43,\\\"refundOrder\\\":379,\\\"refundAmount\\\":576.06,\\\"newUser\\\":1,\\\"createTime\\\":1582136419000}]}}\",\"optTime\":1689327726000}]}}', '2023-07-15 09:59:44');
INSERT INTO `tp_opt_log` VALUES (1681125894395047938, '系统微服务: 支付统计', 1, '分页条件查询', 'com.park.systemserve.controller.PayCountController.queryPagePayCount', 'POST', 'system', '/system/pay/page/1/-1', '192.168.3.22', '', '[1,-1,{\"startTime\":null,\"endTime\":null,\"roadId\":\"0\",\"inspectorName\":\"\"}]', '{\"code\":\"200\",\"message\":\"请求成功\",\"data\":{\"pageNum\":null,\"pageSize\":null,\"total\":0,\"pageData\":[{\"id\":\"14\",\"countTime\":1594685542000,\"activePay\":686,\"cashPay\":507,\"codePay\":767,\"wechatPayAmount\":0.38,\"alipayPayAmount\":186.27,\"cashPayAmount\":561.84,\"unionPayAmount\":104.32,\"createTime\":1661174376000},{\"id\":\"8\",\"countTime\":1564225797000,\"activePay\":472,\"cashPay\":746,\"codePay\":796,\"wechatPayAmount\":195.45,\"alipayPayAmount\":507.62,\"cashPayAmount\":868.35,\"unionPayAmount\":722.12,\"createTime\":1313245319000},{\"id\":\"15\",\"countTime\":1557460914000,\"activePay\":940,\"cashPay\":389,\"codePay\":672,\"wechatPayAmount\":937.34,\"alipayPayAmount\":448.54,\"cashPayAmount\":694.97,\"unionPayAmount\":180.61,\"createTime\":1363446388000},{\"id\":\"3\",\"countTime\":1510865003000,\"activePay\":249,\"cashPay\":247,\"codePay\":742,\"wechatPayAmount\":558.17,\"alipayPayAmount\":788.87,\"cashPayAmount\":784.12,\"unionPayAmount\":143.04,\"createTime\":1441321976000},{\"id\":\"18\",\"countTime\":1395945444000,\"activePay\":604,\"cashPay\":573,\"codePay\":741,\"wechatPayAmount\":463.26,\"alipayPayAmount\":144.26,\"cashPayAmount\":970.50,\"unionPayAmount\":582.33,\"createTime\":1044809216000},{\"id\":\"11\",\"countTime\":1370187195000,\"activePay\":286,\"cashPay\":524,\"codePay\":113,\"wechatPayAmount\":524.84,\"alipayPayAmount\":404.41,\"cashPayAmount\":446.39,\"unionPayAmount\":716.95,\"createTime\":1359875756000},{\"id\":\"5\",\"countTime\":1363857856000,\"activePay\":788,\"cashPay\":601,\"codePay\":51,\"wechatPayAmount\":704.90,\"alipayPayAmount\":305.36,\"cashPayAmount\":85.42,\"unionPayAmount\":595.39,\"createTime\":1626042931000},{\"id\":\"16\",\"countTime\":1272108472000,\"activePay\":873,\"cashPay\":983,\"codePay\":827,\"wechatPayAmount\":446.75,\"alipayPayAmount\":767.99,\"cashPayAmount\":273.55,\"unionPayAmount\":3.28,\"createTime\":1678233266000},{\"id\":\"20\",\"countTime\":1263565824000,\"activePay\":65,\"cashPay\":264,\"codePay\":34,\"wechatPayAmount\":542.39,\"alipayPayAmount\":155.58,\"cashPayAmount\":14.13,\"unionPayAmount\":426.87,\"createTime\":969387238000},{\"id\":\"17\",\"countTime\":1230195510000,\"activePay\":147,\"cashPay\":771,\"codePay\":242,\"wechatPayAmount\":799.52,\"alipayPayAmount\":95.22,\"cashPayAmount\":965.60,\"unionPayAmount\":94.22,\"createTime\":1234449135000},{\"id\":\"9\",\"countTime\":1205744341000,\"activePay\":768,\"cashPay\":644,\"codePay\":82,\"wechatPayAmount\":301.21,\"alipayPayAmount\":181.47,\"cashPayAmount\":150.05,\"unionPayAmount\":821.52,\"createTime\":1644243684000},{\"id\":\"10\",\"countTime\":1159885910000,\"activePay\":290,\"cashPay\":228,\"codePay\":724,\"wechatPayAmount\":226.54,\"alipayPayAmount\":908.78,\"cashPayAmount\":937.47,\"unionPayAmount\":482.54,\"createTime\":1604895828000},{\"id\":\"1\",\"countTime\":1154907944000,\"activePay\":315,\"cashPay\":748,\"codePay\":785,\"wechatPayAmount\":872.73,\"alipayPayAmount\":895.74,\"cashPayAmount\":892.92,\"unionPayAmount\":890.26,\"createTime\":1238838193000},{\"id\":\"19\",\"countTime\":1133141557000,\"activePay\":28,\"cashPay\":808,\"codePay\":806,\"wechatPayAmount\":492.43,\"alipayPayAmount\":858.11,\"cashPayAmount\":647.66,\"unionPayAmount\":753.97,\"createTime\":1554240838000},{\"id\":\"4\",\"countTime\":1092564092000,\"activePay\":672,\"cashPay\":769,\"codePay\":791,\"wechatPayAmount\":668.58,\"alipayPayAmount\":442.09,\"cashPayAmount\":748.81,\"unionPayAmount\":986.86,\"createTime\":1343254721000},{\"id\":\"7\",\"countTime\":1092230652000,\"activePay\":643,\"cashPay\":279,\"codePay\":123,\"wechatPayAmount\":101.06,\"alipayPayAmount\":838.53,\"cashPayAmount\":13.48,\"unionPayAmount\":535.94,\"createTime\":1502764090000},{\"id\":\"13\",\"countTime\":1085801087000,\"activePay\":589,\"cashPay\":249,\"codePay\":786,\"wechatPayAmount\":515.25,\"alipayPayAmount\":139.34,\"cashPayAmount\":507.41,\"unionPayAmount\":194.27,\"createTime\":1196030446000},{\"id\":\"2\",\"countTime\":1071539181000,\"activePay\":176,\"cashPay\":506,\"codePay\":352,\"wechatPayAmount\":207.05,\"alipayPayAmount\":299.24,\"cashPayAmount\":205.12,\"unionPayAmount\":659.65,\"createTime\":1093436502000},{\"id\":\"6\",\"countTime\":1038532226000,\"activePay\":499,\"cashPay\":112,\"codePay\":99,\"wechatPayAmount\":442.61,\"alipayPayAmount\":949.96,\"cashPayAmount\":371.21,\"unionPayAmount\":981.57,\"createTime\":1227355659000},{\"id\":\"12\",\"countTime\":999039450000,\"activePay\":996,\"cashPay\":600,\"codePay\":833,\"wechatPayAmount\":933.18,\"alipayPayAmount\":816.24,\"cashPayAmount\":113.54,\"unionPayAmount\":984.73,\"createTime\":1649700807000}]}}', '2023-07-18 02:17:02');
INSERT INTO `tp_opt_log` VALUES (1681126009008599042, '系统微服务: 登录日志管理', 1, '分页条件查询', 'com.park.systemserve.controller.LoginLogController.queryPageLoginLog', 'POST', 'system', '/system/login/Log/page/1/-1', '192.168.3.22', '', '[1,-1,{\"startTime\":null,\"endTime\":null,\"roadId\":\"0\",\"inspectorName\":\"\"}]', '{\"code\":\"200\",\"message\":\"请求成功\",\"data\":{\"pageNum\":null,\"pageSize\":null,\"total\":0,\"pageData\":[{\"id\":\"4\",\"username\":\"Nanjustar\",\"ipAddress\":\"127.0.0.1\",\"ipSource\":\"\",\"brower\":\"Firefox 11\",\"os\":\"Windows 10\",\"state\":1,\"createTime\":1685728915000,\"wechat\":\"登陆成功！\",\"mobile\":null},{\"id\":\"3\",\"username\":\"Nanjustar\",\"ipAddress\":\"127.0.0.1\",\"ipSource\":\"\",\"brower\":\"Chrome 11\",\"os\":\"Windows 10\",\"state\":1,\"createTime\":1684771742000,\"wechat\":\"登陆成功！\",\"mobile\":null},{\"id\":\"2\",\"username\":\"admin\",\"ipAddress\":\"127.0.0.1\",\"ipSource\":\"\",\"brower\":\"Firefox 11\",\"os\":\"Windows 10\",\"state\":1,\"createTime\":1684755937000,\"wechat\":\"登陆成功！\",\"mobile\":null},{\"id\":\"1\",\"username\":\"Nanjustar\",\"ipAddress\":\"127.0.0.1\",\"ipSource\":\"\",\"brower\":\"Firefox 11\",\"os\":\"Windows 10\",\"state\":1,\"createTime\":1684755835000,\"wechat\":\"登陆成功！\",\"mobile\":null}]}}', '2023-07-18 02:17:29');
INSERT INTO `tp_opt_log` VALUES (1681126085663698946, '系统微服务: 操作日志管理', 1, '分页条件查询', 'com.park.systemserve.controller.OptLogController.queryPageOptLog', 'POST', 'system', '/system/opt/page/1/-1', '192.168.3.22', '', '[1,-1,{\"startTime\":null,\"endTime\":null,\"roadId\":\"0\",\"inspectorName\":\"\"}]', '{\"code\":\"200\",\"message\":\"请求成功\",\"data\":{\"pageNum\":null,\"pageSize\":null,\"total\":0,\"pageData\":[{\"id\":\"1681126009008599042\",\"title\":\"系统微服务: 登录日志管理\",\"businessType\":1,\"methodName\":\"分页条件查询\",\"methodUrl\":\"com.park.systemserve.controller.LoginLogController.queryPageLoginLog\",\"reqType\":\"POST\",\"optName\":\"system\",\"optUrl\":\"/system/login/Log/page/1/-1\",\"optIp\":\"192.168.3.22\",\"optAddress\":\"\",\"reqParam\":\"[1,-1,{\\\"startTime\\\":null,\\\"endTime\\\":null,\\\"roadId\\\":\\\"0\\\",\\\"inspectorName\\\":\\\"\\\"}]\",\"respData\":\"{\\\"code\\\":\\\"200\\\",\\\"message\\\":\\\"请求成功\\\",\\\"data\\\":{\\\"pageNum\\\":null,\\\"pageSize\\\":null,\\\"total\\\":0,\\\"pageData\\\":[{\\\"id\\\":\\\"4\\\",\\\"username\\\":\\\"Nanjustar\\\",\\\"ipAddress\\\":\\\"127.0.0.1\\\",\\\"ipSource\\\":\\\"\\\",\\\"brower\\\":\\\"Firefox 11\\\",\\\"os\\\":\\\"Windows 10\\\",\\\"state\\\":1,\\\"createTime\\\":1685728915000,\\\"wechat\\\":\\\"登陆成功！\\\",\\\"mobile\\\":null},{\\\"id\\\":\\\"3\\\",\\\"username\\\":\\\"Nanjustar\\\",\\\"ipAddress\\\":\\\"127.0.0.1\\\",\\\"ipSource\\\":\\\"\\\",\\\"brower\\\":\\\"Chrome 11\\\",\\\"os\\\":\\\"Windows 10\\\",\\\"state\\\":1,\\\"createTime\\\":1684771742000,\\\"wechat\\\":\\\"登陆成功！\\\",\\\"mobile\\\":null},{\\\"id\\\":\\\"2\\\",\\\"username\\\":\\\"admin\\\",\\\"ipAddress\\\":\\\"127.0.0.1\\\",\\\"ipSource\\\":\\\"\\\",\\\"brower\\\":\\\"Firefox 11\\\",\\\"os\\\":\\\"Windows 10\\\",\\\"state\\\":1,\\\"createTime\\\":1684755937000,\\\"wechat\\\":\\\"登陆成功！\\\",\\\"mobile\\\":null},{\\\"id\\\":\\\"1\\\",\\\"username\\\":\\\"Nanjustar\\\",\\\"ipAddress\\\":\\\"127.0.0.1\\\",\\\"ipSource\\\":\\\"\\\",\\\"brower\\\":\\\"Firefox 11\\\",\\\"os\\\":\\\"Windows 10\\\",\\\"state\\\":1,\\\"createTime\\\":1684755835000,\\\"wechat\\\":\\\"登陆成功！\\\",\\\"mobile\\\":null}]}}\",\"optTime\":1689646649000},{\"id\":\"1681125894395047938\",\"title\":\"系统微服务: 支付统计\",\"businessType\":1,\"methodName\":\"分页条件查询\",\"methodUrl\":\"com.park.systemserve.controller.PayCountController.queryPagePayCount\",\"reqType\":\"POST\",\"optName\":\"system\",\"optUrl\":\"/system/pay/page/1/-1\",\"optIp\":\"192.168.3.22\",\"optAddress\":\"\",\"reqParam\":\"[1,-1,{\\\"startTime\\\":null,\\\"endTime\\\":null,\\\"roadId\\\":\\\"0\\\",\\\"inspectorName\\\":\\\"\\\"}]\",\"respData\":\"{\\\"code\\\":\\\"200\\\",\\\"message\\\":\\\"请求成功\\\",\\\"data\\\":{\\\"pageNum\\\":null,\\\"pageSize\\\":null,\\\"total\\\":0,\\\"pageData\\\":[{\\\"id\\\":\\\"14\\\",\\\"countTime\\\":1594685542000,\\\"activePay\\\":686,\\\"cashPay\\\":507,\\\"codePay\\\":767,\\\"wechatPayAmount\\\":0.38,\\\"alipayPayAmount\\\":186.27,\\\"cashPayAmount\\\":561.84,\\\"unionPayAmount\\\":104.32,\\\"createTime\\\":1661174376000},{\\\"id\\\":\\\"8\\\",\\\"countTime\\\":1564225797000,\\\"activePay\\\":472,\\\"cashPay\\\":746,\\\"codePay\\\":796,\\\"wechatPayAmount\\\":195.45,\\\"alipayPayAmount\\\":507.62,\\\"cashPayAmount\\\":868.35,\\\"unionPayAmount\\\":722.12,\\\"createTime\\\":1313245319000},{\\\"id\\\":\\\"15\\\",\\\"countTime\\\":1557460914000,\\\"activePay\\\":940,\\\"cashPay\\\":389,\\\"codePay\\\":672,\\\"wechatPayAmount\\\":937.34,\\\"alipayPayAmount\\\":448.54,\\\"cashPayAmount\\\":694.97,\\\"unionPayAmount\\\":180.61,\\\"createTime\\\":1363446388000},{\\\"id\\\":\\\"3\\\",\\\"countTime\\\":1510865003000,\\\"activePay\\\":249,\\\"cashPay\\\":247,\\\"codePay\\\":742,\\\"wechatPayAmount\\\":558.17,\\\"alipayPayAmount\\\":788.87,\\\"cashPayAmount\\\":784.12,\\\"unionPayAmount\\\":143.04,\\\"createTime\\\":1441321976000},{\\\"id\\\":\\\"18\\\",\\\"countTime\\\":1395945444000,\\\"activePay\\\":604,\\\"cashPay\\\":573,\\\"codePay\\\":741,\\\"wechatPayAmount\\\":463.26,\\\"alipayPayAmount\\\":144.26,\\\"cashPayAmount\\\":970.50,\\\"unionPayAmount\\\":582.33,\\\"createTime\\\":1044809216000},{\\\"id\\\":\\\"11\\\",\\\"countTime\\\":1370187195000,\\\"activePay\\\":286,\\\"cashPay\\\":524,\\\"codePay\\\":113,\\\"wechatPayAmount\\\":524.84,\\\"alipayPayAmount\\\":404.41,\\\"cashPayAmount\\\":446.39,\\\"unionPayAmount\\\":716.95,\\\"createTime\\\":1359875756000},{\\\"id\\\":\\\"5\\\",\\\"countTime\\\":1363857856000,\\\"activePay\\\":788,\\\"cashPay\\\":601,\\\"codePay\\\":51,\\\"wechatPayAmount\\\":704.90,\\\"alipayPayAmount\\\":305.36,\\\"cashPayAmount\\\":85.42,\\\"unionPayAmount\\\":595.39,\\\"createTime\\\":1626042931000},{\\\"id\\\":\\\"16\\\",\\\"countTime\\\":1272108472000,\\\"activePay\\\":873,\\\"cashPay\\\":983,\\\"codePay\\\":827,\\\"wechatPayAmount\\\":446.75,\\\"alipayPayAmount\\\":767.99,\\\"cashPayAmount\\\":273.55,\\\"unionPayAmount\\\":3.28,\\\"createTime\\\":1678233266000},{\\\"id\\\":\\\"20\\\",\\\"countTime\\\":1263565824000,\\\"activePay\\\":65,\\\"cashPay\\\":264,\\\"codePay\\\":34,\\\"wechatPayAmount\\\":542.39,\\\"alipayPayAmount\\\":155.58,\\\"cashPayAmount\\\":14.13,\\\"unionPayAmount\\\":426.87,\\\"createTime\\\":969387238000},{\\\"id\\\":\\\"17\\\",\\\"countTime\\\":1230195510000,\\\"activePay\\\":147,\\\"cashPay\\\":771,\\\"codePay\\\":242,\\\"wechatPayAmount\\\":799.52,\\\"alipayPayAmount\\\":95.22,\\\"cashPayAmount\\\":965.60,\\\"unionPayAmount\\\":94.22,\\\"createTime\\\":1234449135000},{\\\"id\\\":\\\"9\\\",\\\"countTime\\\":1205744341000,\\\"activePay\\\":768,\\\"cashPay\\\":644,\\\"codePay\\\":82,\\\"wechatPayAmount\\\":301.21,\\\"alipayPayAmount\\\":181.47,\\\"cashPayAmount\\\":150.05,\\\"unionPayAmount\\\":821.52,\\\"createTime\\\":1644243684000},{\\\"id\\\":\\\"10\\\",\\\"countTime\\\":1159885910000,\\\"activePay\\\":290,\\\"cashPay\\\":228,\\\"codePay\\\":724,\\\"wechatPayAmount\\\":226.54,\\\"alipayPayAmount\\\":908.78,\\\"cashPayAmount\\\":937.47,\\\"unionPayAmount\\\":482.54,\\\"createTime\\\":1604895828000},{\\\"id\\\":\\\"1\\\",\\\"countTime\\\":1154907944000,\\\"activePay\\\":315,\\\"cashPay\\\":748,\\\"codePay\\\":785,\\\"wechatPayAmount\\\":872.73,\\\"alipayPayAmount\\\":895.74,\\\"cashPayAmount\\\":892.92,\\\"unionPayAmount\\\":890.26,\\\"createTime\\\":1238838193000},{\\\"id\\\":\\\"19\\\",\\\"countTime\\\":1133141557000,\\\"activePay\\\":28,\\\"cashPay\\\":808,\\\"codePay\\\":806,\\\"wechatPayAmount\\\":492.43,\\\"alipayPayAmount\\\":858.11,\\\"cashPayAmount\\\":647.66,\\\"unionPayAmount\\\":753.97,\\\"createTime\\\":1554240838000},{\\\"id\\\":\\\"4\\\",\\\"countTime\\\":1092564092000,\\\"activePay\\\":672,\\\"cashPay\\\":769,\\\"codePay\\\":791,\\\"wechatPayAmount\\\":668.58,\\\"alipayPayAmount\\\":442.09,\\\"cashPayAmount\\\":748.81,\\\"unionPayAmount\\\":986.86,\\\"createTime\\\":1343254721000},{\\\"id\\\":\\\"7\\\",\\\"countTime\\\":1092230652000,\\\"activePay\\\":643,\\\"cashPay\\\":279,\\\"codePay\\\":123,\\\"wechatPayAmount\\\":101.06,\\\"alipayPayAmount\\\":838.53,\\\"cashPayAmount\\\":13.48,\\\"unionPayAmount\\\":535.94,\\\"createTime\\\":1502764090000},{\\\"id\\\":\\\"13\\\",\\\"countTime\\\":1085801087000,\\\"activePay\\\":589,\\\"cashPay\\\":249,\\\"codePay\\\":786,\\\"wechatPayAmount\\\":515.25,\\\"alipayPayAmount\\\":139.34,\\\"cashPayAmount\\\":507.41,\\\"unionPayAmount\\\":194.27,\\\"createTime\\\":1196030446000},{\\\"id\\\":\\\"2\\\",\\\"countTime\\\":1071539181000,\\\"activePay\\\":176,\\\"cashPay\\\":506,\\\"codePay\\\":352,\\\"wechatPayAmount\\\":207.05,\\\"alipayPayAmount\\\":299.24,\\\"cashPayAmount\\\":205.12,\\\"unionPayAmount\\\":659.65,\\\"createTime\\\":1093436502000},{\\\"id\\\":\\\"6\\\",\\\"countTime\\\":1038532226000,\\\"activePay\\\":499,\\\"cashPay\\\":112,\\\"codePay\\\":99,\\\"wechatPayAmount\\\":442.61,\\\"alipayPayAmount\\\":949.96,\\\"cashPayAmount\\\":371.21,\\\"unionPayAmount\\\":981.57,\\\"createTime\\\":1227355659000},{\\\"id\\\":\\\"12\\\",\\\"countTime\\\":999039450000,\\\"activePay\\\":996,\\\"cashPay\\\":600,\\\"codePay\\\":833,\\\"wechatPayAmount\\\":933.18,\\\"alipayPayAmount\\\":816.24,\\\"cashPayAmount\\\":113.54,\\\"unionPayAmount\\\":984.73,\\\"createTime\\\":1649700807000}]}}\",\"optTime\":1689646622000},{\"id\":\"1680155173313392641\",\"title\":\"系统微服务: 操作日志管理\",\"businessType\":1,\"methodName\":\"分页条件查询\",\"methodUrl\":\"com.park.systemserve.controller.OptLogController.queryPageOptLog\",\"reqType\":\"POST\",\"optName\":\"system\",\"optUrl\":\"/system/opt/page/1/5\",\"optIp\":\"0:0:0:0:0:0:0:1\",\"optAddress\":\"\",\"reqParam\":\"[1,5,{\\\"startTime\\\":null,\\\"endTime\\\":null,\\\"roadId\\\":\\\"0\\\",\\\"inspectorName\\\":\\\"\\\"}]\",\"respData\":\"{\\\"code\\\":\\\"200\\\",\\\"message\\\":\\\"请求成功\\\",\\\"data\\\":{\\\"pageNum\\\":null,\\\"pageSize\\\":null,\\\"total\\\":3,\\\"pageData\\\":[{\\\"id\\\":\\\"1680154894870327297\\\",\\\"title\\\":\\\"系统微服务: 登录日志管理\\\",\\\"businessType\\\":1,\\\"methodName\\\":\\\"分页条件查询\\\",\\\"methodUrl\\\":\\\"com.park.systemserve.controller.LoginLogController.queryPageLoginLog\\\",\\\"reqType\\\":\\\"POST\\\",\\\"optName\\\":\\\"system\\\",\\\"optUrl\\\":\\\"/system/login/Log/page/1/5\\\",\\\"optIp\\\":\\\"0:0:0:0:0:0:0:1\\\",\\\"optAddress\\\":\\\"\\\",\\\"reqParam\\\":\\\"[1,5,{\\\\\\\"startTime\\\\\\\":null,\\\\\\\"endTime\\\\\\\":null,\\\\\\\"roadId\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"inspectorName\\\\\\\":\\\\\\\"\\\\\\\"}]\\\",\\\"respData\\\":\\\"{\\\\\\\"code\\\\\\\":\\\\\\\"200\\\\\\\",\\\\\\\"message\\\\\\\":\\\\\\\"请求成功\\\\\\\",\\\\\\\"data\\\\\\\":{\\\\\\\"pageNum\\\\\\\":null,\\\\\\\"pageSize\\\\\\\":null,\\\\\\\"total\\\\\\\":4,\\\\\\\"pageData\\\\\\\":[{\\\\\\\"id\\\\\\\":\\\\\\\"4\\\\\\\",\\\\\\\"username\\\\\\\":\\\\\\\"Nanjustar\\\\\\\",\\\\\\\"ipAddress\\\\\\\":\\\\\\\"127.0.0.1\\\\\\\",\\\\\\\"ipSource\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"brower\\\\\\\":\\\\\\\"Firefox 11\\\\\\\",\\\\\\\"os\\\\\\\":\\\\\\\"Windows 10\\\\\\\",\\\\\\\"state\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1685728915000,\\\\\\\"wechat\\\\\\\":\\\\\\\"登陆成功！\\\\\\\",\\\\\\\"mobile\\\\\\\":null},{\\\\\\\"id\\\\\\\":\\\\\\\"3\\\\\\\",\\\\\\\"username\\\\\\\":\\\\\\\"Nanjustar\\\\\\\",\\\\\\\"ipAddress\\\\\\\":\\\\\\\"127.0.0.1\\\\\\\",\\\\\\\"ipSource\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"brower\\\\\\\":\\\\\\\"Chrome 11\\\\\\\",\\\\\\\"os\\\\\\\":\\\\\\\"Windows 10\\\\\\\",\\\\\\\"state\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1684771742000,\\\\\\\"wechat\\\\\\\":\\\\\\\"登陆成功！\\\\\\\",\\\\\\\"mobile\\\\\\\":null},{\\\\\\\"id\\\\\\\":\\\\\\\"2\\\\\\\",\\\\\\\"username\\\\\\\":\\\\\\\"admin\\\\\\\",\\\\\\\"ipAddress\\\\\\\":\\\\\\\"127.0.0.1\\\\\\\",\\\\\\\"ipSource\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"brower\\\\\\\":\\\\\\\"Firefox 11\\\\\\\",\\\\\\\"os\\\\\\\":\\\\\\\"Windows 10\\\\\\\",\\\\\\\"state\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1684755937000,\\\\\\\"wechat\\\\\\\":\\\\\\\"登陆成功！\\\\\\\",\\\\\\\"mobile\\\\\\\":null},{\\\\\\\"id\\\\\\\":\\\\\\\"1\\\\\\\",\\\\\\\"username\\\\\\\":\\\\\\\"Nanjustar\\\\\\\",\\\\\\\"ipAddress\\\\\\\":\\\\\\\"127.0.0.1\\\\\\\",\\\\\\\"ipSource\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"brower\\\\\\\":\\\\\\\"Firefox 11\\\\\\\",\\\\\\\"os\\\\\\\":\\\\\\\"Windows 10\\\\\\\",\\\\\\\"state\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1684755835000,\\\\\\\"wechat\\\\\\\":\\\\\\\"登陆成功！\\\\\\\",\\\\\\\"mobile\\\\\\\":null}]}}\\\",\\\"optTime\\\":1689415117000},{\\\"id\\\":\\\"1680154728322904066\\\",\\\"title\\\":\\\"系统微服务: 操作日志管理\\\",\\\"businessType\\\":1,\\\"methodName\\\":\\\"分页条件查询\\\",\\\"methodUrl\\\":\\\"com.park.systemserve.controller.OptLogController.queryPageOptLog\\\",\\\"reqType\\\":\\\"POST\\\",\\\"optName\\\":\\\"system\\\",\\\"optUrl\\\":\\\"/system/opt/page/1/5\\\",\\\"optIp\\\":\\\"0:0:0:0:0:0:0:1\\\",\\\"optAddress\\\":\\\"\\\",\\\"reqParam\\\":\\\"[1,5,{\\\\\\\"startTime\\\\\\\":null,\\\\\\\"endTime\\\\\\\":null,\\\\\\\"roadId\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"inspectorName\\\\\\\":\\\\\\\"\\\\\\\"}]\\\",\\\"respData\\\":\\\"{\\\\\\\"code\\\\\\\":\\\\\\\"200\\\\\\\",\\\\\\\"message\\\\\\\":\\\\\\\"请求成功\\\\\\\",\\\\\\\"data\\\\\\\":{\\\\\\\"pageNum\\\\\\\":null,\\\\\\\"pageSize\\\\\\\":null,\\\\\\\"total\\\\\\\":1,\\\\\\\"pageData\\\\\\\":[{\\\\\\\"id\\\\\\\":\\\\\\\"1679788350122242049\\\\\\\",\\\\\\\"title\\\\\\\":\\\\\\\"系统微服务: 营收统计\\\\\\\",\\\\\\\"businessType\\\\\\\":1,\\\\\\\"methodName\\\\\\\":\\\\\\\"分页条件查询\\\\\\\",\\\\\\\"methodUrl\\\\\\\":\\\\\\\"com.park.systemserve.controller.RevenueCountController.queryPageRevenueCount\\\\\\\",\\\\\\\"reqType\\\\\\\":\\\\\\\"POST\\\\\\\",\\\\\\\"optName\\\\\\\":null,\\\\\\\"optUrl\\\\\\\":\\\\\\\"/system/revenue/page/1/5\\\\\\\",\\\\\\\"optIp\\\\\\\":\\\\\\\"0:0:0:0:0:0:0:1\\\\\\\",\\\\\\\"optAddress\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"reqParam\\\\\\\":\\\\\\\"[1,5,{\\\\\\\\\\\\\\\"startTime\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"endTime\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"roadId\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"inspectorName\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\"}]\\\\\\\",\\\\\\\"respData\\\\\\\":\\\\\\\"{\\\\\\\\\\\\\\\"code\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"200\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"message\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"请求成功\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"data\\\\\\\\\\\\\\\":{\\\\\\\\\\\\\\\"pageNum\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"pageSize\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"total\\\\\\\\\\\\\\\":20,\\\\\\\\\\\\\\\"pageData\\\\\\\\\\\\\\\":[{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"2\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\":1687501577000,\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\":764,\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\":575.72,\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\":279,\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\":747.01,\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\":616,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1451166333000},{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"19\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\":1686653816000,\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\":44,\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\":970.21,\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\":51,\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\":27.73,\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\":28,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1334867965000},{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"9\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\":1585404486000,\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\":24,\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\":679.75,\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\":916,\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\":183.03,\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\":142,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1335276831000},{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"10\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\":1574730532000,\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\":456,\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\":988.84,\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\":600,\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\":414.76,\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\":381,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1328030954000},{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"4\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\":1545864704000,\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\":770,\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\":4.43,\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\":379,\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\":576.06,\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\":1,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1582136419000}]}}\\\\\\\",\\\\\\\"optTime\\\\\\\":1689327726000}]}}\\\",\\\"optTime\\\":1689415078000},{\\\"id\\\":\\\"1679788350122242049\\\",\\\"title\\\":\\\"系统微服务: 营收统计\\\",\\\"businessType\\\":1,\\\"methodName\\\":\\\"分页条件查询\\\",\\\"methodUrl\\\":\\\"com.park.systemserve.controller.RevenueCountController.queryPageRevenueCount\\\",\\\"reqType\\\":\\\"POST\\\",\\\"optName\\\":null,\\\"optUrl\\\":\\\"/system/revenue/page/1/5\\\",\\\"optIp\\\":\\\"0:0:0:0:0:0:0:1\\\",\\\"optAddress\\\":\\\"\\\",\\\"reqParam\\\":\\\"[1,5,{\\\\\\\"startTime\\\\\\\":null,\\\\\\\"endTime\\\\\\\":null,\\\\\\\"roadId\\\\\\\":null,\\\\\\\"inspectorName\\\\\\\":\\\\\\\"\\\\\\\"}]\\\",\\\"respData\\\":\\\"{\\\\\\\"code\\\\\\\":\\\\\\\"200\\\\\\\",\\\\\\\"message\\\\\\\":\\\\\\\"请求成功\\\\\\\",\\\\\\\"data\\\\\\\":{\\\\\\\"pageNum\\\\\\\":null,\\\\\\\"pageSize\\\\\\\":null,\\\\\\\"total\\\\\\\":20,\\\\\\\"pageData\\\\\\\":[{\\\\\\\"id\\\\\\\":\\\\\\\"2\\\\\\\",\\\\\\\"countTime\\\\\\\":1687501577000,\\\\\\\"orderCount\\\\\\\":764,\\\\\\\"revenue\\\\\\\":575.72,\\\\\\\"refundOrder\\\\\\\":279,\\\\\\\"refundAmount\\\\\\\":747.01,\\\\\\\"newUser\\\\\\\":616,\\\\\\\"createTime\\\\\\\":1451166333000},{\\\\\\\"id\\\\\\\":\\\\\\\"19\\\\\\\",\\\\\\\"countTime\\\\\\\":1686653816000,\\\\\\\"orderCount\\\\\\\":44,\\\\\\\"revenue\\\\\\\":970.21,\\\\\\\"refundOrder\\\\\\\":51,\\\\\\\"refundAmount\\\\\\\":27.73,\\\\\\\"newUser\\\\\\\":28,\\\\\\\"createTime\\\\\\\":1334867965000},{\\\\\\\"id\\\\\\\":\\\\\\\"9\\\\\\\",\\\\\\\"countTime\\\\\\\":1585404486000,\\\\\\\"orderCount\\\\\\\":24,\\\\\\\"revenue\\\\\\\":679.75,\\\\\\\"refundOrder\\\\\\\":916,\\\\\\\"refundAmount\\\\\\\":183.03,\\\\\\\"newUser\\\\\\\":142,\\\\\\\"createTime\\\\\\\":1335276831000},{\\\\\\\"id\\\\\\\":\\\\\\\"10\\\\\\\",\\\\\\\"countTime\\\\\\\":1574730532000,\\\\\\\"orderCount\\\\\\\":456,\\\\\\\"revenue\\\\\\\":988.84,\\\\\\\"refundOrder\\\\\\\":600,\\\\\\\"refundAmount\\\\\\\":414.76,\\\\\\\"newUser\\\\\\\":381,\\\\\\\"createTime\\\\\\\":1328030954000},{\\\\\\\"id\\\\\\\":\\\\\\\"4\\\\\\\",\\\\\\\"countTime\\\\\\\":1545864704000,\\\\\\\"orderCount\\\\\\\":770,\\\\\\\"revenue\\\\\\\":4.43,\\\\\\\"refundOrder\\\\\\\":379,\\\\\\\"refundAmount\\\\\\\":576.06,\\\\\\\"newUser\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1582136419000}]}}\\\",\\\"optTime\\\":1689327726000}]}}\",\"optTime\":1689415184000},{\"id\":\"1680154894870327297\",\"title\":\"系统微服务: 登录日志管理\",\"businessType\":1,\"methodName\":\"分页条件查询\",\"methodUrl\":\"com.park.systemserve.controller.LoginLogController.queryPageLoginLog\",\"reqType\":\"POST\",\"optName\":\"system\",\"optUrl\":\"/system/login/Log/page/1/5\",\"optIp\":\"0:0:0:0:0:0:0:1\",\"optAddress\":\"\",\"reqParam\":\"[1,5,{\\\"startTime\\\":null,\\\"endTime\\\":null,\\\"roadId\\\":\\\"0\\\",\\\"inspectorName\\\":\\\"\\\"}]\",\"respData\":\"{\\\"code\\\":\\\"200\\\",\\\"message\\\":\\\"请求成功\\\",\\\"data\\\":{\\\"pageNum\\\":null,\\\"pageSize\\\":null,\\\"total\\\":4,\\\"pageData\\\":[{\\\"id\\\":\\\"4\\\",\\\"username\\\":\\\"Nanjustar\\\",\\\"ipAddress\\\":\\\"127.0.0.1\\\",\\\"ipSource\\\":\\\"\\\",\\\"brower\\\":\\\"Firefox 11\\\",\\\"os\\\":\\\"Windows 10\\\",\\\"state\\\":1,\\\"createTime\\\":1685728915000,\\\"wechat\\\":\\\"登陆成功！\\\",\\\"mobile\\\":null},{\\\"id\\\":\\\"3\\\",\\\"username\\\":\\\"Nanjustar\\\",\\\"ipAddress\\\":\\\"127.0.0.1\\\",\\\"ipSource\\\":\\\"\\\",\\\"brower\\\":\\\"Chrome 11\\\",\\\"os\\\":\\\"Windows 10\\\",\\\"state\\\":1,\\\"createTime\\\":1684771742000,\\\"wechat\\\":\\\"登陆成功！\\\",\\\"mobile\\\":null},{\\\"id\\\":\\\"2\\\",\\\"username\\\":\\\"admin\\\",\\\"ipAddress\\\":\\\"127.0.0.1\\\",\\\"ipSource\\\":\\\"\\\",\\\"brower\\\":\\\"Firefox 11\\\",\\\"os\\\":\\\"Windows 10\\\",\\\"state\\\":1,\\\"createTime\\\":1684755937000,\\\"wechat\\\":\\\"登陆成功！\\\",\\\"mobile\\\":null},{\\\"id\\\":\\\"1\\\",\\\"username\\\":\\\"Nanjustar\\\",\\\"ipAddress\\\":\\\"127.0.0.1\\\",\\\"ipSource\\\":\\\"\\\",\\\"brower\\\":\\\"Firefox 11\\\",\\\"os\\\":\\\"Windows 10\\\",\\\"state\\\":1,\\\"createTime\\\":1684755835000,\\\"wechat\\\":\\\"登陆成功！\\\",\\\"mobile\\\":null}]}}\",\"optTime\":1689415117000},{\"id\":\"1680154728322904066\",\"title\":\"系统微服务: 操作日志管理\",\"businessType\":1,\"methodName\":\"分页条件查询\",\"methodUrl\":\"com.park.systemserve.controller.OptLogController.queryPageOptLog\",\"reqType\":\"POST\",\"optName\":\"system\",\"optUrl\":\"/system/opt/page/1/5\",\"optIp\":\"0:0:0:0:0:0:0:1\",\"optAddress\":\"\",\"reqParam\":\"[1,5,{\\\"startTime\\\":null,\\\"endTime\\\":null,\\\"roadId\\\":\\\"0\\\",\\\"inspectorName\\\":\\\"\\\"}]\",\"respData\":\"{\\\"code\\\":\\\"200\\\",\\\"message\\\":\\\"请求成功\\\",\\\"data\\\":{\\\"pageNum\\\":null,\\\"pageSize\\\":null,\\\"total\\\":1,\\\"pageData\\\":[{\\\"id\\\":\\\"1679788350122242049\\\",\\\"title\\\":\\\"系统微服务: 营收统计\\\",\\\"businessType\\\":1,\\\"methodName\\\":\\\"分页条件查询\\\",\\\"methodUrl\\\":\\\"com.park.systemserve.controller.RevenueCountController.queryPageRevenueCount\\\",\\\"reqType\\\":\\\"POST\\\",\\\"optName\\\":null,\\\"optUrl\\\":\\\"/system/revenue/page/1/5\\\",\\\"optIp\\\":\\\"0:0:0:0:0:0:0:1\\\",\\\"optAddress\\\":\\\"\\\",\\\"reqParam\\\":\\\"[1,5,{\\\\\\\"startTime\\\\\\\":null,\\\\\\\"endTime\\\\\\\":null,\\\\\\\"roadId\\\\\\\":null,\\\\\\\"inspectorName\\\\\\\":\\\\\\\"\\\\\\\"}]\\\",\\\"respData\\\":\\\"{\\\\\\\"code\\\\\\\":\\\\\\\"200\\\\\\\",\\\\\\\"message\\\\\\\":\\\\\\\"请求成功\\\\\\\",\\\\\\\"data\\\\\\\":{\\\\\\\"pageNum\\\\\\\":null,\\\\\\\"pageSize\\\\\\\":null,\\\\\\\"total\\\\\\\":20,\\\\\\\"pageData\\\\\\\":[{\\\\\\\"id\\\\\\\":\\\\\\\"2\\\\\\\",\\\\\\\"countTime\\\\\\\":1687501577000,\\\\\\\"orderCount\\\\\\\":764,\\\\\\\"revenue\\\\\\\":575.72,\\\\\\\"refundOrder\\\\\\\":279,\\\\\\\"refundAmount\\\\\\\":747.01,\\\\\\\"newUser\\\\\\\":616,\\\\\\\"createTime\\\\\\\":1451166333000},{\\\\\\\"id\\\\\\\":\\\\\\\"19\\\\\\\",\\\\\\\"countTime\\\\\\\":1686653816000,\\\\\\\"orderCount\\\\\\\":44,\\\\\\\"revenue\\\\\\\":970.21,\\\\\\\"refundOrder\\\\\\\":51,\\\\\\\"refundAmount\\\\\\\":27.73,\\\\\\\"newUser\\\\\\\":28,\\\\\\\"createTime\\\\\\\":1334867965000},{\\\\\\\"id\\\\\\\":\\\\\\\"9\\\\\\\",\\\\\\\"countTime\\\\\\\":1585404486000,\\\\\\\"orderCount\\\\\\\":24,\\\\\\\"revenue\\\\\\\":679.75,\\\\\\\"refundOrder\\\\\\\":916,\\\\\\\"refundAmount\\\\\\\":183.03,\\\\\\\"newUser\\\\\\\":142,\\\\\\\"createTime\\\\\\\":1335276831000},{\\\\\\\"id\\\\\\\":\\\\\\\"10\\\\\\\",\\\\\\\"countTime\\\\\\\":1574730532000,\\\\\\\"orderCount\\\\\\\":456,\\\\\\\"revenue\\\\\\\":988.84,\\\\\\\"refundOrder\\\\\\\":600,\\\\\\\"refundAmount\\\\\\\":414.76,\\\\\\\"newUser\\\\\\\":381,\\\\\\\"createTime\\\\\\\":1328030954000},{\\\\\\\"id\\\\\\\":\\\\\\\"4\\\\\\\",\\\\\\\"countTime\\\\\\\":1545864704000,\\\\\\\"orderCount\\\\\\\":770,\\\\\\\"revenue\\\\\\\":4.43,\\\\\\\"refundOrder\\\\\\\":379,\\\\\\\"refundAmount\\\\\\\":576.06,\\\\\\\"newUser\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1582136419000}]}}\\\",\\\"optTime\\\":1689327726000}]}}\",\"optTime\":1689415078000},{\"id\":\"1679788350122242049\",\"title\":\"系统微服务: 营收统计\",\"businessType\":1,\"methodName\":\"分页条件查询\",\"methodUrl\":\"com.park.systemserve.controller.RevenueCountController.queryPageRevenueCount\",\"reqType\":\"POST\",\"optName\":null,\"optUrl\":\"/system/revenue/page/1/5\",\"optIp\":\"0:0:0:0:0:0:0:1\",\"optAddress\":\"\",\"reqParam\":\"[1,5,{\\\"startTime\\\":null,\\\"endTime\\\":null,\\\"roadId\\\":null,\\\"inspectorName\\\":\\\"\\\"}]\",\"respData\":\"{\\\"code\\\":\\\"200\\\",\\\"message\\\":\\\"请求成功\\\",\\\"data\\\":{\\\"pageNum\\\":null,\\\"pageSize\\\":null,\\\"total\\\":20,\\\"pageData\\\":[{\\\"id\\\":\\\"2\\\",\\\"countTime\\\":1687501577000,\\\"orderCount\\\":764,\\\"revenue\\\":575.72,\\\"refundOrder\\\":279,\\\"refundAmount\\\":747.01,\\\"newUser\\\":616,\\\"createTime\\\":1451166333000},{\\\"id\\\":\\\"19\\\",\\\"countTime\\\":1686653816000,\\\"orderCount\\\":44,\\\"revenue\\\":970.21,\\\"refundOrder\\\":51,\\\"refundAmount\\\":27.73,\\\"newUser\\\":28,\\\"createTime\\\":1334867965000},{\\\"id\\\":\\\"9\\\",\\\"countTime\\\":1585404486000,\\\"orderCount\\\":24,\\\"revenue\\\":679.75,\\\"refundOrder\\\":916,\\\"refundAmount\\\":183.03,\\\"newUser\\\":142,\\\"createTime\\\":1335276831000},{\\\"id\\\":\\\"10\\\",\\\"countTime\\\":1574730532000,\\\"orderCount\\\":456,\\\"revenue\\\":988.84,\\\"refundOrder\\\":600,\\\"refundAmount\\\":414.76,\\\"newUser\\\":381,\\\"createTime\\\":1328030954000},{\\\"id\\\":\\\"4\\\",\\\"countTime\\\":1545864704000,\\\"orderCount\\\":770,\\\"revenue\\\":4.43,\\\"refundOrder\\\":379,\\\"refundAmount\\\":576.06,\\\"newUser\\\":1,\\\"createTime\\\":1582136419000}]}}\",\"optTime\":1689327726000}]}}', '2023-07-18 02:17:47');
INSERT INTO `tp_opt_log` VALUES (1681186615480856577, '打卡', 1, '打卡接口', 'com.park.dataserve.controller.AttendanceController.clock', 'POST', 'system', '/data/attendance', '0:0:0:0:0:0:0:1', '', '[{\"inspectorId\":\"234\",\"clockTime\":\"2023-07-20 05\",\"clockDate\":\"2023-07-20\",\"clockType\":0}]', '{\"code\":\"200\",\"message\":\"请求成功\",\"data\":null}', '2023-07-18 06:18:18');
INSERT INTO `tp_opt_log` VALUES (1681192829031854081, '系统微服务: 操作日志管理', 1, '分页条件查询', 'com.park.systemserve.controller.OptLogController.queryPageOptLog', 'POST', 'system', '/system/opt/page/1/-1', '192.168.3.22', '', '[1,-1,{\"startTime\":null,\"endTime\":null,\"roadId\":\"0\",\"inspectorName\":\"\"}]', '{\"code\":\"200\",\"message\":\"请求成功\",\"data\":{\"pageNum\":null,\"pageSize\":null,\"total\":0,\"pageData\":[{\"id\":\"1681186615480856577\",\"title\":\"打卡\",\"businessType\":1,\"methodName\":\"打卡接口\",\"methodUrl\":\"com.park.dataserve.controller.AttendanceController.clock\",\"reqType\":\"POST\",\"optName\":\"system\",\"optUrl\":\"/data/attendance\",\"optIp\":\"0:0:0:0:0:0:0:1\",\"optAddress\":\"\",\"reqParam\":\"[{\\\"inspectorId\\\":\\\"234\\\",\\\"clockTime\\\":\\\"2023-07-20 05\\\",\\\"clockDate\\\":\\\"2023-07-20\\\",\\\"clockType\\\":0}]\",\"respData\":\"{\\\"code\\\":\\\"200\\\",\\\"message\\\":\\\"请求成功\\\",\\\"data\\\":null}\",\"optTime\":1689661098000},{\"id\":\"1681126085663698946\",\"title\":\"系统微服务: 操作日志管理\",\"businessType\":1,\"methodName\":\"分页条件查询\",\"methodUrl\":\"com.park.systemserve.controller.OptLogController.queryPageOptLog\",\"reqType\":\"POST\",\"optName\":\"system\",\"optUrl\":\"/system/opt/page/1/-1\",\"optIp\":\"192.168.3.22\",\"optAddress\":\"\",\"reqParam\":\"[1,-1,{\\\"startTime\\\":null,\\\"endTime\\\":null,\\\"roadId\\\":\\\"0\\\",\\\"inspectorName\\\":\\\"\\\"}]\",\"respData\":\"{\\\"code\\\":\\\"200\\\",\\\"message\\\":\\\"请求成功\\\",\\\"data\\\":{\\\"pageNum\\\":null,\\\"pageSize\\\":null,\\\"total\\\":0,\\\"pageData\\\":[{\\\"id\\\":\\\"1681126009008599042\\\",\\\"title\\\":\\\"系统微服务: 登录日志管理\\\",\\\"businessType\\\":1,\\\"methodName\\\":\\\"分页条件查询\\\",\\\"methodUrl\\\":\\\"com.park.systemserve.controller.LoginLogController.queryPageLoginLog\\\",\\\"reqType\\\":\\\"POST\\\",\\\"optName\\\":\\\"system\\\",\\\"optUrl\\\":\\\"/system/login/Log/page/1/-1\\\",\\\"optIp\\\":\\\"192.168.3.22\\\",\\\"optAddress\\\":\\\"\\\",\\\"reqParam\\\":\\\"[1,-1,{\\\\\\\"startTime\\\\\\\":null,\\\\\\\"endTime\\\\\\\":null,\\\\\\\"roadId\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"inspectorName\\\\\\\":\\\\\\\"\\\\\\\"}]\\\",\\\"respData\\\":\\\"{\\\\\\\"code\\\\\\\":\\\\\\\"200\\\\\\\",\\\\\\\"message\\\\\\\":\\\\\\\"请求成功\\\\\\\",\\\\\\\"data\\\\\\\":{\\\\\\\"pageNum\\\\\\\":null,\\\\\\\"pageSize\\\\\\\":null,\\\\\\\"total\\\\\\\":0,\\\\\\\"pageData\\\\\\\":[{\\\\\\\"id\\\\\\\":\\\\\\\"4\\\\\\\",\\\\\\\"username\\\\\\\":\\\\\\\"Nanjustar\\\\\\\",\\\\\\\"ipAddress\\\\\\\":\\\\\\\"127.0.0.1\\\\\\\",\\\\\\\"ipSource\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"brower\\\\\\\":\\\\\\\"Firefox 11\\\\\\\",\\\\\\\"os\\\\\\\":\\\\\\\"Windows 10\\\\\\\",\\\\\\\"state\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1685728915000,\\\\\\\"wechat\\\\\\\":\\\\\\\"登陆成功！\\\\\\\",\\\\\\\"mobile\\\\\\\":null},{\\\\\\\"id\\\\\\\":\\\\\\\"3\\\\\\\",\\\\\\\"username\\\\\\\":\\\\\\\"Nanjustar\\\\\\\",\\\\\\\"ipAddress\\\\\\\":\\\\\\\"127.0.0.1\\\\\\\",\\\\\\\"ipSource\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"brower\\\\\\\":\\\\\\\"Chrome 11\\\\\\\",\\\\\\\"os\\\\\\\":\\\\\\\"Windows 10\\\\\\\",\\\\\\\"state\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1684771742000,\\\\\\\"wechat\\\\\\\":\\\\\\\"登陆成功！\\\\\\\",\\\\\\\"mobile\\\\\\\":null},{\\\\\\\"id\\\\\\\":\\\\\\\"2\\\\\\\",\\\\\\\"username\\\\\\\":\\\\\\\"admin\\\\\\\",\\\\\\\"ipAddress\\\\\\\":\\\\\\\"127.0.0.1\\\\\\\",\\\\\\\"ipSource\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"brower\\\\\\\":\\\\\\\"Firefox 11\\\\\\\",\\\\\\\"os\\\\\\\":\\\\\\\"Windows 10\\\\\\\",\\\\\\\"state\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1684755937000,\\\\\\\"wechat\\\\\\\":\\\\\\\"登陆成功！\\\\\\\",\\\\\\\"mobile\\\\\\\":null},{\\\\\\\"id\\\\\\\":\\\\\\\"1\\\\\\\",\\\\\\\"username\\\\\\\":\\\\\\\"Nanjustar\\\\\\\",\\\\\\\"ipAddress\\\\\\\":\\\\\\\"127.0.0.1\\\\\\\",\\\\\\\"ipSource\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"brower\\\\\\\":\\\\\\\"Firefox 11\\\\\\\",\\\\\\\"os\\\\\\\":\\\\\\\"Windows 10\\\\\\\",\\\\\\\"state\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1684755835000,\\\\\\\"wechat\\\\\\\":\\\\\\\"登陆成功！\\\\\\\",\\\\\\\"mobile\\\\\\\":null}]}}\\\",\\\"optTime\\\":1689646649000},{\\\"id\\\":\\\"1681125894395047938\\\",\\\"title\\\":\\\"系统微服务: 支付统计\\\",\\\"businessType\\\":1,\\\"methodName\\\":\\\"分页条件查询\\\",\\\"methodUrl\\\":\\\"com.park.systemserve.controller.PayCountController.queryPagePayCount\\\",\\\"reqType\\\":\\\"POST\\\",\\\"optName\\\":\\\"system\\\",\\\"optUrl\\\":\\\"/system/pay/page/1/-1\\\",\\\"optIp\\\":\\\"192.168.3.22\\\",\\\"optAddress\\\":\\\"\\\",\\\"reqParam\\\":\\\"[1,-1,{\\\\\\\"startTime\\\\\\\":null,\\\\\\\"endTime\\\\\\\":null,\\\\\\\"roadId\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"inspectorName\\\\\\\":\\\\\\\"\\\\\\\"}]\\\",\\\"respData\\\":\\\"{\\\\\\\"code\\\\\\\":\\\\\\\"200\\\\\\\",\\\\\\\"message\\\\\\\":\\\\\\\"请求成功\\\\\\\",\\\\\\\"data\\\\\\\":{\\\\\\\"pageNum\\\\\\\":null,\\\\\\\"pageSize\\\\\\\":null,\\\\\\\"total\\\\\\\":0,\\\\\\\"pageData\\\\\\\":[{\\\\\\\"id\\\\\\\":\\\\\\\"14\\\\\\\",\\\\\\\"countTime\\\\\\\":1594685542000,\\\\\\\"activePay\\\\\\\":686,\\\\\\\"cashPay\\\\\\\":507,\\\\\\\"codePay\\\\\\\":767,\\\\\\\"wechatPayAmount\\\\\\\":0.38,\\\\\\\"alipayPayAmount\\\\\\\":186.27,\\\\\\\"cashPayAmount\\\\\\\":561.84,\\\\\\\"unionPayAmount\\\\\\\":104.32,\\\\\\\"createTime\\\\\\\":1661174376000},{\\\\\\\"id\\\\\\\":\\\\\\\"8\\\\\\\",\\\\\\\"countTime\\\\\\\":1564225797000,\\\\\\\"activePay\\\\\\\":472,\\\\\\\"cashPay\\\\\\\":746,\\\\\\\"codePay\\\\\\\":796,\\\\\\\"wechatPayAmount\\\\\\\":195.45,\\\\\\\"alipayPayAmount\\\\\\\":507.62,\\\\\\\"cashPayAmount\\\\\\\":868.35,\\\\\\\"unionPayAmount\\\\\\\":722.12,\\\\\\\"createTime\\\\\\\":1313245319000},{\\\\\\\"id\\\\\\\":\\\\\\\"15\\\\\\\",\\\\\\\"countTime\\\\\\\":1557460914000,\\\\\\\"activePay\\\\\\\":940,\\\\\\\"cashPay\\\\\\\":389,\\\\\\\"codePay\\\\\\\":672,\\\\\\\"wechatPayAmount\\\\\\\":937.34,\\\\\\\"alipayPayAmount\\\\\\\":448.54,\\\\\\\"cashPayAmount\\\\\\\":694.97,\\\\\\\"unionPayAmount\\\\\\\":180.61,\\\\\\\"createTime\\\\\\\":1363446388000},{\\\\\\\"id\\\\\\\":\\\\\\\"3\\\\\\\",\\\\\\\"countTime\\\\\\\":1510865003000,\\\\\\\"activePay\\\\\\\":249,\\\\\\\"cashPay\\\\\\\":247,\\\\\\\"codePay\\\\\\\":742,\\\\\\\"wechatPayAmount\\\\\\\":558.17,\\\\\\\"alipayPayAmount\\\\\\\":788.87,\\\\\\\"cashPayAmount\\\\\\\":784.12,\\\\\\\"unionPayAmount\\\\\\\":143.04,\\\\\\\"createTime\\\\\\\":1441321976000},{\\\\\\\"id\\\\\\\":\\\\\\\"18\\\\\\\",\\\\\\\"countTime\\\\\\\":1395945444000,\\\\\\\"activePay\\\\\\\":604,\\\\\\\"cashPay\\\\\\\":573,\\\\\\\"codePay\\\\\\\":741,\\\\\\\"wechatPayAmount\\\\\\\":463.26,\\\\\\\"alipayPayAmount\\\\\\\":144.26,\\\\\\\"cashPayAmount\\\\\\\":970.50,\\\\\\\"unionPayAmount\\\\\\\":582.33,\\\\\\\"createTime\\\\\\\":1044809216000},{\\\\\\\"id\\\\\\\":\\\\\\\"11\\\\\\\",\\\\\\\"countTime\\\\\\\":1370187195000,\\\\\\\"activePay\\\\\\\":286,\\\\\\\"cashPay\\\\\\\":524,\\\\\\\"codePay\\\\\\\":113,\\\\\\\"wechatPayAmount\\\\\\\":524.84,\\\\\\\"alipayPayAmount\\\\\\\":404.41,\\\\\\\"cashPayAmount\\\\\\\":446.39,\\\\\\\"unionPayAmount\\\\\\\":716.95,\\\\\\\"createTime\\\\\\\":1359875756000},{\\\\\\\"id\\\\\\\":\\\\\\\"5\\\\\\\",\\\\\\\"countTime\\\\\\\":1363857856000,\\\\\\\"activePay\\\\\\\":788,\\\\\\\"cashPay\\\\\\\":601,\\\\\\\"codePay\\\\\\\":51,\\\\\\\"wechatPayAmount\\\\\\\":704.90,\\\\\\\"alipayPayAmount\\\\\\\":305.36,\\\\\\\"cashPayAmount\\\\\\\":85.42,\\\\\\\"unionPayAmount\\\\\\\":595.39,\\\\\\\"createTime\\\\\\\":1626042931000},{\\\\\\\"id\\\\\\\":\\\\\\\"16\\\\\\\",\\\\\\\"countTime\\\\\\\":1272108472000,\\\\\\\"activePay\\\\\\\":873,\\\\\\\"cashPay\\\\\\\":983,\\\\\\\"codePay\\\\\\\":827,\\\\\\\"wechatPayAmount\\\\\\\":446.75,\\\\\\\"alipayPayAmount\\\\\\\":767.99,\\\\\\\"cashPayAmount\\\\\\\":273.55,\\\\\\\"unionPayAmount\\\\\\\":3.28,\\\\\\\"createTime\\\\\\\":1678233266000},{\\\\\\\"id\\\\\\\":\\\\\\\"20\\\\\\\",\\\\\\\"countTime\\\\\\\":1263565824000,\\\\\\\"activePay\\\\\\\":65,\\\\\\\"cashPay\\\\\\\":264,\\\\\\\"codePay\\\\\\\":34,\\\\\\\"wechatPayAmount\\\\\\\":542.39,\\\\\\\"alipayPayAmount\\\\\\\":155.58,\\\\\\\"cashPayAmount\\\\\\\":14.13,\\\\\\\"unionPayAmount\\\\\\\":426.87,\\\\\\\"createTime\\\\\\\":969387238000},{\\\\\\\"id\\\\\\\":\\\\\\\"17\\\\\\\",\\\\\\\"countTime\\\\\\\":1230195510000,\\\\\\\"activePay\\\\\\\":147,\\\\\\\"cashPay\\\\\\\":771,\\\\\\\"codePay\\\\\\\":242,\\\\\\\"wechatPayAmount\\\\\\\":799.52,\\\\\\\"alipayPayAmount\\\\\\\":95.22,\\\\\\\"cashPayAmount\\\\\\\":965.60,\\\\\\\"unionPayAmount\\\\\\\":94.22,\\\\\\\"createTime\\\\\\\":1234449135000},{\\\\\\\"id\\\\\\\":\\\\\\\"9\\\\\\\",\\\\\\\"countTime\\\\\\\":1205744341000,\\\\\\\"activePay\\\\\\\":768,\\\\\\\"cashPay\\\\\\\":644,\\\\\\\"codePay\\\\\\\":82,\\\\\\\"wechatPayAmount\\\\\\\":301.21,\\\\\\\"alipayPayAmount\\\\\\\":181.47,\\\\\\\"cashPayAmount\\\\\\\":150.05,\\\\\\\"unionPayAmount\\\\\\\":821.52,\\\\\\\"createTime\\\\\\\":1644243684000},{\\\\\\\"id\\\\\\\":\\\\\\\"10\\\\\\\",\\\\\\\"countTime\\\\\\\":1159885910000,\\\\\\\"activePay\\\\\\\":290,\\\\\\\"cashPay\\\\\\\":228,\\\\\\\"codePay\\\\\\\":724,\\\\\\\"wechatPayAmount\\\\\\\":226.54,\\\\\\\"alipayPayAmount\\\\\\\":908.78,\\\\\\\"cashPayAmount\\\\\\\":937.47,\\\\\\\"unionPayAmount\\\\\\\":482.54,\\\\\\\"createTime\\\\\\\":1604895828000},{\\\\\\\"id\\\\\\\":\\\\\\\"1\\\\\\\",\\\\\\\"countTime\\\\\\\":1154907944000,\\\\\\\"activePay\\\\\\\":315,\\\\\\\"cashPay\\\\\\\":748,\\\\\\\"codePay\\\\\\\":785,\\\\\\\"wechatPayAmount\\\\\\\":872.73,\\\\\\\"alipayPayAmount\\\\\\\":895.74,\\\\\\\"cashPayAmount\\\\\\\":892.92,\\\\\\\"unionPayAmount\\\\\\\":890.26,\\\\\\\"createTime\\\\\\\":1238838193000},{\\\\\\\"id\\\\\\\":\\\\\\\"19\\\\\\\",\\\\\\\"countTime\\\\\\\":1133141557000,\\\\\\\"activePay\\\\\\\":28,\\\\\\\"cashPay\\\\\\\":808,\\\\\\\"codePay\\\\\\\":806,\\\\\\\"wechatPayAmount\\\\\\\":492.43,\\\\\\\"alipayPayAmount\\\\\\\":858.11,\\\\\\\"cashPayAmount\\\\\\\":647.66,\\\\\\\"unionPayAmount\\\\\\\":753.97,\\\\\\\"createTime\\\\\\\":1554240838000},{\\\\\\\"id\\\\\\\":\\\\\\\"4\\\\\\\",\\\\\\\"countTime\\\\\\\":1092564092000,\\\\\\\"activePay\\\\\\\":672,\\\\\\\"cashPay\\\\\\\":769,\\\\\\\"codePay\\\\\\\":791,\\\\\\\"wechatPayAmount\\\\\\\":668.58,\\\\\\\"alipayPayAmount\\\\\\\":442.09,\\\\\\\"cashPayAmount\\\\\\\":748.81,\\\\\\\"unionPayAmount\\\\\\\":986.86,\\\\\\\"createTime\\\\\\\":1343254721000},{\\\\\\\"id\\\\\\\":\\\\\\\"7\\\\\\\",\\\\\\\"countTime\\\\\\\":1092230652000,\\\\\\\"activePay\\\\\\\":643,\\\\\\\"cashPay\\\\\\\":279,\\\\\\\"codePay\\\\\\\":123,\\\\\\\"wechatPayAmount\\\\\\\":101.06,\\\\\\\"alipayPayAmount\\\\\\\":838.53,\\\\\\\"cashPayAmount\\\\\\\":13.48,\\\\\\\"unionPayAmount\\\\\\\":535.94,\\\\\\\"createTime\\\\\\\":1502764090000},{\\\\\\\"id\\\\\\\":\\\\\\\"13\\\\\\\",\\\\\\\"countTime\\\\\\\":1085801087000,\\\\\\\"activePay\\\\\\\":589,\\\\\\\"cashPay\\\\\\\":249,\\\\\\\"codePay\\\\\\\":786,\\\\\\\"wechatPayAmount\\\\\\\":515.25,\\\\\\\"alipayPayAmount\\\\\\\":139.34,\\\\\\\"cashPayAmount\\\\\\\":507.41,\\\\\\\"unionPayAmount\\\\\\\":194.27,\\\\\\\"createTime\\\\\\\":1196030446000},{\\\\\\\"id\\\\\\\":\\\\\\\"2\\\\\\\",\\\\\\\"countTime\\\\\\\":1071539181000,\\\\\\\"activePay\\\\\\\":176,\\\\\\\"cashPay\\\\\\\":506,\\\\\\\"codePay\\\\\\\":352,\\\\\\\"wechatPayAmount\\\\\\\":207.05,\\\\\\\"alipayPayAmount\\\\\\\":299.24,\\\\\\\"cashPayAmount\\\\\\\":205.12,\\\\\\\"unionPayAmount\\\\\\\":659.65,\\\\\\\"createTime\\\\\\\":1093436502000},{\\\\\\\"id\\\\\\\":\\\\\\\"6\\\\\\\",\\\\\\\"countTime\\\\\\\":1038532226000,\\\\\\\"activePay\\\\\\\":499,\\\\\\\"cashPay\\\\\\\":112,\\\\\\\"codePay\\\\\\\":99,\\\\\\\"wechatPayAmount\\\\\\\":442.61,\\\\\\\"alipayPayAmount\\\\\\\":949.96,\\\\\\\"cashPayAmount\\\\\\\":371.21,\\\\\\\"unionPayAmount\\\\\\\":981.57,\\\\\\\"createTime\\\\\\\":1227355659000},{\\\\\\\"id\\\\\\\":\\\\\\\"12\\\\\\\",\\\\\\\"countTime\\\\\\\":999039450000,\\\\\\\"activePay\\\\\\\":996,\\\\\\\"cashPay\\\\\\\":600,\\\\\\\"codePay\\\\\\\":833,\\\\\\\"wechatPayAmount\\\\\\\":933.18,\\\\\\\"alipayPayAmount\\\\\\\":816.24,\\\\\\\"cashPayAmount\\\\\\\":113.54,\\\\\\\"unionPayAmount\\\\\\\":984.73,\\\\\\\"createTime\\\\\\\":1649700807000}]}}\\\",\\\"optTime\\\":1689646622000},{\\\"id\\\":\\\"1680155173313392641\\\",\\\"title\\\":\\\"系统微服务: 操作日志管理\\\",\\\"businessType\\\":1,\\\"methodName\\\":\\\"分页条件查询\\\",\\\"methodUrl\\\":\\\"com.park.systemserve.controller.OptLogController.queryPageOptLog\\\",\\\"reqType\\\":\\\"POST\\\",\\\"optName\\\":\\\"system\\\",\\\"optUrl\\\":\\\"/system/opt/page/1/5\\\",\\\"optIp\\\":\\\"0:0:0:0:0:0:0:1\\\",\\\"optAddress\\\":\\\"\\\",\\\"reqParam\\\":\\\"[1,5,{\\\\\\\"startTime\\\\\\\":null,\\\\\\\"endTime\\\\\\\":null,\\\\\\\"roadId\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"inspectorName\\\\\\\":\\\\\\\"\\\\\\\"}]\\\",\\\"respData\\\":\\\"{\\\\\\\"code\\\\\\\":\\\\\\\"200\\\\\\\",\\\\\\\"message\\\\\\\":\\\\\\\"请求成功\\\\\\\",\\\\\\\"data\\\\\\\":{\\\\\\\"pageNum\\\\\\\":null,\\\\\\\"pageSize\\\\\\\":null,\\\\\\\"total\\\\\\\":3,\\\\\\\"pageData\\\\\\\":[{\\\\\\\"id\\\\\\\":\\\\\\\"1680154894870327297\\\\\\\",\\\\\\\"title\\\\\\\":\\\\\\\"系统微服务: 登录日志管理\\\\\\\",\\\\\\\"businessType\\\\\\\":1,\\\\\\\"methodName\\\\\\\":\\\\\\\"分页条件查询\\\\\\\",\\\\\\\"methodUrl\\\\\\\":\\\\\\\"com.park.systemserve.controller.LoginLogController.queryPageLoginLog\\\\\\\",\\\\\\\"reqType\\\\\\\":\\\\\\\"POST\\\\\\\",\\\\\\\"optName\\\\\\\":\\\\\\\"system\\\\\\\",\\\\\\\"optUrl\\\\\\\":\\\\\\\"/system/login/Log/page/1/5\\\\\\\",\\\\\\\"optIp\\\\\\\":\\\\\\\"0:0:0:0:0:0:0:1\\\\\\\",\\\\\\\"optAddress\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"reqParam\\\\\\\":\\\\\\\"[1,5,{\\\\\\\\\\\\\\\"startTime\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"endTime\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"roadId\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"0\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"inspectorName\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\"}]\\\\\\\",\\\\\\\"respData\\\\\\\":\\\\\\\"{\\\\\\\\\\\\\\\"code\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"200\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"message\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"请求成功\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"data\\\\\\\\\\\\\\\":{\\\\\\\\\\\\\\\"pageNum\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"pageSize\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"total\\\\\\\\\\\\\\\":4,\\\\\\\\\\\\\\\"pageData\\\\\\\\\\\\\\\":[{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"4\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"username\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"Nanjustar\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"ipAddress\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"127.0.0.1\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"ipSource\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"brower\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"Firefox 11\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"os\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"Windows 10\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"state\\\\\\\\\\\\\\\":1,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1685728915000,\\\\\\\\\\\\\\\"wechat\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"登陆成功！\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"mobile\\\\\\\\\\\\\\\":null},{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"3\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"username\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"Nanjustar\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"ipAddress\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"127.0.0.1\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"ipSource\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"brower\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"Chrome 11\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"os\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"Windows 10\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"state\\\\\\\\\\\\\\\":1,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1684771742000,\\\\\\\\\\\\\\\"wechat\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"登陆成功！\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"mobile\\\\\\\\\\\\\\\":null},{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"2\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"username\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"admin\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"ipAddress\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"127.0.0.1\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"ipSource\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"brower\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"Firefox 11\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"os\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"Windows 10\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"state\\\\\\\\\\\\\\\":1,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1684755937000,\\\\\\\\\\\\\\\"wechat\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"登陆成功！\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"mobile\\\\\\\\\\\\\\\":null},{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"1\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"username\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"Nanjustar\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"ipAddress\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"127.0.0.1\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"ipSource\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"brower\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"Firefox 11\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"os\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"Windows 10\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"state\\\\\\\\\\\\\\\":1,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1684755835000,\\\\\\\\\\\\\\\"wechat\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"登陆成功！\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"mobile\\\\\\\\\\\\\\\":null}]}}\\\\\\\",\\\\\\\"optTime\\\\\\\":1689415117000},{\\\\\\\"id\\\\\\\":\\\\\\\"1680154728322904066\\\\\\\",\\\\\\\"title\\\\\\\":\\\\\\\"系统微服务: 操作日志管理\\\\\\\",\\\\\\\"businessType\\\\\\\":1,\\\\\\\"methodName\\\\\\\":\\\\\\\"分页条件查询\\\\\\\",\\\\\\\"methodUrl\\\\\\\":\\\\\\\"com.park.systemserve.controller.OptLogController.queryPageOptLog\\\\\\\",\\\\\\\"reqType\\\\\\\":\\\\\\\"POST\\\\\\\",\\\\\\\"optName\\\\\\\":\\\\\\\"system\\\\\\\",\\\\\\\"optUrl\\\\\\\":\\\\\\\"/system/opt/page/1/5\\\\\\\",\\\\\\\"optIp\\\\\\\":\\\\\\\"0:0:0:0:0:0:0:1\\\\\\\",\\\\\\\"optAddress\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"reqParam\\\\\\\":\\\\\\\"[1,5,{\\\\\\\\\\\\\\\"startTime\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"endTime\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"roadId\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"0\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"inspectorName\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\"}]\\\\\\\",\\\\\\\"respData\\\\\\\":\\\\\\\"{\\\\\\\\\\\\\\\"code\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"200\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"message\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"请求成功\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"data\\\\\\\\\\\\\\\":{\\\\\\\\\\\\\\\"pageNum\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"pageSize\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"total\\\\\\\\\\\\\\\":1,\\\\\\\\\\\\\\\"pageData\\\\\\\\\\\\\\\":[{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"1679788350122242049\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"title\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"系统微服务: 营收统计\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"businessType\\\\\\\\\\\\\\\":1,\\\\\\\\\\\\\\\"methodName\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"分页条件查询\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"methodUrl\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"com.park.systemserve.controller.RevenueCountController.queryPageRevenueCount\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"reqType\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"POST\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"optName\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"optUrl\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"/system/revenue/page/1/5\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"optIp\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"0:0:0:0:0:0:0:1\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"optAddress\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"reqParam\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"[1,5,{\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"startTime\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"endTime\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"roadId\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"inspectorName\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"}]\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"respData\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"{\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"code\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"200\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"message\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"请求成功\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"data\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":{\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"pageNum\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"pageSize\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"total\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":20,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"pageData\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":[{\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"2\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":1687501577000,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":764,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":575.72,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":279,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":747.01,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":616,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":1451166333000},{\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"19\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":1686653816000,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":44,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":970.21,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":51,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":27.73,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":28,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":1334867965000},{\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"9\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":1585404486000,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":24,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":679.75,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":916,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":183.03,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":142,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":1335276831000},{\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"10\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":1574730532000,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":456,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":988.84,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":600,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":414.76,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":381,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":1328030954000},{\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"4\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":1545864704000,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":770,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":4.43,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":379,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":576.06,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":1,\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\":1582136419000}]}}\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"optTime\\\\\\\\\\\\\\\":1689327726000}]}}\\\\\\\",\\\\\\\"optTime\\\\\\\":1689415078000},{\\\\\\\"id\\\\\\\":\\\\\\\"1679788350122242049\\\\\\\",\\\\\\\"title\\\\\\\":\\\\\\\"系统微服务: 营收统计\\\\\\\",\\\\\\\"businessType\\\\\\\":1,\\\\\\\"methodName\\\\\\\":\\\\\\\"分页条件查询\\\\\\\",\\\\\\\"methodUrl\\\\\\\":\\\\\\\"com.park.systemserve.controller.RevenueCountController.queryPageRevenueCount\\\\\\\",\\\\\\\"reqType\\\\\\\":\\\\\\\"POST\\\\\\\",\\\\\\\"optName\\\\\\\":null,\\\\\\\"optUrl\\\\\\\":\\\\\\\"/system/revenue/page/1/5\\\\\\\",\\\\\\\"optIp\\\\\\\":\\\\\\\"0:0:0:0:0:0:0:1\\\\\\\",\\\\\\\"optAddress\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"reqParam\\\\\\\":\\\\\\\"[1,5,{\\\\\\\\\\\\\\\"startTime\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"endTime\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"roadId\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"inspectorName\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\"}]\\\\\\\",\\\\\\\"respData\\\\\\\":\\\\\\\"{\\\\\\\\\\\\\\\"code\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"200\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"message\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"请求成功\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"data\\\\\\\\\\\\\\\":{\\\\\\\\\\\\\\\"pageNum\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"pageSize\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"total\\\\\\\\\\\\\\\":20,\\\\\\\\\\\\\\\"pageData\\\\\\\\\\\\\\\":[{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"2\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\":1687501577000,\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\":764,\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\":575.72,\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\":279,\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\":747.01,\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\":616,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1451166333000},{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"19\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\":1686653816000,\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\":44,\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\":970.21,\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\":51,\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\":27.73,\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\":28,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1334867965000},{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"9\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\":1585404486000,\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\":24,\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\":679.75,\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\":916,\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\":183.03,\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\":142,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1335276831000},{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"10\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\":1574730532000,\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\":456,\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\":988.84,\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\":600,\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\":414.76,\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\":381,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1328030954000},{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"4\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\":1545864704000,\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\":770,\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\":4.43,\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\":379,\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\":576.06,\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\":1,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1582136419000}]}}\\\\\\\",\\\\\\\"optTime\\\\\\\":1689327726000}]}}\\\",\\\"optTime\\\":1689415184000},{\\\"id\\\":\\\"1680154894870327297\\\",\\\"title\\\":\\\"系统微服务: 登录日志管理\\\",\\\"businessType\\\":1,\\\"methodName\\\":\\\"分页条件查询\\\",\\\"methodUrl\\\":\\\"com.park.systemserve.controller.LoginLogController.queryPageLoginLog\\\",\\\"reqType\\\":\\\"POST\\\",\\\"optName\\\":\\\"system\\\",\\\"optUrl\\\":\\\"/system/login/Log/page/1/5\\\",\\\"optIp\\\":\\\"0:0:0:0:0:0:0:1\\\",\\\"optAddress\\\":\\\"\\\",\\\"reqParam\\\":\\\"[1,5,{\\\\\\\"startTime\\\\\\\":null,\\\\\\\"endTime\\\\\\\":null,\\\\\\\"roadId\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"inspectorName\\\\\\\":\\\\\\\"\\\\\\\"}]\\\",\\\"respData\\\":\\\"{\\\\\\\"code\\\\\\\":\\\\\\\"200\\\\\\\",\\\\\\\"message\\\\\\\":\\\\\\\"请求成功\\\\\\\",\\\\\\\"data\\\\\\\":{\\\\\\\"pageNum\\\\\\\":null,\\\\\\\"pageSize\\\\\\\":null,\\\\\\\"total\\\\\\\":4,\\\\\\\"pageData\\\\\\\":[{\\\\\\\"id\\\\\\\":\\\\\\\"4\\\\\\\",\\\\\\\"username\\\\\\\":\\\\\\\"Nanjustar\\\\\\\",\\\\\\\"ipAddress\\\\\\\":\\\\\\\"127.0.0.1\\\\\\\",\\\\\\\"ipSource\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"brower\\\\\\\":\\\\\\\"Firefox 11\\\\\\\",\\\\\\\"os\\\\\\\":\\\\\\\"Windows 10\\\\\\\",\\\\\\\"state\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1685728915000,\\\\\\\"wechat\\\\\\\":\\\\\\\"登陆成功！\\\\\\\",\\\\\\\"mobile\\\\\\\":null},{\\\\\\\"id\\\\\\\":\\\\\\\"3\\\\\\\",\\\\\\\"username\\\\\\\":\\\\\\\"Nanjustar\\\\\\\",\\\\\\\"ipAddress\\\\\\\":\\\\\\\"127.0.0.1\\\\\\\",\\\\\\\"ipSource\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"brower\\\\\\\":\\\\\\\"Chrome 11\\\\\\\",\\\\\\\"os\\\\\\\":\\\\\\\"Windows 10\\\\\\\",\\\\\\\"state\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1684771742000,\\\\\\\"wechat\\\\\\\":\\\\\\\"登陆成功！\\\\\\\",\\\\\\\"mobile\\\\\\\":null},{\\\\\\\"id\\\\\\\":\\\\\\\"2\\\\\\\",\\\\\\\"username\\\\\\\":\\\\\\\"admin\\\\\\\",\\\\\\\"ipAddress\\\\\\\":\\\\\\\"127.0.0.1\\\\\\\",\\\\\\\"ipSource\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"brower\\\\\\\":\\\\\\\"Firefox 11\\\\\\\",\\\\\\\"os\\\\\\\":\\\\\\\"Windows 10\\\\\\\",\\\\\\\"state\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1684755937000,\\\\\\\"wechat\\\\\\\":\\\\\\\"登陆成功！\\\\\\\",\\\\\\\"mobile\\\\\\\":null},{\\\\\\\"id\\\\\\\":\\\\\\\"1\\\\\\\",\\\\\\\"username\\\\\\\":\\\\\\\"Nanjustar\\\\\\\",\\\\\\\"ipAddress\\\\\\\":\\\\\\\"127.0.0.1\\\\\\\",\\\\\\\"ipSource\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"brower\\\\\\\":\\\\\\\"Firefox 11\\\\\\\",\\\\\\\"os\\\\\\\":\\\\\\\"Windows 10\\\\\\\",\\\\\\\"state\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1684755835000,\\\\\\\"wechat\\\\\\\":\\\\\\\"登陆成功！\\\\\\\",\\\\\\\"mobile\\\\\\\":null}]}}\\\",\\\"optTime\\\":1689415117000},{\\\"id\\\":\\\"1680154728322904066\\\",\\\"title\\\":\\\"系统微服务: 操作日志管理\\\",\\\"businessType\\\":1,\\\"methodName\\\":\\\"分页条件查询\\\",\\\"methodUrl\\\":\\\"com.park.systemserve.controller.OptLogController.queryPageOptLog\\\",\\\"reqType\\\":\\\"POST\\\",\\\"optName\\\":\\\"system\\\",\\\"optUrl\\\":\\\"/system/opt/page/1/5\\\",\\\"optIp\\\":\\\"0:0:0:0:0:0:0:1\\\",\\\"optAddress\\\":\\\"\\\",\\\"reqParam\\\":\\\"[1,5,{\\\\\\\"startTime\\\\\\\":null,\\\\\\\"endTime\\\\\\\":null,\\\\\\\"roadId\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"inspectorName\\\\\\\":\\\\\\\"\\\\\\\"}]\\\",\\\"respData\\\":\\\"{\\\\\\\"code\\\\\\\":\\\\\\\"200\\\\\\\",\\\\\\\"message\\\\\\\":\\\\\\\"请求成功\\\\\\\",\\\\\\\"data\\\\\\\":{\\\\\\\"pageNum\\\\\\\":null,\\\\\\\"pageSize\\\\\\\":null,\\\\\\\"total\\\\\\\":1,\\\\\\\"pageData\\\\\\\":[{\\\\\\\"id\\\\\\\":\\\\\\\"1679788350122242049\\\\\\\",\\\\\\\"title\\\\\\\":\\\\\\\"系统微服务: 营收统计\\\\\\\",\\\\\\\"businessType\\\\\\\":1,\\\\\\\"methodName\\\\\\\":\\\\\\\"分页条件查询\\\\\\\",\\\\\\\"methodUrl\\\\\\\":\\\\\\\"com.park.systemserve.controller.RevenueCountController.queryPageRevenueCount\\\\\\\",\\\\\\\"reqType\\\\\\\":\\\\\\\"POST\\\\\\\",\\\\\\\"optName\\\\\\\":null,\\\\\\\"optUrl\\\\\\\":\\\\\\\"/system/revenue/page/1/5\\\\\\\",\\\\\\\"optIp\\\\\\\":\\\\\\\"0:0:0:0:0:0:0:1\\\\\\\",\\\\\\\"optAddress\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"reqParam\\\\\\\":\\\\\\\"[1,5,{\\\\\\\\\\\\\\\"startTime\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"endTime\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"roadId\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"inspectorName\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\"}]\\\\\\\",\\\\\\\"respData\\\\\\\":\\\\\\\"{\\\\\\\\\\\\\\\"code\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"200\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"message\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"请求成功\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"data\\\\\\\\\\\\\\\":{\\\\\\\\\\\\\\\"pageNum\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"pageSize\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"total\\\\\\\\\\\\\\\":20,\\\\\\\\\\\\\\\"pageData\\\\\\\\\\\\\\\":[{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"2\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\":1687501577000,\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\":764,\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\":575.72,\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\":279,\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\":747.01,\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\":616,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1451166333000},{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"19\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\":1686653816000,\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\":44,\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\":970.21,\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\":51,\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\":27.73,\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\":28,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1334867965000},{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"9\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\":1585404486000,\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\":24,\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\":679.75,\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\":916,\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\":183.03,\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\":142,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1335276831000},{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"10\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\":1574730532000,\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\":456,\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\":988.84,\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\":600,\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\":414.76,\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\":381,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1328030954000},{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"4\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\":1545864704000,\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\":770,\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\":4.43,\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\":379,\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\":576.06,\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\":1,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1582136419000}]}}\\\\\\\",\\\\\\\"optTime\\\\\\\":1689327726000}]}}\\\",\\\"optTime\\\":1689415078000},{\\\"id\\\":\\\"1679788350122242049\\\",\\\"title\\\":\\\"系统微服务: 营收统计\\\",\\\"businessType\\\":1,\\\"methodName\\\":\\\"分页条件查询\\\",\\\"methodUrl\\\":\\\"com.park.systemserve.controller.RevenueCountController.queryPageRevenueCount\\\",\\\"reqType\\\":\\\"POST\\\",\\\"optName\\\":null,\\\"optUrl\\\":\\\"/system/revenue/page/1/5\\\",\\\"optIp\\\":\\\"0:0:0:0:0:0:0:1\\\",\\\"optAddress\\\":\\\"\\\",\\\"reqParam\\\":\\\"[1,5,{\\\\\\\"startTime\\\\\\\":null,\\\\\\\"endTime\\\\\\\":null,\\\\\\\"roadId\\\\\\\":null,\\\\\\\"inspectorName\\\\\\\":\\\\\\\"\\\\\\\"}]\\\",\\\"respData\\\":\\\"{\\\\\\\"code\\\\\\\":\\\\\\\"200\\\\\\\",\\\\\\\"message\\\\\\\":\\\\\\\"请求成功\\\\\\\",\\\\\\\"data\\\\\\\":{\\\\\\\"pageNum\\\\\\\":null,\\\\\\\"pageSize\\\\\\\":null,\\\\\\\"total\\\\\\\":20,\\\\\\\"pageData\\\\\\\":[{\\\\\\\"id\\\\\\\":\\\\\\\"2\\\\\\\",\\\\\\\"countTime\\\\\\\":1687501577000,\\\\\\\"orderCount\\\\\\\":764,\\\\\\\"revenue\\\\\\\":575.72,\\\\\\\"refundOrder\\\\\\\":279,\\\\\\\"refundAmount\\\\\\\":747.01,\\\\\\\"newUser\\\\\\\":616,\\\\\\\"createTime\\\\\\\":1451166333000},{\\\\\\\"id\\\\\\\":\\\\\\\"19\\\\\\\",\\\\\\\"countTime\\\\\\\":1686653816000,\\\\\\\"orderCount\\\\\\\":44,\\\\\\\"revenue\\\\\\\":970.21,\\\\\\\"refundOrder\\\\\\\":51,\\\\\\\"refundAmount\\\\\\\":27.73,\\\\\\\"newUser\\\\\\\":28,\\\\\\\"createTime\\\\\\\":1334867965000},{\\\\\\\"id\\\\\\\":\\\\\\\"9\\\\\\\",\\\\\\\"countTime\\\\\\\":1585404486000,\\\\\\\"orderCount\\\\\\\":24,\\\\\\\"revenue\\\\\\\":679.75,\\\\\\\"refundOrder\\\\\\\":916,\\\\\\\"refundAmount\\\\\\\":183.03,\\\\\\\"newUser\\\\\\\":142,\\\\\\\"createTime\\\\\\\":1335276831000},{\\\\\\\"id\\\\\\\":\\\\\\\"10\\\\\\\",\\\\\\\"countTime\\\\\\\":1574730532000,\\\\\\\"orderCount\\\\\\\":456,\\\\\\\"revenue\\\\\\\":988.84,\\\\\\\"refundOrder\\\\\\\":600,\\\\\\\"refundAmount\\\\\\\":414.76,\\\\\\\"newUser\\\\\\\":381,\\\\\\\"createTime\\\\\\\":1328030954000},{\\\\\\\"id\\\\\\\":\\\\\\\"4\\\\\\\",\\\\\\\"countTime\\\\\\\":1545864704000,\\\\\\\"orderCount\\\\\\\":770,\\\\\\\"revenue\\\\\\\":4.43,\\\\\\\"refundOrder\\\\\\\":379,\\\\\\\"refundAmount\\\\\\\":576.06,\\\\\\\"newUser\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1582136419000}]}}\\\",\\\"optTime\\\":1689327726000}]}}\",\"optTime\":1689646667000},{\"id\":\"1681126009008599042\",\"title\":\"系统微服务: 登录日志管理\",\"businessType\":1,\"methodName\":\"分页条件查询\",\"methodUrl\":\"com.park.systemserve.controller.LoginLogController.queryPageLoginLog\",\"reqType\":\"POST\",\"optName\":\"system\",\"optUrl\":\"/system/login/Log/page/1/-1\",\"optIp\":\"192.168.3.22\",\"optAddress\":\"\",\"reqParam\":\"[1,-1,{\\\"startTime\\\":null,\\\"endTime\\\":null,\\\"roadId\\\":\\\"0\\\",\\\"inspectorName\\\":\\\"\\\"}]\",\"respData\":\"{\\\"code\\\":\\\"200\\\",\\\"message\\\":\\\"请求成功\\\",\\\"data\\\":{\\\"pageNum\\\":null,\\\"pageSize\\\":null,\\\"total\\\":0,\\\"pageData\\\":[{\\\"id\\\":\\\"4\\\",\\\"username\\\":\\\"Nanjustar\\\",\\\"ipAddress\\\":\\\"127.0.0.1\\\",\\\"ipSource\\\":\\\"\\\",\\\"brower\\\":\\\"Firefox 11\\\",\\\"os\\\":\\\"Windows 10\\\",\\\"state\\\":1,\\\"createTime\\\":1685728915000,\\\"wechat\\\":\\\"登陆成功！\\\",\\\"mobile\\\":null},{\\\"id\\\":\\\"3\\\",\\\"username\\\":\\\"Nanjustar\\\",\\\"ipAddress\\\":\\\"127.0.0.1\\\",\\\"ipSource\\\":\\\"\\\",\\\"brower\\\":\\\"Chrome 11\\\",\\\"os\\\":\\\"Windows 10\\\",\\\"state\\\":1,\\\"createTime\\\":1684771742000,\\\"wechat\\\":\\\"登陆成功！\\\",\\\"mobile\\\":null},{\\\"id\\\":\\\"2\\\",\\\"username\\\":\\\"admin\\\",\\\"ipAddress\\\":\\\"127.0.0.1\\\",\\\"ipSource\\\":\\\"\\\",\\\"brower\\\":\\\"Firefox 11\\\",\\\"os\\\":\\\"Windows 10\\\",\\\"state\\\":1,\\\"createTime\\\":1684755937000,\\\"wechat\\\":\\\"登陆成功！\\\",\\\"mobile\\\":null},{\\\"id\\\":\\\"1\\\",\\\"username\\\":\\\"Nanjustar\\\",\\\"ipAddress\\\":\\\"127.0.0.1\\\",\\\"ipSource\\\":\\\"\\\",\\\"brower\\\":\\\"Firefox 11\\\",\\\"os\\\":\\\"Windows 10\\\",\\\"state\\\":1,\\\"createTime\\\":1684755835000,\\\"wechat\\\":\\\"登陆成功！\\\",\\\"mobile\\\":null}]}}\",\"optTime\":1689646649000},{\"id\":\"1681125894395047938\",\"title\":\"系统微服务: 支付统计\",\"businessType\":1,\"methodName\":\"分页条件查询\",\"methodUrl\":\"com.park.systemserve.controller.PayCountController.queryPagePayCount\",\"reqType\":\"POST\",\"optName\":\"system\",\"optUrl\":\"/system/pay/page/1/-1\",\"optIp\":\"192.168.3.22\",\"optAddress\":\"\",\"reqParam\":\"[1,-1,{\\\"startTime\\\":null,\\\"endTime\\\":null,\\\"roadId\\\":\\\"0\\\",\\\"inspectorName\\\":\\\"\\\"}]\",\"respData\":\"{\\\"code\\\":\\\"200\\\",\\\"message\\\":\\\"请求成功\\\",\\\"data\\\":{\\\"pageNum\\\":null,\\\"pageSize\\\":null,\\\"total\\\":0,\\\"pageData\\\":[{\\\"id\\\":\\\"14\\\",\\\"countTime\\\":1594685542000,\\\"activePay\\\":686,\\\"cashPay\\\":507,\\\"codePay\\\":767,\\\"wechatPayAmount\\\":0.38,\\\"alipayPayAmount\\\":186.27,\\\"cashPayAmount\\\":561.84,\\\"unionPayAmount\\\":104.32,\\\"createTime\\\":1661174376000},{\\\"id\\\":\\\"8\\\",\\\"countTime\\\":1564225797000,\\\"activePay\\\":472,\\\"cashPay\\\":746,\\\"codePay\\\":796,\\\"wechatPayAmount\\\":195.45,\\\"alipayPayAmount\\\":507.62,\\\"cashPayAmount\\\":868.35,\\\"unionPayAmount\\\":722.12,\\\"createTime\\\":1313245319000},{\\\"id\\\":\\\"15\\\",\\\"countTime\\\":1557460914000,\\\"activePay\\\":940,\\\"cashPay\\\":389,\\\"codePay\\\":672,\\\"wechatPayAmount\\\":937.34,\\\"alipayPayAmount\\\":448.54,\\\"cashPayAmount\\\":694.97,\\\"unionPayAmount\\\":180.61,\\\"createTime\\\":1363446388000},{\\\"id\\\":\\\"3\\\",\\\"countTime\\\":1510865003000,\\\"activePay\\\":249,\\\"cashPay\\\":247,\\\"codePay\\\":742,\\\"wechatPayAmount\\\":558.17,\\\"alipayPayAmount\\\":788.87,\\\"cashPayAmount\\\":784.12,\\\"unionPayAmount\\\":143.04,\\\"createTime\\\":1441321976000},{\\\"id\\\":\\\"18\\\",\\\"countTime\\\":1395945444000,\\\"activePay\\\":604,\\\"cashPay\\\":573,\\\"codePay\\\":741,\\\"wechatPayAmount\\\":463.26,\\\"alipayPayAmount\\\":144.26,\\\"cashPayAmount\\\":970.50,\\\"unionPayAmount\\\":582.33,\\\"createTime\\\":1044809216000},{\\\"id\\\":\\\"11\\\",\\\"countTime\\\":1370187195000,\\\"activePay\\\":286,\\\"cashPay\\\":524,\\\"codePay\\\":113,\\\"wechatPayAmount\\\":524.84,\\\"alipayPayAmount\\\":404.41,\\\"cashPayAmount\\\":446.39,\\\"unionPayAmount\\\":716.95,\\\"createTime\\\":1359875756000},{\\\"id\\\":\\\"5\\\",\\\"countTime\\\":1363857856000,\\\"activePay\\\":788,\\\"cashPay\\\":601,\\\"codePay\\\":51,\\\"wechatPayAmount\\\":704.90,\\\"alipayPayAmount\\\":305.36,\\\"cashPayAmount\\\":85.42,\\\"unionPayAmount\\\":595.39,\\\"createTime\\\":1626042931000},{\\\"id\\\":\\\"16\\\",\\\"countTime\\\":1272108472000,\\\"activePay\\\":873,\\\"cashPay\\\":983,\\\"codePay\\\":827,\\\"wechatPayAmount\\\":446.75,\\\"alipayPayAmount\\\":767.99,\\\"cashPayAmount\\\":273.55,\\\"unionPayAmount\\\":3.28,\\\"createTime\\\":1678233266000},{\\\"id\\\":\\\"20\\\",\\\"countTime\\\":1263565824000,\\\"activePay\\\":65,\\\"cashPay\\\":264,\\\"codePay\\\":34,\\\"wechatPayAmount\\\":542.39,\\\"alipayPayAmount\\\":155.58,\\\"cashPayAmount\\\":14.13,\\\"unionPayAmount\\\":426.87,\\\"createTime\\\":969387238000},{\\\"id\\\":\\\"17\\\",\\\"countTime\\\":1230195510000,\\\"activePay\\\":147,\\\"cashPay\\\":771,\\\"codePay\\\":242,\\\"wechatPayAmount\\\":799.52,\\\"alipayPayAmount\\\":95.22,\\\"cashPayAmount\\\":965.60,\\\"unionPayAmount\\\":94.22,\\\"createTime\\\":1234449135000},{\\\"id\\\":\\\"9\\\",\\\"countTime\\\":1205744341000,\\\"activePay\\\":768,\\\"cashPay\\\":644,\\\"codePay\\\":82,\\\"wechatPayAmount\\\":301.21,\\\"alipayPayAmount\\\":181.47,\\\"cashPayAmount\\\":150.05,\\\"unionPayAmount\\\":821.52,\\\"createTime\\\":1644243684000},{\\\"id\\\":\\\"10\\\",\\\"countTime\\\":1159885910000,\\\"activePay\\\":290,\\\"cashPay\\\":228,\\\"codePay\\\":724,\\\"wechatPayAmount\\\":226.54,\\\"alipayPayAmount\\\":908.78,\\\"cashPayAmount\\\":937.47,\\\"unionPayAmount\\\":482.54,\\\"createTime\\\":1604895828000},{\\\"id\\\":\\\"1\\\",\\\"countTime\\\":1154907944000,\\\"activePay\\\":315,\\\"cashPay\\\":748,\\\"codePay\\\":785,\\\"wechatPayAmount\\\":872.73,\\\"alipayPayAmount\\\":895.74,\\\"cashPayAmount\\\":892.92,\\\"unionPayAmount\\\":890.26,\\\"createTime\\\":1238838193000},{\\\"id\\\":\\\"19\\\",\\\"countTime\\\":1133141557000,\\\"activePay\\\":28,\\\"cashPay\\\":808,\\\"codePay\\\":806,\\\"wechatPayAmount\\\":492.43,\\\"alipayPayAmount\\\":858.11,\\\"cashPayAmount\\\":647.66,\\\"unionPayAmount\\\":753.97,\\\"createTime\\\":1554240838000},{\\\"id\\\":\\\"4\\\",\\\"countTime\\\":1092564092000,\\\"activePay\\\":672,\\\"cashPay\\\":769,\\\"codePay\\\":791,\\\"wechatPayAmount\\\":668.58,\\\"alipayPayAmount\\\":442.09,\\\"cashPayAmount\\\":748.81,\\\"unionPayAmount\\\":986.86,\\\"createTime\\\":1343254721000},{\\\"id\\\":\\\"7\\\",\\\"countTime\\\":1092230652000,\\\"activePay\\\":643,\\\"cashPay\\\":279,\\\"codePay\\\":123,\\\"wechatPayAmount\\\":101.06,\\\"alipayPayAmount\\\":838.53,\\\"cashPayAmount\\\":13.48,\\\"unionPayAmount\\\":535.94,\\\"createTime\\\":1502764090000},{\\\"id\\\":\\\"13\\\",\\\"countTime\\\":1085801087000,\\\"activePay\\\":589,\\\"cashPay\\\":249,\\\"codePay\\\":786,\\\"wechatPayAmount\\\":515.25,\\\"alipayPayAmount\\\":139.34,\\\"cashPayAmount\\\":507.41,\\\"unionPayAmount\\\":194.27,\\\"createTime\\\":1196030446000},{\\\"id\\\":\\\"2\\\",\\\"countTime\\\":1071539181000,\\\"activePay\\\":176,\\\"cashPay\\\":506,\\\"codePay\\\":352,\\\"wechatPayAmount\\\":207.05,\\\"alipayPayAmount\\\":299.24,\\\"cashPayAmount\\\":205.12,\\\"unionPayAmount\\\":659.65,\\\"createTime\\\":1093436502000},{\\\"id\\\":\\\"6\\\",\\\"countTime\\\":1038532226000,\\\"activePay\\\":499,\\\"cashPay\\\":112,\\\"codePay\\\":99,\\\"wechatPayAmount\\\":442.61,\\\"alipayPayAmount\\\":949.96,\\\"cashPayAmount\\\":371.21,\\\"unionPayAmount\\\":981.57,\\\"createTime\\\":1227355659000},{\\\"id\\\":\\\"12\\\",\\\"countTime\\\":999039450000,\\\"activePay\\\":996,\\\"cashPay\\\":600,\\\"codePay\\\":833,\\\"wechatPayAmount\\\":933.18,\\\"alipayPayAmount\\\":816.24,\\\"cashPayAmount\\\":113.54,\\\"unionPayAmount\\\":984.73,\\\"createTime\\\":1649700807000}]}}\",\"optTime\":1689646622000},{\"id\":\"1680155173313392641\",\"title\":\"系统微服务: 操作日志管理\",\"businessType\":1,\"methodName\":\"分页条件查询\",\"methodUrl\":\"com.park.systemserve.controller.OptLogController.queryPageOptLog\",\"reqType\":\"POST\",\"optName\":\"system\",\"optUrl\":\"/system/opt/page/1/5\",\"optIp\":\"0:0:0:0:0:0:0:1\",\"optAddress\":\"\",\"reqParam\":\"[1,5,{\\\"startTime\\\":null,\\\"endTime\\\":null,\\\"roadId\\\":\\\"0\\\",\\\"inspectorName\\\":\\\"\\\"}]\",\"respData\":\"{\\\"code\\\":\\\"200\\\",\\\"message\\\":\\\"请求成功\\\",\\\"data\\\":{\\\"pageNum\\\":null,\\\"pageSize\\\":null,\\\"total\\\":3,\\\"pageData\\\":[{\\\"id\\\":\\\"1680154894870327297\\\",\\\"title\\\":\\\"系统微服务: 登录日志管理\\\",\\\"businessType\\\":1,\\\"methodName\\\":\\\"分页条件查询\\\",\\\"methodUrl\\\":\\\"com.park.systemserve.controller.LoginLogController.queryPageLoginLog\\\",\\\"reqType\\\":\\\"POST\\\",\\\"optName\\\":\\\"system\\\",\\\"optUrl\\\":\\\"/system/login/Log/page/1/5\\\",\\\"optIp\\\":\\\"0:0:0:0:0:0:0:1\\\",\\\"optAddress\\\":\\\"\\\",\\\"reqParam\\\":\\\"[1,5,{\\\\\\\"startTime\\\\\\\":null,\\\\\\\"endTime\\\\\\\":null,\\\\\\\"roadId\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"inspectorName\\\\\\\":\\\\\\\"\\\\\\\"}]\\\",\\\"respData\\\":\\\"{\\\\\\\"code\\\\\\\":\\\\\\\"200\\\\\\\",\\\\\\\"message\\\\\\\":\\\\\\\"请求成功\\\\\\\",\\\\\\\"data\\\\\\\":{\\\\\\\"pageNum\\\\\\\":null,\\\\\\\"pageSize\\\\\\\":null,\\\\\\\"total\\\\\\\":4,\\\\\\\"pageData\\\\\\\":[{\\\\\\\"id\\\\\\\":\\\\\\\"4\\\\\\\",\\\\\\\"username\\\\\\\":\\\\\\\"Nanjustar\\\\\\\",\\\\\\\"ipAddress\\\\\\\":\\\\\\\"127.0.0.1\\\\\\\",\\\\\\\"ipSource\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"brower\\\\\\\":\\\\\\\"Firefox 11\\\\\\\",\\\\\\\"os\\\\\\\":\\\\\\\"Windows 10\\\\\\\",\\\\\\\"state\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1685728915000,\\\\\\\"wechat\\\\\\\":\\\\\\\"登陆成功！\\\\\\\",\\\\\\\"mobile\\\\\\\":null},{\\\\\\\"id\\\\\\\":\\\\\\\"3\\\\\\\",\\\\\\\"username\\\\\\\":\\\\\\\"Nanjustar\\\\\\\",\\\\\\\"ipAddress\\\\\\\":\\\\\\\"127.0.0.1\\\\\\\",\\\\\\\"ipSource\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"brower\\\\\\\":\\\\\\\"Chrome 11\\\\\\\",\\\\\\\"os\\\\\\\":\\\\\\\"Windows 10\\\\\\\",\\\\\\\"state\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1684771742000,\\\\\\\"wechat\\\\\\\":\\\\\\\"登陆成功！\\\\\\\",\\\\\\\"mobile\\\\\\\":null},{\\\\\\\"id\\\\\\\":\\\\\\\"2\\\\\\\",\\\\\\\"username\\\\\\\":\\\\\\\"admin\\\\\\\",\\\\\\\"ipAddress\\\\\\\":\\\\\\\"127.0.0.1\\\\\\\",\\\\\\\"ipSource\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"brower\\\\\\\":\\\\\\\"Firefox 11\\\\\\\",\\\\\\\"os\\\\\\\":\\\\\\\"Windows 10\\\\\\\",\\\\\\\"state\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1684755937000,\\\\\\\"wechat\\\\\\\":\\\\\\\"登陆成功！\\\\\\\",\\\\\\\"mobile\\\\\\\":null},{\\\\\\\"id\\\\\\\":\\\\\\\"1\\\\\\\",\\\\\\\"username\\\\\\\":\\\\\\\"Nanjustar\\\\\\\",\\\\\\\"ipAddress\\\\\\\":\\\\\\\"127.0.0.1\\\\\\\",\\\\\\\"ipSource\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"brower\\\\\\\":\\\\\\\"Firefox 11\\\\\\\",\\\\\\\"os\\\\\\\":\\\\\\\"Windows 10\\\\\\\",\\\\\\\"state\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1684755835000,\\\\\\\"wechat\\\\\\\":\\\\\\\"登陆成功！\\\\\\\",\\\\\\\"mobile\\\\\\\":null}]}}\\\",\\\"optTime\\\":1689415117000},{\\\"id\\\":\\\"1680154728322904066\\\",\\\"title\\\":\\\"系统微服务: 操作日志管理\\\",\\\"businessType\\\":1,\\\"methodName\\\":\\\"分页条件查询\\\",\\\"methodUrl\\\":\\\"com.park.systemserve.controller.OptLogController.queryPageOptLog\\\",\\\"reqType\\\":\\\"POST\\\",\\\"optName\\\":\\\"system\\\",\\\"optUrl\\\":\\\"/system/opt/page/1/5\\\",\\\"optIp\\\":\\\"0:0:0:0:0:0:0:1\\\",\\\"optAddress\\\":\\\"\\\",\\\"reqParam\\\":\\\"[1,5,{\\\\\\\"startTime\\\\\\\":null,\\\\\\\"endTime\\\\\\\":null,\\\\\\\"roadId\\\\\\\":\\\\\\\"0\\\\\\\",\\\\\\\"inspectorName\\\\\\\":\\\\\\\"\\\\\\\"}]\\\",\\\"respData\\\":\\\"{\\\\\\\"code\\\\\\\":\\\\\\\"200\\\\\\\",\\\\\\\"message\\\\\\\":\\\\\\\"请求成功\\\\\\\",\\\\\\\"data\\\\\\\":{\\\\\\\"pageNum\\\\\\\":null,\\\\\\\"pageSize\\\\\\\":null,\\\\\\\"total\\\\\\\":1,\\\\\\\"pageData\\\\\\\":[{\\\\\\\"id\\\\\\\":\\\\\\\"1679788350122242049\\\\\\\",\\\\\\\"title\\\\\\\":\\\\\\\"系统微服务: 营收统计\\\\\\\",\\\\\\\"businessType\\\\\\\":1,\\\\\\\"methodName\\\\\\\":\\\\\\\"分页条件查询\\\\\\\",\\\\\\\"methodUrl\\\\\\\":\\\\\\\"com.park.systemserve.controller.RevenueCountController.queryPageRevenueCount\\\\\\\",\\\\\\\"reqType\\\\\\\":\\\\\\\"POST\\\\\\\",\\\\\\\"optName\\\\\\\":null,\\\\\\\"optUrl\\\\\\\":\\\\\\\"/system/revenue/page/1/5\\\\\\\",\\\\\\\"optIp\\\\\\\":\\\\\\\"0:0:0:0:0:0:0:1\\\\\\\",\\\\\\\"optAddress\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"reqParam\\\\\\\":\\\\\\\"[1,5,{\\\\\\\\\\\\\\\"startTime\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"endTime\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"roadId\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"inspectorName\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"\\\\\\\\\\\\\\\"}]\\\\\\\",\\\\\\\"respData\\\\\\\":\\\\\\\"{\\\\\\\\\\\\\\\"code\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"200\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"message\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"请求成功\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"data\\\\\\\\\\\\\\\":{\\\\\\\\\\\\\\\"pageNum\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"pageSize\\\\\\\\\\\\\\\":null,\\\\\\\\\\\\\\\"total\\\\\\\\\\\\\\\":20,\\\\\\\\\\\\\\\"pageData\\\\\\\\\\\\\\\":[{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"2\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\":1687501577000,\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\":764,\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\":575.72,\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\":279,\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\":747.01,\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\":616,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1451166333000},{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"19\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\":1686653816000,\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\":44,\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\":970.21,\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\":51,\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\":27.73,\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\":28,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1334867965000},{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"9\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\":1585404486000,\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\":24,\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\":679.75,\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\":916,\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\":183.03,\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\":142,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1335276831000},{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"10\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\":1574730532000,\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\":456,\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\":988.84,\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\":600,\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\":414.76,\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\":381,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1328030954000},{\\\\\\\\\\\\\\\"id\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"4\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"countTime\\\\\\\\\\\\\\\":1545864704000,\\\\\\\\\\\\\\\"orderCount\\\\\\\\\\\\\\\":770,\\\\\\\\\\\\\\\"revenue\\\\\\\\\\\\\\\":4.43,\\\\\\\\\\\\\\\"refundOrder\\\\\\\\\\\\\\\":379,\\\\\\\\\\\\\\\"refundAmount\\\\\\\\\\\\\\\":576.06,\\\\\\\\\\\\\\\"newUser\\\\\\\\\\\\\\\":1,\\\\\\\\\\\\\\\"createTime\\\\\\\\\\\\\\\":1582136419000}]}}\\\\\\\",\\\\\\\"optTime\\\\\\\":1689327726000}]}}\\\",\\\"optTime\\\":1689415078000},{\\\"id\\\":\\\"1679788350122242049\\\",\\\"title\\\":\\\"系统微服务: 营收统计\\\",\\\"businessType\\\":1,\\\"methodName\\\":\\\"分页条件查询\\\",\\\"methodUrl\\\":\\\"com.park.systemserve.controller.RevenueCountController.queryPageRevenueCount\\\",\\\"reqType\\\":\\\"POST\\\",\\\"optName\\\":null,\\\"optUrl\\\":\\\"/system/revenue/page/1/5\\\",\\\"optIp\\\":\\\"0:0:0:0:0:0:0:1\\\",\\\"optAddress\\\":\\\"\\\",\\\"reqParam\\\":\\\"[1,5,{\\\\\\\"startTime\\\\\\\":null,\\\\\\\"endTime\\\\\\\":null,\\\\\\\"roadId\\\\\\\":null,\\\\\\\"inspectorName\\\\\\\":\\\\\\\"\\\\\\\"}]\\\",\\\"respData\\\":\\\"{\\\\\\\"code\\\\\\\":\\\\\\\"200\\\\\\\",\\\\\\\"message\\\\\\\":\\\\\\\"请求成功\\\\\\\",\\\\\\\"data\\\\\\\":{\\\\\\\"pageNum\\\\\\\":null,\\\\\\\"pageSize\\\\\\\":null,\\\\\\\"total\\\\\\\":20,\\\\\\\"pageData\\\\\\\":[{\\\\\\\"id\\\\\\\":\\\\\\\"2\\\\\\\",\\\\\\\"countTime\\\\\\\":1687501577000,\\\\\\\"orderCount\\\\\\\":764,\\\\\\\"revenue\\\\\\\":575.72,\\\\\\\"refundOrder\\\\\\\":279,\\\\\\\"refundAmount\\\\\\\":747.01,\\\\\\\"newUser\\\\\\\":616,\\\\\\\"createTime\\\\\\\":1451166333000},{\\\\\\\"id\\\\\\\":\\\\\\\"19\\\\\\\",\\\\\\\"countTime\\\\\\\":1686653816000,\\\\\\\"orderCount\\\\\\\":44,\\\\\\\"revenue\\\\\\\":970.21,\\\\\\\"refundOrder\\\\\\\":51,\\\\\\\"refundAmount\\\\\\\":27.73,\\\\\\\"newUser\\\\\\\":28,\\\\\\\"createTime\\\\\\\":1334867965000},{\\\\\\\"id\\\\\\\":\\\\\\\"9\\\\\\\",\\\\\\\"countTime\\\\\\\":1585404486000,\\\\\\\"orderCount\\\\\\\":24,\\\\\\\"revenue\\\\\\\":679.75,\\\\\\\"refundOrder\\\\\\\":916,\\\\\\\"refundAmount\\\\\\\":183.03,\\\\\\\"newUser\\\\\\\":142,\\\\\\\"createTime\\\\\\\":1335276831000},{\\\\\\\"id\\\\\\\":\\\\\\\"10\\\\\\\",\\\\\\\"countTime\\\\\\\":1574730532000,\\\\\\\"orderCount\\\\\\\":456,\\\\\\\"revenue\\\\\\\":988.84,\\\\\\\"refundOrder\\\\\\\":600,\\\\\\\"refundAmount\\\\\\\":414.76,\\\\\\\"newUser\\\\\\\":381,\\\\\\\"createTime\\\\\\\":1328030954000},{\\\\\\\"id\\\\\\\":\\\\\\\"4\\\\\\\",\\\\\\\"countTime\\\\\\\":1545864704000,\\\\\\\"orderCount\\\\\\\":770,\\\\\\\"revenue\\\\\\\":4.43,\\\\\\\"refundOrder\\\\\\\":379,\\\\\\\"refundAmount\\\\\\\":576.06,\\\\\\\"newUser\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1582136419000}]}}\\\",\\\"optTime\\\":1689327726000}]}}\",\"optTime\":1689415184000},{\"id\":\"1680154894870327297\",\"title\":\"系统微服务: 登录日志管理\",\"businessType\":1,\"methodName\":\"分页条件查询\",\"methodUrl\":\"com.park.systemserve.controller.LoginLogController.queryPageLoginLog\",\"reqType\":\"POST\",\"optName\":\"system\",\"optUrl\":\"/system/login/Log/page/1/5\",\"optIp\":\"0:0:0:0:0:0:0:1\",\"optAddress\":\"\",\"reqParam\":\"[1,5,{\\\"startTime\\\":null,\\\"endTime\\\":null,\\\"roadId\\\":\\\"0\\\",\\\"inspectorName\\\":\\\"\\\"}]\",\"respData\":\"{\\\"code\\\":\\\"200\\\",\\\"message\\\":\\\"请求成功\\\",\\\"data\\\":{\\\"pageNum\\\":null,\\\"pageSize\\\":null,\\\"total\\\":4,\\\"pageData\\\":[{\\\"id\\\":\\\"4\\\",\\\"username\\\":\\\"Nanjustar\\\",\\\"ipAddress\\\":\\\"127.0.0.1\\\",\\\"ipSource\\\":\\\"\\\",\\\"brower\\\":\\\"Firefox 11\\\",\\\"os\\\":\\\"Windows 10\\\",\\\"state\\\":1,\\\"createTime\\\":1685728915000,\\\"wechat\\\":\\\"登陆成功！\\\",\\\"mobile\\\":null},{\\\"id\\\":\\\"3\\\",\\\"username\\\":\\\"Nanjustar\\\",\\\"ipAddress\\\":\\\"127.0.0.1\\\",\\\"ipSource\\\":\\\"\\\",\\\"brower\\\":\\\"Chrome 11\\\",\\\"os\\\":\\\"Windows 10\\\",\\\"state\\\":1,\\\"createTime\\\":1684771742000,\\\"wechat\\\":\\\"登陆成功！\\\",\\\"mobile\\\":null},{\\\"id\\\":\\\"2\\\",\\\"username\\\":\\\"admin\\\",\\\"ipAddress\\\":\\\"127.0.0.1\\\",\\\"ipSource\\\":\\\"\\\",\\\"brower\\\":\\\"Firefox 11\\\",\\\"os\\\":\\\"Windows 10\\\",\\\"state\\\":1,\\\"createTime\\\":1684755937000,\\\"wechat\\\":\\\"登陆成功！\\\",\\\"mobile\\\":null},{\\\"id\\\":\\\"1\\\",\\\"username\\\":\\\"Nanjustar\\\",\\\"ipAddress\\\":\\\"127.0.0.1\\\",\\\"ipSource\\\":\\\"\\\",\\\"brower\\\":\\\"Firefox 11\\\",\\\"os\\\":\\\"Windows 10\\\",\\\"state\\\":1,\\\"createTime\\\":1684755835000,\\\"wechat\\\":\\\"登陆成功！\\\",\\\"mobile\\\":null}]}}\",\"optTime\":1689415117000},{\"id\":\"1680154728322904066\",\"title\":\"系统微服务: 操作日志管理\",\"businessType\":1,\"methodName\":\"分页条件查询\",\"methodUrl\":\"com.park.systemserve.controller.OptLogController.queryPageOptLog\",\"reqType\":\"POST\",\"optName\":\"system\",\"optUrl\":\"/system/opt/page/1/5\",\"optIp\":\"0:0:0:0:0:0:0:1\",\"optAddress\":\"\",\"reqParam\":\"[1,5,{\\\"startTime\\\":null,\\\"endTime\\\":null,\\\"roadId\\\":\\\"0\\\",\\\"inspectorName\\\":\\\"\\\"}]\",\"respData\":\"{\\\"code\\\":\\\"200\\\",\\\"message\\\":\\\"请求成功\\\",\\\"data\\\":{\\\"pageNum\\\":null,\\\"pageSize\\\":null,\\\"total\\\":1,\\\"pageData\\\":[{\\\"id\\\":\\\"1679788350122242049\\\",\\\"title\\\":\\\"系统微服务: 营收统计\\\",\\\"businessType\\\":1,\\\"methodName\\\":\\\"分页条件查询\\\",\\\"methodUrl\\\":\\\"com.park.systemserve.controller.RevenueCountController.queryPageRevenueCount\\\",\\\"reqType\\\":\\\"POST\\\",\\\"optName\\\":null,\\\"optUrl\\\":\\\"/system/revenue/page/1/5\\\",\\\"optIp\\\":\\\"0:0:0:0:0:0:0:1\\\",\\\"optAddress\\\":\\\"\\\",\\\"reqParam\\\":\\\"[1,5,{\\\\\\\"startTime\\\\\\\":null,\\\\\\\"endTime\\\\\\\":null,\\\\\\\"roadId\\\\\\\":null,\\\\\\\"inspectorName\\\\\\\":\\\\\\\"\\\\\\\"}]\\\",\\\"respData\\\":\\\"{\\\\\\\"code\\\\\\\":\\\\\\\"200\\\\\\\",\\\\\\\"message\\\\\\\":\\\\\\\"请求成功\\\\\\\",\\\\\\\"data\\\\\\\":{\\\\\\\"pageNum\\\\\\\":null,\\\\\\\"pageSize\\\\\\\":null,\\\\\\\"total\\\\\\\":20,\\\\\\\"pageData\\\\\\\":[{\\\\\\\"id\\\\\\\":\\\\\\\"2\\\\\\\",\\\\\\\"countTime\\\\\\\":1687501577000,\\\\\\\"orderCount\\\\\\\":764,\\\\\\\"revenue\\\\\\\":575.72,\\\\\\\"refundOrder\\\\\\\":279,\\\\\\\"refundAmount\\\\\\\":747.01,\\\\\\\"newUser\\\\\\\":616,\\\\\\\"createTime\\\\\\\":1451166333000},{\\\\\\\"id\\\\\\\":\\\\\\\"19\\\\\\\",\\\\\\\"countTime\\\\\\\":1686653816000,\\\\\\\"orderCount\\\\\\\":44,\\\\\\\"revenue\\\\\\\":970.21,\\\\\\\"refundOrder\\\\\\\":51,\\\\\\\"refundAmount\\\\\\\":27.73,\\\\\\\"newUser\\\\\\\":28,\\\\\\\"createTime\\\\\\\":1334867965000},{\\\\\\\"id\\\\\\\":\\\\\\\"9\\\\\\\",\\\\\\\"countTime\\\\\\\":1585404486000,\\\\\\\"orderCount\\\\\\\":24,\\\\\\\"revenue\\\\\\\":679.75,\\\\\\\"refundOrder\\\\\\\":916,\\\\\\\"refundAmount\\\\\\\":183.03,\\\\\\\"newUser\\\\\\\":142,\\\\\\\"createTime\\\\\\\":1335276831000},{\\\\\\\"id\\\\\\\":\\\\\\\"10\\\\\\\",\\\\\\\"countTime\\\\\\\":1574730532000,\\\\\\\"orderCount\\\\\\\":456,\\\\\\\"revenue\\\\\\\":988.84,\\\\\\\"refundOrder\\\\\\\":600,\\\\\\\"refundAmount\\\\\\\":414.76,\\\\\\\"newUser\\\\\\\":381,\\\\\\\"createTime\\\\\\\":1328030954000},{\\\\\\\"id\\\\\\\":\\\\\\\"4\\\\\\\",\\\\\\\"countTime\\\\\\\":1545864704000,\\\\\\\"orderCount\\\\\\\":770,\\\\\\\"revenue\\\\\\\":4.43,\\\\\\\"refundOrder\\\\\\\":379,\\\\\\\"refundAmount\\\\\\\":576.06,\\\\\\\"newUser\\\\\\\":1,\\\\\\\"createTime\\\\\\\":1582136419000}]}}\\\",\\\"optTime\\\":1689327726000}]}}\",\"optTime\":1689415078000},{\"id\":\"1679788350122242049\",\"title\":\"系统微服务: 营收统计\",\"businessType\":1,\"methodName\":\"分页条件查询\",\"methodUrl\":\"com.park.systemserve.controller.RevenueCountController.queryPageRevenueCount\",\"reqType\":\"POST\",\"optName\":null,\"optUrl\":\"/system/revenue/page/1/5\",\"optIp\":\"0:0:0:0:0:0:0:1\",\"optAddress\":\"\",\"reqParam\":\"[1,5,{\\\"startTime\\\":null,\\\"endTime\\\":null,\\\"roadId\\\":null,\\\"inspectorName\\\":\\\"\\\"}]\",\"respData\":\"{\\\"code\\\":\\\"200\\\",\\\"message\\\":\\\"请求成功\\\",\\\"data\\\":{\\\"pageNum\\\":null,\\\"pageSize\\\":null,\\\"total\\\":20,\\\"pageData\\\":[{\\\"id\\\":\\\"2\\\",\\\"countTime\\\":1687501577000,\\\"orderCount\\\":764,\\\"revenue\\\":575.72,\\\"refundOrder\\\":279,\\\"refundAmount\\\":747.01,\\\"newUser\\\":616,\\\"createTime\\\":1451166333000},{\\\"id\\\":\\\"19\\\",\\\"countTime\\\":1686653816000,\\\"orderCount\\\":44,\\\"revenue\\\":970.21,\\\"refundOrder\\\":51,\\\"refundAmount\\\":27.73,\\\"newUser\\\":28,\\\"createTime\\\":1334867965000},{\\\"id\\\":\\\"9\\\",\\\"countTime\\\":1585404486000,\\\"orderCount\\\":24,\\\"revenue\\\":679.75,\\\"refundOrder\\\":916,\\\"refundAmount\\\":183.03,\\\"newUser\\\":142,\\\"createTime\\\":1335276831000},{\\\"id\\\":\\\"10\\\",\\\"countTime\\\":1574730532000,\\\"orderCount\\\":456,\\\"revenue\\\":988.84,\\\"refundOrder\\\":600,\\\"refundAmount\\\":414.76,\\\"newUser\\\":381,\\\"createTime\\\":1328030954000},{\\\"id\\\":\\\"4\\\",\\\"countTime\\\":1545864704000,\\\"orderCount\\\":770,\\\"revenue\\\":4.43,\\\"refundOrder\\\":379,\\\"refundAmount\\\":576.06,\\\"newUser\\\":1,\\\"createTime\\\":1582136419000}]}}\",\"optTime\":1689327726000}]}}', '2023-07-18 06:43:00');
INSERT INTO `tp_opt_log` VALUES (1681226821089148929, '系统微服务: 营收统计', 1, '添加营收统计信息', 'com.park.systemserve.controller.RevenueCountController.addRevenueCount', 'POST', 'system', '/system/revenue', '192.168.3.22', '', '[{\"countTime\":\"2023-07-18 16:58:03\",\"orderCount\":null,\"revenue\":null,\"refundOrder\":null,\"refundAmount\":null,\"newUser\":null}]', '{\"code\":\"200\",\"message\":\"请求成功\",\"data\":1}', '2023-07-18 16:58:04');
INSERT INTO `tp_opt_log` VALUES (1681227576219058177, '系统微服务: 营收统计', 1, '添加营收统计信息', 'com.park.systemserve.controller.RevenueCountController.addRevenueCount', 'POST', 'system', '/system/revenue', '192.168.3.22', '', '[{\"countTime\":\"2023-07-18 17:01:04\",\"orderCount\":null,\"revenue\":null,\"refundOrder\":null,\"refundAmount\":null,\"newUser\":null}]', '{\"code\":\"200\",\"message\":\"请求成功\",\"data\":1}', '2023-07-18 17:01:05');
INSERT INTO `tp_opt_log` VALUES (1681228523703943169, '系统微服务: 营收统计', 1, '添加营收统计信息', 'com.park.systemserve.controller.RevenueCountController.addRevenueCount', 'POST', 'system', '/system/revenue', '192.168.3.22', '', '[{\"countTime\":\"2023-07-18 17:04:49\",\"orderCount\":1,\"revenue\":3333.0,\"refundOrder\":0,\"refundAmount\":0,\"newUser\":2}]', '{\"code\":\"200\",\"message\":\"请求成功\",\"data\":1}', '2023-07-18 17:04:50');
INSERT INTO `tp_opt_log` VALUES (1681261744017641473, '系统微服务: 支付统计', 1, '添加支付统计记录', 'com.park.systemserve.controller.PayCountController.addPayCount', 'POST', 'system', '/system/pay', '192.168.3.22', '', '[{\"countTime\":\"2023-07-18 19:16:50\",\"activePay\":1,\"cashPay\":0,\"codePay\":0,\"wechatPayAmount\":0,\"alipayPayAmount\":304.00,\"cashPayAmount\":0,\"unionPayAmount\":0}]', '{\"code\":\"200\",\"message\":\"请求成功\",\"data\":1}', '2023-07-18 19:16:51');

-- ----------------------------
-- Table structure for tp_order_list
-- ----------------------------
DROP TABLE IF EXISTS `tp_order_list`;
CREATE TABLE `tp_order_list`  (
  `id` bigint NOT NULL COMMENT '订单ID，雪花算法自动生成',
  `order_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单编号，前缀【ORN】+ 雪花算法自动生成',
  `order_submission_time` datetime NOT NULL COMMENT '订单提交时间，创建时间',
  `plate_number` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '车牌号码，格式【粤B】+ 英文与字母组合（五位）',
  `road_segment` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所属路段',
  `berth_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '泊位编号',
  `inspector_id` bigint NULL DEFAULT NULL COMMENT '巡检员,用于关联员工表的巡检员ID',
  `order_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '订单金额，【订单金额】 = 【停车时间】* 【对应时间段单价】',
  `order_state` int NOT NULL DEFAULT 0 COMMENT '订单状态，0-进行中，1-待支付，2-已支付，3-已完成',
  `bound_user` bigint NULL DEFAULT NULL COMMENT '绑定用户,绑定用户表的id',
  `magnetic_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '地磁编号',
  `drive_time` datetime NULL DEFAULT NULL COMMENT '驶入时间',
  `leave_time` datetime NULL DEFAULT NULL COMMENT '驶离时间',
  `park_time` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '停车时长，【驶离时间】 — 【驶入时间】',
  `order_abnormal_state` int NOT NULL DEFAULT 0 COMMENT '订单是否异常，0-正常订单（默认），1-异常订单（用户申诉后订单状态变为异常）',
  `order_reality_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '实付金额，实际付款金额',
  `way_pay` int NULL DEFAULT NULL COMMENT '支付方式，1-支付宝支付，2-银联，3-微信支付，4-现金支付',
  `pay_time` datetime NULL DEFAULT NULL COMMENT '支付时间',
  `pay_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '支付流水号',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_deleted` int NOT NULL DEFAULT 0 COMMENT '逻辑删除，0-未删除（默认），1-已删除',
  `version` int NOT NULL DEFAULT 0 COMMENT '乐观锁，版本号（默认0）',
  `field1` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段一',
  `field2` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段二',
  `field3` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段三',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `order_number`(`order_number` ASC) USING BTREE,
  INDEX `plate_number`(`plate_number` ASC) USING BTREE,
  INDEX `magnetic_number`(`magnetic_number` ASC) USING BTREE,
  INDEX `order_state`(`order_state` ASC) USING BTREE,
  INDEX `order_abnormal_state`(`order_abnormal_state` ASC) USING BTREE,
  INDEX `way_pay`(`way_pay` ASC) USING BTREE,
  INDEX `create_time`(`create_time` ASC) USING BTREE,
  INDEX `update_time`(`update_time` ASC) USING BTREE,
  INDEX `is_deleted`(`is_deleted` ASC) USING BTREE,
  INDEX `version`(`version` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '订单表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_order_list
-- ----------------------------
INSERT INTO `tp_order_list` VALUES (1, 'ORN1', '2023-07-17 00:00:00', '粤B1234', 'Road 1', 'Berth 1', 1, 304.00, 2, 1, 'Mag 1', '2023-07-14 08:00:00', '2023-07-17 12:05:07', '4565', 0, 304.00, 1, '2023-07-18 08:52:55', 'PAYMENT7080089106377740288', '2023-07-17 09:00:00', '2023-07-18 17:57:53', 0, 0, 'Field 1', 'Field 2', 'Field 3');
INSERT INTO `tp_order_list` VALUES (2, 'ORN2', '2023-07-17 01:00:00', '粤B2345', 'Road 2', 'Berth 2', 1, 20.00, 1, 2, 'Mag 2', '2023-07-14 10:00:00', '2023-07-14 11:30:00', '1.5 hours', 0, 20.00, 2, '2023-07-14 11:30:00', 'Pay 2', '2023-07-17 11:30:00', '2023-07-18 17:57:54', 0, 0, 'Field 1', 'Field 2', 'Field 3');
INSERT INTO `tp_order_list` VALUES (3, 'ORN3', '2023-07-17 02:00:00', '粤B3456', 'Road 3', 'Berth 3', 2, 15.00, 2, 3, 'Mag 3', '2023-07-14 12:00:00', '2023-07-14 14:00:00', '2 hours', 0, 15.00, 3, '2023-07-14 14:00:00', 'Pay 3', '2023-07-17 14:00:00', '2023-07-18 17:57:54', 0, 0, 'Field 1', 'Field 2', 'Field 3');
INSERT INTO `tp_order_list` VALUES (4, 'ORN4', '2023-07-17 03:00:00', '粤B4567', 'Road 4', 'Berth 4', 2, 18.50, 3, 4, 'Mag 4', '2023-07-14 16:00:00', '2023-07-14 18:30:00', '2.5 hours', 0, 18.50, 1, '2023-07-14 18:30:00', 'Pay 4', '2023-07-17 18:30:00', '2023-07-18 17:57:56', 0, 0, 'Field 1', 'Field 2', 'Field 3');
INSERT INTO `tp_order_list` VALUES (5, 'ORN5', '2023-07-17 04:00:00', '粤B5678', 'Road 5', 'Berth 5', 1, 12.80, 2, 5, 'Mag 5', '2023-07-14 20:00:00', '2023-07-14 21:15:00', '1.25 hours', 0, 12.80, 2, '2023-07-14 21:15:00', 'Pay 5', '2023-07-17 21:15:00', '2023-07-18 17:57:57', 0, 0, 'Field 1', 'Field 2', 'Field 3');
INSERT INTO `tp_order_list` VALUES (1679716725026836482, 'ORN6', '2023-07-17 20:18:34', 'dawdaw', '个人饿', '324234', 1, 3333.00, 2, 1, '3123123', '2023-07-13 20:18:34', '2023-07-13 20:18:34', '1小时', 0, 9999.00, 1, '2023-07-13 20:18:34', '3423', '2023-07-17 20:18:34', '2023-07-18 17:57:58', 0, 0, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tp_parking_position
-- ----------------------------
DROP TABLE IF EXISTS `tp_parking_position`;
CREATE TABLE `tp_parking_position`  (
  `id` bigint NOT NULL COMMENT '泊位id',
  `road_segment_id` bigint NULL DEFAULT NULL COMMENT '泊车路段ID',
  `parking_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '泊位名称',
  `berth_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '泊位编号',
  `magnetic_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '3' COMMENT '地磁编号',
  `state` int NOT NULL COMMENT '泊位状态：1.有车 2.无车 3.未激活',
  `activation_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '激活时间',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `field1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段1',
  `field2` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段2',
  `field3` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段3',
  `version` int NOT NULL DEFAULT 0 COMMENT '乐观锁,默认0',
  `is_deleted` int NOT NULL DEFAULT 0 COMMENT '逻辑删除',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '泊位信息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_parking_position
-- ----------------------------
INSERT INTO `tp_parking_position` VALUES (1, 1, '放心存放', '79986321', '78936974312', 2, '2023-07-14 14:45:13', '2023-07-11 14:45:20', NULL, NULL, NULL, 0, 0, '2023-07-14 14:45:29');
INSERT INTO `tp_parking_position` VALUES (2, 6, '适合度四核', 'HG76969006678', '676767676767', 0, '2023-07-11 08:17:50', '2023-07-11 08:17:50', '', '', '', 0, 0, '2023-07-12 08:17:50');
INSERT INTO `tp_parking_position` VALUES (3, 3, '水电费豆腐干大肥肥', '7998632666', '78936974999', 3, '2023-07-10 02:45:13', '2023-07-12 06:45:20', '', '', '', 0, 0, '2023-07-14 06:45:29');
INSERT INTO `tp_parking_position` VALUES (4, 4, '翻译大赛塔尔夫人阿非人防', '66666632666', '1111111111', 2, '2023-07-09 18:45:13', '2023-07-11 22:45:20', '', '', '', 0, 1, '2023-07-14 11:59:00');
INSERT INTO `tp_parking_position` VALUES (5, 8, '新东方给你的', 'JUK 8901678', 'k789789', 0, '2023-07-01 10:45:13', '2023-08-01 10:45:13', '', '', '', 0, 0, '2023-08-06 10:45:13');
INSERT INTO `tp_parking_position` VALUES (6, 2, '56话878', 'DI7078910602483208192', 'm1266664', 0, NULL, '2023-07-15 02:49:55', NULL, NULL, NULL, 0, 0, '2023-07-15 02:49:55');
INSERT INTO `tp_parking_position` VALUES (7, 5, '说的GV父类拷贝了', 'DI7079717901162385408', '789123369', 0, NULL, '2023-07-17 08:17:50', NULL, NULL, NULL, 0, 0, '2023-07-17 08:17:50');
INSERT INTO `tp_parking_position` VALUES (8, 7, '说的GV父类拷贝了', 'DI7079718141693136896', '789123369', 0, NULL, '2023-07-17 08:18:47', NULL, NULL, NULL, 0, 0, '2023-07-17 08:18:47');
INSERT INTO `tp_parking_position` VALUES (9, 9, '说的GV父类拷贝了', 'DI7079718316306206720', '789123369', 0, NULL, '2023-07-17 08:19:29', NULL, NULL, NULL, 0, 0, '2023-07-17 08:19:29');
INSERT INTO `tp_parking_position` VALUES (1680870694392500225, 10, '和苟富贵', 'DI7079734288995979264', '963', 0, NULL, '2023-07-17 09:22:57', NULL, NULL, NULL, 0, 0, '2023-07-17 09:22:57');
INSERT INTO `tp_parking_position` VALUES (1680872593535598594, 11, '很快就和', 'DI7079736188067778560', '963666888', 0, NULL, '2023-07-17 09:30:30', NULL, NULL, NULL, 0, 0, '2023-07-17 09:30:30');
INSERT INTO `tp_parking_position` VALUES (1681148095814950913, 0, '', '12341234123', '', 1, '2023-07-14 06:45:13', '2023-07-18 03:45:15', '', '', '', 0, 0, '2023-07-18 04:04:38');

-- ----------------------------
-- Table structure for tp_pay_count
-- ----------------------------
DROP TABLE IF EXISTS `tp_pay_count`;
CREATE TABLE `tp_pay_count`  (
  `id` bigint NOT NULL COMMENT '营收统计主键id',
  `count_time` datetime NULL DEFAULT NULL COMMENT '统计日期',
  `active_pay` int NULL DEFAULT NULL COMMENT '当日主动支付次数统计（次）',
  `cash_pay` int NULL DEFAULT NULL COMMENT '当日现金支付次数统计（次）',
  `code_pay` int NULL DEFAULT NULL COMMENT '当日二维码收款次数统计（次）',
  `wechat_pay_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '当日微信支付金额统计（元）',
  `alipay_pay_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '当日支付宝支付金额统计（元）',
  `cash_pay_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '当日现金支付金额统计（元）',
  `union_pay_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '当日银联支付金额统计（元）',
  `create_time` datetime NULL DEFAULT NULL COMMENT '生成该条数据的时间戳',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = 'ade-支付统计表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_pay_count
-- ----------------------------
INSERT INTO `tp_pay_count` VALUES (1, '2006-08-06 23:45:44', 315, 748, 785, 872.73, 895.74, 892.92, 890.26, '2009-04-04 09:43:13');
INSERT INTO `tp_pay_count` VALUES (2, '2003-12-16 01:46:21', 176, 506, 352, 207.05, 299.24, 205.12, 659.65, '2004-08-25 12:21:42');
INSERT INTO `tp_pay_count` VALUES (3, '2017-11-16 20:43:23', 249, 247, 742, 558.17, 788.87, 784.12, 143.04, '2015-09-03 23:12:56');
INSERT INTO `tp_pay_count` VALUES (4, '2004-08-15 10:01:32', 672, 769, 791, 668.58, 442.09, 748.81, 986.86, '2012-07-25 22:18:41');
INSERT INTO `tp_pay_count` VALUES (5, '2013-03-21 09:24:16', 788, 601, 51, 704.90, 305.36, 85.42, 595.39, '2021-07-11 22:35:31');
INSERT INTO `tp_pay_count` VALUES (6, '2002-11-29 01:10:26', 499, 112, 99, 442.61, 949.96, 371.21, 981.57, '2008-11-22 12:07:39');
INSERT INTO `tp_pay_count` VALUES (7, '2004-08-11 13:24:12', 643, 279, 123, 101.06, 838.53, 13.48, 535.94, '2017-08-15 02:28:10');
INSERT INTO `tp_pay_count` VALUES (8, '2019-07-27 11:09:57', 472, 746, 796, 195.45, 507.62, 868.35, 722.12, '2011-08-13 14:21:59');
INSERT INTO `tp_pay_count` VALUES (9, '2008-03-17 08:59:01', 768, 644, 82, 301.21, 181.47, 150.05, 821.52, '2022-02-07 14:21:24');
INSERT INTO `tp_pay_count` VALUES (10, '2006-10-03 14:31:50', 290, 228, 724, 226.54, 908.78, 937.47, 482.54, '2020-11-09 04:23:48');
INSERT INTO `tp_pay_count` VALUES (11, '2013-06-02 15:33:15', 286, 524, 113, 524.84, 404.41, 446.39, 716.95, '2013-02-03 07:15:56');
INSERT INTO `tp_pay_count` VALUES (12, '2001-08-28 22:57:30', 996, 600, 833, 933.18, 816.24, 113.54, 984.73, '2022-04-11 18:13:27');
INSERT INTO `tp_pay_count` VALUES (13, '2004-05-29 03:24:47', 589, 249, 786, 515.25, 139.34, 507.41, 194.27, '2007-11-25 22:40:46');
INSERT INTO `tp_pay_count` VALUES (14, '2020-07-14 00:12:22', 686, 507, 767, 0.38, 186.27, 561.84, 104.32, '2022-08-22 13:19:36');
INSERT INTO `tp_pay_count` VALUES (15, '2019-05-10 04:01:54', 940, 389, 672, 937.34, 448.54, 694.97, 180.61, '2013-03-16 15:06:28');
INSERT INTO `tp_pay_count` VALUES (16, '2010-04-24 11:27:52', 873, 983, 827, 446.75, 767.99, 273.55, 3.28, '2023-03-07 23:54:26');
INSERT INTO `tp_pay_count` VALUES (17, '2008-12-25 08:58:30', 147, 771, 242, 799.52, 95.22, 965.60, 94.22, '2009-02-12 14:32:15');
INSERT INTO `tp_pay_count` VALUES (18, '2014-03-27 18:37:24', 604, 573, 741, 463.26, 144.26, 970.50, 582.33, '2003-02-09 16:46:56');
INSERT INTO `tp_pay_count` VALUES (19, '2005-11-28 01:32:37', 28, 808, 806, 492.43, 858.11, 647.66, 753.97, '2019-04-02 21:33:58');
INSERT INTO `tp_pay_count` VALUES (20, '2010-01-15 14:30:24', 65, 264, 34, 542.39, 155.58, 14.13, 426.87, '2000-09-19 18:13:58');
INSERT INTO `tp_pay_count` VALUES (1681261742478331905, '2023-07-18 19:16:50', 1, 0, 0, 0.00, 304.00, 0.00, 0.00, '2023-07-18 19:16:50');

-- ----------------------------
-- Table structure for tp_pda
-- ----------------------------
DROP TABLE IF EXISTS `tp_pda`;
CREATE TABLE `tp_pda`  (
  `id` bigint NOT NULL COMMENT '主键',
  `device_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设备编号',
  `device_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设备名称',
  `road` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所属路段',
  `current_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '当前使用者',
  `state` int NULL DEFAULT NULL COMMENT '设备状态，0:在线1:离线',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `is_deleted` int NULL DEFAULT 0 COMMENT '1表示删除，0表示没有删除,逻辑删除',
  `version` int(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '乐观锁',
  `road_id` bigint NULL DEFAULT NULL COMMENT '路段id',
  `filed2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段2',
  `filed3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段3',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '手持PDA表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_pda
-- ----------------------------
INSERT INTO `tp_pda` VALUES (1, '4123213', 'A设备', '天安1街', 'A巡检员', 0, '2023-07-12 10:28:21', NULL, 0, 1, 1, NULL, NULL);
INSERT INTO `tp_pda` VALUES (2, '4123213', 'A设备', '天安1街', 'A巡检员', 0, '2023-07-12 10:29:02', NULL, 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_pda` VALUES (3, '4123213', 'A设备', '天安1街', 'A巡检员', 1, '2023-07-12 10:29:05', NULL, 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_pda` VALUES (4, '4123213', 'A设备', '天安1街', 'A巡检员', 1, '2023-07-12 10:29:21', NULL, 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_pda` VALUES (5, '4123214', 'A设备', '天安1街', 'A巡检员', 0, '2023-07-12 10:29:40', NULL, 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_pda` VALUES (6, '4123214', 'A设备', '天安1街', 'A巡检员', 0, '2023-07-12 10:30:07', NULL, 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_pda` VALUES (7, '4123214', 'A设备', '天安1街', 'A巡检员', 1, '2023-07-12 10:30:10', NULL, 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_pda` VALUES (8, '4123214', 'A设备', '天安1街', 'A巡检员', 1, '2023-07-12 10:30:26', NULL, 0, 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tp_pda_log
-- ----------------------------
DROP TABLE IF EXISTS `tp_pda_log`;
CREATE TABLE `tp_pda_log`  (
  `id` bigint NOT NULL COMMENT '主键',
  `set_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设备编号',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `login` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '登录人',
  `road` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所属路段',
  `is_deleted` int NULL DEFAULT 0 COMMENT '1表示删除，0表示没有删除,逻辑删除',
  `version` int(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '乐观锁',
  `park_filed1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段1',
  `park_filed2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段2',
  `park_filed3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段3',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = 'PDA登录日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_pda_log
-- ----------------------------
INSERT INTO `tp_pda_log` VALUES (1, '4123213', '2023-07-12 10:34:05', 'A巡检员', '天安1街', 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_pda_log` VALUES (2, '4123213', '2023-07-12 10:34:27', 'A巡检员', '天安1街', 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_pda_log` VALUES (3, '4123213', '2023-07-12 10:34:39', 'A巡检员', '天安1街', 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_pda_log` VALUES (4, '4123213', '2023-07-12 10:34:55', 'A巡检员', '天安1街', 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_pda_log` VALUES (5, '4123214', '2023-07-12 10:35:09', 'A巡检员', '天安1街', 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_pda_log` VALUES (6, '4123214', '2023-07-12 10:35:23', 'A巡检员', '天安1街', 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_pda_log` VALUES (7, '4123214', '2023-07-12 10:35:35', 'A巡检员', '天安1街', 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_pda_log` VALUES (8, '4123214', '2023-07-12 10:35:47', 'A巡检员', '天安1街', 0, 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tp_people
-- ----------------------------
DROP TABLE IF EXISTS `tp_people`;
CREATE TABLE `tp_people`  (
  `id` bigint NOT NULL COMMENT '主键',
  `employee_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '员工名称',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号码',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码',
  `road` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '管理路段',
  `duty_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '执勤时间',
  `order_rate` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '订单完成率',
  `owning_group` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所属分组',
  `role_state` int NULL DEFAULT NULL COMMENT '角色状态,0:巡检员1:运维人员',
  `state` int NULL DEFAULT NULL COMMENT '状态,0:禁用1:正常',
  `create_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建时间',
  `is_deleted` int NULL DEFAULT 0 COMMENT '1表示删除，0表示没有删除,逻辑删除',
  `version` int(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '乐观锁',
  `filed1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段1',
  `filed2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段2',
  `filed3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段3',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '员工管理表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_people
-- ----------------------------
INSERT INTO `tp_people` VALUES (1, '张三丰', '13656789998', '23423', '', '', '', '', 0, 0, '2021.12.31', 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_people` VALUES (2, '张三丰', '13656789999', NULL, '', '9：00-16：00', '98%', '', 0, 0, '2021.12.31', 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_people` VALUES (3, 'Mark', '13656782141', NULL, '', '', '', '电信一区', 1, 0, '2021.12.31', 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_people` VALUES (4, 'Mark', '13656782141', NULL, '', '', '', '电信一区', 1, 0, '2021.12.31', 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_people` VALUES (5, '张三丰', '13656788888', NULL, '', '9：00-16：00', '96%', '', 0, 0, '2021.12.31', 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_people` VALUES (6, '张三丰', '13656788888', NULL, '', '9：00-16：00', '96%', '', 0, 0, '2021.12.31', 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_people` VALUES (7, 'Mark', '13656782142', NULL, '', '', '', '电信一区', 1, 0, '2021.12.31', 0, 1, NULL, NULL, NULL);
INSERT INTO `tp_people` VALUES (8, 'Mark', '13656782142', NULL, '', '', '', '电信一区', 1, 0, '2021.12.31', 0, 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tp_people_road
-- ----------------------------
DROP TABLE IF EXISTS `tp_people_road`;
CREATE TABLE `tp_people_road`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `road_id` bigint NULL DEFAULT NULL COMMENT '路段id',
  `inspector_id` bigint NULL DEFAULT NULL COMMENT '巡检员id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除',
  `version` int NOT NULL DEFAULT 0 COMMENT '乐观锁,默认1',
  `reserve_field1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段1',
  `reserve_field2` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段2',
  `reserve_field3` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段3',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '巡检、路段中间表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_people_road
-- ----------------------------
INSERT INTO `tp_people_road` VALUES (1, 2, 1, '2023-07-13 19:29:41', '2023-07-13 19:29:45', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_people_road` VALUES (2, 2, 2, '2023-07-13 19:30:07', '2023-07-13 19:30:10', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_people_road` VALUES (3, 3, 1, '2023-07-13 19:30:18', '2023-07-13 19:30:21', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_people_road` VALUES (4, 4, 6, '2023-07-13 19:30:28', '2023-07-13 19:30:31', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_people_road` VALUES (5, 4, 1, '2023-07-14 11:01:45', '2023-07-14 11:01:47', 0, 0, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tp_picture_storage
-- ----------------------------
DROP TABLE IF EXISTS `tp_picture_storage`;
CREATE TABLE `tp_picture_storage`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '图片表ID',
  `order_id` bigint NOT NULL COMMENT '订单表ID，与订单表关联的字段',
  `order_number` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单编号，与订单表关联的字段',
  `picture_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '图片路径',
  `picture_attribute` int NOT NULL DEFAULT 0 COMMENT '图片属性，0-驶入图片，1-申诉图片',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_deleted` int NOT NULL DEFAULT 0 COMMENT '逻辑删除，0-未删除（默认），1-已删除',
  `version` int NOT NULL DEFAULT 0 COMMENT '乐观锁，版本号（默认0）',
  `field1` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段一',
  `field2` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段二',
  `field3` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段三',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `order_id`(`order_id` ASC) USING BTREE,
  INDEX `order_number`(`order_number` ASC) USING BTREE,
  INDEX `picture_attribute`(`picture_attribute` ASC) USING BTREE,
  INDEX `create_time`(`create_time` ASC) USING BTREE,
  INDEX `update_time`(`update_time` ASC) USING BTREE,
  INDEX `is_deleted`(`is_deleted` ASC) USING BTREE,
  INDEX `version`(`version` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1681226360117719042 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '存储驶入图片表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_picture_storage
-- ----------------------------
INSERT INTO `tp_picture_storage` VALUES (1, 1, 'ppppp', 'lll1', 0, '2023-07-18 08:45:17', '2023-07-18 08:45:17', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_picture_storage` VALUES (1679761398470578177, 1, '111', 'dawiodjawidjawdij', 0, '2023-07-14 15:55:00', '2023-07-14 15:55:00', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_picture_storage` VALUES (1679761487880556545, 2, '111', 'dawiodjawidjawdij', 0, '2023-07-14 15:55:21', '2023-07-14 15:55:21', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_picture_storage` VALUES (1679761522114465793, 3, '111', 'dawiodjawidjawdij', 0, '2023-07-14 15:55:30', '2023-07-14 15:55:59', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_picture_storage` VALUES (1679764588171685889, 666, 'ooooooooo', 'first1', 0, '2023-07-14 16:07:41', '2023-07-14 16:07:41', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_picture_storage` VALUES (1679764589480308737, 666, 'ooooooooo', 'first2', 0, '2023-07-14 16:07:41', '2023-07-14 16:07:41', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_picture_storage` VALUES (1679764589480308738, 666, 'ooooooooo', 'first3', 0, '2023-07-14 16:07:41', '2023-07-14 16:07:41', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_picture_storage` VALUES (1679764589547417602, 666, 'ooooooooo', 'first4', 0, '2023-07-14 16:07:41', '2023-07-14 16:07:41', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_picture_storage` VALUES (1679764821467262977, 787878, '111', 'dawiodjawidjawdij', 0, '2023-07-14 16:08:36', '2023-07-14 16:09:17', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_picture_storage` VALUES (1681226360117719041, 1, 'ORN1', 'field', 0, '2023-07-18 16:56:15', '2023-07-18 16:56:15', 0, 0, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tp_revenue_count
-- ----------------------------
DROP TABLE IF EXISTS `tp_revenue_count`;
CREATE TABLE `tp_revenue_count`  (
  `id` bigint NOT NULL COMMENT '营收统计主键id',
  `count_time` datetime NULL DEFAULT NULL COMMENT '统计日期',
  `order_count` int NULL DEFAULT NULL COMMENT '统计当日的订单总数（包含异常订单）',
  `revenue` decimal(10, 2) NULL DEFAULT NULL COMMENT '当日营收额总数（收入减去退款金额）',
  `refund_order` int NULL DEFAULT NULL COMMENT '统计当日退款订单总数',
  `refund_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '统计当日退款金额总数',
  `new_user` int NULL DEFAULT NULL COMMENT '统计当日新注册用户数',
  `create_time` datetime NULL DEFAULT NULL COMMENT '生成该条数据的时间戳',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = 'ade-营收统计表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_revenue_count
-- ----------------------------
INSERT INTO `tp_revenue_count` VALUES (1, '2010-01-16 00:56:54', 565, 465.66, 23, 704.56, 850, '2021-11-25 08:56:51');
INSERT INTO `tp_revenue_count` VALUES (2, '2023-06-23 06:26:17', 764, 575.72, 279, 747.01, 616, '2015-12-26 21:45:33');
INSERT INTO `tp_revenue_count` VALUES (3, '2016-02-24 03:13:43', 346, 314.34, 641, 607.08, 629, '2013-03-01 19:48:33');
INSERT INTO `tp_revenue_count` VALUES (4, '2018-12-26 22:51:44', 770, 4.43, 379, 576.06, 1, '2020-02-19 18:20:19');
INSERT INTO `tp_revenue_count` VALUES (5, '2013-11-01 13:54:13', 968, 499.75, 879, 21.02, 615, '2004-03-05 03:07:02');
INSERT INTO `tp_revenue_count` VALUES (6, '2004-05-24 00:01:01', 951, 581.66, 259, 682.85, 212, '2000-08-09 22:13:41');
INSERT INTO `tp_revenue_count` VALUES (7, '2004-10-24 08:26:22', 422, 505.31, 900, 473.74, 429, '2003-07-22 15:15:59');
INSERT INTO `tp_revenue_count` VALUES (8, '2013-08-20 19:13:35', 546, 1.41, 483, 165.85, 464, '2015-10-13 00:53:09');
INSERT INTO `tp_revenue_count` VALUES (9, '2020-03-28 14:08:06', 24, 679.75, 916, 183.03, 142, '2012-04-24 14:13:51');
INSERT INTO `tp_revenue_count` VALUES (10, '2019-11-26 01:08:52', 456, 988.84, 600, 414.76, 381, '2012-01-31 17:29:14');
INSERT INTO `tp_revenue_count` VALUES (11, '2013-09-17 13:58:09', 925, 780.34, 548, 912.99, 891, '2016-12-05 21:54:56');
INSERT INTO `tp_revenue_count` VALUES (12, '2017-03-17 15:50:54', 934, 194.74, 729, 940.82, 804, '2009-07-25 04:44:46');
INSERT INTO `tp_revenue_count` VALUES (13, '2005-10-18 01:06:25', 674, 275.45, 704, 421.90, 986, '2015-04-22 12:43:45');
INSERT INTO `tp_revenue_count` VALUES (14, '2011-02-05 06:41:43', 381, 921.95, 440, 421.56, 152, '2023-01-19 21:10:34');
INSERT INTO `tp_revenue_count` VALUES (15, '2013-09-28 15:32:22', 394, 514.87, 62, 288.58, 42, '2005-07-29 18:56:19');
INSERT INTO `tp_revenue_count` VALUES (16, '2001-04-09 09:44:22', 651, 497.36, 497, 253.93, 935, '2009-04-07 20:56:41');
INSERT INTO `tp_revenue_count` VALUES (17, '2012-11-11 22:21:45', 712, 541.47, 808, 777.63, 804, '2002-05-10 10:14:53');
INSERT INTO `tp_revenue_count` VALUES (18, '2018-02-04 03:26:13', 472, 417.94, 672, 980.96, 198, '2013-02-04 13:04:40');
INSERT INTO `tp_revenue_count` VALUES (19, '2023-06-13 10:56:56', 44, 970.21, 51, 27.73, 28, '2012-04-19 20:39:25');
INSERT INTO `tp_revenue_count` VALUES (20, '2008-04-11 23:07:29', 916, 520.83, 973, 623.36, 487, '2010-02-15 09:12:47');
INSERT INTO `tp_revenue_count` VALUES (1681226819646308353, '2023-07-18 16:58:03', NULL, NULL, NULL, NULL, NULL, '2023-07-18 16:58:04');
INSERT INTO `tp_revenue_count` VALUES (1681227575954817025, '2023-07-18 17:01:04', NULL, NULL, NULL, NULL, NULL, '2023-07-18 17:01:04');
INSERT INTO `tp_revenue_count` VALUES (1681228523448090625, '2023-07-18 17:04:49', 1, 3333.00, 0, 0.00, 2, '2023-07-18 17:04:50');

-- ----------------------------
-- Table structure for tp_road_count
-- ----------------------------
DROP TABLE IF EXISTS `tp_road_count`;
CREATE TABLE `tp_road_count`  (
  `id` bigint NOT NULL COMMENT '营收统计主键id',
  `count_time` datetime NULL DEFAULT NULL COMMENT '统计日期',
  `road_count_num` int NULL DEFAULT NULL COMMENT '路段总数（个）',
  `road_id` bigint NULL DEFAULT NULL COMMENT '统计的路段id',
  `order_count_num` int NULL DEFAULT NULL COMMENT '当日该路段订单总数',
  `revenue_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '当日该路段营收金额（元）',
  `refund_order` int NULL DEFAULT NULL COMMENT '统计当日退款订单总数',
  `refund_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '统计当日退款金额总数',
  `zero_order_num` int NULL DEFAULT NULL COMMENT '当日无订单的路段数（个）',
  `create_time` datetime NULL DEFAULT NULL COMMENT '生成该条数据的时间戳',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = 'ade-路段统计表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_road_count
-- ----------------------------
INSERT INTO `tp_road_count` VALUES (1, '2004-08-13 18:22:46', 674, 922, 82, 470.40, 502, 172.07, 657, '2022-11-24 20:22:32');
INSERT INTO `tp_road_count` VALUES (2, '2020-03-24 19:51:26', 862, 662, 806, 979.17, 145, 855.83, 177, '2005-01-18 07:47:25');
INSERT INTO `tp_road_count` VALUES (3, '2005-09-04 22:21:02', 255, 67, 479, 421.35, 918, 464.89, 621, '2021-08-12 02:34:25');
INSERT INTO `tp_road_count` VALUES (4, '2010-11-28 05:50:01', 805, 376, 28, 431.46, 315, 196.17, 331, '2009-09-16 00:49:09');
INSERT INTO `tp_road_count` VALUES (5, '2023-06-28 19:38:02', 703, 912, 71, 77.73, 885, 131.57, 206, '2013-01-04 13:03:53');
INSERT INTO `tp_road_count` VALUES (6, '2009-10-11 07:59:52', 241, 134, 335, 993.28, 342, 767.06, 192, '2009-07-11 06:15:45');
INSERT INTO `tp_road_count` VALUES (7, '2011-07-05 04:08:14', 105, 699, 575, 998.50, 791, 72.24, 164, '2012-11-09 08:58:09');
INSERT INTO `tp_road_count` VALUES (8, '2016-11-15 14:48:26', 763, 343, 635, 982.65, 15, 605.95, 895, '2003-06-14 20:17:11');
INSERT INTO `tp_road_count` VALUES (9, '2009-03-30 23:51:38', 493, 18, 222, 247.08, 572, 987.80, 714, '2006-11-07 05:09:28');
INSERT INTO `tp_road_count` VALUES (10, '2006-05-07 09:14:11', 707, 620, 290, 894.48, 89, 918.54, 775, '2008-11-23 21:31:08');
INSERT INTO `tp_road_count` VALUES (11, '2022-03-11 13:24:41', 253, 942, 463, 844.53, 197, 281.71, 596, '2007-03-22 10:08:13');
INSERT INTO `tp_road_count` VALUES (12, '2015-04-15 05:45:03', 711, 18, 508, 304.34, 397, 611.61, 326, '2022-03-15 14:30:18');
INSERT INTO `tp_road_count` VALUES (13, '2002-09-12 12:16:03', 195, 887, 199, 232.01, 347, 526.91, 67, '2014-01-22 16:20:57');
INSERT INTO `tp_road_count` VALUES (14, '2004-10-02 00:28:20', 564, 425, 965, 447.28, 240, 238.40, 786, '2014-07-25 01:30:06');
INSERT INTO `tp_road_count` VALUES (15, '2007-07-20 01:27:16', 725, 816, 74, 796.86, 163, 282.84, 808, '2003-03-11 20:32:27');
INSERT INTO `tp_road_count` VALUES (16, '2011-01-01 07:02:04', 571, 889, 77, 545.18, 969, 355.29, 433, '2015-01-05 16:29:21');
INSERT INTO `tp_road_count` VALUES (17, '2011-05-12 15:01:33', 813, 803, 262, 596.72, 177, 73.38, 841, '2020-04-22 07:00:52');
INSERT INTO `tp_road_count` VALUES (18, '2000-05-01 18:45:25', 382, 933, 455, 950.54, 179, 9.48, 881, '2000-12-03 07:17:51');
INSERT INTO `tp_road_count` VALUES (19, '2010-12-02 09:50:14', 646, 964, 974, 132.14, 854, 10.67, 734, '2012-06-20 20:25:05');
INSERT INTO `tp_road_count` VALUES (20, '2005-01-30 00:02:48', 2, 372, 524, 998.17, 230, 307.74, 45, '2018-10-24 18:15:33');

-- ----------------------------
-- Table structure for tp_road_segment
-- ----------------------------
DROP TABLE IF EXISTS `tp_road_segment`;
CREATE TABLE `tp_road_segment`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '路段id',
  `road_segment` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '路段编号',
  `segment_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '路段名称',
  `segment_type` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '路段类型',
  `region` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属地区',
  `parking_spots` int NOT NULL COMMENT '泊位数量',
  `restricted_spots` int NOT NULL COMMENT '限制泊位数',
  `inspector` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '巡检员',
  `maintenance_staff` bigint NOT NULL COMMENT '运维人员ID',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `field1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段1',
  `field2` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段2',
  `field3` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段3',
  `version` int NULL DEFAULT 0 COMMENT '乐观锁,默认1',
  `is_deleted` int NULL DEFAULT 0 COMMENT '逻辑删除',
  `longitude` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '经度',
  `latitude` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '纬度',
  PRIMARY KEY (`id`, `latitude`) USING BTREE,
  INDEX `id`(`id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '路段信息' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_road_segment
-- ----------------------------
INSERT INTO `tp_road_segment` VALUES (1, '', '南京18路段', 'ABC级', '学校附近', 16, 16, '花花', 1, '2023-07-11 19:10:37', '2023-07-13 19:10:45', NULL, NULL, NULL, 0, 0, '113.26713,23.158288', '114.125041,22.540504至32°37″');
INSERT INTO `tp_road_segment` VALUES (2, '', '斯达18路段', 'EFG级', '学校附近', 16, 16, '花花', 1, '2023-07-11 19:10:37', '2023-07-13 19:10:45', NULL, NULL, NULL, 0, 0, '113.26713,23.158288', '114.125041,22.540504');
INSERT INTO `tp_road_segment` VALUES (3, '', '和大佛斐然', 'FGTY123', '发热豆腐', 82, 89, '的粉丝热', 6, '2023-07-11 07:46:37', '2023-07-14 07:46:37', '', '', '', 0, 0, '113.26713,23.158288', '114.125041,22.540504');
INSERT INTO `tp_road_segment` VALUES (4, '', '分担分担放的', 'TYUOP1268', '电放费反射', 7, 9, '教皇方济各66688999', 66, '2023-07-10 23:46:37', '2023-07-08 23:46:37', '', '', '', 0, 0, '113.26713,23.158288', '114.125041,22.540504');
INSERT INTO `tp_road_segment` VALUES (5, '', '很多风格和音乐风格', 'TYUOP1268', '电放费反射', 7, 9, '同一天天优雅大方', 66, '2023-07-10 23:46:37', '2023-07-08 23:46:37', '', '', '', 0, 0, '113.26713,23.158288', '114.125041,22.540504');
INSERT INTO `tp_road_segment` VALUES (6, '', '分担分担放的', 'TYUOP1268', '电放费反射', 7, 9, '教皇方济各', 66, '2023-07-10 23:46:37', '2023-07-08 23:46:37', '', '', '', 0, 0, '113.26713,23.158288', '114.125041,22.540504');

-- ----------------------------
-- Table structure for tp_roadsegment_chargingrule
-- ----------------------------
DROP TABLE IF EXISTS `tp_roadsegment_chargingrule`;
CREATE TABLE `tp_roadsegment_chargingrule`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '中间表ID',
  `road_segment_id` bigint NOT NULL COMMENT '泊车路段ID',
  `charging_rule_id` bigint NOT NULL COMMENT '收费规则表ID',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `reserve_field1` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段1',
  `reserve_field2` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段2',
  `reserve_field3` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段3',
  `version` int NOT NULL DEFAULT 0 COMMENT '乐观锁，默认1',
  `is_deleted` int NOT NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `road_segment_id`(`road_segment_id` ASC) USING BTREE,
  INDEX `charging_rule_id`(`charging_rule_id` ASC) USING BTREE,
  CONSTRAINT `tp_roadsegment_chargingrule_ibfk_1` FOREIGN KEY (`road_segment_id`) REFERENCES `tp_road_segment` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tp_roadsegment_chargingrule_ibfk_2` FOREIGN KEY (`charging_rule_id`) REFERENCES `tp_charging_rule` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '泊车路段与收费规则关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_roadsegment_chargingrule
-- ----------------------------

-- ----------------------------
-- Table structure for tp_trade_log
-- ----------------------------
DROP TABLE IF EXISTS `tp_trade_log`;
CREATE TABLE `tp_trade_log`  (
  `id` bigint NOT NULL COMMENT '交易日志主键id',
  `order_id` bigint NULL DEFAULT NULL COMMENT '订单id',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '交易标题',
  `pay_platform` int NULL DEFAULT NULL COMMENT '支付平台,1支付宝,2银联,3微信,4现金',
  `trade_type` int NULL DEFAULT NULL COMMENT '交易类型,1支付,2退款',
  `amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '交易金额,单位元',
  `req_flow` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '请求流水号,支付模块生成,唯一',
  `state` int NULL DEFAULT 0 COMMENT '交易状态,0进行中,1成功,2失败,默认0',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `is_deleted` int NULL DEFAULT NULL COMMENT '逻辑删除字段,0正常,1删除.默认0',
  `version` int NULL DEFAULT 1 COMMENT '乐观锁默认1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = 'ade-交易记录表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_trade_log
-- ----------------------------
INSERT INTO `tp_trade_log` VALUES (1681225512034258945, 1, '停车费', 1, 1, 304.00, 'PAYMENT7080089106377740288', 0, '2023-07-18 16:52:52', '2023-07-18 16:52:52', 0, 1);

-- ----------------------------
-- Table structure for tp_user
-- ----------------------------
DROP TABLE IF EXISTS `tp_user`;
CREATE TABLE `tp_user`  (
  `id` bigint NOT NULL COMMENT '用户主键id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户名称',
  `mobile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '手机号',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码',
  `wechat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '微信号 微信注册/登录返回的id',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像',
  `bind_number` int NULL DEFAULT NULL COMMENT '绑定车辆数量',
  `is_bind` int NULL DEFAULT NULL COMMENT '是否绑定微信号 0: 绑定 1: 没绑定',
  `state` int NULL DEFAULT NULL COMMENT '状态 0：正常 1：异常',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间 注册时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `version` int NULL DEFAULT 0 COMMENT '乐观锁 默认：0',
  `is_deleted` int NULL DEFAULT 0 COMMENT '是否删除 0:未删除 1:已删除',
  `field1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段1',
  `field2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段2',
  `field3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备用字段3',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tp_user
-- ----------------------------
INSERT INTO `tp_user` VALUES (1234, '张三四', '12344444', NULL, '666', '43', NULL, NULL, NULL, '2023-07-06 15:25:53', NULL, 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_user` VALUES (123456789, '李四', '1333445', '$2a$10$pokEB7IapvBovFBXqDYVEO9h1/PxOd2J8/XTY.hU1mESfEpdC.VHq', '777', 'jpg', 0, 0, 0, '2023-07-14 08:37:53', '2023-07-14 08:37:53', 0, 0, NULL, NULL, NULL);
INSERT INTO `tp_user` VALUES (1681211882737696769, '王五2', '12345678999', '$2a$10$N.v2s2gLkOB6axO.UkZCrOMbuPzYdWD8/P03l1T4QZO6jzapD.Eha', '45ijg40r9gk4', 'img1', 0, 0, 0, '2023-07-18 07:58:43', '2023-07-18 07:58:43', 0, 0, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
