package com.park.payserve.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * ade-交易记录表
 * @TableName tp_trade_log
 */
@TableName(value ="tp_trade_log")
@Data
public class TradeLog implements Serializable {
    /**
     * 交易日志主键id
     */
    @Excel(name = "交易记录id", orderNum = "1", width = 40)
    @JsonSerialize(using = ToStringSerializer.class)
    @TableId(value = "id")
    private Long id;

    /**
     * 订单id
     */
    @Excel(name = "订单id", orderNum = "2", width = 40)
    @JsonSerialize(using = ToStringSerializer.class)
    @TableField(value = "order_id")
    private Long orderId;

    /**
     * 交易标题
     */
    @Excel(name = "交易标题", orderNum = "3", width = 40)
    @TableField(value = "title")
    private String title;

    /**
     * 支付平台,1支付宝,2银联,3微信,4现金
     */
    @Excel(name = "支付平台,1支付宝,2银联,3微信,4现金", orderNum = "4", width = 40)
    @TableField(value = "pay_platform")
    private Integer payPlatform;

    /**
     * 交易类型,1支付,2退款
     */
    @Excel(name = "交易类型,1支付,2退款", orderNum = "5", width = 40)
    @TableField(value = "trade_type")
    private Integer tradeType;

    /**
     * 交易金额,单位元
     */
    @Excel(name = "交易金额,单位元", orderNum = "6", width = 40)
    @TableField(value = "amount")
    private BigDecimal amount;

    /**
     * 请求流水号,支付模块生成,唯一
     */
    @Excel(name = "请求流水号", orderNum = "7", width = 40)
    @TableField(value = "req_flow")
    private String reqFlow;

    /**
     * 交易状态,0进行中,1成功,2失败,默认0
     */
    @Excel(name = "交易状态,0进行中,1成功,2失败", orderNum = "8", width = 40)
    @TableField(value = "state")
    private Integer state;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间", orderNum = "9", width = 40,exportFormat = "yyyy-MM-dd")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新时间
     */
    @Excel(name = "更新时间", orderNum = "10", width = 40,exportFormat = "yyyy-MM-dd")
    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 逻辑删除字段,0正常,1删除.默认0
     */
    @TableField(value = "is_deleted",fill = FieldFill.INSERT)
    private Integer isDeleted;

    /**
     * 乐观锁默认1
     */
    @TableField(value = "version")
    private Integer version;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}