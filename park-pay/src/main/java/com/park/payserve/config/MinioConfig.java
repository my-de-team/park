package com.park.payserve.config;

import io.minio.MinioClient;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidPortException;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
/*表示该配置类需要从yml文件中读取前缀为minio的key对应配置类字段*/
@ConfigurationProperties(prefix = "minio")
@Data
public class MinioConfig {
    /*属性的名称必须跟yml里面的key最后一截一致*/
    private String endpoint;
    private Integer port;
    private String accessKey;
    private String secretKey;

    @Bean
    public MinioClient getMinioClient() throws InvalidPortException, InvalidEndpointException {
        MinioClient minioClient = new MinioClient(endpoint, port, accessKey, secretKey, false);
        return minioClient;
    }
}
