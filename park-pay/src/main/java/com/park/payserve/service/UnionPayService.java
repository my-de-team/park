package com.park.payserve.service;

import com.park.vos.payservevo.TradeLogVo;

public interface UnionPayService {

    String addOrder(TradeLogVo logVo);
}
