package com.park.payserve.service;

import com.park.payserve.config.AliPayHelper;
import com.park.payserve.config.AlipayConfig;
import com.park.utils.Result;
import com.park.vos.payservevo.TradeLogVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

public interface AliPayService {


    public Result<Object> placeOrderForPCWeb(TradeLogVo aliPayRequest) throws IOException;



    // sync return page
    public void orderCallbackInSync(HttpServletRequest request, HttpServletResponse response);


    public String orderCallbackInAsync(HttpServletRequest request);

  
  
    public boolean checkData(Map<String, String> map, TradeLogVo tradeLogVo);

}
