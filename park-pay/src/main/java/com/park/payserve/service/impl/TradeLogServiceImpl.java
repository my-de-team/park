package com.park.payserve.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.park.payserve.entity.TradeLog;
import com.park.payserve.service.TradeLogService;
import com.park.payserve.mapper.TradeLogMapper;
import com.park.payserve.utils.BeanCopyUtils;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.utils.SnowflakeIdWorker;
import com.park.vos.payservevo.TradeLogVo;
import com.park.vos.systemvo.QueryTradeLogVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author zdx
 * @description 针对表【tp_trade_log(ade-交易记录表)】的数据库操作Service实现
 * @createDate 2023-07-14 16:10:56
 */
@Service
public class TradeLogServiceImpl extends ServiceImpl<TradeLogMapper, TradeLog>
        implements TradeLogService {
    @Resource
    private TradeLogMapper mapper;
    @Resource
    private RestTemplate restTemplate;
    @Resource
    private SnowflakeIdWorker snowflakeIdWorker;

    //处理支付的方法,根据平台分别处理,调用不同的平台(支付宝,银联)
    @Override
    public Result addTradeLog(TradeLogVo logVo) {
        //接收参数
        TradeLog tradeLog = new TradeLog();
        //复制参数到交易记录业务实体类并插入数据库
        BeanUtils.copyProperties(logVo, tradeLog);
        //生成请求流水号（规则）
        String pre = logVo.getTradeType() == 1 ? "PAYMENT" : "REFUND";
        tradeLog.setReqFlow(pre + snowflakeIdWorker.nextId());
        tradeLog.setState(1);//先设置为1，交易成功，后续支付API跑通后在回调中修改
        //插入数据库
        int insert = mapper.insert(tradeLog);
        /* ---支付平台,1支付宝,2银联,3微信,4现金--- */
        if (ObjectUtils.isEmpty(logVo.getPayPlatform())) {
            throw new RuntimeException("请选择支付平台");
        }
        /*  ---交易类型,1支付,2退款---  */

        return Result.success(tradeLog.getReqFlow());
    }

    @Override
    public TradeLogVo getTradeLogByOrderId(Long orderId) {
        QueryWrapper<TradeLog> wrapper = new QueryWrapper<>();
        wrapper.eq("order_id",orderId);
        TradeLog tradeLog = mapper.selectOne(wrapper);
        TradeLogVo tradeLogVo = new TradeLogVo();
        if (tradeLog == null){
            throw new RuntimeException("该订单号没有交易记录");
        }
        BeanUtils.copyProperties(tradeLog,tradeLogVo);
        return tradeLogVo;
    }

    @Override
    public PageUtil<TradeLogVo> queryPageTradeLog(Integer pageNum, Integer pageSize, QueryTradeLogVo queryVo) {
        //1.构建分页查询对象,传入分页设置参数
        IPage<TradeLog> countIPage = new Page<>(pageNum,pageSize);
        //2.构建查询条件
        QueryWrapper<TradeLog> wrapper = new QueryWrapper<>();
        if (!ObjectUtils.isEmpty(queryVo)){
            if (!ObjectUtils.isEmpty(queryVo.getStartTime())){
                wrapper.ge("update_time",queryVo.getStartTime());
            }
            if (!ObjectUtils.isEmpty(queryVo.getEndTime())){
                wrapper.le("update_time",queryVo.getEndTime());
            }
        }
        wrapper.orderByDesc("update_time");
        IPage<TradeLog> resultPage = mapper.selectPage(countIPage, wrapper);

        //3.封装分页结果集
        PageUtil<TradeLogVo> pageUtil = new PageUtil<>();

        pageUtil.setPageData(BeanCopyUtils.copyList(resultPage.getRecords(),TradeLogVo.class));
        pageUtil.setTotal(resultPage.getTotal());
        return pageUtil;
    }
}




