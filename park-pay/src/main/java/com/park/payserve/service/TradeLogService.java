package com.park.payserve.service;

import com.park.payserve.entity.TradeLog;
import com.baomidou.mybatisplus.extension.service.IService;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.vos.payservevo.TradeLogVo;
import com.park.vos.systemvo.QueryTradeLogVo;

/**
* @author zdx
* @description 针对表【tp_trade_log(ade-交易记录表)】的数据库操作Service
* @createDate 2023-07-14 16:10:56
*/
public interface TradeLogService extends IService<TradeLog> {

    Result addTradeLog(TradeLogVo logVo);

    TradeLogVo getTradeLogByOrderId(Long orderId);

    PageUtil<TradeLogVo> queryPageTradeLog(Integer pageNum, Integer pageSize, QueryTradeLogVo queryVo);
}
