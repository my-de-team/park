package com.park.payserve.controller;

import com.park.payserve.sdk.AcpService;
import com.park.payserve.sdk.DemoBase;
import com.park.payserve.sdk.SDKConfig;
import com.park.payserve.sdk.SDKConstants;
import com.park.payserve.service.UnionPayService;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import com.park.vos.payservevo.TradeLogVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import static com.park.payserve.sdk.DemoBase.encoding;

@RestController
@RequestMapping("/pay/union")
@Api(tags = "支付微服务: 银联支付接口")
public class UnionPayController {
    @Resource
    private UnionPayService unionPayService;

    /*生成订单必要参数 : 商户id(开放平台商户账号里面),交易金额*/

    @PostMapping("/order")
    @ApiOperation("生成订单接口")
    public Result<Object> addOrder(@RequestBody TradeLogVo logVo)   {
        String html = unionPayService.addOrder(logVo);
        System.out.println(html);
        return Result.success(html);
    }

    /**
     * 异步通知支付结果
     *
     * @param request
     */
    @PostMapping("/callBack")
    @ApiOperation("异步回调")
    public void callBack(HttpServletRequest request, HttpServletResponse resp) throws Exception {
        System.out.println("callBack接收后台通知开始");

        String encoding = request.getParameter(SDKConstants.param_encoding);
        // 获取银联通知服务器发送的后台通知参数
        Map<String, String> reqParam = getAllRequestParam(request);
        System.out.println(reqParam);

        //重要！验证签名前不要修改reqParam中的键值对的内容，否则会验签不过
        if (!AcpService.validate(reqParam, encoding)) {
            System.out.println("验证签名结果[失败].");
            //验签失败，需解决验签问题

        } else {
            System.out.println("验证签名结果[成功].");
            //【注：为了安全验签成功才应该写商户的成功处理逻辑】交易成功，更新商户订单状态

            String orderId = reqParam.get("orderId"); //获取后台通知的数据，其他字段也可用类似方式获取
            String respCode = reqParam.get("respCode");
            //判断respCode=00、A6后，对涉及资金类的交易，请再发起查询接口查询，确定交易成功后更新数据库。
        }
        System.out.println("callBack接收后台通知结束");
        //返回给银联服务器http 200  状态码
        resp.getWriter().print("ok");
    }

    /**
     * @Title: alipayQuery @Description: 交易查询 @param @param
     * map @param @return @param @throws Exception 设定文件 @return Result<Object>
     * 返回类型 @throws
     */
    @PostMapping("/unionPayQuery")
    public Result<Object> unionPayQuery(@RequestBody Map<String, Object> map) throws Exception {
        /** 调取支付订单号 */
        String outTradeNo = (String) map.get("rechargeId");
        if (StringUtils.isEmpty(outTradeNo)) {
            return Result.success("null");
        }

        Map<String, String> data = new HashMap<String, String>();

        /***银联全渠道系统，产品参数，除了encoding自行选择外其他不需修改***/
        data.put("version", DemoBase.version);                 //版本号
        data.put("encoding", encoding);               //字符集编码 可以使用UTF-8,GBK两种方式
        data.put("signMethod", SDKConfig.getConfig().getSignMethod()); //签名方法
        data.put("txnType", "00");                             //交易类型 00-默认
        data.put("txnSubType", "00");                          //交易子类型  默认00
        data.put("bizType", "000201");                         //业务类型 B2C网关支付，手机wap支付

        /***商户接入参数***/
        data.put("merId", "777290058110048");                  //商户号码，请改成自己申请的商户号或者open上注册得来的777商户号测试
        data.put("accessType", "0");                           //接入类型，商户接入固定填0，不需修改

        /***要调通交易以下字段必须修改***/
        data.put("orderId", "orderId");                 //****商户订单号，每次发交易测试需修改为被查询的交易的订单号
        data.put("txnTime", "txnTime");                 //****订单发送时间，每次发交易测试需修改为被查询的交易的订单发送时间

        /**请求参数设置完毕，以下对请求参数进行签名并发送http post请求，接收同步应答报文------------->**/

        Map<String, String> reqData = AcpService.sign(data, encoding);//报文中certId,signature的值是在signData方法中获取并自动赋值的，只要证书配置正确即可。

        String url = SDKConfig.getConfig().getSingleQueryUrl();// 交易请求url从配置文件读取对应属性文件acp_sdk.properties中的 acpsdk.singleQueryUrl
        //这里调用signData之后，调用submitUrl之前不能对submitFromData中的键值对做任何修改，如果修改会导致验签不通过
        Map<String, String> rspData = AcpService.post(reqData, url, encoding);

        /**对应答码的处理，请根据您的业务逻辑来编写程序,以下应答码处理逻辑仅供参考------------->**/
        //应答码规范参考open.unionpay.com帮助中心 下载  产品接口规范  《平台接入接口规范-第5部分-附录》
        assert rspData != null;
        if (!rspData.isEmpty()) {
            if (AcpService.validate(rspData, encoding)) {
                System.out.println("验证签名成功");
                if ("00".equals(rspData.get("respCode"))) {//如果查询交易成功
                    //处理被查询交易的应答码逻辑
                    String origRespCode = rspData.get("origRespCode");
                    if ("00".equals(origRespCode)) {
                        //交易成功，更新商户订单状态
                        System.out.println("交易成功，更新商户订单状态");
                        //TODO
                    } else if ("03".equals(origRespCode) ||
                            "04".equals(origRespCode) ||
                            "05".equals(origRespCode)) {
                        //需再次发起交易状态查询交易
                        System.out.println("需再次发起交易状态查询交易");
                        //TODO
                    } else {
                        //其他应答码为失败请排查原因
                        System.out.println("其他应答码为失败请排查原因");
                        //TODO
                    }
                } else {//查询交易本身失败，或者未查到原交易，检查查询交易报文要素
                    //TODO
                    System.out.println("查询交易本身失败，或者未查到原交易，检查查询交易报文要素");
                }
            } else {
                return Result.err(ResultCode.INSERT_FAIL);
                //TODO 检查验证签名失败的原因
            }
        } else {
            //未返回正确的http状态
            System.out.println("未获取到返回报文或返回http状态码非200");
        }
        String reqMessage = DemoBase.genHtmlResult(reqData);
        String rspMessage = DemoBase.genHtmlResult(rspData);
        //resp.getWriter().write("</br>请求报文:<br/>" + reqMessage + "<br/>" + "应答报文:</br>" + rspMessage + "");

        return Result.err(ResultCode.EXCEPTION);
    }
    /**
     * 获取请求参数中所有的信息
     * 当商户上送frontUrl或backUrl地址中带有参数信息的时候，
     * 这种方式会将url地址中的参数读到map中，会导多出来这些信息从而致验签失败，这个时候可以自行修改过滤掉url中的参数或者使用getAllRequestParamStream方法。
     * @param request
     * @return
     */
    public static Map<String, String> getAllRequestParam(
            final HttpServletRequest request) {
        Map<String, String> res = new HashMap<String, String>();
        Enumeration<?> temp = request.getParameterNames();
        if (null != temp) {
            while (temp.hasMoreElements()) {
                String en = (String) temp.nextElement();
                String value = request.getParameter(en);
                res.put(en, value);
                // 在报文上送时，如果字段的值为空，则不上送<下面的处理为在获取所有参数数据时，判断若值为空，则删除这个字段>
                if (res.get(en) == null || "".equals(res.get(en))) {
                    // System.out.println("======为空的字段名===="+en);
                    res.remove(en);
                }
            }
        }
        return res;
    }
}
