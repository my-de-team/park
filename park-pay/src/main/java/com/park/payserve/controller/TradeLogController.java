package com.park.payserve.controller;

import com.park.annotation.Log;
import com.park.payserve.entity.TradeLog;
import com.park.payserve.service.TradeLogService;
import com.park.payserve.utils.ExcelUtils;
import com.park.utils.PageUtil;
import com.park.utils.Result;
import com.park.vos.payservevo.TradeLogVo;
import com.park.vos.systemvo.QueryCountCommonVo;
import com.park.vos.systemvo.QueryTradeLogVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/pay")
@Api(tags = "支付微服务: 支付接口")
public class TradeLogController {
    @Resource
    private TradeLogService tradeLogService;

    /**
     * 调用第三方支付的接口
     * @param logVo 支付必要信息
     * @return 返回流水号,第三方支付平台的同步结果
     */
    @PostMapping
    @Log
    @ApiOperation("调用支付接口")
    public Result addTradeLog(@RequestBody TradeLogVo logVo){
        //调用service处理,根据参数的平台处理不同业务
        Result result = tradeLogService.addTradeLog(logVo);
        return Result.success(result);
    }

    /**
     * 根据订单id查询支付记录
     * @param orderId 订单id
     * @return 返回记录vo
     */
    @GetMapping("/{orderId}")
    @Log
    @ApiOperation("根据订单id查询支付记录")
    public Result<TradeLogVo> getTradeLogByOrderId(@PathVariable("orderId") Long orderId){
        //调用service处理,根据参数的平台处理不同业务
        TradeLogVo result = tradeLogService.getTradeLogByOrderId(orderId);
        return Result.success(result);
    }
    /** 交易记录分页查询*/

    @Log
    @PostMapping("/page/{pageNum}/{pageSize}")
    @ApiOperation("分页条件查询")
    public Result<PageUtil<TradeLogVo>> queryPageTradeLog(@PathVariable("pageNum")Integer pageNum,
                                        @PathVariable("pageSize")Integer pageSize,
                                        @RequestBody QueryTradeLogVo queryVo){
        PageUtil<TradeLogVo> tradeLogVoPageUtil = tradeLogService.queryPageTradeLog(pageNum,pageSize,queryVo);
        return Result.success(tradeLogVoPageUtil);
    }

    @ApiOperation("导出excel文件")
    @GetMapping("/exportExcel")
    public void export(HttpServletResponse response) {
        try {
            // 查询所有店铺信息
            String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

            List<TradeLog> list = tradeLogService.list();
            // List<?> list, String title, String sheetName, Class<?> pojoClass, String fileName, HttpServletResponse response
            ExcelUtils.exportExcel(list, "交易流水", "交易数据", TradeLog.class, date+"交易记录.xls", response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @ApiOperation("导入excel文件")
    @PostMapping("/importExcel")
    public void importExcel(@RequestPart("file") MultipartFile file) {
        List<TradeLog> tradeLogList = ExcelUtils.importExcel(file, 1, 1, TradeLog.class);
        System.out.println("导入数据共" + tradeLogList.size() + "行");
        tradeLogList.forEach(System.out::println);
        // 通过批量添加数据到数据库
        // shopService.saveBatch(shops, shops.size());
    }


}
