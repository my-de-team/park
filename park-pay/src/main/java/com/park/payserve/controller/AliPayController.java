package com.park.payserve.controller;

import com.park.payserve.service.AliPayService;
import com.park.utils.Result;
import com.park.vos.payservevo.TradeLogVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/pay/alipay")
@Api(tags = "支付微服务：支付宝支付")
public class AliPayController {  
  
    @Resource
    AliPayService aliPayService;

    /**
     * 生成支付宝支付订单的接口
     * @param logVo 必要的参数
     * @return 调用返回结果
     */
    @PostMapping("/order")
    @ApiOperation("生成支付宝支付订单")
    public Result<Object> placeOrderForPCWeb(@RequestBody TradeLogVo logVo) {
        try {  
            return aliPayService.placeOrderForPCWeb(logVo);
        } catch (IOException e) {
            throw new RuntimeException(e);  
        }  
    }

    /* 支付宝异步回调接口*/
    @PostMapping("/callback/async")
    @ApiOperation("支付宝异步回调")
    public String asyncCallback(HttpServletRequest request) {
        return aliPayService.orderCallbackInAsync(request);
    }

    /* 支付宝同步回调接口*/
    @GetMapping("/callback/sync")
    @ApiOperation("支付宝同步回调")
    public void syncCallback(HttpServletRequest request, HttpServletResponse response) {
        aliPayService.orderCallbackInSync(request, response);
    }  

}
