package com.park.payserve;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import com.park.utils.SnowflakeIdWorker;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

@SpringBootApplication
@MapperScan("com.park.payserve.mapper")
@EnableFeignClients(basePackages = {"com.park.api"})
@EnableKnife4j
@EnableSwagger2WebMvc
@EnableCaching
@EnableScheduling
public class PayApplication8087 {

    public static void main(String[] args) {
        SpringApplication.run(PayApplication8087.class, args);
    }


}
