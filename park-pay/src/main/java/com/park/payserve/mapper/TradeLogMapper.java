package com.park.payserve.mapper;

import com.park.payserve.entity.TradeLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author zdx
* @description 针对表【tp_trade_log(ade-交易记录表)】的数据库操作Mapper
* @createDate 2023-07-14 16:10:56
* @Entity com.park.payserve.entity.TradeLog
*/
public interface TradeLogMapper extends BaseMapper<TradeLog> {

}




