package com.park.filter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.park.utils.Result;
import com.park.utils.ResultCode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class AuthFilter implements GlobalFilter, Ordered {

    @Value("${whiteList}")
    private List<String> whiteList;

    @Resource
    private RedisTemplate redisTemplate;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        //路径判断 放行登录与swagger请求
        String path = request.getURI().getPath();
        System.out.println("获取到的path: " + path);
        for (String whitePath : whiteList) {
            if (whitePath.equals(path)) {
                return chain.filter(exchange);
            }
            if (whitePath.equals("/park-data/send/*")) {
                return chain.filter(exchange);
            }
        }
        //获取token
        String authorization = request.getHeaders().getFirst("Authorization");
        if (StringUtils.isEmpty(authorization)) {
            return error(response, ResultCode.TOKEN_FAIL);
        }
        String token = authorization.split(" ")[1];
        //redis获取token
        Long expire = redisTemplate.getExpire("token:" + token);
        if (expire == -2) {
            return error(response, ResultCode.TOKEN_FAIL);
        }

        redisTemplate.opsForValue().set(token, "张三");

        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }

    /**
     * token非法 响应数据
     *
     * @param response
     * @param resultCode
     * @return
     */
    public Mono<Void> error(ServerHttpResponse response, ResultCode resultCode) {
        //创建相应的result对象
        Result errInfo = Result.err(resultCode);
        //把result转换成字节数组
        ObjectMapper objectMapper = new ObjectMapper();
        byte[] resultByte = null;
        try {
            resultByte = objectMapper.writeValueAsBytes(errInfo);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        DataBuffer dataBuffer = response.bufferFactory().wrap(resultByte);
        return response.writeWith(Mono.just(dataBuffer));
    }
}
