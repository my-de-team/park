package com.park;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GatewayApplication8082 {

    public static void main(String[] args) {
        /*开启网关sentinel对其他服务的心跳检测功能*/
        System.setProperty("csp.sentinel.app.type", "1");
        SpringApplication.run(GatewayApplication8082.class, args);
    }
}
