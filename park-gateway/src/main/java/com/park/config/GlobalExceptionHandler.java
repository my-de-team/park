package com.park.config;

import com.park.utils.Result;
import com.park.utils.ResultCode;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.text.SimpleDateFormat;
import java.util.Date;

//@Component
//@Order(-1)
//public class GlobalExceptionHandler implements ErrorWebExceptionHandler {
//    @Override
//    public Mono<Void> handle(ServerWebExchange serverWebExchange, Throwable throwable) {
//        return null;
//    }
//}
@RestControllerAdvice
public class GlobalExceptionHandler {
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @ExceptionHandler
    public Result execute(Exception e) {
        System.out.println("出现异常！！！已进入全局异常处理");
        String formatDate = dateFormat.format(new Date());

        //出现其他错误
        e.printStackTrace();
        return Result.err(ResultCode.EXCEPTION);
    }
}
